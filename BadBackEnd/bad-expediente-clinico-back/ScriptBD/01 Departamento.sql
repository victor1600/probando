INSERT INTO public.db_departamento(
	id_departamento, cod_departamento, nombre_departamento)
	VALUES ('6bdff522-2508-44b2-add7-72d84256ada3', '01', 'Ahuachapán'),
		   ('885dc85f-6dac-4c12-8435-ab99e1055e4f', '02', 'Santa Ana'),
		   ('a33742c9-d223-4633-b757-1aaf17b8db8b', '03', 'Sonsonate'),
		   ('738cf853-6555-4749-a16b-aa1b8cb5212c', '04', 'Chalatenango'),
		   ('08a1da3f-9ab9-4e35-a406-7bd90abe6811', '05', 'La Libertad'),
		   ('7ad1c218-0878-4da2-999f-99160e331390', '06', 'San Salvador'),
		   ('24ab4cbc-3f0c-46b2-a274-968bc6cd4a59', '07', 'Cuscatlán'),
		   ('390aaaaa-f629-4bf2-8378-3e3c443ee6c0', '08', 'La Paz'),
		   ('c0a0c635-932c-4e2c-8eb0-c0ea20ec823f', '09', 'Cabañas'),
		   ('8fcbf1df-91fb-46dc-87aa-e69ca75a6c37', '10', 'San Vicente'),
		   ('c846f6c3-d342-41f3-a928-43dadb182c2b', '11', 'Usulután'),
		   ('7e2936cd-2211-43ef-aaa6-f0f2c00fea16', '12', 'San Miguel'),
		   ('b073aa40-fbf9-409f-8fd2-455af060fc6e', '13', 'Morazán'),
		   ('bfe65614-dbee-4811-ac53-c35d820d50a6', '14', 'La Unión');

