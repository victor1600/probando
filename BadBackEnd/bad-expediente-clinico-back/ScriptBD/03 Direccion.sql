INSERT INTO public.db_direccion(
	id_direccion, calle_avenida, numero_casa, numero_pasaje, urbanizacion, id_municipio)
	VALUES ('0196df37-0185-4dc9-9579-4118f74f61fa', '2ª Calle Poniente y 2ª Avenida Sur', 'No. 504', null, 'Barrio El Calvario', '874b346d-7fc3-4a59-8bae-09aeb7b190c1'),
		   ('25f7e486-fdc4-49d4-a9d3-b4fc94875bae', '2a. Calle Poniente y Avenida José María Castro', null, null, 'Barrio El Centro', 'db183b17-2db8-4c65-aba4-ac5bb9e62cd8'),
		   ('0799c33d-b85d-44fb-a201-e1b576d26c6d', 'Diagonal Centroamérica, Avenida Alvarado', null, null, 'Colonia Buenos Aires 3', 'e164b6f1-1fae-4c8d-a751-c685d4a92caf'),
		   ('90c3c51c-fbfa-4263-beef-81f9b362e0e2', '10ª Avenida Sur y Calle Lara', 'No. 934', null, 'Barrio San Jacinto', 'e164b6f1-1fae-4c8d-a751-c685d4a92caf'),
		   ('54848d8b-e501-47db-a8e5-1eef1229f98d', 'Avenida Independencia y Alameda Juan Pablo II', 'No. 437', null, null, 'e164b6f1-1fae-4c8d-a751-c685d4a92caf'),
		   ('ca58864b-2bcd-412f-9f92-30fe3e90fd03', 'Calle El Bambú', null, null, 'Colonia San Antonio', '5ffc748b-82a4-4a5b-a3a7-3cdf171d32d8'),
		   /* Responsable */
		   ('770ee3ae-0197-4f23-bde7-233b7e66433e', '2ª Calle Poniente y 2ª Avenida Sur', 'No. 504', null, 'Barrio El Calvario', '874b346d-7fc3-4a59-8bae-09aeb7b190c1'),
		   ('b4736f9c-c426-44ad-acac-72a7bcb3f538', '2a. Calle Poniente y Avenida José María Castro', null, null, 'Barrio El Centro', 'db183b17-2db8-4c65-aba4-ac5bb9e62cd8'),
		   ('cc132ce6-ef4c-4015-a873-b015afd448e0', 'Diagonal Centroamérica, Avenida Alvarado', null, null, 'Colonia Buenos Aires 3', 'e164b6f1-1fae-4c8d-a751-c685d4a92caf'),
		   ('40b59af9-5ae3-4050-ac69-365b30c09ad3', '10ª Avenida Sur y Calle Lara', 'No. 934', null, 'Barrio San Jacinto', 'e164b6f1-1fae-4c8d-a751-c685d4a92caf'),
		   ('37196078-c3eb-4d13-b7df-5009acea7ea3', 'Avenida Independencia y Alameda Juan Pablo II', 'No. 437', null, null, 'e164b6f1-1fae-4c8d-a751-c685d4a92caf'),
		   ('edda14a8-1a79-4f1f-9c6d-985a0a5218df', 'Calle El Bambú', null, null, 'Colonia San Antonio', '5ffc748b-82a4-4a5b-a3a7-3cdf171d32d8'),
		   /* Personal Medico */
		   ('9b96235e-4fcb-45d2-ab1a-a6c7e9d7fdae', '2a. Avenida Norte y 1a. Calle Poniente', 'No. 23', null, null, 'e136f379-25e6-43be-a1bd-0b2c6acdc262'),
		   ('7eda6ca5-0132-4023-9569-9b2493f15ae3', 'Avenida Las Camelias y Calle Los Abetos', 'No. 21', null, 'Colonia San Francisco,', 'e164b6f1-1fae-4c8d-a751-c685d4a92caf'),
		   ('a881a109-c4a6-49ab-b848-16596089d650', 'Calle Central Poniente y 1ª Avenida Sur', 'No 1', null, 'Barrio El Centro', 'e02e37bb-8161-4d8d-b809-7fc645f66370'),
		   ('68be8f36-83b9-45cb-9fd3-6b408ba60bf9', '2ª Avenida Sur y 2ª Calle Oriente', '2-1', null, 'Barrio el Centro', 'b8b6dee5-8241-41d2-894c-1f51bcb5bad5'),
		   ('4191bc8a-500c-440d-89ca-15102e616488', '59 Avenida Sur y Avenida Olímpica', null, null, 'Colonia Flor Blanca', 'e164b6f1-1fae-4c8d-a751-c685d4a92caf'),
		   ('9f7e3629-5293-4edd-b250-f546ac5175d3', '91 Av. Norte', '# 220', 'Pje. San Miguel', 'Colonia Escalon', 'e164b6f1-1fae-4c8d-a751-c685d4a92caf');
