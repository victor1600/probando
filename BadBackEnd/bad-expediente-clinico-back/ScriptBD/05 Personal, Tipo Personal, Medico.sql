
/* Tipo Personal Medico */
INSERT INTO public.db_tipo_personal_medico(
	id_tipo_personal, nombre_tipo_personal)
	VALUES ('f53d137f-f26d-46a8-9ee0-6a1c82df5459','Medico'),
		   ('853aa9f7-9d33-4d27-9ae4-13e0d33c096a','Enfermera'),
		   ('96855c6a-7e4b-437d-9612-9f99ac7f4660','FisioTerapeuta'),
		   ('e2eeea74-238e-4fcb-9383-a198f69efcdb','Recepcionista'),
		   ('8216eb55-0da8-4200-9551-86b0f18372b9','Laboratorista');
		   
/* Medico */
INSERT INTO public.db_medico(
	id_medico, pacientes_por_hora)
	VALUES ('448a1b1f-a08f-4f9c-b33c-7d7f7502a17a',4),
		   ('998e5f55-4ac3-46c8-8854-9f3a279974a0', 6);

/* Personal Medico	*/
INSERT INTO public.db_personal_medico(
	estado_personal, jvp, id_personal, id_medico, id_tipo_personal)
	VALUES (true,'12345','736534b5-03f6-47a8-9593-bfd5ea167651','448a1b1f-a08f-4f9c-b33c-7d7f7502a17a','f53d137f-f26d-46a8-9ee0-6a1c82df5459'),
		   (true,'78910','a4846880-7774-42be-9548-dfc5ffcb1b71',null,'853aa9f7-9d33-4d27-9ae4-13e0d33c096a'),
		   (true,'11223','39b617fd-e579-49cb-9acb-19919af99194',null,'96855c6a-7e4b-437d-9612-9f99ac7f4660'),
		   (true,'33445','2c2e7f98-bbfe-45eb-8c26-187f86a1fb06',null,'e2eeea74-238e-4fcb-9383-a198f69efcdb'),
		   (true,'55667','82a8bdb1-bdc1-4ce3-8a05-303de1be3226','998e5f55-4ac3-46c8-8854-9f3a279974a0','f53d137f-f26d-46a8-9ee0-6a1c82df5459'),
		   (true,'88915','cfc8ba4e-c327-49ae-bfb4-4d3b90ad8286',null,'853aa9f7-9d33-4d27-9ae4-13e0d33c096a');
