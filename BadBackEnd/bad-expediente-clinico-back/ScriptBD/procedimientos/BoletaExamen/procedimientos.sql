-------- lista activos -----
CREATE OR REPLACE FUNCTION lista_boleta_examen() RETURNS setof db_boleta_examen
AS
$$
SELECT *
FROM db_boleta_examen
where esta_activo = TRUE;
$$
    LANGUAGE sql;
    
---- toggle
CREATE OR REPLACE FUNCTION toggle_boleta_examen(p_id uuid, p_state bool, out result bool)
as
$$
BEGIN
    update db_boleta_examen
    set esta_activo = p_state
    where id_boleta = p_id;
    result := true;
exception
    when others then
        result := false;
        RAISE NOTICE 'FALSE';

end;
$$
    language plpgsql;
    
    --------- validacion ---
    CREATE OR REPLACE FUNCTION validacion_boleta(p_id uuid,
                                                out mensaje varchar)
as
$$
DECLARE

    check_fk numeric;

BEGIN
    select into check_fk count(*)
    from db_examen
    where id_examen = p_id;

    if check_fk = 0 then
        mensaje := 'No existe el tipo de medicamento asociado.';

    else
        mensaje := 'Se puede insertar registro';

    end if;

end;
$$
    language plpgsql;

