-------- listar
CREATE OR REPLACE FUNCTION lista_tratamientos_activos() RETURNS setof db_tratamiento
AS
$$
SELECT *
FROM db_tratamiento
where esta_activo = TRUE;
$$
    LANGUAGE sql;
------------------ toggle
CREATE OR REPLACE FUNCTION toggle_tratamiento(p_id uuid, p_state bool, out result bool)
as
$$
BEGIN
    update db_tratamiento
    set esta_activo = p_state
    where id_tratamiento = p_id;
    result := true;
exception
    when others then
        result := false;
        RAISE NOTICE 'FALSE';

end;
$$
    language plpgsql;
    
    --- validacion ---
CREATE OR REPLACE FUNCTION validacion_tratamiento(p_id uuid, p_fk uuid,
                                                  out mensaje varchar)
as
$$
DECLARE

    check_fk numeric;

BEGIN
    select into check_fk count(*)
    from db_tratamiento
    where id_consulta = p_fk
      and id_tratamiento != p_id;

    if check_fk > 0 then
        mensaje := 'Esa consulta ya tiene una tratamiento asociado.';

    else
        mensaje := 'Se puede insertar registro';

    end if;

end;
$$
    language plpgsql;
