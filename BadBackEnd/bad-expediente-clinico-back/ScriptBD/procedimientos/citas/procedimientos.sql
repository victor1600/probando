-------------- Lista 
CREATE OR REPLACE FUNCTION lista_citas() RETURNS setof db_cita
AS
$$
SELECT *
FROM db_cita
where esta_activo = TRUE;
$$
    LANGUAGE sql;
    
    
    
---- toggle
CREATE OR REPLACE FUNCTION toggle_cita(p_id uuid, p_state bool, out result bool)
as
$$
BEGIN
    update db_cita
    set esta_activo = p_state
    where id_cita = p_id;
    result := true;
exception
    when others then
        result := false;
        RAISE NOTICE 'FALSE';

end;
$$
    language plpgsql;
    
    ------------- checker
 
CREATE OR REPLACE FUNCTION validacion_cita(p_id uuid, p_fk uuid,
                                           out mensaje varchar)
as
$$
DECLARE

    check_fk numeric;

BEGIN
    select into check_fk count(*)
    from db_cita
    where id_consulta = p_fk
      and id_cita != p_id;

    if check_fk > 0 then
        mensaje := 'Esa consulta ya tiene una cita asociada.';

    else
        mensaje := 'Se puede insertar registro';

    end if;

end;
$$
    language plpgsql;
    
------------- DOCTORES DISPONIBLES ---------------------------
CREATE OR REPLACE FUNCTION doctores_disponibles(p_fecha_cita varchar, p_hora_elegida time, p_id_especialidad uuid) RETURNS setof db_medico
AS
$$
SELECT *
FROM db_medico
-- verificar que el doctor no tenga ya llena su agenda a esa hora.
WHERE db_medico.id_medico NOT IN (
    SELECT cuenta.id_medico
    FROM (select db_cita.id_medico, count(*)
          from db_cita
                   inner join db_asignacion_horario dah on db_cita.id_medico = dah.id_medico
--                    inner join db_medico dm on dah.id_medico = dm.id_medico
               -- Verificando en las citas que ya tiene, que el doctor este libre N minutos(duracion de la consulta)
               -- antes y n minutos despues para que consultas no choquen entre si
          where db_cita.hora_cita > p_hora_elegida - (duracion_consulta::varchar || ' minute')::interval
            and db_cita.hora_cita < p_hora_elegida + (duracion_consulta::varchar || ' minute')::interval
            and db_cita.fecha_cita = p_fecha_cita::date
          group by db_cita.id_medico
          HAVING COUNT(*) > 1) cuenta
)
  -- verificar especialidad seleccionada
  and db_medico.id_especialidad = p_id_especialidad
  -- verificar hora y dia, si esta disponible
  AND id_medico IN (
    SELECT id_medico
    FROM db_asignacion_horario
    WHERE extract(dow from p_fecha_cita::date)  = fecha_inicio::integer
      and p_hora_elegida::varchar BETWEEN hora_inicio
        AND hora_fin
)
$$
    LANGUAGE sql;

