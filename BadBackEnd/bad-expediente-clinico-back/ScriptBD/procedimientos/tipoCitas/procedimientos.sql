---------------- validacion --------------------------------
CREATE OR REPLACE FUNCTION validacion_tipo_cita(p_nombre varchar, p_id uuid,
                                                out mensaje varchar)
as
$$
DECLARE

    check_nombre numeric;

BEGIN
    select into check_nombre count(*)
    from db_tipo_cita
    where upper(nombre_tipo_cita) = upper(p_nombre)
      and id_tipo_cita != p_id;


    if check_nombre > 0 then
        mensaje := 'Ya existe un tipo de cita con ese nombre.';

    else
        mensaje := 'Se puede insertar registro';

    end if;

end;
$$
    language plpgsql;
    
    
    
---------------------------------- toggle --------------------------
CREATE OR REPLACE FUNCTION toggle_tipo_cita(p_id uuid, p_state bool, out result bool)
as
$$
BEGIN
    update db_tipo_cita
    set esta_activo = p_state
    where id_tipo_cita = p_id;
    result := true;
exception
    when others then
        result := false;
        RAISE NOTICE 'FALSE';

end;
$$
    language plpgsql;
---- listar ---
CREATE OR REPLACE FUNCTION lista_tipo_citas_activos() RETURNS setof db_tipo_cita
AS
$$
SELECT *
FROM db_tipo_cita
where esta_activo = TRUE;
$$
    LANGUAGE sql;

