----------- toggle ---------------
CREATE OR REPLACE FUNCTION toggle_tipoM(p_id uuid, p_state bool, out result bool)
as
$$
BEGIN
    update db_tipo_medicamento
    set esta_activo = p_state
    where id_tipo_medicamento = p_id;
    result := true;
exception
    when others then
        result := false;
        RAISE NOTICE 'FALSE';

end;
$$
    language plpgsql;
    
--------------- listar -----------------------
CREATE OR REPLACE FUNCTION lista_tipo_medicamentos_activos() RETURNS setof db_tipo_medicamento
AS
$$
SELECT *
FROM db_tipo_medicamento
where esta_activo = TRUE;
$$
    LANGUAGE sql;

