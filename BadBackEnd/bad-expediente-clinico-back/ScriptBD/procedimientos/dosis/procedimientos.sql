CREATE OR REPLACE FUNCTION lista_dosis_activos() RETURNS setof db_dosis
AS
$$
SELECT *
FROM db_dosis
where esta_activo = TRUE;
$$
    LANGUAGE sql;


------------- validacion dosis ------------------------


CREATE OR REPLACE FUNCTION validacion_dosis(p_id_med uuid, p_id_tratamiento uuid,
                                            out mensaje varchar)
as
$$
DECLARE
    check_id_med         numeric;
    check_id_tratamiento numeric;
BEGIN
    select into check_id_med count(*)
    from db_medicamento
    where id_medicamento = p_id_med;

    select into check_id_tratamiento count(*)
    from db_tratamiento
    where id_tratamiento = p_id_tratamiento;


    if check_id_med = 0 then
        mensaje := 'No existe el medicamento asociado.';

    elsif check_id_tratamiento = 0 then
        mensaje := 'No existe el tratamiento asociado';

    else
        mensaje := 'Se puede insertar registro';

    end if;
end;
$$
    language plpgsql;


---------------------- desactivar y activar -------------
CREATE OR REPLACE FUNCTION toggle_dosis(p_id uuid, p_state bool, out result bool)
as
$$
BEGIN
    update db_dosis
    set esta_activo = p_state
    where id_dosis = p_id;
    result := true;
exception
    when others then
        result := false;
        RAISE NOTICE 'FALSE';

end;
$$
    language plpgsql;

