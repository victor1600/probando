CREATE OR REPLACE FUNCTION lista_tipo_examenes() RETURNS setof db_tipo_examen
AS
$$
SELECT *
FROM db_tipo_examen
where esta_activo = TRUE;
$$
    LANGUAGE sql;
    
    
    ------------- toggle ----------
    CREATE OR REPLACE FUNCTION toggle_tipo_examenes(p_id uuid, p_state bool, out result bool)
as
$$
BEGIN
    update db_tipo_examen
    set esta_activo = p_state
    where id_tipo_examen = p_id;
    result := true;
exception
    when others then
        result := false;
        RAISE NOTICE 'FALSE';

end;
$$
    language plpgsql;
