CREATE OR REPLACE FUNCTION lista_horario() RETURNS setof db_horario
AS
$$
SELECT *
FROM db_horario
where esta_activo = TRUE;
$$
    LANGUAGE sql;
    
    
    ------------- toggle ----------
    CREATE OR REPLACE FUNCTION toggle_horario(p_id uuid, p_state bool, out result bool)
as
$$
BEGIN
    update db_horario
    set esta_activo = p_state
    where id_horario= p_id;
    result := true;
exception
    when others then
        result := false;
        RAISE NOTICE 'FALSE';

end;
$$
    language plpgsql;
