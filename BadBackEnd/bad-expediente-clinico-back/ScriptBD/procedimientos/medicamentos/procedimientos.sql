

------- reutilizable ----------

CREATE OR REPLACE FUNCTION validacion_medicamento(p_nombre_med varchar, p_codigo_med varchar, p_id_tipo_med uuid,

                                              out mensaje varchar)
as
$$
DECLARE
    check_id_tipo_med numeric;
    check_nombre_med  numeric;
    check_cod_med     numeric;

BEGIN
    select into check_id_tipo_med count(*)
    from db_tipo_medicamento
    where id_tipo_medicamento = p_id_tipo_med;

    select into check_nombre_med count(*)
    from db_medicamento
    where upper(nombre_medicamento) = upper(p_nombre_med);

    select into check_cod_med count(*)
    from db_medicamento
    where upper(codigo_medicamento) = upper(p_codigo_med);

    if check_id_tipo_med = 0 then
        mensaje := 'No existe el tipo de medicamento asociado.';

    elsif check_nombre_med > 0 then
        mensaje := 'Ya existe un medicamento con ese nombre.';

    elsif check_cod_med > 0 then
        mensaje := 'Ya existe un medicamento con ese codigo.';

    else
        mensaje := 'Se puede insertar registro';

    end if;

end;
$$
    language plpgsql;


------------ --------- LISTAR ACTIVOS
CREATE OR REPLACE FUNCTION lista_medicamentos_activos() RETURNS setof db_medicamento
AS
$$
SELECT *
FROM db_medicamento
where esta_activo = TRUE;
$$
    LANGUAGE sql;

----------------- toggle ------------
CREATE OR REPLACE FUNCTION toggle_medicamentos(p_id uuid, p_state bool, out result bool)
as
$$
BEGIN
    update db_medicamento
    set esta_activo = p_state
    where id_medicamento = p_id;
    result := true;
exception
    when others then
        result := false;
        RAISE NOTICE 'FALSE';

end;
$$
    language plpgsql;

---------------------------------
----------- toggle ---------------
CREATE OR REPLACE FUNCTION toggle_tipoM(p_id uuid, p_state bool, out result bool)
as
$$
BEGIN
    update db_tipo_medicamento
    set esta_activo = p_state
    where id_tipo_medicamento = p_id;
    result := true;
exception
    when others then
        result := false;
        RAISE NOTICE 'FALSE';

end;
$$
    language plpgsql;
    
--------------- listar -----------------------
CREATE OR REPLACE FUNCTION lista_tipo_medicamentos_activos() RETURNS setof db_tipo_medicamento
AS
$$
SELECT *
FROM db_tipo_medicamento
where esta_activo = TRUE;
$$
    LANGUAGE sql;

