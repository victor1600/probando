
/* Persona */
INSERT INTO public.db_persona(
	id_persona, apellido1persona, apellido2persona, apellido3persona, celular_persona, dui_persona, fecha_nacimiento_persona, nit_persona, nombre1persona, nombre2persona, nombre3persona, sexo_persona, telefono_persona, id_direccion)
	VALUES ('63f9b2d1-1d1a-4852-8a7d-a8cdacb323d9', 'Alvarado', null, null, '7531-5961', '01748942-1', '1994-12-30', '0601-301294-101-1', 'Javier', null, null, 'M', null, '0196df37-0185-4dc9-9579-4118f74f61fa'),
		   ('3f827ec9-acb2-4750-9988-8ba522487b5d', 'Castillo', null, null, '7531-5962', '01748942-1', '1995-12-30', '0512-301295-101-1', 'Lucia', null, null, 'F', null, '25f7e486-fdc4-49d4-a9d3-b4fc94875bae'),
		   ('86c94033-a9c8-4346-93aa-7de0cfb92552', 'Redfield', null, null, '7531-5963', '01748942-1', '1992-12-30', '0602-301292-101-1', 'Chris', null, null, 'M', null, '0799c33d-b85d-44fb-a201-e1b576d26c6d'),
		   ('caf7a17d-25c2-43a2-97ff-a79109d3bb90', 'Martinez', null, null, '7531-5964', '01748942-1', '1991-12-30', '0602-301291-101-1', 'Sofia', null, null, 'F', null, '90c3c51c-fbfa-4263-beef-81f9b362e0e2'),
		   ('e93dd419-b159-4899-90cd-b72b609e277d', 'Lara', null, null, '7531-5965', '01748942-1', '1990-12-30', '0601-301290-101-1', 'Hazel', null, null, 'F', null, '54848d8b-e501-47db-a8e5-1eef1229f98d'),
		   ('30fb59b9-238b-48d3-87ac-6b8556e2a642', 'Lopez', null, null, '7531-5966', '01748942-1', '1996-12-30', '0601-301296-101-1', 'Enrique', null, null, 'M', null, 'ca58864b-2bcd-412f-9f92-30fe3e90fd03'),
            /* Responsable */
		   ('1f59698f-a6ca-4ee5-b825-1bdfa371ab1f', 'Alvarado', null, null, '7650-3261', '01748942-1', '1974-12-30', '0601-301274-101-1', 'Edwin', null, null, 'M', null, '770ee3ae-0197-4f23-bde7-233b7e66433e'),
		   ('c334d27c-2be2-416a-92a4-63c4d3d68157', 'Castillo', null, null, '7650-3362', '01748942-1', '1975-12-30', '0512-301275-102-1', 'Laura', null, null, 'F', null, 'b4736f9c-c426-44ad-acac-72a7bcb3f538'),
		   ('4d83cc62-181c-4267-93ed-5907f95f1b26', 'Redfield', null, null, '7650-3463', '01748942-1', '1972-12-30', '0602-301272-103-1', 'Claire', null, null, 'F', null, 'cc132ce6-ef4c-4015-a873-b015afd448e0'),
		   ('457935a3-69f0-4337-b3ce-c810fd80acf4', 'Martinez', null, null, '7650-3564', '01748942-1', '1971-12-30', '0602-301271-104-1', 'Lorena', null, null, 'F', null, '40b59af9-5ae3-4050-ac69-365b30c09ad3'),
		   ('eb947135-ff02-4211-8024-4d98bd748fb8', 'Lara', null, null, '7650-3565', '01748942-1', '1970-12-30', '0601-301270-105-1', 'Gudalupe', null, null, 'F', null, '37196078-c3eb-4d13-b7df-5009acea7ea3'),
		   ('de36484c-2501-4a54-bb42-fe88ce1283fe', 'Lopez', null, null, '7650-3766', '01748942-1', '1976-12-30', '0601-301276-106-1', 'Alejandro', null, null, 'M', null, 'edda14a8-1a79-4f1f-9c6d-985a0a5218df'),
	        /* Personal Medico */
		   ('736534b5-03f6-47a8-9593-bfd5ea167651', 'Aguilar', null, null, '7531-5967', '01748942-1', '1995-12-30', '0601-301295-101-1', 'Cindy', null, null, 'F', null, '68be8f36-83b9-45cb-9fd3-6b408ba60bf9'),
		   ('a4846880-7774-42be-9548-dfc5ffcb1b71', 'Tesla', null, null, '7531-5968', '01748942-1', '1994-12-30', '0601-301294-101-1', 'Nikola', null, null, 'M', null, '9f7e3629-5293-4edd-b250-f546ac5175d3'),
		   ('39b617fd-e579-49cb-9acb-19919af99194', 'Kamado', null, null, '7531-5969', '01748942-1', '1993-12-30', '0601-301293-101-1', 'Tanjiro', null, null, 'M', null, '4191bc8a-500c-440d-89ca-15102e616488'),
		   ('2c2e7f98-bbfe-45eb-8c26-187f86a1fb06', 'Kennedy', null, null, '7531-5970', '01748942-1', '1992-12-30', '0601-301292-101-1', 'Leon', null, null, 'M', null, 'a881a109-c4a6-49ab-b848-16596089d650'),
		   ('82a8bdb1-bdc1-4ce3-8a05-303de1be3226', 'Reyes', null, null, '7531-5970', '01748942-1', '1991-12-30', '0601-301291-101-1', 'Manuel', null, null, 'M', null, '7eda6ca5-0132-4023-9569-9b2493f15ae3'),
		   ('cfc8ba4e-c327-49ae-bfb4-4d3b90ad8286', 'Calderon', null, null, '7531-5970', '01748942-1', '1990-12-30', '0601-301290-101-1', 'Diana', null, null, 'F', null, '9b96235e-4fcb-45d2-ab1a-a6c7e9d7fdae');

/* Responsable */
INSERT INTO public.db_responsable(
	id_responsable)
	VALUES ('1f59698f-a6ca-4ee5-b825-1bdfa371ab1f'),
           ('c334d27c-2be2-416a-92a4-63c4d3d68157'),
           ('4d83cc62-181c-4267-93ed-5907f95f1b26'),
           ('457935a3-69f0-4337-b3ce-c810fd80acf4'),
           ('eb947135-ff02-4211-8024-4d98bd748fb8'),
           ('de36484c-2501-4a54-bb42-fe88ce1283fe');

/* Paciente	*/
INSERT INTO public.db_paciente(
	estado_civil_paciente, estado_paciente, id_paciente, id_responsable)
	VALUES ('S',true,'63f9b2d1-1d1a-4852-8a7d-a8cdacb323d9','1f59698f-a6ca-4ee5-b825-1bdfa371ab1f'),
		   ('S',true,'3f827ec9-acb2-4750-9988-8ba522487b5d','c334d27c-2be2-416a-92a4-63c4d3d68157'),
		   ('S',true,'86c94033-a9c8-4346-93aa-7de0cfb92552','4d83cc62-181c-4267-93ed-5907f95f1b26'),
		   ('C',true,'caf7a17d-25c2-43a2-97ff-a79109d3bb90','457935a3-69f0-4337-b3ce-c810fd80acf4'),
		   ('C',true,'e93dd419-b159-4899-90cd-b72b609e277d','eb947135-ff02-4211-8024-4d98bd748fb8'),
		   ('C',true,'30fb59b9-238b-48d3-87ac-6b8556e2a642','de36484c-2501-4a54-bb42-fe88ce1283fe');


