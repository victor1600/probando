
-- Usuario es : userD
-- Clave es: password1
INSERT INTO db_usuario (id_usuario, nombre_usuario, clave_usuario) VALUES ('c1d683cb-19ca-4eb7-a16e-eea98f1d25b5', 'userD', '$2a$04$GJ85Ihcglhbqac2zc3z3A.C3v55FMmN8.qGQ8FlNBCuyLtQ5/TyMO');
INSERT INTO db_rol (id_rol, descripcion_rol, nombre_rol) VALUES ('77fd0c61-0cd7-495f-bce3-f510eec61f53', 'Importar Semilla', 'SEED');
INSERT INTO db_usuario_rol (usuario_id, rol_id) VALUES ('c1d683cb-19ca-4eb7-a16e-eea98f1d25b5', '77fd0c61-0cd7-495f-bce3-f510eec61f53');

-- Si se borra el procedimiento es este:
-- Solo deben ponerlo una vez si ya lo tienen NOOOO LO COPIEN SOLO ES UNA VEZ
CREATE OR REPLACE FUNCTION proc_check_permiso
(p_nombre_usuario VARCHAR, p_nombre_permiso VARCHAR, OUT p_permiso_aprobado INT4)
AS
$$
DECLARE
p_validar_permiso NUMERIC;
BEGIN
SELECT INTO p_validar_permiso COUNT(*)
FROM db_usuario_rol
WHERE
usuario_id=(SELECT id_usuario FROM db_usuario WHERE nombre_usuario=p_nombre_usuario)
AND rol_id=
(SELECT id_rol FROM db_rol WHERE nombre_rol = p_nombre_permiso);
IF p_validar_permiso = 1 THEN
p_permiso_aprobado := 1;
ELSE
p_permiso_aprobado := 0;
END IF;
END;
$$
LANGUAGE PLPGSQL;


