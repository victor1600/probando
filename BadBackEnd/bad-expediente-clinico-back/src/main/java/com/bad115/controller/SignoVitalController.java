package com.bad115.controller;

import com.bad115.model.DetalleResponseHttp;
import com.bad115.model.SignoVitalModel;
import com.bad115.service.SignoVitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/signosVitales")

public class SignoVitalController {
    @Autowired
    @Qualifier("SignoVitalService")
    SignoVitalService service;

    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_SVT')")
    @GetMapping("/todas")
    public ResponseEntity listarTodasVitales() {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/signos/todas");
        List<SignoVitalModel> signos = service.obtenerTodasVitales();
        if (signos.isEmpty()) {
            detalle.setMensaje("signos Vitales aun no se registran");
            detalle.setHttpCodeName("no se encontraron signos Vitales");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(signos, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_SVId')")
    @GetMapping("/{idVitales}")
    public ResponseEntity obtenerSignosId(@PathVariable("idVitales") UUID idVitales){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/signos/"+ idVitales);

        SignoVitalModel signo = service.obtenerVitalId(idVitales);
        if (signo==null){
            detalle.setMensaje("NO SE encontro Los signos vitales con id: "+ idVitales);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else{
            return new ResponseEntity(signo, HttpStatus.FOUND);
        }
    }



    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_SVAdd')")
    @PostMapping("/agregar")
    public ResponseEntity crearSignoVital(@RequestBody @Valid SignoVitalModel signoVital){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/signos/agregar");
        UUID uuid = UUID.randomUUID();
        signoVital.setIdVital(uuid);
        Boolean estado=service.agregarVital(signoVital);
        if (estado==true){
            detalle.setMensaje("se agrego el signo vital Estatura " + signoVital.getEstaturaVital()
                    +", Peso: "+ signoVital.getPesoVital()
                    +",presion "+ signoVital.getPresionVital()+",respiracion "+ signoVital.getRespiracionVital()
                    +",ritmo Cardiaco "+ signoVital.getRitmoVital()+",temperatura "+ signoVital.getTemperaturaVital());
            detalle.setHttpCodeName("Agregada con exito");
            detalle.setHttpCodeNumber(201);
            detalle.setHttpCodeType("SUCCESSFULL");
            return new ResponseEntity(detalle, HttpStatus.CREATED);
        }else {
                detalle.setMensaje("NO SE AGREGO LA Signos vitales"+ signoVital.toString());
                detalle.setHttpCodeName("No se pudo Agregar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_SVUpd')")
    @PutMapping("actualizar/{idVital}")
    public ResponseEntity actualizarVital(@PathVariable("idVital") UUID idVital, @RequestBody @Valid SignoVitalModel signo) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/signos/actualizar/"+idVital);
        SignoVitalModel signoA = service.obtenerVitalId(idVital);
        if(signoA==null){
            detalle.setMensaje("NO SE encontraron signos vitales con id: "+ idVital);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else{
            signoA.setEstaturaVital(signo.getEstaturaVital());
            signoA.setPesoVital(signo.getPesoVital());
            signoA.setPresionVital(signo.getPresionVital());
            signoA.setRespiracionVital(signo.getRespiracionVital());
            signoA.setRitmoVital(signo.getRitmoVital());
            signoA.setTemperaturaVital(signo.getTemperaturaVital());
            Boolean estado = service.actualizarVital(signoA);
            if(estado==true){
                detalle.setMensaje("se actualizo el signo vital Estatura " + signo.getEstaturaVital()
                        +", Peso: "+ signo.getPesoVital()
                        +",presion "+ signo.getPresionVital()+",respiracion "+ signo.getRespiracionVital()
                        +",ritmo Cardiaco "+ signo.getRitmoVital()+",temperatura "+ signo.getTemperaturaVital());
                detalle.setHttpCodeName("Actualizada con exito");
                detalle.setHttpCodeNumber(202);
                detalle.setHttpCodeType("SUCCESSFULL");
                return new ResponseEntity(detalle, HttpStatus.ACCEPTED);
            }else {

                    detalle.setMensaje("NO SE Actualizo LA PERSONA"+ signo.toString());
                    detalle.setHttpCodeName("No se pudo Actualizar");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeType("ERROR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }
            }
        }



    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_SVDel')")
    @DeleteMapping("eliminar/{idVital}")
    public ResponseEntity eliminarVital(@PathVariable("idVital") UUID idVital) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/signos/eliminar/"+idVital);
        SignoVitalModel signo = service.obtenerVitalId(idVital);
        if (signo==null){
            detalle.setMensaje("NO SEpuede eliminar porque no se encontro signos con con id: "+ idVital);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else{
            Boolean estado = service.eliminarVital(idVital);
            if (estado==true){
                detalle.setMensaje("se elimino el signo vital con  Estatura " + signo.getEstaturaVital()
                        +", Peso: "+ signo.getPesoVital()
                        +",presion "+ signo.getPresionVital()+",respiracion "+ signo.getRespiracionVital()
                        +",ritmo Cardiaco "+ signo.getRitmoVital()+",temperatura "+ signo.getTemperaturaVital());
                detalle.setHttpCodeName("Eliminado con exito");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFULL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            }else{
                detalle.setMensaje("NO SE Elimnino signo con id "+ idVital);
                detalle.setHttpCodeName("No se pudo eliminar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }

}



