package com.bad115.controller;

import com.bad115.model.DetalleResponseHttp;
import com.bad115.model.ServicioGastoModel;
import com.bad115.model.ServicioModel;
import com.bad115.service.ServicioGastoService;
import com.bad115.service.ServicioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/servgas")
public class ServicioGastoController {

    @Autowired
    @Qualifier("ServicioGastoService")
    ServicioGastoService service;
    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_ServGasT')")
    @GetMapping("/todas")
    public ResponseEntity listarTodosServiciosGastos() {
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
            detalle.setUrl("/serviciosGastos");
        List<ServicioGastoModel> Servicio = service.obtenerTodasServicioGasto();
        if (Servicio.isEmpty()) {
            detalle.setMensaje("Aun no se registran servicioGasto");
            detalle.setHttpCodeName("no se encontraron servicioGasto");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(Servicio, HttpStatus.FOUND);
        }
    }



//    @CrossOrigin(origins = "http://localhost:3000")
//    @PreAuthorize("hasRole('ROLE_ServGasId')")
//    @GetMapping("/{idServicioGasto}")
//    public ResponseEntity obtenerServicioGastoId(@PathVariable("idServicioGasto") UUID idServicioGasto){
//        Date date = new Date();
//        DetalleResponseHttp detalle = new DetalleResponseHttp();
//        detalle.setFecha(fechaFormat.format(date));
//        detalle.setUrl("/servicioGasto/"+ idServicioGasto);
//
//        ServicioGastoModel servicio = service.obtenerServicioGastoId(idServicioGasto);
//        if (servicio==null){
//            detalle.setMensaje("No se encontro el servicio con id: "+ idServicioGasto);
//            detalle.setHttpCodeName("No existe el id");
//            detalle.setHttpCodeNumber(404);
//            detalle.setHttpCodeType("ERROR");
//            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
//        }else{
//            return new ResponseEntity(servicio, HttpStatus.FOUND);
//        }
//    }
//
//    @CrossOrigin(origins = "http://localhost:3000")
//    @PreAuthorize("hasRole('ROLE_ServGasAdd')")
//    @PostMapping("/agregar")
//    public ResponseEntity crearServicioGasto(@RequestBody @Valid ServicioGastoModel servicio){
//        Date date = new Date();
//        DetalleResponseHttp detalle = new DetalleResponseHttp();
//        detalle.setFecha(fechaFormat.format(date));
//        detalle.setUrl("/servgas/agregar");
//        UUID uuid = UUID.randomUUID();
//        servicio.setIdServicioGasto(uuid);
//        Boolean estado=service.agregarServicioGasto(servicio);
//        if (estado==true){
//            detalle.setMensaje("se agrego el servicioGasto con nombre " + servicio.getIdServicioGasto() +"\n  con Gasto "+ servicio.getGasto() +"\n  con servicio "+ servicio.getServicio());
//            detalle.setHttpCodeName("Agregada con exito");
//            detalle.setHttpCodeNumber(201);
//            detalle.setHttpCodeType("SUCCESSFULL");
//            return new ResponseEntity(detalle, HttpStatus.CREATED);
//        }else {
//            ServicioGastoModel servicioU = service.obtenerServicioGastoId(servicio.getIdServicioGasto());
//            if (servicioU==null){
//                detalle.setMensaje("No se agrego el servicioGasto "+ servicio.toString());
//                detalle.setHttpCodeName("No se pudo Agregar");
//                detalle.setHttpCodeNumber(409);
//                detalle.setHttpCodeType("ERROR");
//                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
//            }else{
//                detalle.setMensaje("El servicioGasto con id: "+ servicio.getIdServicioGasto() + " ya existe");
//                detalle.setHttpCodeName("Conflicto en id del servicio");
//                detalle.setHttpCodeNumber(409);
//                detalle.setHttpCodeType("ERROR");
//                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
//            }
//        }
//    }

}
