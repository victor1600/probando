package com.bad115.controller;

import com.bad115.model.ExpedienteModel;
import com.bad115.model.DetalleResponseHttp;
import com.bad115.service.ExpedienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/expediente")
public class ExpedienteController {

    @Autowired
    @Qualifier("ExpedienteService")
    ExpedienteService service;

    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_ExpT')")
    @GetMapping("/todas")
    public ResponseEntity listarTodasExpediente() {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/expediente/todas");
        List<ExpedienteModel> expediente = service.obtenerTodasExpediente();
        if (expediente.isEmpty()) {
            detalle.setMensaje("Expediente aun no se registran");
            detalle.setHttpCodeName("no se encontraron registros de expediente");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(expediente, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_ExpId')")
    @GetMapping("/{idExpediente}")
    public ResponseEntity obtenerExpedienteId(@PathVariable("idExpediente") UUID idExpediente){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/expediente/"+ idExpediente);

        ExpedienteModel expediente = service.obtenerExpedienteId(idExpediente);
        if (expediente==null){
            detalle.setMensaje("No se encontro el Registro expediente con id: "+ idExpediente);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else{
            return new ResponseEntity(expediente, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_ExpAdd')")
    @PostMapping("/agregar")
    public ResponseEntity crearExpediente(@RequestBody @Valid ExpedienteModel Expediente){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/expediente/agregar");
        UUID uuid = UUID.randomUUID();
        Expediente.setIdExpediente(uuid);
        Boolean estado=service.agregarExpediente(Expediente);
        if (estado==true){
            detalle.setMensaje("se agrego el registro expediente con codigo " + Expediente.getCodigoExpediente());
            detalle.setHttpCodeName("Agregada con exito");
            detalle.setHttpCodeNumber(201);
            detalle.setHttpCodeType("SUCCESSFULL");
            return new ResponseEntity(detalle, HttpStatus.CREATED);
        }else {
            ExpedienteModel ExpedienteU = service.obtenerExpedienteCodigo(Expediente.getCodigoExpediente());
            if (ExpedienteU==null){
                detalle.setMensaje("No se agrego el registro de Expediente "+ Expediente.toString());
                detalle.setHttpCodeName("No se pudo Agregar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }else{
                detalle.setMensaje("El registro Expediente con codigo: "+ Expediente.getCodigoExpediente() + " ya existe");
                detalle.setHttpCodeName("Conflicto en codigo de Expediente");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_ExpUpd')")
    @PutMapping("actualizar/{idExpediente}")
    public ResponseEntity actualizarExpediente(@PathVariable("idExpediente") UUID idExpediente, @RequestBody @Valid ExpedienteModel expediente) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/expediente/actualizar/"+idExpediente);
        ExpedienteModel expedienteA = service.obtenerExpedienteId(idExpediente);
        if(expedienteA==null){
            detalle.setMensaje("No se encontro el expediente con id: "+ idExpediente);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else{
            expedienteA.setCodigoExpediente(expediente.getCodigoExpediente());
            expedienteA.setAperturaExpediente(expediente.getAperturaExpediente());
            Boolean estado = service.actualizarExpediente(expedienteA);
            if(estado==true){
                detalle.setMensaje("se actualizo el expediente" + expediente.getCodigoExpediente());
                detalle.setHttpCodeName("Actualizada con exito");
                detalle.setHttpCodeNumber(202);
                detalle.setHttpCodeType("SUCCESSFULL");
                return new ResponseEntity(detalle, HttpStatus.ACCEPTED);
            }else {
                ExpedienteModel expedienteU = service.obtenerExpedienteCodigo(expediente.getCodigoExpediente());
                if (expedienteU==null){
                    detalle.setMensaje("NO SE Actualizo el expediente "+ expediente.toString());
                    detalle.setHttpCodeName("No se pudo Actualizar");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeType("ERROR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }else{
                    detalle.setMensaje("El expediente ya existe con codigo "+ expediente.getCodigoExpediente());
                    detalle.setHttpCodeName("Conflicto en el codigo");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeType("ERROR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }
            }
        }
    }



}
