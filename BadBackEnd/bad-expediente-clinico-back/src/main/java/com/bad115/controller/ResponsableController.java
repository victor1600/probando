package com.bad115.controller;

import com.bad115.model.DetalleResponseHttp;
import com.bad115.model.DireccionModel;
import com.bad115.model.ResponsableModel;
import com.bad115.service.DireccionService;
import com.bad115.service.ResponsableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/responsables")
public class ResponsableController {

    @Autowired
    @Qualifier("DireccionService")
    DireccionService serviceDir;

    @Autowired
    @Qualifier("ResponsableService")
    ResponsableService serviceRes;
    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_ResponsableAll')")
    @GetMapping("/todos")
    public ResponseEntity ListarTodosResponsables(){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/responsables/todos");

        List<ResponsableModel> responsable = serviceRes.obtenerTodosResponsable();
        if(responsable.isEmpty()){
            detalle.setMensaje("No hay Responsables registrados.");
            detalle.setHttpCodeName("No se encontraron Responsables.");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity(responsable, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_ResponsableId')")
    @GetMapping("/{idResponsable}")
    public ResponseEntity obtenerPacienteId(@PathVariable("idResponsable") UUID idResponsable){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/responsables/"+idResponsable);

        ResponsableModel responsable = serviceRes.findByIdResponsable(idResponsable);
        if (responsable==null){
            detalle.setMensaje("Responsable no encontrado.");
            detalle.setHttpCodeName("Responsable no encontrado.");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new  ResponseEntity(responsable,HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_ResponsableUpd')")
    @PutMapping("actualizar/{idResponsable}")
    public ResponseEntity actualizarResponsable(@PathVariable("idResponsable") UUID idResponsable, @RequestBody @Valid ResponsableModel responsable){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/Responsables/actualizar/"+idResponsable);
        detalle.setHttpCodeName("Fallo al actualizar responsable.");
        detalle.setHttpCodeNumber(409);
        detalle.setHttpCodeType("ERROR");

        ResponsableModel responsableActual = serviceRes.findByIdResponsable(idResponsable);
        if (responsableActual==null){
            detalle.setMensaje("No se encontro el Responsable, fallo al actualizar.");
            detalle.setHttpCodeName("Responsable no encontrado");
            detalle.setHttpCodeNumber(404);
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            DetalleResponseHttp validacion = validarResponsable(responsable);
            if (validacion.getHttpCodeNumber() == 409) {
                detalle.setMensaje(validacion.getMensaje());
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }else {
                DireccionModel direccionR = responsable.getDireccion();
                Boolean estadoDR = serviceDir.guardarDireccion(direccionR);
                System.out.println("responsable"+responsable.toString());
                if(estadoDR){
                    Boolean estadoRes = serviceRes.guardarResponsable(responsable);
                    if(estadoRes) {
                        detalle.setMensaje("Se actualizo el Respopnsable: "+responsableActual.getNombre1Persona()+" "+responsableActual.getApellido1Persona());
                        detalle.setHttpCodeName("Se actualizo con exito");
                        detalle.setHttpCodeNumber(202);
                        detalle.setHttpCodeType("SUCCESSFUL");
                        return new ResponseEntity(detalle, HttpStatus.ACCEPTED);
                    }else {
                        detalle.setMensaje("Fallo al actualizar Responsable.");
                        return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                    }
                }else {
                    detalle.setMensaje("Fallo al actualizar Dirección.");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }
            }
        }
    }

    DetalleResponseHttp validarResponsable(ResponsableModel responsable){
        DetalleResponseHttp detalle = new DetalleResponseHttp();

        if (responsable.getNombre1Persona() == null) {
            detalle.setMensaje("Debe ingresar el primer nombre del responsable.");
            detalle.setHttpCodeNumber(409);
        } else {
            if (responsable.getApellido1Persona() == null) {
                detalle.setMensaje("Debe ingresar el primer apellido del responsable.");
                detalle.setHttpCodeNumber(409);
            }else {
                if (responsable.getDuiPersona() == null){
                    detalle.setMensaje("Debe ingresar el DUI del responsable.");
                    detalle.setHttpCodeNumber(409);
                }else {
                    if (responsable.getNitPersona() == null){
                        detalle.setMensaje("Debe ingresar el NIT del responsable.");
                        detalle.setHttpCodeNumber(409);
                    }else {
                        if (responsable.getDireccion().getUrbanizacion() == null) {
                            detalle.setMensaje("Debe ingresar urbanizacion, colonia o barrio.");
                            detalle.setHttpCodeNumber(409);
                        }
                    }
                }
            }
        }
        return detalle;
    }
}
