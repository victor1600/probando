package com.bad115.controller;

import com.bad115.model.Cie10Model;
import com.bad115.model.DetalleResponseHttp;
import com.bad115.model.ServicioModel;
import com.bad115.service.ServicioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/servicio")
public class ServicioController {

    @Autowired
    @Qualifier("ServicioService")
    ServicioService service;
    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_ServT')")
    @GetMapping("/todas")
    public ResponseEntity listartTodosServicios() {
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/servicios");
        List<ServicioModel> Servicio = service.lista_activos();
        if (Servicio.isEmpty()) {
            detalle.setMensaje("Aun no se registran servicio");
            detalle.setHttpCodeName("no se encontraron servicio");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(Servicio, HttpStatus.FOUND);
        }
    }



    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_ServId')")
    @GetMapping("/{idServicio}")
    public ResponseEntity obtenerServicioId(@PathVariable("idServicio") UUID idServicio){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/servicio/"+ idServicio);

        ServicioModel servicio = service.obtenerServicioId(idServicio);
        if (servicio==null){
            detalle.setMensaje("No se encontro el servicio con id: "+ idServicio);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else{
            return new ResponseEntity(servicio, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_ServAdd')")
    @PostMapping("/agregar")
    public ResponseEntity crearServicio(@RequestBody @Valid ServicioModel servicio){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/servicio/agregar");
        UUID uuid = UUID.randomUUID();
        servicio.setIdServicio(uuid);
        Boolean estado=service.agregarServicio(servicio);
        if (estado==true){
            detalle.setMensaje("se agrego el servicio con nombre " + servicio.getNombreServicio() +"\n  con descripcion "+ servicio.getDescripcionServicio() +"\n  con costo "+ servicio.getCostoServicio());
            detalle.setHttpCodeName("Agregada con exito");
            detalle.setHttpCodeNumber(201);
            detalle.setHttpCodeType("SUCCESSFULL");
            return new ResponseEntity(detalle, HttpStatus.CREATED);
        }else {
            ServicioModel servicioU = service.obtenerServicioNombre(servicio.getNombreServicio());
            if (servicioU==null){
                detalle.setMensaje("No se agrego el servicio "+ servicio.toString());
                detalle.setHttpCodeName("No se pudo Agregar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }else{
                detalle.setMensaje("El servicio con nombre: "+ servicio.getNombreServicio() + " ya existe");
                detalle.setHttpCodeName("Conflicto en nombre del servicio");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ServUpd')")
    @PutMapping("actualizar/{idServicio}")
    public ResponseEntity actualizarSerrvicio(@PathVariable("idServicio") UUID idServicio, @RequestBody @Valid ServicioModel servicio) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/servicio/actualizar/"+idServicio);
        ServicioModel servicioA = service.obtenerServicioId(idServicio);
        if(servicioA==null){
            detalle.setMensaje("NO SE encontro el servicio con id: "+ idServicio);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else{
            servicioA.setNombreServicio(servicio.getNombreServicio());
            servicioA.setDescripcionServicio(servicio.getDescripcionServicio());
            servicioA.setCostoServicio(servicio.getCostoServicio());
            servicioA.setEstaActivo(servicio.getEstaActivo());
            Boolean estado = service.actualizarServicio(servicioA);
            if(estado==true){
                detalle.setMensaje("se actualizo el gasto" + servicio.getNombreServicio() +"\n "+ servicio.getDescripcionServicio()+"\n "+ servicio.getCostoServicio()+"\n "+ servicio.getEstaActivo());
                detalle.setHttpCodeName("Actualizada con exito");
                detalle.setHttpCodeNumber(202);
                detalle.setHttpCodeType("SUCCESSFULL");
                return new ResponseEntity(detalle, HttpStatus.ACCEPTED);
            }else {
                ServicioModel servicioU = service.obtenerServicioNombre(servicio.getNombreServicio());
                if (servicioU==null){
                    detalle.setMensaje("NO SE Actualizo El servicio "+ servicio.toString());
                    detalle.setHttpCodeName("No se pudo Actualizar");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeType("ERROR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }else{
                    detalle.setMensaje("El servicio ya existe con nombre "+ servicio.getNombreServicio());
                    detalle.setHttpCodeName("Conflicto en el nombre");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeType("ERROR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_ServDel')")
    @DeleteMapping("eliminar/{idServicio}")
    public ResponseEntity eliminarServicioId(@PathVariable("idServicio") UUID idServicio) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/sericio/eliminar/" + idServicio);
        ServicioModel servicio = service.obtenerServicioId(idServicio);
        if (servicio == null) {
            detalle.setMensaje("No se encontro el servicio, por lo tanto no se puede eliminar con id: " + idServicio);
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            Boolean estado = service.toggle(idServicio, false);// eliminarservicio(idServicio);
            if (estado == true) {
                detalle.setMensaje("Se elimino con exito el servicio " + servicio.getNombreServicio());
                detalle.setHttpCodeName("Eliminado con exito");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            } else {
                detalle.setMensaje("Error al eliminar servicio con id: " + idServicio);
                detalle.setHttpCodeName("Error al eliminar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }

}
