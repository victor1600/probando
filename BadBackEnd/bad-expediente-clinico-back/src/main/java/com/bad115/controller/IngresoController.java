package com.bad115.controller;

import com.bad115.model.IngresoModel;
import com.bad115.model.DetalleResponseHttp;
import com.bad115.service.IngresoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/ingreso")
public class IngresoController {

    @Autowired
    @Qualifier("IngresoService")
    IngresoService service;

    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/todos")
    public ResponseEntity ListarIngresos(){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/ingreso/todos");

        List<IngresoModel> ingreso = service.obtenerTodosLosIngresos();
        if(ingreso.isEmpty()){
            detalle.setMensaje("No hay ingresos registrados.");
            detalle.setHttpCodeName("No se encontraron los ingresos.");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity(ingreso, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/{idIngreso}")
    public ResponseEntity obtenerIngresoId(@PathVariable("idIngreso") UUID idIngreso){
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/ingreso/"+idIngreso);
        IngresoModel ingreso = service.obtenerIngresoId(idIngreso);
        if (ingreso==null){
            detalle.setMensaje("No se encontro el ingreso con id: "+idIngreso);
            detalle.setHttpCodeName("Ingreso no encontrado.");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new  ResponseEntity(ingreso,HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/agregar")
    public ResponseEntity crearIngreso(@RequestBody @Valid IngresoModel ingreso){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/ingreso/agregar");
        UUID uuid = UUID.randomUUID();
        ingreso.setIdIngreso(uuid);

        Boolean estado=service.agregarIngreso(ingreso);
        if (estado){
            detalle.setMensaje("Se agrego el ingreso :"+ingreso.getFechaIngreso());
            detalle.setHttpCodeType("SUCCESSFUL");
            detalle.setHttpCodeNumber(201);
            detalle.setHttpCodeName("Agregado con exito");
            return new ResponseEntity(detalle, HttpStatus.CREATED);
        }else {
            IngresoModel ingresoValidar = service.obtenerIngresoFecha(ingreso.getFechaIngreso());
            if (ingresoValidar==null){
                detalle.setMensaje("No se pudo agregar el ingreso: " + ingreso.toString());
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("NO SE PUDO AGREGAR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
            else{
                detalle.setMensaje("Ya existe el ingreso: "+ingreso.getFechaIngreso());
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("CONFLICTO EN NOMBRE");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }

        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PutMapping("actualizar/{idIngreso}")
    public ResponseEntity actualizarIngreso(@PathVariable("idIngreso") UUID idIngreso, @RequestBody @Valid IngresoModel ingreso){
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/ingreso/actualizar/"+idIngreso);
        IngresoModel ingresoUpdate = service.obtenerIngresoId(idIngreso);
        if (ingresoUpdate==null){
            detalle.setMensaje("No se encontro, no puede ser Actualizado el ingreso: "+ingreso.getFechaIngreso());
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            ingresoUpdate.setFechaIngreso(ingreso.getFechaIngreso());
            ingresoUpdate.setFechaAltaIngreso(ingreso.getFechaAltaIngreso());
            ingresoUpdate.setMotivoIngreso(ingreso.getMotivoIngreso());
            Boolean estado = service.actualizarIngreso(ingresoUpdate);
            if (estado){
                detalle.setMensaje("Se actualizo el ingreso: "+ingresoUpdate.getFechaIngreso());
                detalle.setHttpCodeName("Se actualizo con exito");
                detalle.setHttpCodeNumber(202);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.ACCEPTED);
            }else {

                    detalle.setMensaje("No se pudo actualizar el ingreso: "+ ingreso.toString());
                    detalle.setHttpCodeType("ERROR");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeName("NO SE PUDO ACTUALIZAR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @DeleteMapping("/eliminar/{idIngreso}")
    public ResponseEntity eliminarIngresoId(@PathVariable("idIngreso") UUID idIngreso){

        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/ingreso/eliminar/"+idIngreso);

        IngresoModel ingreso = service.obtenerIngresoId(idIngreso);
        if (ingreso==null){
            detalle.setMensaje("No se puede eliminar, el ingreso no existe");
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            Boolean estado = service.eliminarIngreso(idIngreso);
            if (estado){
                detalle.setMensaje("Se elimino con exito el ingreso: "+ingreso.getFechaIngreso());
                detalle.setHttpCodeName("Eliminado con exito");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            }else {
                detalle.setMensaje("Error al eliminar ingreso: "+ingreso.getFechaIngreso());
                detalle.setHttpCodeName("Error al eliminar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }

    }


}