package com.bad115.controller;

import com.bad115.configuration.util.UsuarioActual;
import com.bad115.model.ConsultaModel;
import com.bad115.model.DetalleResponseHttp;
import com.bad115.model.ReferenciaMedicaModel;
import com.bad115.service.ConsultaService;
import com.bad115.service.ReferenciaMedicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/referenciaMedica")
public class ReferenciaMedicaController {

    @Autowired
    @Qualifier("ReferenciaMedicaService")
    ReferenciaMedicaService service;

    @Autowired
    @Qualifier("ConsultaService")
    ConsultaService serviceC;

    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    UsuarioActual usuarioActual = new UsuarioActual();

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_RefMedTds')")
    @GetMapping("/todas")
    ResponseEntity listarTodasRefernciasMedicas(){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/referenciaMedica/todas");
        List<ReferenciaMedicaModel> referencias = service.obtenerTodosReferencias();
        if (referencias.isEmpty()){
            detalle.setMensaje("Aun no se registran referencias");
            detalle.setHttpCodeName("NO SE ENCONTRARON Referencias Medicas");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return  new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity(referencias, HttpStatus.FOUND);
        }

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_RefMedId')")
    @GetMapping("/id/{idReferencia}")
    ResponseEntity obtenerIdReferencia(@PathVariable("idReferencia") UUID idReferencia){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/referenciaMedica/id/"+idReferencia);
        ReferenciaMedicaModel referencia = service.obtenerIdReferencia(idReferencia);
        if (referencia == null){
            detalle.setMensaje("No se encontro referencia medica con id: "+idReferencia);
            detalle.setHttpCodeName("Referencia Medica No Encontrada");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return  new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity(referencia, HttpStatus.FOUND);
        }

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_RefMedFk')")
    @GetMapping("/fk/{fkReferencia}")
    ResponseEntity obtenerFkReferencia(@PathVariable("fkReferencia") UUID fkReferencia){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/referenciaMedica/fk/"+fkReferencia);

        System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        System.out.println(usuarioActual.obtenerUsuario());




        ReferenciaMedicaModel referencia = service.obtenerFkReferencia(fkReferencia);
        if (referencia == null){
            detalle.setMensaje("No se encontro referencia medica con fk: "+fkReferencia);
            detalle.setHttpCodeName("Referencia Medica No Encontrada");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return  new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity(referencia, HttpStatus.FOUND);
        }

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_RefMedAdd')")
    @PostMapping("/agregar")
    ResponseEntity agregarReferencia(@RequestBody @Valid ReferenciaMedicaModel referencia){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/referenciaMedica/agregar/");
        //buscaruuidForanea
        ConsultaModel consultaF = serviceC.obtenerIdConsulta(referencia.getFkConsulta());

        if (consultaF == null){
            detalle.setMensaje("No se puede agregar porque no existe la consulta relacionada con el id: "+ referencia.getFkConsulta());
            detalle.setHttpCodeType("ERROR");
            detalle.setHttpCodeNumber(409);
            detalle.setHttpCodeName("Consulta Relacionada No encontrada");
            return new ResponseEntity(detalle, HttpStatus.CONFLICT);
        }else {
            ReferenciaMedicaModel referenciaU = service.obtenerFkReferencia(referencia.getFkConsulta());
            if (referenciaU == null){
                referencia.setFkConsultaId(consultaF);
                Boolean estado = service.guardarReferenciaMedica(referencia);
                if (estado==true){
                    detalle.setMensaje("La referencia se guardo con exito con id: "+referencia.getIdReferencia());
                    detalle.setHttpCodeType("SUCCESSFUL");
                    detalle.setHttpCodeNumber(201);
                    detalle.setHttpCodeName("Agregado con exito");
                    return new ResponseEntity(detalle, HttpStatus.CREATED);
                }else {
                    detalle.setMensaje("Hubo error al agregar la referencia: " +referencia.toString());
                    detalle.setHttpCodeType("ERROR");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeName("ERROR DESCONOCIDO AL AGREGAR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }
            }else {
                detalle.setMensaje("No se puede agregar porque esa consulta ya tiene una referencia asociada: "+ referencia.getFkConsulta());
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("Consulta ya esta relacionada");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_RefMedUpd')")
    @PutMapping("/actualizar/{idReferencia}")
    ResponseEntity actualizarReferencia(@PathVariable("idReferencia") UUID idReferencia, @RequestBody @Valid ReferenciaMedicaModel referencia){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/referenciaMedica/actualizar/"+idReferencia);

        ReferenciaMedicaModel referenciaA = service.obtenerIdReferencia(idReferencia);

        if (referenciaA==null){
            detalle.setMensaje("No se puede actualizar, ya que no existe esta refencia con id : "+idReferencia);
            detalle.setHttpCodeType("ERROR");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeName("Referencia no encontrada");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            referenciaA.setDetalleReferencia(referencia.getDetalleReferencia());
            Boolean estado = service.guardarReferenciaMedica(referenciaA);
            if (estado==true){
                detalle.setMensaje("La referencia se actualizo con exito con id: "+referencia.getIdReferencia());
                detalle.setHttpCodeType("SUCCESSFUL");
                detalle.setHttpCodeNumber(202);
                detalle.setHttpCodeName("Actualizado con exito");
                return new ResponseEntity(detalle, HttpStatus.ACCEPTED);
            }else {
                detalle.setMensaje("Hubo error al agregar la referencia: " +referencia.toString());
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("ERROR DESCONOCIDO AL ACTUALIZAR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_RefMeDel')")
    @DeleteMapping("/eliminar/{idReferencia}")
    ResponseEntity eliminarIdReferencia(@PathVariable("idReferencia") UUID idReferencia){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/referenciaMedica/eliminar/"+idReferencia);
        ReferenciaMedicaModel referencia = service.obtenerIdReferencia(idReferencia);
        if (referencia == null){
            detalle.setMensaje("No se permite eliminar porque no se encontro referncia con id : "+idReferencia);
            detalle.setHttpCodeName("Referencia Medica No Encontrada");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return  new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            Boolean estado = service.eliminarReferenciaMedica(idReferencia);
            if (estado==true){
                detalle.setMensaje("se elimino con exito la referencia con id: "+ idReferencia );
                detalle.setHttpCodeName("Eliminado con exito");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFULL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            }else{
                detalle.setMensaje("No se elimnino el registro de la refrencia con id: "+idReferencia);
                detalle.setHttpCodeName("No se pudo eliminar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }

    }



}
