package com.bad115.controller;

import com.bad115.model.*;
import com.bad115.service.DireccionService;
import com.bad115.service.PacienteService;
import com.bad115.service.ResponsableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/pacientes")
public class PacienteController {

    @Autowired
    @Qualifier("PacienteService")
    PacienteService servicePac;

    @Autowired
    @Qualifier("DireccionService")
    DireccionService serviceDir;

    @Autowired
    @Qualifier("ResponsableService")
    ResponsableService serviceRes;
    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_PacienteAll')")
    @GetMapping("/todos")
    public ResponseEntity ListarTodosPacientes(){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/pacientes/todos");

        List<PacienteModel> pacientes = servicePac.obtenerTodosPacientes();
        if(pacientes.isEmpty()){
            detalle.setMensaje("No hay pacientes registrados.");
            detalle.setHttpCodeName("No se encontraron pacientes.");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity(pacientes, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_PacienteId')")
    @GetMapping("/{idPaciente}")
    public ResponseEntity obtenerPacienteId(@PathVariable("idPaciente") UUID idPaciente){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/pacientes/"+idPaciente);

        PacienteModel paciente = servicePac.findByIdPaciente(idPaciente);
        if (paciente==null){
            detalle.setMensaje("Paciente no encontrado.");
            detalle.setHttpCodeName("Paciente no encontrado.");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new  ResponseEntity(paciente,HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_PacienteAdd')")
    @PostMapping("/agregar")
    public ResponseEntity crearPaciente(@RequestBody @Valid PacienteModel paciente) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/pacientes/agregar");
        detalle.setHttpCodeName("Fallo al agregar paciente");
        detalle.setHttpCodeNumber(409);
        detalle.setHttpCodeType("ERROR");
        System.out.println("Estoy dentro de la funcion guardar, antes de validar paciente");
        //Direccion Paciente
        UUID dirP = UUID.randomUUID();
        DireccionModel direccionP = paciente.getDireccion();
        direccionP.setIdDireccion(dirP);
        //Responsable
        ResponsableModel responsable = paciente.getResponsable();
        DireccionModel direccionR = responsable.getDireccion();
        UUID dirR = UUID.randomUUID();
        direccionR.setIdDireccion(dirR);
        responsable.setDireccion(direccionR);

        //Validar Datos de Entrada
        DetalleResponseHttp validacion = validarDatos(paciente);
        if (validacion.getHttpCodeNumber() == 409) {
            detalle.setMensaje(validacion.getMensaje());
            return new ResponseEntity(detalle, HttpStatus.CONFLICT);
        }else {
            System.out.println("Ya se valido paciente");
            Boolean estadoDP = serviceDir.guardarDireccion(direccionP);
            Boolean estadoDR = serviceDir.guardarDireccion(direccionR);
            if(estadoDP && estadoDR) {
                System.out.println("xxx1");
                Boolean estadoRes = serviceRes.guardarResponsable(responsable);
                if(estadoRes) {
                    System.out.println("ya se guardo direccion, estoy antes de paciente");
                    Boolean estadoPac = servicePac.guardarPaciente(paciente);
                    if (estadoPac) {
                        System.out.println("Exito");
                        detalle.setMensaje("Se agrego el paciente: "+paciente.getNombre1Persona()+" "+paciente.getApellido1Persona());
                        detalle.setHttpCodeName("Agregado con exito");
                        detalle.setHttpCodeNumber(201);
                        detalle.setHttpCodeType("SUCCESSFUL");
                        return new ResponseEntity(detalle, HttpStatus.CREATED);
                    }else {
                        System.out.println("Fallo P");
                        serviceDir.eliminarDireccion(dirP);
                        serviceDir.eliminarDireccion(dirR);
                        serviceRes.eliminarResponsable(responsable.getDuiPersona());
                        detalle.setMensaje("Fallo al ingresar paciente.");
                        return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                    }
                }else {
                    System.out.println("Fallo R");
                    serviceDir.eliminarDireccion(dirP);
                    serviceDir.eliminarDireccion(dirR);
                    detalle.setMensaje("Fallo al ingresar responsable.");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }
            }else {
                System.out.println("Fallo D");
                detalle.setMensaje("Fallo al ingresar dirección.");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_PacienteUpd')")
    @PutMapping("actualizar/{idPaciente}")
    public ResponseEntity actualizarPaciente(@PathVariable("idPaciente") UUID idPaciente, @RequestBody @Valid PacienteModel paciente){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/pacientes/actualizar/"+idPaciente);
        detalle.setHttpCodeName("Fallo al actualizar paciente");
        detalle.setHttpCodeNumber(409);
        detalle.setHttpCodeType("ERROR");

        PacienteModel pacienteActual = servicePac.findByIdPaciente(idPaciente);
        if (pacienteActual==null){
            detalle.setMensaje("No se encontro el paciente, fallo al actualizar.");
            detalle.setHttpCodeName("Paciente no encontrado");
            detalle.setHttpCodeNumber(404);
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            DetalleResponseHttp validacion = validarDatos(paciente);
            if (validacion.getHttpCodeNumber() == 409) {
                detalle.setMensaje(validacion.getMensaje());
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }else {
                DireccionModel direccionP = paciente.getDireccion();
                Boolean estadoDP = serviceDir.guardarDireccion(direccionP);
                if(estadoDP){
                    ResponsableModel responsable = paciente.getResponsable();
                    Boolean estadoRes = serviceRes.guardarResponsable(responsable);
                    if(estadoRes) {
                        Boolean estadoP = servicePac.guardarPaciente(paciente);
                        if (estadoP) {
                            detalle.setMensaje("Se actualizo el Paciente: "+pacienteActual.getNombre1Persona()+" "+pacienteActual.getApellido1Persona());
                            detalle.setHttpCodeName("Se actualizo con exito");
                            detalle.setHttpCodeNumber(202);
                            detalle.setHttpCodeType("SUCCESSFUL");
                            return new ResponseEntity(detalle, HttpStatus.ACCEPTED);
                        }else {
                            detalle.setMensaje("Fallo al actualizar Paciente: "+pacienteActual.getNombre1Persona()+" "+pacienteActual.getApellido1Persona());
                            return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                        }
                    }else {
                        detalle.setMensaje("Fallo al actualizar Responsable.");
                        return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                    }
                }else {
                    System.out.println("Error");
                    detalle.setMensaje("Fallo al actualizar Dirección.");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_PacienteState')")
    @PutMapping("/estado/{idPaciente}")
    public ResponseEntity cambiarEstadoPaciente(@PathVariable("idPaciente") UUID idPaciente){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/pacientes/estado/"+idPaciente);

        PacienteModel paciente = servicePac.findByIdPaciente(idPaciente);
        if (paciente==null){
            detalle.setMensaje("Paciente no encontrado, fallo al cambiar estado.");
            detalle.setHttpCodeName("Paciente no encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            Boolean estadoP = servicePac.estadoPaciente(idPaciente);
            if (estadoP){
                detalle.setMensaje("Se cambio el estado del paciente: "+paciente.getNombre1Persona()+" "+paciente.getApellido1Persona());
                detalle.setHttpCodeName("Cambio de estado realizado con exito.");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            }else {
                detalle.setMensaje("Fallo al cambiar estado del paciente.");
                detalle.setHttpCodeName("Error al cambiar estado.");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }

    DetalleResponseHttp validarDatos(PacienteModel paciente) {

        DetalleResponseHttp detalle = new DetalleResponseHttp();
        DireccionModel direccionP = paciente.getDireccion();
        ResponsableModel responsable = paciente.getResponsable();

        if (direccionP.getUrbanizacion() == null) {
            detalle.setMensaje("Debe ingresar urbanizacion, colonia o barrio.");
            detalle.setHttpCodeNumber(409);
        } else {
            DetalleResponseHttp valildarRes = validarResponsable(responsable);
            if (valildarRes.getHttpCodeNumber() == 409) {
                detalle.setMensaje(valildarRes.getMensaje());
                detalle.setHttpCodeNumber(409);
            } else {
                DetalleResponseHttp validarPac = validarPaciente(paciente);
                if (validarPac.getHttpCodeNumber() == 409) {
                    detalle.setMensaje(validarPac.getMensaje());
                    detalle.setHttpCodeNumber(409);
                } else {
                    String fechaNacPaciente = (paciente.getFechaNacimientoPersona());
                    DateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
                    Date fecha=null;
                    try {
                        fecha = formato.parse(fechaNacPaciente);
                    } catch (ParseException e) {
                        detalle.setMensaje("La fecha de nacimiento del paciente no es valida.");
                        detalle.setHttpCodeNumber(409);
                    }
                    Calendar fechaNac = new GregorianCalendar();
                    fechaNac.setTime(fecha);
                    int edad[] = calcularEdad(fechaNac);
                    if ((edad[1] >= 18 && edad[2] > 2) || (edad[1] >= 18 && edad[2] == 2 && edad[3] > 0)) {
                        DetalleResponseHttp validarDoc = validarDocumentos(paciente);
                        if (validarDoc.getHttpCodeNumber() == 409) {
                            detalle.setMensaje(validarDoc.getMensaje());
                            detalle.setHttpCodeNumber(409);
                        }
                    }
                }
            }
        }
        return detalle;
    }

    DetalleResponseHttp validarPaciente(PacienteModel paciente){
        DetalleResponseHttp detalle = new DetalleResponseHttp();

        if (paciente.getNombre1Persona() == null) {
            detalle.setMensaje("Debe ingresar el primer nombre del paciente.");
            detalle.setHttpCodeNumber(409);
        }else {
            if (paciente.getApellido1Persona() == null) {
                detalle.setMensaje("Debe ingresar el primer apellido del paciente.");
                detalle.setHttpCodeNumber(409);
            }else {
                if (paciente.getEstadoCivilPaciente() == null){
                    detalle.setMensaje("Debe ingresar el estado civil del paciente.");
                    detalle.setHttpCodeNumber(409);
                }else {
                    if (paciente.getSexoPersona() == null){
                        detalle.setMensaje("Debe ingresar el sexo del paciente.");
                        detalle.setHttpCodeNumber(409);
                    }else {
                        if (paciente.getFechaNacimientoPersona() == null){
                            detalle.setMensaje("Debe ingresar la fecha de nacimiento del paciente.");
                            detalle.setHttpCodeNumber(409);
                        }
                    }
                }
            }
        }
        return detalle;
    }

    DetalleResponseHttp validarResponsable(ResponsableModel responsable){
        DetalleResponseHttp detalle = new DetalleResponseHttp();

        if (responsable.getNombre1Persona() == null) {
            detalle.setMensaje("Debe ingresar el primer nombre del responsable.");
            detalle.setHttpCodeNumber(409);
        } else {
            if (responsable.getApellido1Persona() == null) {
                detalle.setMensaje("Debe ingresar el primer apellido del responsable.");
                detalle.setHttpCodeNumber(409);
            }else {
                if (responsable.getDuiPersona() == null){
                    detalle.setMensaje("Debe ingresar el DUI del responsable.");
                    detalle.setHttpCodeNumber(409);
                }else {
                    if (responsable.getNitPersona() == null){
                        detalle.setMensaje("Debe ingresar el NIT del responsable.");
                        detalle.setHttpCodeNumber(409);
                    }
                }
            }
        }
        return detalle;
    }

    DetalleResponseHttp validarDocumentos(PacienteModel paciente){
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        if (paciente.getDuiPersona() == null){
            detalle.setMensaje("Debe ingresar el DUI del paciente.");
            detalle.setHttpCodeNumber(409);
        }else {
            if (paciente.getNitPersona() == null) {
                detalle.setMensaje("Debe ingresar el NIT del paciente.");
                detalle.setHttpCodeNumber(409);
            }
        }
        return detalle;
    }

    private int[] calcularEdad(Calendar fechaNac) {
        int[] edad = new int[4]; int year=1, month=2, day=3;

        Calendar today = Calendar.getInstance();
        edad[year] = today.get(Calendar.YEAR) - fechaNac.get(Calendar.YEAR);
        edad[month] = today.get(Calendar.MONTH) - fechaNac.get(Calendar.MONTH);
        edad[day] = today.get(Calendar.DAY_OF_MONTH) - fechaNac.get(Calendar.DAY_OF_MONTH);

        if (edad[month] < 0 || (edad[month] == 0 && edad[day] < 0)) {
            edad[year] = edad[year] - 1;
        }
        return edad;
    }
}
