package com.bad115.controller;

import com.bad115.model.AsuetoModel;
import com.bad115.model.DetalleResponseHttp;
import com.bad115.model.ServicioModel;
import com.bad115.service.AsuetoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/asueto")
public class AsuetoController {

    @Autowired
    @Qualifier("AsuetoService")
    AsuetoService service;
    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");


    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_AsuT')")
    @GetMapping("/todas")
    public ResponseEntity listartTodasAsuetos() {
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/asuetos");
        List<AsuetoModel> Asueto = service.lista_activos();
        if (Asueto.isEmpty()) {
            detalle.setMensaje("Aun no se registran Asueto");
            detalle.setHttpCodeName("no se encontraron Asuetos");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(Asueto, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_AsuId')")
    @GetMapping("/{idAsueto}")
    public ResponseEntity obtenerAsuetoId(@PathVariable("idAsueto") UUID idAsueto){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/asueto/"+ idAsueto);

        AsuetoModel asueto = service.obtenerAsuetoId(idAsueto);
        if (asueto==null){
            detalle.setMensaje("No se encontro el asueto con id: "+ idAsueto);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else{
            return new ResponseEntity(asueto, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_AsuAdd')")
    @PostMapping("/agregar")
    public ResponseEntity crearAsueto(@RequestBody @Valid AsuetoModel asueto){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/asueto/agregar");
        UUID uuid = UUID.randomUUID();
        asueto.setIdAsueto(uuid);
        Boolean estado=service.agregarAsueto(asueto);
        if (estado==true){
            detalle.setMensaje("se agrego el asueto con nombre " + asueto.getNombreAsueto() +"\n  con descripcion "+ asueto.getFechaAsueto());
            detalle.setHttpCodeName("Agregada con exito");
            detalle.setHttpCodeNumber(201);
            detalle.setHttpCodeType("SUCCESSFULL");
            return new ResponseEntity(detalle, HttpStatus.CREATED);
        }else {
            AsuetoModel asuetoU = service.obtenerAsuetoNombre(asueto.getNombreAsueto());
            if (asuetoU==null){
                detalle.setMensaje("No se agrego el asueto "+ asueto.toString());
                detalle.setHttpCodeName("No se pudo Agregar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }else{
                detalle.setMensaje("El asueto con nombre: "+ asueto.getNombreAsueto() + " ya existe");
                detalle.setHttpCodeName("Conflicto en nombre del asueto");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('AsuUpd')")
    @PutMapping("actualizar/{idAsueto}")
    public ResponseEntity actualizarAsueto(@PathVariable("idAsueto") UUID idAsueto, @RequestBody @Valid AsuetoModel servicio) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/asueto/actualizar/"+idAsueto);
        AsuetoModel servicioA = service.obtenerAsuetoId(idAsueto);
        if(servicioA==null){
            detalle.setMensaje("NO SE encontro el servicio con id: "+ idAsueto);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else{
            servicioA.setNombreAsueto(servicio.getNombreAsueto());
            servicioA.setFechaAsueto(servicio.getFechaAsueto());
            servicioA.setEstaActivo(servicio.getEstaActivo());
            Boolean estado = service.actualizarAsueto(servicioA);
            if(estado==true){
                detalle.setMensaje("se actualizo el servicio" + servicio.getNombreAsueto() +"\n "+ servicio.getFechaAsueto()+"\n "+ servicio.getEstaActivo());
                detalle.setHttpCodeName("Actualizada con exito");
                detalle.setHttpCodeNumber(202);
                detalle.setHttpCodeType("SUCCESSFULL");
                return new ResponseEntity(detalle, HttpStatus.ACCEPTED);
            }else {
                AsuetoModel servicioU = service.obtenerAsuetoNombre(servicio.getNombreAsueto());
                if (servicioU==null){
                    detalle.setMensaje("NO SE Actualizo El asueto "+ servicio.toString());
                    detalle.setHttpCodeName("No se pudo Actualizar");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeType("ERROR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }else{
                    detalle.setMensaje("El asueto ya existe con nombre "+ servicio.getNombreAsueto());
                    detalle.setHttpCodeName("Conflicto en el nombre");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeType("ERROR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_AsuDel')")
    @DeleteMapping("eliminar/{idAsueto}")
    public ResponseEntity eliminarAsuetoId(@PathVariable("idAsueto") UUID idAsueto) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/asueto/eliminar/" + idAsueto);
        AsuetoModel servicio = service.obtenerAsuetoId(idAsueto);
        if (servicio == null) {
            detalle.setMensaje("No se encontro el asueto, por lo tanto no se puede eliminar con id: " + idAsueto);
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            Boolean estado = service.toggle(idAsueto, false);// eliminarservicio(idAsueto);
            if (estado == true) {
                detalle.setMensaje("Se elimino con exito el asueto " + servicio.getNombreAsueto());
                detalle.setHttpCodeName("Eliminado con exito");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            } else {
                detalle.setMensaje("Error al eliminar asueto con id: " + idAsueto);
                detalle.setHttpCodeName("Error al eliminar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }


}
