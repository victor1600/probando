package com.bad115.controller;

import com.bad115.model.DetalleResponseHttp;
import com.bad115.model.DosisModel;
import com.bad115.model.MedicamentoModel;
import com.bad115.model.TratamientoModel;
import com.bad115.service.DosisService;
import com.bad115.service.MedicamentoService;
import com.bad115.service.TratamientoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/dosis")
public class DosisController {

    @Autowired
    @Qualifier("DosisService")
    DosisService service;

    @Autowired
    @Qualifier("TratamientoService")
    TratamientoService service2;

    @Autowired
    @Qualifier("MedicamentoService")
    MedicamentoService service3;

    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_dosisT')")
    @GetMapping("/todas")
    public ResponseEntity listarDosis() {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/dosis/todas");
        List<DosisModel> dosis = service.lista_activos();
        if (dosis.isEmpty()) {
            detalle.setMensaje("Aun no se registran dosis");
            detalle.setHttpCodeName("NO SE ENCONTRARON dosis");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(dosis, HttpStatus.OK);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_dosisId')")
    @GetMapping("/{idDosis}")
    public ResponseEntity obtenerDosisPorId(@PathVariable("idDosis") UUID idDosis) {
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/dosis/" + idDosis);
        DosisModel dosis = service.obtenerById(idDosis);
        if (dosis == null) {
            detalle.setMensaje("No se encontro la dosis con id: " + idDosis);
            detalle.setHttpCodeName("Dosis NO ENCONTRADA");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(dosis, HttpStatus.OK);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_dosisAdd')")
    @PostMapping("/agregar")
    public ResponseEntity agregarDosis(@RequestBody @Valid DosisModel dosis) {
        UUID idDosis = UUID.randomUUID();
        dosis.setIdDosis(idDosis);

        DetalleResponseHttp detalle = new DetalleResponseHttp();
        Date date = new Date();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/dosis/agregar/" + idDosis);
        String mensaje = service.checker(dosis.getFkMedicamento(),dosis.getFkTratamiento());
        if(!mensaje.equals("Se puede insertar registro")){
            detalle.setMensaje(mensaje);
            detalle.setHttpCodeName("No se pudo Agregar");
            detalle.setHttpCodeNumber(409);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.CONFLICT);
        }
        else {
            TratamientoModel tratamiento = service2.obtenerTratamientoPorId(dosis.getFkTratamiento());
            MedicamentoModel medicamento = service3.obtenerById(dosis.getFkMedicamento());
            dosis.setTratamiento(tratamiento);
            dosis.setMedicamento(medicamento);
            Boolean resultado = service.guardar(dosis);
            if (resultado == true) {
                detalle.setMensaje("Se agrego la dosis: " + dosis.getCantidadDosis() + " " + dosis.getFrencuenciaDosis()); // tratamiento.getDescripcionTratamiento());
                detalle.setHttpCodeType("SUCCESSFUL");
                detalle.setHttpCodeNumber(201);
                detalle.setHttpCodeName("Agregado con exito");
                return new ResponseEntity(detalle, HttpStatus.OK);
            } else {
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("CONFLICTO");
                detalle.setMensaje("No se puede agregar la dosis. Ha ocurrido un error.");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);

            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_dosisUpd')")
    @PutMapping("actualizar/{idDosis}")
    public ResponseEntity actualizarDosis(@PathVariable("idDosis") UUID idDosis, @RequestBody @Valid DosisModel dosis) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/dosis/actualizar/" + idDosis);
        DosisModel dosisActualizar = service.obtenerById(idDosis);
        if (dosisActualizar == null) {
            detalle.setMensaje("No se encontro, por lo tanto no se puede actualizar dosis con id: " + idDosis);
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            String mensaje = service.checker(dosis.getFkMedicamento(),dosis.getFkTratamiento());
            if(!mensaje.equals("Se puede insertar registro")){
                detalle.setMensaje(mensaje);
                detalle.setHttpCodeName("No se pudo Agregar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            } else{
                TratamientoModel tratamiento = service2.obtenerTratamientoPorId(dosis.getFkTratamiento());
                MedicamentoModel medicamento = service3.obtenerById(dosis.getFkMedicamento());
                dosisActualizar.setTratamiento(tratamiento);
                dosisActualizar.setMedicamento(medicamento);
                dosisActualizar.setCantidadDosis(dosis.getCantidadDosis());
                dosisActualizar.setEstaActivo(dosis.getEstaActivo());
                dosisActualizar.setUnidadDosis(dosis.getUnidadDosis());
                dosisActualizar.setFrencuenciaDosis(dosis.getFrencuenciaDosis());
                Boolean resultado = service.guardar(dosisActualizar);
                if (resultado == true) {
                    detalle.setMensaje("Se agrego la dosis: " + dosisActualizar.getCantidadDosis() + " " + dosisActualizar.getFrencuenciaDosis()); // tratamiento.getDescripcionTratamiento());
                    detalle.setHttpCodeType("SUCCESSFUL");
                    detalle.setHttpCodeNumber(201);
                    detalle.setHttpCodeName("Agregado con exito");
                    return new ResponseEntity(detalle, HttpStatus.OK);
                } else {
                    detalle.setHttpCodeType("ERROR");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeName("CONFLICTO");
                    detalle.setMensaje("No se puede actualizar la dosis. Ha ocurrido un error.");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);

                }
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_dosisDel')")
    @DeleteMapping("eliminar/{idDosis}")
    public ResponseEntity eliminarDosis(@PathVariable("idDosis") UUID idDosis) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tratamientos/eliminar/" + idDosis);
        DosisModel dosis = service.obtenerById(idDosis);
        if (dosis == null) {
            detalle.setMensaje("No se encontro, por lo tanto no se puede eliminar la dosis con id: " + idDosis);
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            //Boolean estado = service.eliminar(idDosis);
            // test
            Boolean estado = service.toggle(idDosis,false);
            if (estado == true) {
                detalle.setMensaje("Se elimino con exito la dosis: " + dosis.getCantidadDosis() + " " + dosis.getFrencuenciaDosis());//tratamiento.getDescripcionTratamiento());
                detalle.setHttpCodeName("Eliminado con exito");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            } else {
                detalle.setMensaje("Error al eliminar dosis con id: " + idDosis);
                detalle.setHttpCodeName("Error al eliminar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }

    }
}