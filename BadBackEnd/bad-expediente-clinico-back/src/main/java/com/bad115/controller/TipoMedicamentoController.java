package com.bad115.controller;

import com.bad115.model.DetalleResponseHttp;
import com.bad115.model.TipoMedicamentoModel;
import com.bad115.service.TipoMedicamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/tipomedicamentos")
public class TipoMedicamentoController {
    @Autowired
    @Qualifier("TipoMedicamentoService")
    TipoMedicamentoService service;
    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_TipoMT')")
    @GetMapping("/todas")
    public ResponseEntity listartTodosTipoMedicamentos() {
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tipoMedicamentos/todas");
//        List<TipoMedicamentoModel> tipoMedicamentos = service.obtenerTodosTipoMedicamentos();
        List<TipoMedicamentoModel> tipoMedicamentos = service.lista_activos(); // obtenerTodosTipoMedicamentos();
        if (tipoMedicamentos.isEmpty()) {
            detalle.setMensaje("Aun no se registran tipos de medicamentos");
            detalle.setHttpCodeName("NO SE ENCONTRARON Tipos de medicamentos");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(tipoMedicamentos, HttpStatus.OK);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_TipoMId')")
    @GetMapping("/{idTipoMedicamento}")
    public ResponseEntity obtenerTipoMedicamentoId(@PathVariable("idTipoMedicamento") UUID idTipoMedicamento) {
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tipoMedicamentos/" + idTipoMedicamento);
        TipoMedicamentoModel tipoMedicamento = service.obtenerTipoMedicamentoId(idTipoMedicamento);
        if (tipoMedicamento == null) {
            detalle.setMensaje("No se encontro el tipo de medicamento con id: " + idTipoMedicamento);
            detalle.setHttpCodeName("Tipo de medicamento NO ENCONTRADO");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(tipoMedicamento, HttpStatus.OK);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_TipoMAdd')")
    @PostMapping("/agregar")
    public ResponseEntity crearTipoMedicamento(@RequestBody @Valid TipoMedicamentoModel tipoMedicamento) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tipoMedicamentos/agregar");
        UUID uuid = UUID.randomUUID();
        tipoMedicamento.setIdTipoMedicamento(uuid);
        System.out.println(uuid);
        Boolean estado = service.agregarTipoMedicamento(tipoMedicamento);
        if (estado == true) {
            detalle.setMensaje("Se agrego el tipo de medicamento: " + tipoMedicamento.getNombreTMedicamento() + " " + tipoMedicamento.getDescripcionTipoMedicamento());
            detalle.setHttpCodeType("SUCCESSFUL");
            detalle.setHttpCodeNumber(201);
            detalle.setHttpCodeName("Agregado con exito");
            return new ResponseEntity(detalle, HttpStatus.OK);
        } else {
            TipoMedicamentoModel tipoMedicamentoU = service.obtenerTipoMedicamentoDescripcion(tipoMedicamento.getNombreTMedicamento());
            if (tipoMedicamentoU == null) {
                detalle.setMensaje("No se pudo agregar el tipo de medicamento: " + tipoMedicamento.toString());
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("NO SE PUDO AGREGAR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            } else {
                detalle.setMensaje("Ya existe el nombre: " + tipoMedicamento.getNombreTMedicamento());
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("CONFLICTO EN DESCRIPCION");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);

            }

        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_TipoMUpd')")
    @PutMapping("actualizar/{idTipoMedicamento}")
    public ResponseEntity actualizarTipoMedicamento(@PathVariable("idTipoMedicamento") UUID idTipoMedicamento, @RequestBody @Valid TipoMedicamentoModel tipoMedicamento) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tipoMedicamentos/actualizar/" + idTipoMedicamento);
        TipoMedicamentoModel tipoMedicamentoA = service.obtenerTipoMedicamentoId(idTipoMedicamento);
        if (tipoMedicamentoA == null) {
            detalle.setMensaje("No se encontro, por lo tanto no se puede actualizar tipo de medicamento con id: " + idTipoMedicamento);
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            tipoMedicamentoA.setNombreTMedicamento(tipoMedicamento.getNombreTMedicamento());
            tipoMedicamentoA.setDescripcionTipoMedicamento(tipoMedicamento.getDescripcionTipoMedicamento());
            tipoMedicamentoA.setEstaActivo(tipoMedicamento.getEstaActivo());
            Boolean estado = service.actualizarTipoMedicamento(tipoMedicamentoA);
            if (estado == true) {
                //throw new OkMesaage("Se actualizo con exito la persona: "+persona.getNombrePersona()+" "+persona.getApellidoPersona());
                detalle.setMensaje("Se actualizo el tipo de medicamento: " + tipoMedicamento.getNombreTMedicamento() + " " + tipoMedicamento.getDescripcionTipoMedicamento());
                detalle.setHttpCodeName("Se actualizo con exito");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            } else {
                TipoMedicamentoModel tipoMedicamentoU = service.obtenerTipoMedicamentoDescripcion(tipoMedicamento.getNombreTMedicamento());
                if (tipoMedicamentoU == null) {
                    detalle.setMensaje("No se pudo actualizar la persona: " + tipoMedicamento.toString());
                    detalle.setHttpCodeType("ERROR");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeName("NO SE PUDO ACTUALIZAR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                } else {
                    detalle.setMensaje("Ya existe el nombre: " + tipoMedicamento.getNombreTMedicamento());
                    detalle.setHttpCodeType("ERROR");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeName("CONFLICTO EN Descripcion");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }
            }
        }

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_TipoMDel')")
    @DeleteMapping("eliminar/{idTipoMedicamento}")
    public ResponseEntity eliminarTipoMedicamentoId(@PathVariable("idTipoMedicamento") UUID idTipoMedicamento) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tipoMedicamentos/eliminar/" + idTipoMedicamento);
        TipoMedicamentoModel tipoMedicamento = service.obtenerTipoMedicamentoId(idTipoMedicamento);
        if (tipoMedicamento == null) {
            detalle.setMensaje("No se encontro, por lo tanto no se puede eliminar el tipo de medicamento con id: " + idTipoMedicamento);
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
//            Boolean estado = service.eliminarTipoMedicamento(idTipoMedicamento);
            Boolean estado = service.toggle(idTipoMedicamento, false);
            if (estado == true) {
                detalle.setMensaje("Se elimino con exito el tipo de medicamento: " + tipoMedicamento.getNombreTMedicamento() + " " + tipoMedicamento.getDescripcionTipoMedicamento());
                detalle.setHttpCodeName("Eliminado con exito");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            } else {
                detalle.setMensaje("Error al eliminar tipo de medicamento con id: " + idTipoMedicamento);
                detalle.setHttpCodeName("Error al eliminar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            } // hey
        }
    }


}




