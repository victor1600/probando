package com.bad115.controller;

import com.bad115.model.*;
import com.bad115.service.BoletaCirugiaService;
import com.bad115.service.CirugiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/boletaCirugia")
public class BoletaCirugiaController {

    @Autowired
    @Qualifier("BoletaCirugiaService")
    BoletaCirugiaService service;

    @Autowired
    @Qualifier("CirugiaService")
    CirugiaService service2;


    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_BoletacirugiaT')")
    @GetMapping("/todas")
    public ResponseEntity ListarBoletaCirugia(){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/boletaCirugia/todas");

        List<BoletaCirugiaModel> boleta = service.obtenerBoletasCirugias();
        if(boleta.isEmpty()){
            detalle.setMensaje("No hay boletas registrados.");
            detalle.setHttpCodeName("No se encontraron las boletas.");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity(boleta, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_BoletacirugiaId')")
    @GetMapping("/{idBoletaCirugia}")
    public ResponseEntity obtenerBoletaCirugiaId(@PathVariable("idBoletaCirugia") UUID idBoletaCirugia){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/boletaCirugia/"+idBoletaCirugia);
        BoletaCirugiaModel boleta = service.obtenerBoletaCirugiaId(idBoletaCirugia);
        if (boleta==null){
            detalle.setMensaje("No se encontro la boleta de cirugia con id: "+idBoletaCirugia);
            detalle.setHttpCodeName("Boleta de cirugia no encontrada.");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new  ResponseEntity(boleta,HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_BoletacirugiaAdd')")
    @PostMapping("/agregar")
    public ResponseEntity crearBoletaCirugia(@RequestBody @Valid BoletaCirugiaModel boleta) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/boletaCirugia/agregar");
        UUID uuid = UUID.randomUUID();
        boleta.setIdBoletaCirugia(uuid);
        System.out.println(("esta es la boleta"+boleta.toString()));
        Boolean estado = service.agregarBoletaCirugia(boleta);
        if (estado) {
            detalle.setMensaje("Se agrego la boleta con la fecha :" + boleta.getFechaBoletaCirugia());
            detalle.setHttpCodeType("SUCCESSFUL");
            detalle.setHttpCodeNumber(201);
            detalle.setHttpCodeName("Agregado con exito");
            return new ResponseEntity(detalle, HttpStatus.CREATED);
        } else {
            BoletaCirugiaModel boletaValidar = service.obtenerBoletaCirugiaFecha(boleta.getFechaBoletaCirugia());
            if (boletaValidar == null) {
                detalle.setMensaje("No se pudo agregar la cirugia: " + boleta.toString());
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("NO SE PUDO AGREGAR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            } else {
                detalle.setMensaje("Ya existe la cirugia: " + boleta.getFechaBoletaCirugia());
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("CONFLICTO EN NOMBRE");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }

        @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_BoletacirugiaUpd')")
    @PutMapping("actualizar/{idBoletaCirugia}")
    public ResponseEntity actualizarBoletaCirugia(@PathVariable("idBoletaCirugia") UUID idBoletaCirugia, @RequestBody @Valid BoletaCirugiaModel boleta){
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/boletaCirugia/actualizar/"+idBoletaCirugia);
        BoletaCirugiaModel boletaUpdate = service.obtenerBoletaCirugiaId(idBoletaCirugia);
        if (boletaUpdate==null){
            detalle.setMensaje("No se encontro, no puede ser Actualizado la boleta: "+boleta.getFechaBoletaCirugia());
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            boletaUpdate.setFechaBoletaCirugia(boleta.getFechaBoletaCirugia());

            Boolean estado = service.actualizarBoletaCirugia(boletaUpdate);
            if (estado){
                detalle.setMensaje("Se actualizo la boleta: "+boletaUpdate.getFechaBoletaCirugia());
                detalle.setHttpCodeName("Se actualizo con exito");
                detalle.setHttpCodeNumber(202);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.ACCEPTED);
            }else {

                    detalle.setMensaje("No se pudo actualizar la boleta: "+ boleta.toString());
                    detalle.setHttpCodeType("ERROR");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeName("NO SE PUDO ACTUALIZAR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                  }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_BoletacirugiaDel')")
    @DeleteMapping("/eliminar/{idBoletaCirugia}")
    public ResponseEntity eliminarBoletaCirugiaId(@PathVariable("idBoletaCirugia") UUID idBoletaCirugia){

        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/boletaCirugia/eliminar/"+idBoletaCirugia);

        BoletaCirugiaModel boleta = service.obtenerBoletaCirugiaId(idBoletaCirugia);
        if (boleta==null){
            detalle.setMensaje("No se puede eliminar, la boleta no existe");
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            Boolean estado = service.eliminarBoletaCirugia(idBoletaCirugia);
            if (estado){
                detalle.setMensaje("Se elimino con exito la boleta: "+boleta.getFechaBoletaCirugia());
                detalle.setHttpCodeName("Eliminado con exito");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            }else {
                detalle.setMensaje("Error al eliminar la boleta: "+boleta.getFechaBoletaCirugia());
                detalle.setHttpCodeName("Error al eliminar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }

    }



}
