package com.bad115.controller;

import com.bad115.model.*;
import com.bad115.service.AsignacionHorarioService;
import com.bad115.service.MedicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/asignacionhora")
public class AsignacionHorarioController {

    @Autowired
    @Qualifier("AsignacionHorarioService")
    AsignacionHorarioService service;

    @Autowired
    @Qualifier("MedicoService")
    MedicoService service2;

    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_AsgHorT')")
    @GetMapping("/todas")
    public ResponseEntity listartTodosAsignacionHor() {
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/asignacionesHors");
        List<AsignacionHorarioModel> AsignaHor = service.lista_activos();
        if (AsignaHor.isEmpty()) {
            detalle.setMensaje("Aun no se registran Asignacion de horario para Medicos");
            detalle.setHttpCodeName("no se encontraron Asignacion de Horarios");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(AsignaHor, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_AsgHorId')")
    @GetMapping("/{idAsHr}")
    public ResponseEntity obtenerAsignacionHrId(@PathVariable("idAsHr") UUID idAsHr){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/asignacionhora/"+ idAsHr);

        AsignacionHorarioModel gasto = service.obtenerIdAsignacionHorario(idAsHr);
        if (gasto==null){
            detalle.setMensaje("No se encontro el Asignacion de horario con id: "+ idAsHr);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else{
            return new ResponseEntity(gasto, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/agregar")
    public ResponseEntity crearAsignacion(@RequestBody @Valid AsignacionHorarioModel asignacion) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/asignacion/agregar");
        UUID uuid = UUID.randomUUID();
        asignacion.setIdAsignacionHorario(uuid);
        // prueba
        MedicoModel asignacion2 = service2.obtenerMedicoId(asignacion.getFkMedico());
        asignacion.setMedico(asignacion2);
        Boolean estado = service.agregarAsigHor(asignacion);
        if (estado == true) {
            detalle.setMensaje("se agrego la asignacion" + asignacion.getFechaInicio()
                    + " " + asignacion.getHoraInicio()
                    + " " + asignacion.getTurno());
            detalle.setHttpCodeName("Agregada con exito");
            detalle.setHttpCodeNumber(201);
            detalle.setHttpCodeType("SUCCESSFULL");
            return new ResponseEntity(detalle, HttpStatus.CREATED);
        } else {
            AsignacionHorarioModel asignacionU = service.obtenerIdAsignacionHorario(asignacion.getIdAsignacionHorario());
            if (asignacionU == null) {
                detalle.setMensaje("NO SE AGREGO el asignacion" + asignacion.toString());
                detalle.setHttpCodeName("No se pudo Agregar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            } else {
                detalle.setMensaje("La asignacion ya existe con id" + asignacion.getIdAsignacionHorario());
                detalle.setHttpCodeName("Conflicto en ID");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }
//
//
    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('AsgHorUpd')")
    @PutMapping("actualizar/{idAsHr}")
    public ResponseEntity actualizarAsignacion(@PathVariable("idAsHr") UUID idAsHr, @RequestBody @Valid AsignacionHorarioModel asignacion) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/actualizar/"+idAsHr);
        AsignacionHorarioModel asignacionA = service.obtenerIdAsignacionHorario(idAsHr);
        if(asignacionA==null){
            detalle.setMensaje("NO SE encontro asignacion con id: "+ idAsHr);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else{
            asignacionA.setFechaInicio(asignacion.getFechaFin());
            asignacionA.setFechaFin(asignacion.getFechaFin());
            asignacionA.setHoraInicio(asignacion.getHoraInicio());
            asignacionA.setHoraFin(asignacion.getHoraFin());
            asignacionA.setDuracionConsulta(asignacion.getDuracionConsulta());
            asignacionA.setFkMedico(asignacion.getFkMedico());
            asignacionA.setTurno(asignacion.getTurno());
            Boolean estado = service.actualizarAsigHor(asignacionA);
            if(estado==true){
                detalle.setMensaje("se actualizo la asignacion" + asignacion.getFechaInicio() +"\n "+ asignacion.getFechaFin()+"\n "+ asignacion.getHoraInicio()+"\n "+ asignacion.getHoraFin()+"\n "+ asignacion.getTurno()+"\n "+ asignacion.getMedico());
                detalle.setHttpCodeName("Actualizada con exito");
                detalle.setHttpCodeNumber(202);
                detalle.setHttpCodeType("SUCCESSFULL");
                return new ResponseEntity(detalle, HttpStatus.ACCEPTED);
            }else {
                AsignacionHorarioModel asignacionU = service.obtenerIdAsignacionHorario(asignacion.getIdAsignacionHorario());
                if (asignacionU==null){
                    detalle.setMensaje("NO SE Actualizo El asignacion "+ asignacion.toString());
                    detalle.setHttpCodeName("No se pudo asignacion");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeType("ERROR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }else{
                    detalle.setMensaje("La asignacion ya existe con id "+ asignacion.getIdAsignacionHorario());
                    detalle.setHttpCodeName("Conflicto en el nombre");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeType("ERROR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_AsgHorDel')")
    @DeleteMapping("eliminar/{idAsHr}")
    public ResponseEntity eliminarGastoId(@PathVariable("idAsHr") UUID idAsHr) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/asignacionhora/eliminar/" + idAsHr);
        AsignacionHorarioModel servicio = service.obtenerIdAsignacionHorario(idAsHr);
        if (servicio == null) {
            detalle.setMensaje("No se encontro la asignacion horario, por lo tanto no se puede eliminar con id: " + idAsHr);
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            Boolean estado = service.toggle(idAsHr, false);// eliminarservicio(idAsHr);
            if (estado == true) {
                detalle.setMensaje("Se elimino con exito el servicio " + servicio.getFechaInicio());
                detalle.setHttpCodeName("Eliminado con exito");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            } else {
                detalle.setMensaje("Error al eliminar servicio con id: " + idAsHr);
                detalle.setHttpCodeName("Error al eliminar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }


}
