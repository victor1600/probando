package com.bad115.controller;

import com.bad115.model.DetalleResponseHttp;
import com.bad115.model.TipoPersonalMedicoModel;
import com.bad115.service.TipoPersonalMedicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/tipoPersonalMedico")
public class TipoPersonalMedicoController {

    @Autowired
    @Qualifier("TipoPersonalMedicoService")
    TipoPersonalMedicoService service;

    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_TipoPersonalAll')")
    @GetMapping("/todos")
    public ResponseEntity ListarTiposPersonalMedico(){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tipoPersonalMedico/todos");

        List<TipoPersonalMedicoModel> tipoPersonal = service.obtenerTodosTipoPersonalMedico();
        if(tipoPersonal.isEmpty()){
            detalle.setMensaje("No hay tipos de personal registrados.");
            detalle.setHttpCodeName("No se encontraron tipos de personal.");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity(tipoPersonal, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_TipoPersonalId')")
    @GetMapping("/{idTipoPersonal}")
    public ResponseEntity obtenerTiposPersonalId(@PathVariable("idTipoPersonal") UUID idTipoPersonal){
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tipoPersonalMedico/"+idTipoPersonal);
        TipoPersonalMedicoModel tipoPersonal = service.obtenerTipoPersonalMedicoId(idTipoPersonal);
        if (tipoPersonal==null){
            detalle.setMensaje("No se encontro el paciente con id: "+idTipoPersonal);
            detalle.setHttpCodeName("Paciente no encontrado.");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new  ResponseEntity(tipoPersonal,HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_TipoPersonalAdd')")
    @PostMapping("/agregar")
    public ResponseEntity crearTipoPersonal(@RequestBody @Valid TipoPersonalMedicoModel tipoPersonal){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tipoPersonalMedico/agregar");

        Boolean estado=service.agregarTipoPersonalMedico(tipoPersonal);
        if (estado){
            detalle.setMensaje("Se agrego el tipo de personal: "+tipoPersonal.getNombreTipoPersonal());
            detalle.setHttpCodeType("SUCCESSFUL");
            detalle.setHttpCodeNumber(201);
            detalle.setHttpCodeName("Agregado con exito");
            return new ResponseEntity(detalle, HttpStatus.CREATED);
        }else {
            TipoPersonalMedicoModel tipoPersonalValidar = service.obtenerTipoPersonalMedicoNombre(tipoPersonal.getNombreTipoPersonal());
            if (tipoPersonalValidar==null){
                detalle.setMensaje("Fallo al agregar tipo de personal: " + tipoPersonal.getNombreTipoPersonal());
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("Fallo al agregar.");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }else{
                detalle.setMensaje("Ya existe el nombre: "+tipoPersonal.getNombreTipoPersonal());
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("Conflicto en el nombre.");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_TipoPersonalUpd')")
    @PutMapping("actualizar/{idTipoPersonal}")
    public ResponseEntity actualizarTipoPersonal(@PathVariable("idTipoPersonal") UUID idTipoPersonal, @RequestBody @Valid TipoPersonalMedicoModel tipoPersonal){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tipoPersonalMedico/actualizar/"+idTipoPersonal);

        TipoPersonalMedicoModel tipoPersonalUpdate = service.obtenerTipoPersonalMedicoId(idTipoPersonal);
        if (tipoPersonalUpdate==null){
            detalle.setMensaje("Fallo al actualizar, tipo de personal no encontrad. "+tipoPersonal.getNombreTipoPersonal());
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            tipoPersonalUpdate.setNombreTipoPersonal(tipoPersonal.getNombreTipoPersonal());
            Boolean estado = service.actualizarTipoPersonalMedico(tipoPersonalUpdate);
            if (estado){
                detalle.setMensaje("Se actualizo el tipo personal: "+tipoPersonalUpdate.getNombreTipoPersonal());
                detalle.setHttpCodeName("Se actualizo con exito");
                detalle.setHttpCodeNumber(202);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.ACCEPTED);
            }else {
                TipoPersonalMedicoModel tipoPersonalValidar = service.obtenerTipoPersonalMedicoNombre(tipoPersonal.getNombreTipoPersonal());
                if (tipoPersonalValidar==null){
                    detalle.setMensaje("Fallo al actualizar el tipo personal: "+ tipoPersonal.getNombreTipoPersonal());
                    detalle.setHttpCodeType("ERROR");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeName("Fallo al actualizar Tipo de Personal.");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }else {
                    detalle.setMensaje("Ya existe el nombre: "+tipoPersonalUpdate.getNombreTipoPersonal());
                    detalle.setHttpCodeType("ERROR");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeName("Conflicto en el nombre.");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_TipoPersonalDel')")
    @DeleteMapping("/eliminar/{idTipoPersonal}")
    public ResponseEntity eliminarTipoPersonalId(@PathVariable("idTipoPersonal") UUID idTipoPersonal){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tipoPersonalMedico/eliminar/"+idTipoPersonal);

        TipoPersonalMedicoModel tipoPersonal = service.obtenerTipoPersonalMedicoId(idTipoPersonal);
        if (tipoPersonal==null){
            detalle.setMensaje("Fallo al eliminar, Tipo Personal no encontrado.");
            detalle.setHttpCodeName("No encontrado.");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            Boolean estado = service.eliminarTipoPersonalMedico(idTipoPersonal);
            if (estado){
                detalle.setMensaje("Se elimino con exito el Tipo Personal: "+tipoPersonal.getNombreTipoPersonal());
                detalle.setHttpCodeName("Eliminado con exito.");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            }else {
                detalle.setMensaje("Fallo al eliminar tipo de personal : "+tipoPersonal.getNombreTipoPersonal());
                detalle.setHttpCodeName("Fallo al eliminar.");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }
}
