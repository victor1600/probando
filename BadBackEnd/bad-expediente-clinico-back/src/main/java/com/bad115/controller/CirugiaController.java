package com.bad115.controller;

import com.bad115.model.DetalleResponseHttp;
import com.bad115.model.CirugiaModel;
import com.bad115.service.CirugiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/cirugia")
public class CirugiaController {

    @Autowired
    @Qualifier("CirugiaService")
    CirugiaService service;
    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_cirugiaT')")
    @GetMapping("/todas")
    public ResponseEntity listarTodasCirugias() {
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/cirugia/todas");
        List<CirugiaModel> cirugias = service.obtenerTodasCirugias();
        if (cirugias.isEmpty()) {
            detalle.setMensaje("Aun no se registran cirugias");
            detalle.setHttpCodeName("NO SE ENCONTRARON cirugias");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(cirugias, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_cirugiaId')")
    @GetMapping("/{idCirugia}")
    public ResponseEntity obtenerCirugiaId(@PathVariable("idCirugia") UUID idCirugia){
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/cirugia/"+idCirugia);
        CirugiaModel cirugia = service.obtenerCirugiaId(idCirugia);
        if (cirugia==null){
            detalle.setMensaje("No se encontro la cirugia con id: "+idCirugia);
            detalle.setHttpCodeName("cirugia no encontrada.");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new  ResponseEntity(cirugia,HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_cirugiaAdd')")
    @PostMapping("/agregar")
    public ResponseEntity crearCirugia(@RequestBody @Valid CirugiaModel cirugia){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/cirugia/agregar");
        UUID uuid = UUID.randomUUID();
        cirugia.setIdCirugia(uuid);

        Boolean estado=service.agregarCirugia(cirugia);
        if (estado){
            detalle.setMensaje("Se agrego la cirugia :"+cirugia.getNombreCirugia());
            detalle.setHttpCodeType("SUCCESSFUL");
            detalle.setHttpCodeNumber(201);
            detalle.setHttpCodeName("Agregado con exito");
            return new ResponseEntity(detalle, HttpStatus.CREATED);
        }else {
            CirugiaModel cirugiaValidar = service.obtenerCirugiaNombre(cirugia.getNombreCirugia());
            if (cirugiaValidar==null){
                detalle.setMensaje("No se pudo agregar la cirugia: " + cirugia.toString());
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("NO SE PUDO AGREGAR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }else{
                detalle.setMensaje("Ya existe la cirugia: "+cirugia.getNombreCirugia());
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("CONFLICTO EN NOMBRE");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_cirugiaUpd')")
    @PutMapping("actualizar/{idCirugia}")
    public ResponseEntity actualizarCirugia(@PathVariable("idCirugia") UUID idCirugia, @RequestBody @Valid CirugiaModel cirugia){
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/cirugia/actualizar/"+idCirugia);
        CirugiaModel cirugiaUpdate = service.obtenerCirugiaId(idCirugia);
        if (cirugiaUpdate==null){
            detalle.setMensaje("No se encontro, no puede ser Actualizada la cirugia: "+cirugia.getNombreCirugia());
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            cirugiaUpdate.setNombreCirugia(cirugia.getNombreCirugia());
            cirugiaUpdate.setClasificacionCirugia(cirugia.getClasificacionCirugia());
            cirugiaUpdate.setDescripcionCirugia(cirugia.getDescripcionCirugia());
            Boolean estado = service.actualizarCirugia(cirugiaUpdate);
            if (estado){
                detalle.setMensaje("Se actualizo la cirugia: "+cirugiaUpdate.getNombreCirugia());
                detalle.setHttpCodeName("Se actualizo con exito");
                detalle.setHttpCodeNumber(202);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.ACCEPTED);
            }else {
                CirugiaModel cirugiaValidar = service.obtenerCirugiaNombre(cirugia.getNombreCirugia());
                if (cirugiaValidar==null){
                    detalle.setMensaje("No se pudo actualizar la cirugia: "+ cirugia.toString());
                    detalle.setHttpCodeType("ERROR");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeName("NO SE PUDO ACTUALIZAR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }else {
                    detalle.setMensaje("Ya existe el nombre: "+cirugiaUpdate.getNombreCirugia());
                    detalle.setHttpCodeType("ERROR");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeName("CONFLICTO EN NOMBRE");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_cirugiaDel')")
    @DeleteMapping("/eliminar/{idCirugia}")
    public ResponseEntity eliminarCirugiaId(@PathVariable("idCirugia") UUID idCirugia){

        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/cirugia/eliminar/"+idCirugia);

        CirugiaModel cirugia = service.obtenerCirugiaId(idCirugia);
        if (cirugia==null){
            detalle.setMensaje("No se puede eliminar, cirugia no existe");
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            Boolean estado = service.eliminarCirugia(idCirugia);
            if (estado){
                detalle.setMensaje("Se elimino con exito la cirugia: "+cirugia.getNombreCirugia());
                detalle.setHttpCodeName("Eliminado con exito");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            }else {
                detalle.setMensaje("Error al eliminar la cirugia: "+cirugia.getNombreCirugia());
                detalle.setHttpCodeName("Error al eliminar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }

    }

}