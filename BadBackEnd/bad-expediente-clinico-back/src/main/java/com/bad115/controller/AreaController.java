package com.bad115.controller;

import com.bad115.model.CamaModel;
import com.bad115.model.DetalleResponseHttp;
import com.bad115.model.AreaModel;
import com.bad115.service.AreaService;
import com.bad115.service.CamaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/area")
public class AreaController {

    @Autowired
    @Qualifier("AreaService")
    AreaService service;

    @Autowired
    @Qualifier("CamaService")
    CamaService service2;

    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_areaT')")
    @GetMapping("/todos")
    public ResponseEntity ListarAreas(){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/area/todos");

        List<AreaModel> area = service.obtenerTodosLasAreas();
        if(area.isEmpty()){
            detalle.setMensaje("No hay areas registradas.");
            detalle.setHttpCodeName("No se encontraron las areas.");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity(area, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_AreaId')")
    @GetMapping("/{idArea}")
    public ResponseEntity obtenerAreaId(@PathVariable("idArea") UUID idArea){
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/area/"+idArea);
        AreaModel area = service.obtenerAreaId(idArea);
        if (area==null){
            detalle.setMensaje("No se encontro el area con id: "+idArea);
            detalle.setHttpCodeName("area no encontrado.");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new  ResponseEntity(area,HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_AreaAdd')")
    @PostMapping("/agregar")
    public ResponseEntity crearArea(@RequestBody @Valid AreaModel area){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/area/agregar");
        UUID uuid = UUID.randomUUID();
        area.setIdArea(uuid);

        Boolean estado=service.agregarArea(area);
        if (estado){
            detalle.setMensaje("Se agrego el area con codigo :"+area.getCodigoArea() +"Nombre" +area.getNombreArea() +", capacidad: "+ area.getCantidadCama()
                    +",cantidadDisponible "+ area.getCantDisponible());
            detalle.setHttpCodeType("SUCCESSFUL");
            detalle.setHttpCodeNumber(201);
            detalle.setHttpCodeName("Agregado con exito");
            return new ResponseEntity(detalle, HttpStatus.CREATED);
        }else {
            AreaModel areaValidar = service.obtenerAreaNombre((area.getNombreArea()));
            if (areaValidar==null){
                detalle.setMensaje("No se pudo agregar el area: " + area.toString());
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("NO SE PUDO AGREGAR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }else{
                detalle.setMensaje("Ya existe el nombre: "+area.getNombreArea());
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("CONFLICTO EN NOMBRE");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_AreaUpd')")
    @PutMapping("actualizar/{idArea}")
    public ResponseEntity actualizarArea(@PathVariable("idArea") UUID idArea, @RequestBody @Valid AreaModel area){
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/area/actualizar/"+idArea);
        AreaModel areaUpdate = service.obtenerAreaId(idArea);
        if (areaUpdate==null){
            detalle.setMensaje("No se encontro, no puede ser Actualizado el area: "+area.getNombreArea());
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            areaUpdate.setCodigoArea(area.getCodigoArea());
            areaUpdate.setNombreArea(area.getNombreArea());
            areaUpdate.setCantidadCama(area.getCantidadCama());
            areaUpdate.setCantDisponible(area.getCantDisponible());
            Boolean estado = service.actualizarArea(areaUpdate);
            if (estado){
                detalle.setMensaje("Se actualizo el area con codigo: "  +areaUpdate.getCodigoArea() +"\n "   +areaUpdate.getNombreArea()+"\n "+ areaUpdate.getCantidadCama()+"\n "+ areaUpdate.getCantDisponible());
                detalle.setHttpCodeName("Se actualizo con exito");
                detalle.setHttpCodeNumber(202);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.ACCEPTED);
            }else {
                AreaModel areaValidar = service.obtenerAreaNombre(area.getNombreArea());
                if (areaValidar==null){
                    detalle.setMensaje("No se pudo actualizar el area: "+ area.toString());
                    detalle.setHttpCodeType("ERROR");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeName("NO SE PUDO ACTUALIZAR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }else {
                    detalle.setMensaje("Ya existe el nombre: "+areaUpdate.getNombreArea());
                    detalle.setHttpCodeType("ERROR");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeName("CONFLICTO EN NOMBRE");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_AreaDel')")
    @DeleteMapping("/eliminar/{idArea}")
    public ResponseEntity eliminarAreaId(@PathVariable("idArea") UUID idArea){

        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/area/eliminar/"+idArea);

        AreaModel area = service.obtenerAreaId(idArea);
        if (area==null){
            detalle.setMensaje("No se puede eliminar, el area no existe");
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            Boolean estado = service.eliminarArea(idArea);
            if (estado){
                detalle.setMensaje("Se elimino con exito el area: "+area.getNombreArea());
                detalle.setHttpCodeName("Eliminado con exito");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            }else {
                detalle.setMensaje("Error al eliminar area: "+area.getNombreArea());
                detalle.setHttpCodeName("Error al eliminar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }

    }

    // Obtener la lista de camas


    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_camaT')")
    @GetMapping("/todas")
    public ResponseEntity ListarCamas(){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/cama/todas");

        List<CamaModel> cama = service2.obtenerCamas();
        if(cama.isEmpty()){
            detalle.setMensaje("No hay areas registradas.");
            detalle.setHttpCodeName("No se encontraron las camas.");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity(cama, HttpStatus.FOUND);
        }
    }








}