package com.bad115.controller;

import com.bad115.model.*;
import com.bad115.service.AsignacionHorarioService;
import com.bad115.service.CitaService;
import com.bad115.service.HorarioService;
import com.bad115.service.MedicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/horario")
public class HorarioController {

    @Autowired
    @Qualifier("HorarioService")
    HorarioService service;

    @Autowired
    @Qualifier("AsignacionHorarioService")
    AsignacionHorarioService service1;

    @Autowired
    @Qualifier("CitaService")
    CitaService service2;

    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_HorarioT')")
    @GetMapping("/todas")
    public ResponseEntity listartTodosHorarios() {
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/horarios");
        List<HorarioModel> horario = service.lista_activos();
        if (horario.isEmpty()) {
            detalle.setMensaje("Aun no se registran horario ");
            detalle.setHttpCodeName("no se encontraron  Horarios");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(horario, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_HorarioId')")
    @GetMapping("/{idHorario}")
    public ResponseEntity obtenerHorarioId(@PathVariable("idHorario") UUID idHorario){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/horario/"+ idHorario);

        HorarioModel gasto = service.obtenerIdHorario(idHorario);
        if (gasto==null){
            detalle.setMensaje("No se encontro el horario con id: "+ idHorario);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else{
            return new ResponseEntity(gasto, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/agregar")
    public ResponseEntity crearHorario(@RequestBody @Valid HorarioModel horario) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/horario/agregar");
        UUID uuid = UUID.randomUUID();
        horario.setIdHorario(uuid);
        // prueba
        AsignacionHorarioModel horario2 = service1.obtenerIdAsignacionHorario(horario.getFkAsignacionHorario());
        horario.setAsignacionHorario(horario2);
        CitaModel cita = service2.obtenerPorId(horario.getFkCita());
        horario.setCita(cita);
        Boolean estado = service.agregarHorario(horario);
        if (estado == true) {
            detalle.setMensaje("se agrego la horario" + horario.getIdHorario()
                    + " " + horario.getCita()
                    + " " + horario.getAsignacionHorario());
            detalle.setHttpCodeName("Agregada con exito");
            detalle.setHttpCodeNumber(201);
            detalle.setHttpCodeType("SUCCESSFULL");
            return new ResponseEntity(detalle, HttpStatus.CREATED);
        } else {
            HorarioModel horarioU = service.obtenerIdHorario(horario.getIdHorario());
            if (horarioU == null) {
                detalle.setMensaje("NO SE AGREGO el horario" + horario.toString());
                detalle.setHttpCodeName("No se pudo Agregar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            } else {
                detalle.setMensaje("La horario ya existe con id" + horario.getIdHorario());
                detalle.setHttpCodeName("Conflicto en ID");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('HorarioUpd')")
    @PutMapping("actualizar/{idHorario}")
    public ResponseEntity actualizarHorario(@PathVariable("idHorario") UUID idHorario, @RequestBody @Valid HorarioModel asignacion) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/actualizar/"+idHorario);
        HorarioModel asignacionA = service.obtenerIdHorario(idHorario);
        if(asignacionA==null){
            detalle.setMensaje("NO SE encontro horario con id: "+ idHorario);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else{
            asignacionA.setFkCita(asignacion.getFkCita());
            asignacionA.setFkAsignacionHorario(asignacion.getFkAsignacionHorario());
            asignacionA.setEstadoHorario(asignacion.getEstadoHorario());
            asignacionA.setEstaActivo(asignacion.getEstaActivo());
            Boolean estado = service.actualizarHorario(asignacionA);
            if(estado==true){
                detalle.setMensaje("se actualizo la asignacion" + asignacion.getIdHorario());
                detalle.setHttpCodeName("Actualizada con exito");
                detalle.setHttpCodeNumber(202);
                detalle.setHttpCodeType("SUCCESSFULL");
                return new ResponseEntity(detalle, HttpStatus.ACCEPTED);
            }else {
                HorarioModel asignacionU = service.obtenerIdHorario(asignacion.getIdHorario());
                if (asignacionU==null){
                    detalle.setMensaje("NO SE Actualizo El asignacion "+ asignacion.toString());
                    detalle.setHttpCodeName("No se pudo asignacion");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeType("ERROR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }else{
                    detalle.setMensaje("el horario ya existe con id "+ asignacion.getIdHorario());
                    detalle.setHttpCodeName("Conflicto en el nombre");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeType("ERROR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_HorarioDel')")
    @DeleteMapping("eliminar/{idHorario}")
    public ResponseEntity eliminarHorarioId(@PathVariable("idHorario") UUID idHorario) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/horario/eliminar/" + idHorario);
        HorarioModel servicio = service.obtenerIdHorario(idHorario);
        if (servicio == null) {
            detalle.setMensaje("No se encontro  horario, por lo tanto no se puede eliminar con id: " + idHorario);
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            Boolean estado = service.toggle(idHorario, false);// eliminarservicio(idHorario);
            if (estado == true) {
                detalle.setMensaje("Se elimino con exito el horario " + servicio.getIdHorario());
                detalle.setHttpCodeName("Eliminado con exito");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            } else {
                detalle.setMensaje("Error al eliminar hoario con id: " + idHorario);
                detalle.setHttpCodeName("Error al eliminar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }



}
