package com.bad115.controller;

import com.bad115.model.*;
import com.bad115.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/citas")
public class CitaController {
    @Autowired
    @Qualifier("CitaService")
    CitaService service;

    @Autowired
    @Qualifier("TipoCitaService")
    TipoCitaService service2;

    @Autowired
    @Qualifier("ConsultaService")
    ConsultaService service3;

    @Autowired
    @Qualifier("MedicoService")
    MedicoService service4;

    @Autowired
    @Qualifier("PacienteService")
    PacienteService service5;

    @Autowired
    @Qualifier("PersonalMedicoService")
    PersonalMedicoService servicePer;

    @Autowired
    @Qualifier("AsignacionHorarioService")
    AsignacionHorarioService asignacionService;

    @Autowired
    @Qualifier("MedicoService")
    MedicoService serviceMed;


    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    // TODO: Roles para citas
    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_citaT')")
    @GetMapping("/todas")
    public ResponseEntity listarDosis() {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/citas/todas");
        List<CitaModel> citas = service.lista_activos();
        if (citas.isEmpty()) {
            detalle.setMensaje("Aun no se registran citas");
            detalle.setHttpCodeName("NO SE ENCONTRARON citas");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(citas, HttpStatus.OK);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_citaId')")
    @GetMapping("/{idCita}")
    public ResponseEntity obtenerDosisPorId(@PathVariable("idCita") UUID idCita) {
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/citas/" + idCita);
        CitaModel cita = service.obtenerPorId(idCita);// obtenerById(idDosis);
        if (cita == null) {
            detalle.setMensaje("No se encontro la cita con id: " + idCita);
            detalle.setHttpCodeName("Cita NO ENCONTRADA");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(cita, HttpStatus.OK);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_citaAdd')")
    @PostMapping("/buscar")
    public ResponseEntity buscarDoctores(@RequestBody @Valid CitaModel cita) {
//        System.out.println(cita);
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/Medico/todos");
        Date diaCorregido = cita.getFechaCita();
        Calendar c = Calendar.getInstance();
        c.setTime(diaCorregido);
        c.add(Calendar.DATE, 1);
        diaCorregido = c.getTime();
        cita.setFechaCita(diaCorregido);
        List<MedicoModel> medicosProcedimiento = serviceMed.doctoresCita(cita.getFechaCita().toString(),
                cita.getHoraCita(), cita.getFkMedicoId());

        List<UUID> listaIdsDoctores = medicosProcedimiento.stream()
                .map(medico -> medico.getIdMedico())
                .collect(Collectors.toList());

        List<PersonalMedicoModel> Medicos = servicePer.obtenerMedicos();


        List<PersonalMedicoModel> listaFiltrada = Medicos.stream()
                .filter(medico -> listaIdsDoctores.contains(medico.getMedico().getIdMedico()))
                .collect(Collectors.toList());

        if (Medicos.isEmpty()) {
            detalle.setMensaje("No hay medicos registrados.");
            detalle.setHttpCodeName("No se encontraron medicos.");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(listaFiltrada, HttpStatus.FOUND);
        }

    }


    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_citaAdd')")
    @PostMapping("/agregar")
    public ResponseEntity agregarCita(@RequestBody @Valid CitaModel cita) {
        System.out.println("aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
        System.out.println(cita.getFechaCita());
        Date diaCorregido = cita.getFechaCita();
        Calendar c = Calendar.getInstance();
        c.setTime(diaCorregido);
        c.add(Calendar.DATE, 1);
        diaCorregido = c.getTime();
        cita.setFechaCita(diaCorregido);
        System.out.println(diaCorregido);
        System.out.println("aquiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");

        UUID idCita = UUID.randomUUID();
        cita.setIdCita(idCita);

        DetalleResponseHttp detalle = new DetalleResponseHttp();
        Date date = new Date();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/citas/agregar/" + idCita);
        String mensaje = " aas";
        TipoCitaModel tipoCita = service2.obtenerPorId(cita.getFkTipoCita());
        cita.setTipoCita(tipoCita);
        MedicoModel medico = service4.findByIdMedico(cita.getFkMedicoId());
        System.out.println(cita.getFkMedicoId());
        System.out.println(medico);
        cita.setMedico(medico);
        PacienteModel paciente = service5.findByIdPaciente(cita.getFkPacienteId());
        cita.setPaciente(paciente);
        Boolean resultado = service.agregar(cita); // guardar(cita);
        if (resultado == true) {
            detalle.setMensaje("Se agreago la cita para: "  + cita.getPaciente().getNombre1Persona() + " " + cita.getPaciente().getApellido1Persona()); // cita.getFechaCita() + " y con hora: " + cita.getHoraCita());
            detalle.setHttpCodeType("SUCCESSFUL");
            detalle.setHttpCodeNumber(201);
            detalle.setHttpCodeName("Agregado con exito");
            return new ResponseEntity(detalle, HttpStatus.OK);
        } else {
            detalle.setHttpCodeType("ERROR");
            detalle.setHttpCodeNumber(409);
            detalle.setHttpCodeName("CONFLICTO");
            detalle.setMensaje("No se puede agregar la cita. Ha ocurrido un error.");
            return new ResponseEntity(detalle, HttpStatus.CONFLICT);

        }
    }

//        }


    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_citaUpd')")
    @PutMapping("actualizar/{idCita}")
    public ResponseEntity actualizarCita(@PathVariable("idCita") UUID idCita, @RequestBody @Valid CitaModel cita) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/citas/actualizar/" + idCita);
        CitaModel citaActualizar = service.obtenerPorId(idCita); // obtenerById(idCita);
        if (citaActualizar == null) {
            detalle.setMensaje("No se encontro, por lo tanto no se puede actualizar cita con id: " + idCita);
            detalle.setHttpCodeName("No encontrada");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            //TODO CALL SP for actualizar dosis
//            String mensaje = " aas"; //service.checker(idCita);
//            if (!mensaje.equals("Se puede insertar registro")) {
//                detalle.setMensaje(mensaje);
//                detalle.setHttpCodeName("No se pudo Agregar");
//                detalle.setHttpCodeNumber(409);
//                detalle.setHttpCodeType("ERROR");
//                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
//            }
            TipoCitaModel tipoCita = service2.obtenerPorId(cita.getFkTipoCita()); // obtenerTratamientoPorId(cita.getFkTratamiento());
            MedicoModel medico = service4.findByIdMedico(cita.getFkMedicoId());
            PacienteModel paciente = service5.findByIdPaciente(cita.getFkPacienteId());
//            MedicamentoModel medicamento = service3.obtenerById(cita.getFkMedicamento());
            citaActualizar.setPaciente(paciente);
            citaActualizar.setMedico(medico);
            citaActualizar.setTipoCita(tipoCita);
            citaActualizar.setEstaActivo(cita.getEstaActivo());
            citaActualizar.setFechaCita(cita.getFechaCita());
            citaActualizar.setHoraCita(cita.getHoraCita());
            Boolean resultado = service.agregar(citaActualizar); // guardar(citaActualizar);
            if (resultado == true) {
                detalle.setMensaje("Se actualizo la cita: " + " con fecha: "
                        + citaActualizar.getFechaCita() + " y con hora: " + citaActualizar.getHoraCita()); // citaActualizar.getCantidadDosis() + " " + citaActualizar.getFrencuenciaDosis()); // tratamiento.getDescripcionTratamiento());
                detalle.setHttpCodeType("SUCCESSFUL");
                detalle.setHttpCodeNumber(201);
                detalle.setHttpCodeName("Actualizado con exito");
                return new ResponseEntity(detalle, HttpStatus.OK);
            } else {
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("CONFLICTO");
                detalle.setMensaje("No se puede actualizar la cita. Ha ocurrido un error.");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);

            }
        }

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_citaDel')")
    @DeleteMapping("eliminar/{idCita}")
    public ResponseEntity eliminarTipoCita(@PathVariable("idCita") UUID idCita) {
        System.out.println(idCita);
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tipocitas/eliminar/" + idCita);
        CitaModel cita = service.obtenerPorId(idCita);
        if (cita == null) {
            detalle.setMensaje("No se encontro, por lo tanto no se puede eliminar la cita con id: " + idCita);
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            Boolean estado = service.toggle(idCita, false);
            if (estado == true) {
                detalle.setMensaje("Se elimino con exito la  cita " + " con fecha: " + cita.getFechaCita() + " y con hora: " + cita.getHoraCita());
                detalle.setHttpCodeName("Eliminado con exito");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            } else {
                detalle.setMensaje("Error al eliminar cita con id: " + idCita);
                detalle.setHttpCodeName("Error al eliminar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }

}
