package com.bad115.controller;

import com.bad115.model.DetalleResponseHttp;
import com.bad115.model.MedicamentoModel;
import com.bad115.model.TipoMedicamentoModel;
import com.bad115.service.MedicamentoService;
import com.bad115.service.TipoMedicamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/medicamentos")
public class MedicamentoController {

    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @Autowired
    @Qualifier("MedicamentoService")
    MedicamentoService service;

    @Autowired
    @Qualifier("TipoMedicamentoService")
    TipoMedicamentoService service2;

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_MedT')")
    @GetMapping("/todas")
    public ResponseEntity obtenerTodosMedicamentos() {
        DetalleResponseHttp detalle = new DetalleResponseHttp();
//        List<MedicamentoModel> medicamentos = service.obtenerTodosMedicamentos();
        List<MedicamentoModel> medicamentos = service.lista_activos();
        if (medicamentos.isEmpty()) {
            detalle.setMensaje("No hay medicamentos todavia.");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            detalle.setHttpCodeNumber(200);
            return new ResponseEntity(medicamentos, HttpStatus.OK);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_MedId')")
    @GetMapping(path = "{id}")
    public ResponseEntity getMedicamentoById(@PathVariable UUID id) {
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        MedicamentoModel optMedicamento = service.obtenerById(id);
        if (optMedicamento != null) {
            detalle.setHttpCodeNumber(200);
            return new ResponseEntity(optMedicamento, HttpStatus.OK);
        } else {
            detalle.setMensaje("No se encuentra el medicamento solicitado.");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }
    }

    // Metodo comun utilizado para no repetir codigo entre agregar y actualizar medicamento.
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity validarMedicamento(MedicamentoModel medicamento, String accion) {
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        // Guarda el resultado de ejecutar la accion
        Boolean resultado;
        if (accion.equals("actualizar") || accion.equals("eliminar")) {
            MedicamentoModel medicamentoBuscar = service.obtenerById(medicamento.getIdMedicamento());
            if (medicamentoBuscar == null) {
                detalle.setMensaje("No existe el medicamento " + accion);
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }

        if (!accion.equals("eliminar")) {
            TipoMedicamentoModel tipoMedicamento = service2.obtenerTipoMedicamentoId(medicamento.getFktipoMedicamento());
            medicamento.setTipoMedicamento(tipoMedicamento);
            if (tipoMedicamento == null) {
                detalle.setMensaje("No se ha seleccionado un tipo de medicamento asociado.");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
            MedicamentoModel medicamentoExistenteCodigo = service.obtenerByCodigo(medicamento.getCodigoMedicamento());
            MedicamentoModel medicamentoExistenteNombre = service.obtenerByNombre(medicamento.getNombreMedicamento());
            if (medicamentoExistenteCodigo != null && (!medicamento.getIdMedicamento().equals(medicamentoExistenteCodigo.getIdMedicamento()))) {
                detalle.setMensaje(accion + "fallido");
                detalle.setHttpCodeNumber(409);
                detalle.setMensaje("Ya existe un medicamento con ese codigo");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            } else if (medicamentoExistenteNombre != null && (!medicamento.getIdMedicamento().equals(medicamentoExistenteNombre.getIdMedicamento()))) {
                detalle.setMensaje(accion + "fallido");
                detalle.setHttpCodeNumber(409);
                detalle.setMensaje("Ya existe un medicamento con ese nombre");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
            resultado = service.guardar(medicamento);
        } else {
            //resultado = service.eliminar(medicamento.getIdMedicamento());
            resultado = service.toggle(medicamento.getIdMedicamento(),false);
        }

        if (resultado == true) {
            detalle.setMensaje(accion + " exitoso");
            detalle.setHttpCodeName(accion + " con exito");
            detalle.setHttpCodeNumber(200);
            detalle.setHttpCodeType("SUCCESSFUL");
            return new ResponseEntity(detalle, HttpStatus.OK);
        } else {
            detalle.setMensaje("No se puede " + accion + " el medicamento. Ha ocurrido un error.");
            return new ResponseEntity(detalle, HttpStatus.CONFLICT);
        }

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_MedAdd')")
    @PostMapping("/agregar")
    public ResponseEntity crearMedicamento(@Valid @RequestBody MedicamentoModel medicamento) {
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        Date date = new Date();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("medicamentos/agregar");
        UUID idMedicamento= UUID.randomUUID();
        medicamento.setIdMedicamento(idMedicamento);

        // STORED PROCEDURE
        String mensaje = service.checker(medicamento.getNombreMedicamento(),
                medicamento.getCodigoMedicamento(),medicamento.getFktipoMedicamento());
        if(!mensaje.equals("Se puede insertar registro")){
            detalle.setMensaje(mensaje);
            detalle.setHttpCodeName("No se pudo Agregar");
            detalle.setHttpCodeNumber(409);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.CONFLICT);
        }
        else {
            TipoMedicamentoModel tipoMedicamento = service2.obtenerTipoMedicamentoId(medicamento.getFktipoMedicamento());
            medicamento.setTipoMedicamento(tipoMedicamento);
            Boolean estado = service.guardar(medicamento);
            if (estado == true) {
                detalle.setMensaje("Se agrego el medicamento: " + medicamento.getNombreMedicamento());//tratamiento.getDescripcionTratamiento());
                detalle.setHttpCodeType("SUCCESSFUL");

                detalle.setHttpCodeNumber(201);
                detalle.setHttpCodeName("Agregado con exito");
                return new ResponseEntity(detalle, HttpStatus.OK);
            } else {
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("CONFLICTO");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);

            }
        }

        // STORED PROCEDURE



        //return validarMedicamento(medicamento, "agregar");

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_MedUpd')")
    @PutMapping("actualizar/{id}")
    public ResponseEntity updateMedicamento(@PathVariable UUID id, @Valid @RequestBody MedicamentoModel medicamento) {
        medicamento.setIdMedicamento(id);
        return validarMedicamento(medicamento, "actualizar");
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_MedDel')")
    @DeleteMapping("eliminar/{id}")
    public ResponseEntity deleteAssignment(@PathVariable UUID id) {
        MedicamentoModel medicamento = new MedicamentoModel();
        medicamento.setIdMedicamento(id);
        return validarMedicamento(medicamento, "eliminar");
    }

}
