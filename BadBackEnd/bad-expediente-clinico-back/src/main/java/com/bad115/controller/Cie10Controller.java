package com.bad115.controller;

import com.bad115.model.Cie10Model;
import com.bad115.model.DetalleResponseHttp;
import com.bad115.service.Cie10Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/cie10")

public class Cie10Controller {

    @Autowired
    @Qualifier("Cie10Service")
    Cie10Service service;

    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_CieT')")
    @GetMapping("/todas")
    public ResponseEntity listarTodasCie10() {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/cie10/todas");
        List<Cie10Model> cie10 = service.obtenerTodasCie10();
        if (cie10.isEmpty()) {
            detalle.setMensaje("Cie10 aun no se registran");
            detalle.setHttpCodeName("no se encontraron registros de CIE10");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(cie10, HttpStatus.FOUND);
        }
    }


    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_CieId')")
    @GetMapping("/{idCie10}")
    public ResponseEntity obtenerCie10Id(@PathVariable("idCie10") UUID idCie10){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/cie10/"+ idCie10);

        Cie10Model persona = service.obtenerCie10Id(idCie10);
        if (persona==null){
            detalle.setMensaje("No se encontro el Registro cie10 con id: "+ idCie10);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else{
            return new ResponseEntity(persona, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_CieUpd')")
    @PutMapping("actualizar/{idCie10}")
    public ResponseEntity actualizarCie10(@PathVariable("idCie10") UUID idCie10, @RequestBody @Valid Cie10Model cie10) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/cie10/actualizar/"+idCie10);
        Cie10Model cie10A = service.obtenerCie10Id(idCie10);
        if(cie10A==null){
            detalle.setMensaje("NO SE encontro el registro CIE10 con id: "+ idCie10);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else{
            cie10A.setCodigoCIE10(cie10.getCodigoCIE10());
            cie10A.setDescripcionCIE10(cie10.getDescripcionCIE10());
            cie10A.setSexoCIE10(cie10.getSexoCIE10());
            cie10A.setLimiteInfCIE10(cie10.getLimiteInfCIE10());
            cie10A.setLimiteSupCIE10(cie10.getLimiteSupCIE10());
            Boolean estado = service.actualizarCie10(cie10A);
            if(estado==true){
                detalle.setMensaje("se actualizo el registro CIE10" + cie10.getCodigoCIE10() +"\n "+ cie10.getDescripcionCIE10()+"\n "+ cie10.getSexoCIE10()+"\n "+ cie10.getLimiteInfCIE10()+"\n "+ cie10.getLimiteSupCIE10());
                detalle.setHttpCodeName("Actualizada con exito");
                detalle.setHttpCodeNumber(202);
                detalle.setHttpCodeType("SUCCESSFULL");
                return new ResponseEntity(detalle, HttpStatus.ACCEPTED);
            }else {
                Cie10Model cie10U = service.obtenerCie10Codigo(cie10.getCodigoCIE10());
                if (cie10U==null){
                    detalle.setMensaje("NO SE Actualizo El registro CIE10 "+ cie10.toString());
                    detalle.setHttpCodeName("No se pudo Actualizar");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeType("ERROR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }else{
                    detalle.setMensaje("El registro CIE10 ya existe con codigo "+ cie10.getCodigoCIE10());
                    detalle.setHttpCodeName("Conflicto en el codigo");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeType("ERROR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_CieAdd')")
    @PostMapping("/agregar")
    public ResponseEntity crearCie10(@RequestBody @Valid Cie10Model cie10){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/cie10/agregar");
        UUID uuid = UUID.randomUUID();
        cie10.setIdCIE10(uuid);
        Boolean estado=service.agregarCie10(cie10);
        if (estado==true){
            detalle.setMensaje("se agrego el registro ci10 con codigo " + cie10.getCodigoCIE10() +"\n  con descripcion "+ cie10.getDescripcionCIE10());
            detalle.setHttpCodeName("Agregada con exito");
            detalle.setHttpCodeNumber(201);
            detalle.setHttpCodeType("SUCCESSFULL");
            return new ResponseEntity(detalle, HttpStatus.CREATED);
        }else {
            Cie10Model cie10U = service.obtenerCie10Codigo(cie10.getCodigoCIE10());
            if (cie10U==null){
                detalle.setMensaje("No se agrego el registro de CIE10 "+ cie10.toString());
                detalle.setHttpCodeName("No se pudo Agregar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }else{
                detalle.setMensaje("El registro CIE10 con codigo: "+ cie10.getCodigoCIE10() + " ya existe");
                detalle.setHttpCodeName("Conflicto en codigo de CIE10");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_CieDel')")
    @DeleteMapping("eliminar/{idCie10}")
    public ResponseEntity eliminarCie10(@PathVariable("idCie10") UUID idCie10) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/cie10/eliminar/"+idCie10);
        Cie10Model cie10 = service.obtenerCie10Id(idCie10);
        if (cie10==null){
            detalle.setMensaje("No se puede eliminar porque no se encontro el registro CIE10 con id: "+ idCie10);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else{
            Boolean estado = service.eliminarCie10(idCie10);
            if (estado==true){
                detalle.setMensaje("se elimino con exito El registro CIE10" + cie10.getCodigoCIE10() +"\n "+ cie10.getDescripcionCIE10()+"\n "+ cie10.getSexoCIE10()+"\n "+ cie10.getLimiteInfCIE10()+"\n "+ cie10.getLimiteSupCIE10());
                detalle.setHttpCodeName("Eliminado con exito");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFULL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            }else{
                detalle.setMensaje("No se elimnino el registro cie10 con id"+ idCie10);
                detalle.setHttpCodeName("No se pudo eliminar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }



}
