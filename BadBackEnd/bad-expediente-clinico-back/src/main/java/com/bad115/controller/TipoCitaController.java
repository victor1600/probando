package com.bad115.controller;

import com.bad115.model.CitaModel;
import com.bad115.model.DetalleResponseHttp;
import com.bad115.model.TipoCitaModel;
import com.bad115.service.TipoCitaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/tipocitas")
public class TipoCitaController {
    @Autowired
    @Qualifier("TipoCitaService")
    TipoCitaService service;

    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    //TODO: UPDATE TO PROPER ROLES
    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_tCitaT')")
    @GetMapping("/todas")
    public ResponseEntity listarTratamientos() {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tipocitas/todas");
        //List<TipoCitaModel> tipoCitas = service.obtenerTodos();
        List<TipoCitaModel> tipoCitas = service.lista_activos();// obtenerTodos();
        if (tipoCitas.isEmpty()) {
            detalle.setMensaje("Aun no se registran tipos de citas");
            detalle.setHttpCodeName("NO SE ENCONTRARON tipos de citas");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(tipoCitas, HttpStatus.OK);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_tCitaId')")
    @GetMapping("/{idTipoCita}")
    public ResponseEntity obtenerTratamientoId(@PathVariable("idTipoCita") UUID idTipoCita) {
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tipocitas/" + idTipoCita);
        TipoCitaModel tipoCita = service.obtenerPorId(idTipoCita);
        if (tipoCita == null) {
            detalle.setMensaje("No se encontro el tipo de cita con id: " + idTipoCita);
            detalle.setHttpCodeName("Cita NO ENCONTRADA");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(tipoCita, HttpStatus.OK);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_tCitaAdd')")
    @PostMapping("/agregar")
    public ResponseEntity crearTipoCita(@RequestBody @Valid TipoCitaModel tipoCita) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("tipocita/agregar");
        UUID uuid = UUID.randomUUID();
        tipoCita.setIdTipoCita(uuid); //setIdTratamiento(uuid);
        // TODO: CALL STORED PROCEDURE TO VALIDATE
        String mensaje = service.checker(tipoCita.getNombreTipoCita(), tipoCita.getIdTipoCita());
        if(!mensaje.equals("Se puede insertar registro")){
            detalle.setMensaje(mensaje);
            detalle.setHttpCodeName("No se pudo Agregar");
            detalle.setHttpCodeNumber(409);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.CONFLICT);
        }else{
            Boolean estado = service.agregar(tipoCita);
            if (estado == true) {
                detalle.setMensaje("Se agrego el tipo de cita: " + tipoCita.getNombreTipoCita());
                detalle.setHttpCodeType("SUCCESSFUL");

                detalle.setHttpCodeNumber(201);
                detalle.setHttpCodeName("Agregado con exito");
                return new ResponseEntity(detalle, HttpStatus.OK);
            } else {
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("CONFLICTO");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);

            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_tCitaUpd')")
    @PutMapping("actualizar/{idTipoCita}")
    public ResponseEntity actualizarTipoCita(@PathVariable("idTipoCita") UUID idTipoCita, @RequestBody @Valid TipoCitaModel tipoCita) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tipocitas/actualizar/" + idTipoCita);

        TipoCitaModel tipoCitaA = service.obtenerPorId(idTipoCita);
        if (tipoCitaA  == null) {
            detalle.setMensaje("No se encontro, por lo tanto no se puede actualizar el tipo de cita con id: " + idTipoCita);
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            // TODO: CALL SP
            String mensaje = service.checker(tipoCita.getNombreTipoCita(), idTipoCita);
            if(!mensaje.equals("Se puede insertar registro")){
                detalle.setMensaje(mensaje);
                detalle.setHttpCodeName("No se pudo Agregar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            } else {
                tipoCitaA.setNombreTipoCita(tipoCita.getNombreTipoCita());
                tipoCitaA.setEstaActivo(tipoCita.getEstaActivo());
//                service.agregar(tipoCitaA);
//                detalle.setMensaje("Se actualizo el tipo de cita: " + tipoCita.getNombreTipoCita());
//                detalle.setHttpCodeName("Se actualizo con exito");
//                detalle.setHttpCodeNumber(200);
//                detalle.setHttpCodeType("SUCCESSFUL");
//                return new ResponseEntity(detalle, HttpStatus.OK);
                Boolean estado = service.agregar(tipoCitaA);
                if (estado == true) {
                    detalle.setMensaje("Se actualizo el tipo de cita: " + tipoCita.getNombreTipoCita());
                    detalle.setHttpCodeType("SUCCESSFUL");

                    detalle.setHttpCodeNumber(201);
                    detalle.setHttpCodeName("Actualizado con exito");
                    return new ResponseEntity(detalle, HttpStatus.OK);
                } else {
                    detalle.setHttpCodeType("ERROR");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeName("CONFLICTO");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);

                }
            }


        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_tCitaDel')")
    @DeleteMapping("eliminar/{idTipoCita}")
    public ResponseEntity eliminarTipoCita(@PathVariable("idTipoCita") UUID idTipoCita){
        System.out.println(idTipoCita);
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tipocitas/eliminar/" + idTipoCita);
        TipoCitaModel tipoCita = service.obtenerPorId(idTipoCita);
        if (tipoCita == null) {
            detalle.setMensaje("No se encontro, por lo tanto no se puede eliminar el tipo de cita con id: " + idTipoCita);
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            //TODO: SUSTITUIR POR SP PARA DESACTIVAR
//            Boolean estado = service.eliminar(idTipoCita);
            Boolean estado = service.toggle(idTipoCita, false);
            if (estado == true) {
                detalle.setMensaje("Se elimino con exito el tipo de cita: " + tipoCita.getNombreTipoCita());
                detalle.setHttpCodeName("Eliminado con exito");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            } else {
                detalle.setMensaje("Error al eliminar cita con id: " + idTipoCita);
                detalle.setHttpCodeName("Error al eliminar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }
}
