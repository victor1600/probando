package com.bad115.controller;

import com.bad115.model.*;
import com.bad115.service.BoletaExamenService;
import com.bad115.service.ConsultaService;
import com.bad115.service.ExamenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/boletas")
public class BoletaExamenController {
    @Autowired
    @Qualifier("BoletaExamenService")
    BoletaExamenService service;

    @Autowired
    @Qualifier("ExamenService")
    ExamenService service2;

    @Autowired
    @Qualifier("ConsultaService")
    ConsultaService service3;

    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_bolExamenT')")
    @GetMapping("/todas")
    public ResponseEntity listarBoletas() {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/boletas/todas");
        List<BoletaExamenModel> boletas = service.lista_activos();
//        List<BoletaExamenModel> boletas = service.obtenerTodas();
        if (boletas.isEmpty()) {
            detalle.setMensaje("Aun no se registran boletas");
            detalle.setHttpCodeName("NO SE ENCONTRARON boletas");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(boletas, HttpStatus.OK);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_bolExamenId')")
    @GetMapping("/{idBoletaExamen}")
    public ResponseEntity obtenerBoletaExamenId(@PathVariable("idBoletaExamen") UUID idBoletaExamen) {
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/boletas/" + idBoletaExamen);
        BoletaExamenModel boleta = service.obtenerById(idBoletaExamen);
        // obtenerPorId(idBoletaExamen);
        if (boleta == null) {
            detalle.setMensaje("No se encontro la boleta con id: " + idBoletaExamen);
            detalle.setHttpCodeName("Boleta NO ENCONTRADA");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(boleta, HttpStatus.OK);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_bolExamenAdd')")
    @PostMapping("/agregar")
    public ResponseEntity agregarBoleta(@RequestBody @Valid BoletaExamenModel boleta) {
        UUID idBoleta = UUID.randomUUID();
        boleta.setIdBoleta(idBoleta);
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        Date date = new Date();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/boletas/agregar/" + idBoleta);
        // TODO: enviar id de cita
        String mensaje = service.checker(boleta.getFkExamen(), boleta.getFkConsulta());
        if (!mensaje.equals("Se puede insertar registro")) {
            detalle.setMensaje(mensaje);
            detalle.setHttpCodeName("No se pudo Agregar");
            detalle.setHttpCodeNumber(409);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.CONFLICT);
        } else {
            ExamenModal examen = service2.obtenerExamenId(boleta.getFkExamen());
            boleta.setExamen(examen);
            ConsultaModel consulta = service3.obtenerIdConsulta(boleta.getFkConsulta());
            boleta.setConsulta(consulta);
            Boolean resultado = service.guardar(boleta);
            if (resultado == true) {
                detalle.setMensaje("Se agrego la boleta: " + boleta.getDescripBoletaExamen());
                detalle.setHttpCodeType("SUCCESSFUL");
                detalle.setHttpCodeNumber(201);
                detalle.setHttpCodeName("Agregado con exito");
                return new ResponseEntity(detalle, HttpStatus.OK);
            } else {
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("CONFLICTO");
                detalle.setMensaje("No se puede agregar la dosis. Ha ocurrido un error.");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);

            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_bolExamenUpd')")
    @PutMapping("actualizar/{idBoleta}")
    public ResponseEntity actualizarBoleta(@PathVariable("idBoleta") UUID idBoleta, @RequestBody @Valid BoletaExamenModel boletaExamen) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/citas/actualizar/" + idBoleta);
        BoletaExamenModel boletaActualizar = service.obtenerById(idBoleta); //obtenerPorId(idBoleta); // obtenerById(idCita);
        if (boletaActualizar == null) {
            detalle.setMensaje("No se encontro, por lo tanto no se puede actualizar la boleta con id: " + idBoleta);
            detalle.setHttpCodeName("No encontrada");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            //TODO CALL SP for actualizar dosis
            String mensaje = service.checker(boletaExamen.getFkExamen(), boletaExamen.getFkConsulta());
            if (!mensaje.equals("Se puede insertar registro")) {
                detalle.setMensaje(mensaje);
                detalle.setHttpCodeName("No se pudo Agregar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            } else {
                ExamenModal examen = service2.obtenerExamenId(boletaExamen.getFkExamen());
                boletaActualizar.setExamen(examen);
                ConsultaModel consulta = service3.obtenerIdConsulta(boletaExamen.getFkConsulta());
                boletaActualizar.setConsulta(consulta);

                boletaActualizar.setDescripBoletaExamen(boletaExamen.getDescripBoletaExamen());
                boletaActualizar.setEstaActivo(boletaExamen.getEstaActivo());
                Boolean resultado = service.guardar(boletaActualizar); // guardar(citaActualizar);
                if (resultado == true) {
                    detalle.setMensaje("Se actualizo boleta: " + boletaActualizar.getDescripBoletaExamen()); // citaActualizar.getCantidadDosis() + " " + citaActualizar.getFrencuenciaDosis()); // tratamiento.getDescripcionTratamiento());
                    detalle.setHttpCodeType("SUCCESSFUL");
                    detalle.setHttpCodeNumber(201);
                    detalle.setHttpCodeName("Actualizado con exito");
                    return new ResponseEntity(detalle, HttpStatus.OK);
                } else {
                    detalle.setHttpCodeType("ERROR");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeName("CONFLICTO");
                    detalle.setMensaje("No se puede actualizar la boleta. Ha ocurrido un error.");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);

                }
            }

        }

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_bolExamenDel')")
    @DeleteMapping("eliminar/{idBoletaExamen}")
    public ResponseEntity eliminarBoletaExamen(@PathVariable("idBoletaExamen") UUID idBoleta) {
        System.out.println(idBoleta);
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/boletas/eliminar/" + idBoleta);
        BoletaExamenModel boleta = service.obtenerById(idBoleta); // obtenerPorId(idBoleta);
        if (boleta == null) {
            detalle.setMensaje("No se encontro, por lo tanto no se puede eliminar la boleta con id: " + idBoleta);
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            Boolean estado = service.toggle(idBoleta, false);
            if (estado == true) {
                detalle.setMensaje("Se elimino con exito la boleta " + " con fecha: " + boleta.getDescripBoletaExamen());
                detalle.setHttpCodeName("Eliminado con exito");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            } else {
                detalle.setMensaje("Error al eliminar boleta con id: " + idBoleta);
                detalle.setHttpCodeName("Error al eliminar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }

}
