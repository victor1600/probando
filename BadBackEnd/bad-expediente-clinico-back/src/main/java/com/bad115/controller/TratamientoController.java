package com.bad115.controller;

import com.bad115.model.ConsultaModel;
import com.bad115.model.DetalleResponseHttp;

import com.bad115.model.TratamientoModel;
import com.bad115.service.CitaService;
import com.bad115.service.ConsultaService;
import com.bad115.service.TratamientoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/tratamientos")
public class TratamientoController {

    @Autowired
    @Qualifier("TratamientoService")
    TratamientoService service;

    @Autowired
    @Qualifier("ConsultaService")
    ConsultaService service2;

    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");


    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_tratT')")
    @GetMapping("/todas")
    public ResponseEntity listarTratamientos() {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tratamientos/todas");
        List<TratamientoModel> tratamientos = service.lista_activos();
        if (tratamientos.isEmpty()) {
            detalle.setMensaje("Aun no se registran tratamientos");
            detalle.setHttpCodeName("NO SE ENCONTRARON tratamientos");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(tratamientos, HttpStatus.OK);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_tratId')")
    @GetMapping("/{idTratamiento}")
    public ResponseEntity obtenerTratamientoId(@PathVariable("idTratamiento") UUID idTratamiento) {
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tratamientos/" + idTratamiento);
        TratamientoModel tratamiento = service.obtenerTratamientoPorId(idTratamiento);
        if (tratamiento == null) {
            detalle.setMensaje("No se encontro el tratamiendo con id: " + idTratamiento);
            detalle.setHttpCodeName("Tratamiento NO ENCONTRADO");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(tratamiento, HttpStatus.OK);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_tratAdd')")
    @PostMapping("/agregar")
    public ResponseEntity crearTratamiento(@RequestBody @Valid TratamientoModel tratamiento) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("tratamientos/agregar");
        UUID uuid = UUID.randomUUID();
        tratamiento.setIdTratamiento(uuid);
        // TODO: CALL STORED PROCEDURE TO VALIDATE
        String mensaje = service.checker(tratamiento.getIdTratamiento(), tratamiento.getFkConsultaId());
        if (!mensaje.equals("Se puede insertar registro")) {
            detalle.setMensaje(mensaje);
            detalle.setHttpCodeName("No se pudo Agregar");
            detalle.setHttpCodeNumber(409);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.CONFLICT);
        } else {
            ConsultaModel consulta = service2.obtenerIdConsulta(tratamiento.getFkConsultaId());
            tratamiento.setConsulta(consulta);
            Boolean estado = service.agregarTratamiento(tratamiento); //agregarTipoMedicamento(tipoMedicamento);
            if (estado == true) {
                detalle.setMensaje("Se agrego el tratamiento: " + tratamiento.getDescripcionTratamiento());
                detalle.setHttpCodeType("SUCCESSFUL");

                detalle.setHttpCodeNumber(201);
                detalle.setHttpCodeName("Agregado con exito");
                return new ResponseEntity(detalle, HttpStatus.OK);
            } else {
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("CONFLICTO");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);

            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_tratUpd')")
    @PutMapping("actualizar/{idTratamiento}")
    public ResponseEntity actualizarTratamiento(@PathVariable("idTratamiento") UUID idTratamiento, @RequestBody @Valid TratamientoModel tratamiento) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tratamientos/actualizar/" + idTratamiento);

        TratamientoModel tratamientoA = service.obtenerTratamientoPorId(idTratamiento);// obtenerPersonalMedicoId(idPersonalMedico);
        if (tratamientoA == null) {
            detalle.setMensaje("No se encontro, por lo tanto no se puede actualizar tratamiento con id: " + idTratamiento);
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            // TODO: CALL SP
            String mensaje = service.checker(idTratamiento, tratamiento.getFkConsultaId());
            System.out.println(mensaje);
            if (!mensaje.equals("Se puede insertar registro")) {
                detalle.setMensaje(mensaje);
                detalle.setHttpCodeName("No se pudo Agregar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            } else {
                tratamientoA.setDescripcionTratamiento(tratamiento.getDescripcionTratamiento());
                tratamientoA.setEstaActivo(tratamiento.getEstaActivo());
                Boolean estado = service.agregarTratamiento(tratamientoA); // TODO: CHECK THIS


//                detalle.setMensaje("Se actualizo el tratamiento: " + tratamiento.getDescripcionTratamiento());
//                detalle.setHttpCodeName("Se actualizo con exito");
//                detalle.setHttpCodeNumber(200);
//                detalle.setHttpCodeType("SUCCESSFUL");
//                return new ResponseEntity(detalle, HttpStatus.OK);
//                Boolean estado = service.agregarTratamiento(tratamiento); //agregarTipoMedicamento(tipoMedicamento);
                if (estado == true) {
                    detalle.setMensaje("Se agrego el tratamiento: " + tratamiento.getDescripcionTratamiento());
                    detalle.setHttpCodeType("SUCCESSFUL");

                    detalle.setHttpCodeNumber(201);
                    detalle.setHttpCodeName("Agregado con exito");
                    return new ResponseEntity(detalle, HttpStatus.OK);
                } else {
                    detalle.setHttpCodeType("ERROR");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeName("CONFLICTO");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);

                }
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_tratDel')")
    @DeleteMapping("eliminar/{idTratamiento}")
    public ResponseEntity eliminarTratamiento(@PathVariable("idTratamiento") UUID idTratamiento) {
        System.out.println(idTratamiento);
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tratamientos/eliminar/" + idTratamiento);
        TratamientoModel tratamiento = service.obtenerTratamientoPorId(idTratamiento);
        if (tratamiento == null) {
            detalle.setMensaje("No se encontro, por lo tanto no se puede eliminar el tratamiento con id: " + idTratamiento);
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            //TODO: SUSTITUIR POR SP PARA DESACTIVAR
            Boolean estado = service.toggle(idTratamiento, false);
            if (estado == true) {
                detalle.setMensaje("Se elimino con exito el tratamiento: " + tratamiento.getDescripcionTratamiento());
                detalle.setHttpCodeName("Eliminado con exito");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            } else {
                detalle.setMensaje("Error al eliminar tratamiento con id: " + idTratamiento);
                detalle.setHttpCodeName("Error al eliminar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }


}
