package com.bad115.controller;

import com.bad115.configuration.mapper.UserMapper;
import com.bad115.configuration.web.UserResponse;
import com.bad115.model.DetalleResponseHttp;
import com.bad115.model.ReferenciaMedicaModel;
import com.bad115.model.RolModel;
import com.bad115.model.UsuarioModel;
import com.bad115.configuration.control.UsuarioServiceControl;
import com.bad115.service.RolService;
import com.bad115.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/usuario")
@CrossOrigin
public class UsuarioController {

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private UsuarioServiceControl usuarioServiceControl;

    @Autowired
    @Qualifier("UsuarioService")
    UsuarioService service;

    @Autowired
    @Qualifier("RolService")
    RolService serviceR;

    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    public UsuarioController(UsuarioServiceControl usuarioServiceControl, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.usuarioServiceControl = usuarioServiceControl;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/todos")
    ResponseEntity listarTodosUsuario(){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/usuario/todos");
        List<UsuarioModel> usuarios =service.obtenerTodosUsuario();
        if (usuarios.isEmpty()){
            detalle.setMensaje("Aun no se registran usuarios");
            detalle.setHttpCodeName("NO SE ENCONTRARON Usuarios");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return  new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity(usuarios, HttpStatus.FOUND);
        }

    }


    //@Secured("ROLE_ADMIN")
    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/{id}")
    public ResponseEntity<UserResponse> getUser(@PathVariable UUID id) {
        final UsuarioModel usuarioModel = usuarioServiceControl.getUser(id);

        if (usuarioModel == null) {
            return ResponseEntity.notFound().build();
        }

        UserResponse userResponse = UserMapper.toResponse(usuarioModel);
        return new ResponseEntity<>(userResponse, HttpStatus.OK);
    }

    //@Secured("ROLE_ADMIN")
    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/{usuario}/{permiso}")
    public ResponseEntity confirmarPermiso(@PathVariable String usuario, @PathVariable String permiso) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/usuario/usuario/permiso");
        Integer estado=service.checkPermiso(usuario,permiso);
        detalle.setMensaje(""+estado);
        detalle.setHttpCodeName("0-> Negado ||  1->Aprobado");
        detalle.setHttpCodeNumber(200);
        detalle.setHttpCodeType("Permiso");
        return new ResponseEntity(detalle, HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/agregar")
    ResponseEntity agregarUsuario(@RequestBody @Valid UsuarioModel usuario){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/usuario/agregar/");

        //Se crea la entidad a agregar
        UsuarioModel usuarioNew = new UsuarioModel();

        usuarioNew.setNombreUsuario(usuario.getNombreUsuario());
        usuarioNew.setClaveUsuario(bCryptPasswordEncoder.encode(usuario.getClaveUsuario()));

        List<RolModel> list = new ArrayList<>();
        for (RolModel rol: usuario.getRoles()) {
            RolModel rolB = serviceR.obtenerIdRol(rol.getIdRol());
            if (rolB==null){}else {
                list.add(rolB);
            }
        }
        Set<RolModel> roles =new HashSet<>(list);

        usuarioNew.setRoles(roles);


        detalle.setMensaje("Necesito averiguar que tiene : \n"+usuario.toString());
        detalle.setHttpCodeType("prueba");
        detalle.setHttpCodeNumber(200);
        detalle.setHttpCodeName("EXITO");
        return new ResponseEntity(usuarioNew, HttpStatus.OK);

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PutMapping("/actualizar2/{nombreUsuario}")
    ResponseEntity actualizarUsuario(@PathVariable("nombreUsuario") String nombreUsuario, @RequestBody @Valid UsuarioModel usuario){

        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/usuario/actualizar2/"+nombreUsuario);

        UsuarioModel usuarioT = service.obtenerUsuarioNombreUsuario(nombreUsuario);

        if (usuarioT==null){
            detalle.setMensaje("No se puede agregar token, no se encontro usuario: "+nombreUsuario);
            detalle.setHttpCodeType("ERROR");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeName("Usuario No encontrado");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            usuarioT.setTokenUsuario(usuario.getTokenUsuario().replace("\"",""));
            usuarioT.setTokenTiempo(usuario.getTokenTiempo().replace("\"",""));
            Boolean estado = service.guardarUsuario(usuarioT);
            if (estado==true){
                detalle.setMensaje("Se actualizo datos del token al usuario: "+usuarioT.getNombreUsuario());
                detalle.setHttpCodeType("SUCCESSFUL");
                detalle.setHttpCodeNumber(202);
                detalle.setHttpCodeName("Actualizado con exito");
                return new ResponseEntity(detalle, HttpStatus.ACCEPTED);
            }else {
                detalle.setMensaje("Hubo error al actualizar token al usaurio: " +nombreUsuario);
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("ERROR DESCONOCIDO AL ACTUALIZAR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }



    }



}