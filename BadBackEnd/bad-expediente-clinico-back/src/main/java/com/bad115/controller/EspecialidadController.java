package com.bad115.controller;

import com.bad115.model.DetalleResponseHttp;
import com.bad115.model.EspecialidadModel;
import com.bad115.service.EspecialidadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/especialidad")

public class EspecialidadController {
    @Autowired
    @Qualifier("EspecialidadService")
    EspecialidadService service;

    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_EspeT')")
    @GetMapping("/todas")
    public ResponseEntity listarTodasEspecialidad() {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/especialidad/todas");
        List<EspecialidadModel> especialidades = service.obtenerTodasEspecilidad();
        if (especialidades.isEmpty()) {
            detalle.setMensaje("especialidades aun no se registran");
            detalle.setHttpCodeName("no se encontraron especialidades");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(especialidades, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_EspeId')")
    @GetMapping("/{idEspecialidad}")
    public ResponseEntity obtenerEspecialidadId(@PathVariable("idEspecialidad") UUID idEspecialidad){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/especialidad/"+ idEspecialidad);

        EspecialidadModel especialidad = service.obtenerEspecialidadId(idEspecialidad);
        if (especialidad==null){
            detalle.setMensaje("NO SE encontro LA especialidad con id: "+ idEspecialidad);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else{
            return new ResponseEntity(especialidad, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_EspeAdd')")
    @PostMapping("/agregar")
    public ResponseEntity crearEspecialidad(@RequestBody @Valid EspecialidadModel especialidad){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/especialidad/agregar");
        UUID uuid = UUID.randomUUID();
        especialidad.setIdEspecialidad(uuid);
        Boolean estado=service.agregarEspecialidad(especialidad);
        if (estado==true){
            detalle.setMensaje("se agrego la especialidad: " + especialidad.getNombreEspecialidad());
            detalle.setHttpCodeName("Agregada con exito");
            detalle.setHttpCodeNumber(201);
            detalle.setHttpCodeType("SUCCESSFULL");
            return new ResponseEntity(detalle, HttpStatus.CREATED);
        }else {
            EspecialidadModel especialidadU = service.obtenerNombreEspecialidad(especialidad.getNombreEspecialidad());
            if (especialidadU==null){
                detalle.setMensaje("NO SE AGREGO LA especialidad: "+ especialidad.toString());
                detalle.setHttpCodeName("No se pudo Agregar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }else{
                detalle.setMensaje("LA especialidad con nombre: "+ especialidad.getNombreEspecialidad() + " ya existe");
                detalle.setHttpCodeName("Conflicto en ID");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_EspeUpd')")
    @PutMapping("actualizar/{idEspecialidad}")
    public ResponseEntity actualizarEspecialidad(@PathVariable("idEspecialidad") UUID idEspecialidad, @RequestBody @Valid EspecialidadModel especialidad) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/especialidades/actualizar/"+idEspecialidad);
        EspecialidadModel especialidadA = service.obtenerEspecialidadId(idEspecialidad);
        if(especialidadA==null){
            detalle.setMensaje("NO SE encontro la especialidad con id: "+ idEspecialidad);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else{
            especialidadA.setNombreEspecialidad(especialidad.getNombreEspecialidad());
            Boolean estado = service.actualizarEspecialidad(especialidadA);
            if(estado==true){
                detalle.setMensaje("se actualizo la especialidad: "+ especialidad.getNombreEspecialidad());
                detalle.setHttpCodeName("Actualizada con exito");
                detalle.setHttpCodeNumber(202);
                detalle.setHttpCodeType("SUCCESSFULL");
                return new ResponseEntity(detalle, HttpStatus.ACCEPTED);
            }else {
                EspecialidadModel especialidadU = service.obtenerNombreEspecialidad(especialidad.getNombreEspecialidad());
                if (especialidadU==null){
                    detalle.setMensaje("NO SE Actualizo LA especialidad: "+ especialidad.toString());
                    detalle.setHttpCodeName("No se pudo Actualizar");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeType("ERROR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }else{
                    detalle.setMensaje("LA especialidad ya existe con nombre: "+ especialidad.getNombreEspecialidad());
                    detalle.setHttpCodeName("Conflicto en nombre");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeType("ERROR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_CieDel')")
    @DeleteMapping("eliminar/{idEspecialidad}")
    public ResponseEntity eliminarEspecialidad(@PathVariable("idEspecialidad") UUID idEspecialidad) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/especialidads/eliminar/"+idEspecialidad);
        EspecialidadModel especialidad = service.obtenerEspecialidadId(idEspecialidad);
        if (especialidad==null){
            detalle.setMensaje("NO SE puede eliminar porque no se encontro LA especialidad con id: "+ idEspecialidad);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else{
            Boolean estado = service.eliminarEspecialidad(idEspecialidad);
            if (estado==true){
                detalle.setMensaje("se elimino con exito la especialidad: " + especialidad.getNombreEspecialidad());
                detalle.setHttpCodeName("Eliminado con exito");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFULL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            }else{
                detalle.setMensaje("NO SE Elimnino LA especialidad con id"+ idEspecialidad);
                detalle.setHttpCodeName("No se pudo eliminar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }



}
