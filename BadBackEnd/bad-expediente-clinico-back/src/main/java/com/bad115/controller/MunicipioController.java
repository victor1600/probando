package com.bad115.controller;

import com.bad115.model.DetalleResponseHttp;
import com.bad115.model.MunicipioModel;
import com.bad115.service.MunicipioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/municipios")
public class MunicipioController {

    @Autowired
    @Qualifier("MunicipioService")
    MunicipioService service_muni;

    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/todos")
    public ResponseEntity ListarMunicipios(){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/municipios/todos");

        List<MunicipioModel> municipios = service_muni.obtenerMunicipios();
        if(municipios.isEmpty()){
            detalle.setMensaje("Listado de municipios no encontrado.");
            detalle.setHttpCodeName("No se encontraron datos.");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity(municipios, HttpStatus.FOUND);
        }
    }
}
