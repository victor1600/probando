package com.bad115.controller;

import com.bad115.model.DetalleResponseHttp;
import com.bad115.model.DireccionModel;
import com.bad115.model.MedicoModel;
import com.bad115.model.PersonalMedicoModel;
import com.bad115.service.EspecialidadService;
import com.bad115.service.MedicoService;
import com.bad115.service.PersonalMedicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/medicos")
public class MedicoController {

    @Autowired
    @Qualifier("PersonalMedicoService")
    PersonalMedicoService servicePer;

    @Autowired
    @Qualifier("MedicoService")
    MedicoService serviceMed;

    @Autowired
    @Qualifier("EspecialidadService")
    EspecialidadService serviceEsp;

    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_PersonalAll')")
    @GetMapping("/todos")
    public ResponseEntity listarTodosPersonalMedico(){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/Medicos/todos");
        List<PersonalMedicoModel> Medicos = servicePer.obtenerMedicos();
        if(Medicos.isEmpty()){
            detalle.setMensaje("No hay medicos registrados.");
            detalle.setHttpCodeName("No se encontraron medicos.");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity(Medicos, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/{idPersonal}")
    public ResponseEntity obtenerPersonalMedicoId(@PathVariable("idPersonal") UUID idPersonalMedico){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/Medicos/"+idPersonalMedico);

        PersonalMedicoModel personalMedico = servicePer.obtenerMedicoId(idPersonalMedico);
        if (personalMedico==null){
            detalle.setMensaje("No se encontro el medico: "+ personalMedico.getNombre1Persona()+" "+personalMedico.getApellido1Persona());
            detalle.setHttpCodeName("Medico no encontrado.");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new  ResponseEntity(personalMedico,HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_PersonalUpd')")
    @PutMapping("actualizar/{idPersonal}")
    public ResponseEntity actualizarPersonalMedico(@PathVariable("idPersonal") UUID idPersonal, @RequestBody @Valid PersonalMedicoModel personal){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/medicos/actualizar/"+idPersonal);
        detalle.setHttpCodeName("Fallo al actualizar Medico.");
        detalle.setHttpCodeNumber(409);
        detalle.setHttpCodeType("ERROR");

        PersonalMedicoModel personalActual = servicePer.obtenerPersonalMedicoId(idPersonal);
        if (personalActual==null){
            detalle.setMensaje("No se encontro el Medico, Fallo al actualizar.");
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            DetalleResponseHttp validacion = validarMedico(personal);
            if (validacion.getMensaje() != null) {
                detalle.setMensaje(validacion.getMensaje());
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }else {
                UUID idMedico = UUID.randomUUID();
                MedicoModel medico = personal.getMedico();
                medico.setIdMedico(idMedico);
                Boolean estadoMed = serviceMed.guardarMedico(medico);
                if(estadoMed){
                    Boolean estadoPer = servicePer.actualizarPersonalMedico(personal);
                    if (estadoPer){
                        detalle.setMensaje("Se actualizo el Medico: "+personalActual.getNombre1Persona()+" "+personalActual.getApellido1Persona());
                        detalle.setHttpCodeName("Se actualizo con exito");
                        detalle.setHttpCodeNumber(202);
                        detalle.setHttpCodeType("SUCCESSFUL");
                        return new ResponseEntity(detalle, HttpStatus.ACCEPTED);
                    }else {
                        detalle.setMensaje("Fallo al actualizar Personal Medico: "+personalActual.getNombre1Persona()+" "+personalActual.getApellido1Persona());
                        return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                    }
                }else {
                    detalle.setMensaje("Fallo al actualizar Medico.");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }
            }
        }
    }

    DetalleResponseHttp validarMedico(PersonalMedicoModel personal){
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        MedicoModel medico = personal.getMedico();
        detalle.setMensaje(null);

        if (personal.getJvp() == null){
            detalle.setMensaje("Debe ingresar el JVP.");
        }else {
            if (medico.getPacientesPorHora() == 0){
                detalle.setMensaje("Debe ingresar pacientes por hora.");
            }
        }
        return detalle;
    }
}
