package com.bad115.controller;

import com.bad115.model.*;
import com.bad115.service.DireccionService;
import com.bad115.service.PersonalMedicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/personalMedico")
public class PersonalMedicoController {
    @Autowired
    @Qualifier("PersonalMedicoService")
    PersonalMedicoService servicePer;

    @Autowired
    @Qualifier("DireccionService")
    DireccionService serviceDir;

    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_PersonalAll')")
    @GetMapping("/todos")
    public ResponseEntity listarTodosPersonalMedico(){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/personalMedico/todos");

        List<PersonalMedicoModel> personalMedico = servicePer.obtenerTodosPersonal();
        if(personalMedico.isEmpty()){
            detalle.setMensaje("No se encontraron miembros del personal.");
            detalle.setHttpCodeName("Personal no encontrado.");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity(personalMedico, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_PersonalId')")
    @GetMapping("/{idPersonal}")
    public ResponseEntity obtenerPersonalMedicoId(@PathVariable("idPersonal") UUID idPersonalMedico){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/personalMedico/"+idPersonalMedico);

        PersonalMedicoModel personalMedico = servicePer.obtenerPersonalMedicoId(idPersonalMedico);
        if (personalMedico==null){
            detalle.setMensaje("No se encontro al personal con nombre: "+ personalMedico.getNombre1Persona()+" "+personalMedico.getApellido1Persona());
            detalle.setHttpCodeName("Personal no encontrado.");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            return new  ResponseEntity(personalMedico,HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_PersonalAdd')")
    @PostMapping("/agregar")
    public ResponseEntity crearPersonalMedico(@RequestBody @Valid PersonalMedicoModel personal) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/personalMedico/agregar");
        detalle.setHttpCodeName("Fallo al agregar nuevo personal.");
        detalle.setHttpCodeNumber(409);
        detalle.setHttpCodeType("ERROR");

        UUID idDireccion = UUID.randomUUID();
        DireccionModel direccion = personal.getDireccion();
        direccion.setIdDireccion(idDireccion);

        DetalleResponseHttp validacion = validarDatos(personal);
        if (validacion.getMensaje() != null) {
            detalle.setMensaje(validacion.getMensaje());
            return new ResponseEntity(detalle, HttpStatus.CONFLICT);
        }else {
            Boolean estadoDir = serviceDir.guardarDireccion(direccion);
            if(estadoDir){
                Boolean estadoPer = servicePer.agregarPersonalMedico(personal);
                if (estadoPer) {
                    detalle.setMensaje("Se agrego al personal: "+personal.getNombre1Persona()+" "+personal.getApellido1Persona());
                    detalle.setHttpCodeName("Agregado con exito");
                    detalle.setHttpCodeNumber(201);
                    detalle.setHttpCodeType("SUCCESSFUL");
                    return new ResponseEntity(detalle, HttpStatus.CREATED);
                } else {
                    serviceDir.eliminarDireccion(idDireccion);
                    detalle.setMensaje("Fallo al ingresar Personal.");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }
            }else{
                detalle.setMensaje("Fallo al ingresar Dirección.");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_PersonalUpd')")
    @PutMapping("actualizar/{idPersonal}")
    public ResponseEntity actualizarPersonalMedico(@PathVariable("idPersonal") UUID idPersonal, @RequestBody @Valid PersonalMedicoModel personal){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/personalMedico/actualizar/"+idPersonal);
        detalle.setHttpCodeName("Fallo al actualizar personal");
        detalle.setHttpCodeNumber(409);
        detalle.setHttpCodeType("ERROR");

        PersonalMedicoModel personalActual = servicePer.obtenerPersonalMedicoId(idPersonal);
        if (personalActual==null){
            detalle.setMensaje("No se encontro, por lo tanto no se puede actualizar persona con id: "+idPersonal);
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            DetalleResponseHttp validacion = validarDatos(personal);
            if (validacion.getMensaje() != null) {
                detalle.setMensaje(validacion.getMensaje());
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }else {
                DireccionModel direccion = personal.getDireccion();
                Boolean estadoDir = serviceDir.guardarDireccion(direccion);
                if(estadoDir){
                    Boolean estadoPer = servicePer.actualizarPersonalMedico(personal);
                    if (estadoPer){
                        detalle.setMensaje("Se actualizo personal Medico: "+personalActual.getNombre1Persona()+" "+personalActual.getApellido1Persona());
                        detalle.setHttpCodeName("Se actualizo con exito");
                        detalle.setHttpCodeNumber(202);
                        detalle.setHttpCodeType("SUCCESSFUL");
                        return new ResponseEntity(detalle, HttpStatus.ACCEPTED);
                    }else {
                        detalle.setMensaje("Fallo al actualizar al Personal: "+personalActual.getNombre1Persona()+" "+personalActual.getApellido1Persona());
                        return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                    }
                }else {
                    detalle.setMensaje("Fallo al actualizar Dirección.");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_PersonalState')")
    @PutMapping("/estado/{idPersonalMedico}")
    public ResponseEntity cambiarEstadoPersonalMedico(@PathVariable("idPersonalMedico") UUID idPersonalMedico){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/personalMedico/estado/"+idPersonalMedico);

        PersonalMedicoModel personalMedico = servicePer.obtenerPersonalMedicoId(idPersonalMedico);
        if (personalMedico==null){
            detalle.setMensaje("Personal no encontrado, fallo al cambiar estado de "+personalMedico.getNombre1Persona()+" "+personalMedico.getApellido1Persona());
            detalle.setHttpCodeName("Personal no encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else {
            Boolean estadoPer = servicePer.estadoPersonalMedico(idPersonalMedico);
            if (estadoPer){
                detalle.setMensaje("Se cambio el estado del personal: "+personalMedico.getNombre1Persona()+" "+personalMedico.getApellido1Persona());
                detalle.setHttpCodeName("Cambio el estado con exito.");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            }else {
                detalle.setMensaje("Fallo al cambiar estado del personal.");
                detalle.setHttpCodeName("Error al cambiar estado");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }

    DetalleResponseHttp validarDatos(PersonalMedicoModel personal) {
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        DireccionModel direccionP = personal.getDireccion();
        detalle.setMensaje(null);

        if (direccionP.getUrbanizacion() == null) {
            detalle.setMensaje("Debe ingresar urbanizacion, colonia o barrio.");
        } else {
            DetalleResponseHttp valildarPer = validarPersonal(personal);
            if (valildarPer.getMensaje() == null) {
                detalle.setMensaje(valildarPer.getMensaje());
            } else {
                String fechaNacPaciente = (personal.getFechaNacimientoPersona());
                DateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date fecha = formato.parse(fechaNacPaciente);
                } catch (ParseException e) {
                    detalle.setMensaje("La fecha de nacimiento del personal no es valida.");
                }
            }
        }
        return detalle;
    }

    DetalleResponseHttp validarPersonal(PersonalMedicoModel responsable){
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setMensaje(null);

        if (responsable.getNombre1Persona() == null) {
            detalle.setMensaje("Debe ingresar el primer nombre.");
        }else {
            if (responsable.getApellido1Persona() == null) {
                detalle.setMensaje("Debe ingresar el primer apellido.");
            }else {
                if (responsable.getFechaNacimientoPersona() == null) {
                    detalle.setMensaje("Debe ingresar la fecha de nacimiento.");
                }else {
                    if (responsable.getSexoPersona() == null) {
                        detalle.setMensaje("Debe ingresar el sexo.");
                    }else{
                        if (responsable.getDuiPersona() == null) {
                            detalle.setMensaje("Debe ingresar el DUI.");
                        }else {
                            if (responsable.getNitPersona() == null) {
                                detalle.setMensaje("Debe ingresar el NIT.");
                            }else {
                                if (responsable.getJvp() == null){
                                    detalle.setMensaje("Debe ingresar el JVP.");
                                }else {
                                    if (responsable.getTipoPersonal() == null){
                                        detalle.setMensaje("Debe ingresar el Tipo de Personal.");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return detalle;
    }
}
