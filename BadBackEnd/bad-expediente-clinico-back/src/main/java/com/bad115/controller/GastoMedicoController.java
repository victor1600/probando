package com.bad115.controller;

import com.bad115.model.DetalleResponseHttp;
import com.bad115.model.GastoMedicoModel;
import com.bad115.model.ServicioModel;
import com.bad115.service.GastoMedicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/gastomed")
public class GastoMedicoController {

    @Autowired
    @Qualifier("GastoMedicoService")
    GastoMedicoService service;
    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_GasT')")
    @GetMapping("/todas")
    public ResponseEntity listartTodosGastos() {
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/gastos");
        List<GastoMedicoModel> GastoMedico = service.lista_activos();
        if (GastoMedico.isEmpty()) {
            detalle.setMensaje("Aun no se registran Gastos Medicos");
            detalle.setHttpCodeName("no se encontraron Gastos Medicos");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(GastoMedico, HttpStatus.FOUND);
        }
    }


    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_GasId')")
    @GetMapping("/{idGasto}")
    public ResponseEntity obtenerGastoId(@PathVariable("idGasto") UUID idGasto){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/gasto/"+ idGasto);

        GastoMedicoModel gasto = service.obtenerIdGastoMedico(idGasto);
        if (gasto==null){
            detalle.setMensaje("No se encontro el gasto con id: "+ idGasto);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else{
            return new ResponseEntity(gasto, HttpStatus.FOUND);
        }
    }


    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_GasAdd')")
    @PostMapping("/agregar")
    public ResponseEntity crearSGasto(@RequestBody @Valid GastoMedicoModel gasto){
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/gasto/agregar");
        UUID uuid = UUID.randomUUID();
        gasto.setIdGastoMedico(uuid);
        Boolean estado=service.agregarGastoMedico(gasto);
        if (estado==true){
            detalle.setMensaje("se agrego el gasto con fecha " + gasto.getFechaGasto());
            detalle.setHttpCodeName("Agregada con exito");
            detalle.setHttpCodeNumber(201);
            detalle.setHttpCodeType("SUCCESSFULL");
            return new ResponseEntity(detalle, HttpStatus.CREATED);
        }else {
            GastoMedicoModel gastoU = service.obtenerIdGastoMedico(gasto.getIdGastoMedico());
            if (gastoU==null){
                detalle.setMensaje("No se agrego el gasto "+ gasto.toString());
                detalle.setHttpCodeName("No se pudo Agregar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }else{
                detalle.setMensaje("El gasto con fecha : "+ gasto.getFechaGasto() + " ya existe");
                detalle.setHttpCodeName("Conflicto en el gasto");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('GasUpd')")
    @PutMapping("actualizar/{idGasto}")
    public ResponseEntity actualizarGasto(@PathVariable("idGasto") UUID idGasto, @RequestBody @Valid GastoMedicoModel gasto) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/actualizar/"+idGasto);
        GastoMedicoModel gastoA = service.obtenerIdGastoMedico(idGasto);
        if(gastoA==null){
            detalle.setMensaje("NO SE encontro el gasto con id: "+ idGasto);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        }else{
            gastoA.setFechaGasto(gasto.getFechaGasto());
            gastoA.setEstaActivo(gasto.getEstaActivo());
            gastoA.setConsulta(gasto.getConsulta());
            Boolean estado = service.actualizarGastoMedico(gastoA);
            if(estado==true){
                detalle.setMensaje("se actualizo el gasto" + gasto.getFechaGasto() +"\n "+ gasto.getConsulta()+"\n "+ gasto.getEstaActivo());
                detalle.setHttpCodeName("Actualizada con exito");
                detalle.setHttpCodeNumber(202);
                detalle.setHttpCodeType("SUCCESSFULL");
                return new ResponseEntity(detalle, HttpStatus.ACCEPTED);
            }else {
                GastoMedicoModel gastoU = service.obtenerIdGastoMedico(gasto.getIdGastoMedico());
                if (gastoU==null){
                    detalle.setMensaje("NO SE Actualizo El gasto "+ gasto.toString());
                    detalle.setHttpCodeName("No se pudo gasto");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeType("ERROR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }else{
                    detalle.setMensaje("El gasto ya existe con id "+ gasto.getIdGastoMedico());
                    detalle.setHttpCodeName("Conflicto en el nombre");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeType("ERROR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }
            }
        }
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_GasDel')")
    @DeleteMapping("eliminar/{idGasto}")
    public ResponseEntity eliminarGastoId(@PathVariable("idGasto") UUID idGasto) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/gasto/eliminar/" + idGasto);
        GastoMedicoModel servicio = service.obtenerIdGastoMedico(idGasto);
        if (servicio == null) {
            detalle.setMensaje("No se encontro el gasto, por lo tanto no se puede eliminar con id: " + idGasto);
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            Boolean estado = service.toggle(idGasto, false);// eliminarservicio(idGasto);
            if (estado == true) {
                detalle.setMensaje("Se elimino con exito el servicio " + servicio.getFechaGasto());
                detalle.setHttpCodeName("Eliminado con exito");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            } else {
                detalle.setMensaje("Error al eliminar servicio con id: " + idGasto);
                detalle.setHttpCodeName("Error al eliminar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }

}
