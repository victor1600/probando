package com.bad115.controller;

import com.bad115.model.DetalleResponseHttp;
import com.bad115.model.TipoExamenModel;
import com.bad115.service.TipoExamenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/tipoexamenes")
public class TipoExamenController {

    @Autowired
    @Qualifier("TipoExamenService")
    TipoExamenService service;
    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_tExamenT')")
    @GetMapping("/todas")
    public ResponseEntity listartTodosTipoExamenes() {
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tipoExamenes");
//        List<TipoExamenModel> TipoExamenes = service.obtenerTodosTipoExamenes();
        List<TipoExamenModel> TipoExamenes = service.lista_activos(); // obtenerTodosTipoExamenes();
        if (TipoExamenes.isEmpty()) {
            detalle.setMensaje("Aun no se registran tipos de examenes");
            detalle.setHttpCodeName("NO SE ENCONTRARON Tipos de examenes");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(TipoExamenes, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_tExamenId')")
    @GetMapping("/{idTipoExamen}")
    public ResponseEntity obtenerTipoExamenId(@PathVariable("idTipoExamen") UUID idTipoExamen) {
        Date date = new Date();

        DetalleResponseHttp detalle = new DetalleResponseHttp();

        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tipoExamenes/" + idTipoExamen);
        TipoExamenModel TipoExamen = service.obtenerTipoExamenId(idTipoExamen);
        if (TipoExamen == null) {
            detalle.setMensaje("No se encontro el tipo de medicamento con id: " + idTipoExamen);
            detalle.setHttpCodeName("Tipo de medicamento NO ENCONTRADO");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");

            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(TipoExamen, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_tExamenAdd')")
    @PostMapping("/agregar")
    public ResponseEntity crearTipoExamen(@RequestBody @Valid TipoExamenModel TipoExamen) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tipoExamenes/agregar");
        UUID uuid = UUID.randomUUID();
        TipoExamen.setIdTipoExamen(uuid);
        System.out.println(uuid);
        Boolean estado = service.agregarTipoExamen(TipoExamen);
        if (estado == true) {
            detalle.setMensaje("Se agrego el tipo de medicamento: " + TipoExamen.getNombreTipoExamen());
            detalle.setHttpCodeType("SUCCESSFUL");
            detalle.setHttpCodeNumber(201);
            detalle.setHttpCodeName("Agregado con exito");
            return new ResponseEntity(detalle, HttpStatus.CREATED);
        } else {
            TipoExamenModel TipoExamenU = service.obtenerTipoExamenNombre(TipoExamen.getNombreTipoExamen());
            if (TipoExamenU == null) {
                detalle.setMensaje("No se pudo agregar el tipo de medicamento: " + TipoExamen.toString());
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("NO SE PUDO AGREGAR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            } else {
                detalle.setMensaje("Ya existe el Nombre: " + TipoExamen.getNombreTipoExamen());
                detalle.setHttpCodeType("ERROR");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeName("CONFLICTO EN Nombre");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);

            }

        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_tExamenUpd')")
    @PutMapping("actualizar/{idTipoExamen}")
    public ResponseEntity actualizarTipoExamen(@PathVariable("idTipoExamen") UUID idTipoExamen, @RequestBody @Valid TipoExamenModel TipoExamen) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tipoExamenes/actualizar/" + idTipoExamen);
        TipoExamenModel TipoExamenA = service.obtenerTipoExamenId(idTipoExamen);
        if (TipoExamenA == null) {
            detalle.setMensaje("No se encontro, por lo tanto no se puede actualizar tipo de medicamento con id: " + idTipoExamen);
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            TipoExamenA.setNombreTipoExamen(TipoExamen.getNombreTipoExamen());
            Boolean estado = service.actualizarTipoExamen(TipoExamenA);
            if (estado == true) {
                //throw new OkMesaage("Se actualizo con exito la persona: "+persona.getNombreTipoExamenPersona()+" "+persona.getApellidoPersona());
                detalle.setMensaje("Se actualizo el tipo de medicamento: " + TipoExamen.getNombreTipoExamen());
                detalle.setHttpCodeName("Se actualizo con exito");
                detalle.setHttpCodeNumber(202);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.ACCEPTED);
            } else {
                TipoExamenModel TipoExamenU = service.obtenerTipoExamenNombre(TipoExamen.getNombreTipoExamen());
                if (TipoExamenU == null) {
                    detalle.setMensaje("No se pudo actualizar la persona: " + TipoExamen.toString());
                    detalle.setHttpCodeType("ERROR");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeName("NO SE PUDO ACTUALIZAR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                } else {
                    detalle.setMensaje("Ya existe la Nombre: " + TipoExamen.getNombreTipoExamen());
                    detalle.setHttpCodeType("ERROR");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeName("CONFLICTO EN Nombre");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }
            }
        }

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PreAuthorize("hasRole('ROLE_tExamenDel')")
    @DeleteMapping("eliminar/{idTipoExamen}")
    public ResponseEntity eliminarTipoExamenId(@PathVariable("idTipoExamen") UUID idTipoExamen) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/tipoExamenes/eliminar/" + idTipoExamen);
        TipoExamenModel TipoExamen = service.obtenerTipoExamenId(idTipoExamen);
        if (TipoExamen == null) {
            detalle.setMensaje("No se encontro, por lo tanto no se puede eliminar el tipo de medicamento con id: " + idTipoExamen);
            detalle.setHttpCodeName("No encontrado");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("Error");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            //Boolean estado = service.eliminarTipoExamen(idTipoExamen);
            Boolean estado = service.toggle(idTipoExamen, false);// eliminarTipoExamen(idTipoExamen);
            if (estado == true) {
                detalle.setMensaje("Se elimino con exito el tipo de medicamento " + TipoExamen.getNombreTipoExamen());
                detalle.setHttpCodeName("Eliminado con exito");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFUL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            } else {
                detalle.setMensaje("Error al eliminar tipo de medicamento con id: " + idTipoExamen);
                detalle.setHttpCodeName("Error al eliminar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            } // hey
        }
    }


}




