package com.bad115.controller;

import com.bad115.model.DetalleResponseHttp;
import com.bad115.model.ExamenModal;
import com.bad115.model.TipoExamenModel;
import com.bad115.service.ExamenService;
import com.bad115.service.TipoExamenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/examen")
public class ExamenController {
    @Autowired
    @Qualifier("ExamenService")
    ExamenService service;

    @Autowired
    @Qualifier("TipoExamenService")
    TipoExamenService service2;

    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/todas")
    public ResponseEntity listarTodasExamenes() {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/examen/todas");
        List<ExamenModal> examenes = service.obtenerTodasExamen();
        if (examenes.isEmpty()) {
            detalle.setMensaje("Examenes aun no se registrados");
            detalle.setHttpCodeName("no se encontraron examenes");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(examenes, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/{idExamen}")
    public ResponseEntity obtenerExamenId(@PathVariable("idExamen") UUID idExamen) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/examenes/" + idExamen);

        ExamenModal examen = service.obtenerExamenId(idExamen);
        if (examen == null) {
            detalle.setMensaje("NO SE encontro examen con id: " + idExamen);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(examen, HttpStatus.FOUND);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/agregar")
    public ResponseEntity crearExamen(@RequestBody @Valid ExamenModal examen) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/examen/agregar");
        UUID uuid = UUID.randomUUID();
        examen.setIdExamen(uuid);
        // prueba
        TipoExamenModel tipoExamen = service2.obtenerTipoExamenId(examen.getFkTipoExamen());
        examen.setTipoExamen(tipoExamen);
        Boolean estado = service.agregarExamen(examen);
        if (estado == true) {
            detalle.setMensaje("se agrego el examen" + examen.getDocumentoExamen()
                    + " " + examen.getImagenExamen()
                    + " " + examen.getVideoExamen());
            detalle.setHttpCodeName("Agregada con exito");
            detalle.setHttpCodeNumber(201);
            detalle.setHttpCodeType("SUCCESSFULL");
            return new ResponseEntity(detalle, HttpStatus.CREATED);
        } else {
            ExamenModal examenU = service.obtenerExamenId(examen.getIdExamen());
            if (examenU == null) {
                detalle.setMensaje("NO SE AGREGO el examen" + examen.toString());
                detalle.setHttpCodeName("No se pudo Agregar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            } else {
                detalle.setMensaje("El examen ya existe con id" + examen.getIdExamen());
                detalle.setHttpCodeName("Conflicto en nombre");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PutMapping("actualizar/{idExamen}")
    public ResponseEntity actualizarExamen(@PathVariable("idExamen") UUID idExamen, @RequestBody @Valid ExamenModal examen) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/examen/actualizar/" + idExamen);
        ExamenModal examenA = service.obtenerExamenId(idExamen);
        if (examenA == null) {
            detalle.setMensaje("NO SE encontro el examen con id: " + idExamen);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            examenA.setDocumentoExamen(examen.getDocumentoExamen());
            examenA.setImagenExamen(examen.getImagenExamen());
            examenA.setVideoExamen(examen.getVideoExamen());
            // TODO: ARREGLAR
            TipoExamenModel tipoExamen = service2.obtenerTipoExamenId(examen.getFkTipoExamen());
            examenA.setTipoExamen(tipoExamen);
//            examenA.setIdTipoExamen(examen.getIdTipoExamen());
            Boolean estado = service.actualizarExamen(examenA);
            if (estado == true) {
                detalle.setMensaje("se actualizo el examen" + examen.getDocumentoExamen()
                        + " " + examen.getImagenExamen()
                        + " " + examen.getVideoExamen());
                detalle.setHttpCodeName("Actualizada con exito");
                detalle.setHttpCodeNumber(202);
                detalle.setHttpCodeType("SUCCESSFULL");
                return new ResponseEntity(detalle, HttpStatus.ACCEPTED);
            } else {
                ExamenModal examenU = service.obtenerExamenId(examen.getIdExamen());
                if (examenU == null) {
                    detalle.setMensaje("NO SE Actualizo LA examen" + examen.toString());
                    detalle.setHttpCodeName("No se pudo Actualizar");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeType("ERROR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                } else {
                    detalle.setMensaje("LA examen ya existe con id" + examen.getIdExamen());
                    detalle.setHttpCodeName("Conflicto en id");
                    detalle.setHttpCodeNumber(409);
                    detalle.setHttpCodeType("ERROR");
                    return new ResponseEntity(detalle, HttpStatus.CONFLICT);
                }
            }
        }

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @DeleteMapping("eliminar/{idExamen}")
    public ResponseEntity eliminarExamen(@PathVariable("idExamen") UUID idExamen) {
        Date date = new Date();
        DetalleResponseHttp detalle = new DetalleResponseHttp();
        detalle.setFecha(fechaFormat.format(date));
        detalle.setUrl("/examen/eliminar/" + idExamen);
        ExamenModal examen = service.obtenerExamenId(idExamen);
        if (examen == null) {
            detalle.setMensaje("NO se puede eliminar porque no se encontro LA examen con id: " + idExamen);
            detalle.setHttpCodeName("No existe el id");
            detalle.setHttpCodeNumber(404);
            detalle.setHttpCodeType("ERROR");
            return new ResponseEntity(detalle, HttpStatus.NOT_FOUND);
        } else {
            Boolean estado = service.eliminarExamen(idExamen);
            if (estado == true) {
                detalle.setMensaje("se elimino con exito el examen" + examen.getIdExamen());
                detalle.setHttpCodeName("Eliminado con exito");
                detalle.setHttpCodeNumber(200);
                detalle.setHttpCodeType("SUCCESSFULL");
                return new ResponseEntity(detalle, HttpStatus.OK);
            } else {
                detalle.setMensaje("NO SE Elimnino LA examen con id" + idExamen);
                detalle.setHttpCodeName("No se pudo eliminar");
                detalle.setHttpCodeNumber(409);
                detalle.setHttpCodeType("ERROR");
                return new ResponseEntity(detalle, HttpStatus.CONFLICT);
            }
        }
    }


}
