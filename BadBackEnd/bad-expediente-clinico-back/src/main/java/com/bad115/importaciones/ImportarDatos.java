package com.bad115.importaciones;

import com.bad115.model.DetalleResponseHttp;
import com.bad115.model.RolModel;
import com.bad115.model.UsuarioModel;
import com.bad115.repository.RolRepository;
import com.bad115.repository.UsuarioRepositry;
import com.bad115.repositoryPersonalizado.UsuarioSqlPersonalizado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/importaciones")
public class ImportarDatos {

    @Autowired
    @Qualifier("RolRepository")
    RolRepository repository;

    @Autowired
    @Qualifier("UsuarioRepository")
    UsuarioRepositry repository2;

    @Autowired
    @Qualifier("UsuarioSqlPersonalizado")
    UsuarioSqlPersonalizado repository3;

    BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");



    @PreAuthorize("hasRole('ROLE_SEED')")
    @GetMapping("/todas")
    public ResponseEntity importaRoles() {

        //RolesNuevos
        List<RolModel> roles = new ArrayList<>();

        //permiso Cristian

        //Permisos de Cie10
        RolModel rolCie01 = new RolModel(UUID.randomUUID(),"CieT","Permite a ver toda la lista de Cie10");
        roles.add(rolCie01);
        RolModel rolCie02 = new RolModel(UUID.randomUUID(),"CieId","Permite a ver un Cie10 por Id");
        roles.add(rolCie02);
        RolModel rolCie03 = new RolModel(UUID.randomUUID(),"CieAdd","Permite agregar un cie10");
        roles.add(rolCie03);
        RolModel rolCie04 = new RolModel(UUID.randomUUID(),"CieUpd","Permite actualizar por Id un Cie10");
        roles.add(rolCie04);
        RolModel rolCie05 = new RolModel(UUID.randomUUID(),"CieDel","Permite eliminar por Id un Cie10");
        roles.add(rolCie05);

        //Permisos Especialidad
        RolModel rolEspe01 = new RolModel(UUID.randomUUID(),"EspeT","Permite a ver toda la lista de especialidades");
        roles.add(rolEspe01);
        RolModel rolEspe02 = new RolModel(UUID.randomUUID(),"EspeId","Permite a ver un especialidades por Id");
        roles.add(rolEspe02);
        RolModel rolEspe03 = new RolModel(UUID.randomUUID(),"EspeAdd","Permite agregar una especialidad");
        roles.add(rolEspe03);
        RolModel rolEspe04 = new RolModel(UUID.randomUUID(),"EspeUpd","Permite actualizar por Id una especialidades");
        roles.add(rolEspe04);
        RolModel rolEspe05 = new RolModel(UUID.randomUUID(),"EspeDel","Permite eliminar por Id una especialidades");
        roles.add(rolEspe05);

        //Permisos Signos Vitales
        RolModel rolSV01 = new RolModel(UUID.randomUUID(),"SVT","Permite a ver toda los signos vitales");
        roles.add(rolSV01);
        RolModel rolSV02 = new RolModel(UUID.randomUUID(),"SVId","Permite a ver un signos vitales por Id");
        roles.add(rolSV02);
        RolModel rolSV03 = new RolModel(UUID.randomUUID(),"SVAdd","Permite agregar una signos vitales");
        roles.add(rolSV03);
        RolModel rolSV04 = new RolModel(UUID.randomUUID(),"SVUpd","Permite actualizar por Id una signos vitales");
        roles.add(rolSV04);
        RolModel rolSV05 = new RolModel(UUID.randomUUID(),"SVDel","Permite eliminar por Id una signos vitales");
        roles.add(rolSV05);

        //Permisos Expediente
        RolModel rolExp01 = new RolModel(UUID.randomUUID(),"ExpT","Permite a ver toda los expediente");
        roles.add(rolExp01);
        RolModel rolExp02 = new RolModel(UUID.randomUUID(),"ExpId","Permite a ver los expediente por Id");
        roles.add(rolExp02);
        RolModel rolExp03 = new RolModel(UUID.randomUUID(),"ExpAdd","Permite agregar expediente");
        roles.add(rolExp03);

        //Permisos Servicio
        RolModel rolServ01 = new RolModel(UUID.randomUUID(),"ServT","Permite a ver todos los sevicios");
        roles.add(rolServ01);
        RolModel rolServ02 = new RolModel(UUID.randomUUID(),"ServId","Permite a ver los sevicios por Id");
        roles.add(rolServ02);
        RolModel rolServ03 = new RolModel(UUID.randomUUID(),"ServAdd","Permite agregar ssevicios");
        roles.add(rolServ03);
        RolModel rolServ04 = new RolModel(UUID.randomUUID(),"ServUpd","Permite actualizar los sevicios");
        roles.add(rolServ04);
        RolModel rolServ05 = new RolModel(UUID.randomUUID(),"ServDel","Permite cambiar de estado los sevicios");
        roles.add(rolServ05);

        //Permisos ServicioGastos
        RolModel rolServGasv01 = new RolModel(UUID.randomUUID(),"ServGasT","Permite a ver todos los sevicios");
        roles.add(rolServGasv01);
        RolModel rolServGas02 = new RolModel(UUID.randomUUID(),"ServGasId","Permite a ver los sevicios por Id");
        roles.add(rolServGas02);
        RolModel rolServGas03 = new RolModel(UUID.randomUUID(),"ServGasAdd","Permite agregar ssevicios");
        roles.add(rolServGas03);

        //Permisos Gastos Medicos
        RolModel rolGas01 = new RolModel(UUID.randomUUID(),"GasT","Permite a ver todos los gastos");
        roles.add(rolGas01);
        RolModel rolGas02 = new RolModel(UUID.randomUUID(),"GasId","Permite a ver los gastos por Id");
        roles.add(rolGas02);
        RolModel rolGas03 = new RolModel(UUID.randomUUID(),"GasAdd","Permite agregar sgastos");
        roles.add(rolGas03);
        RolModel rolGas04 = new RolModel(UUID.randomUUID(),"GasUpd","Permite actualizar los gastos");
        roles.add(rolGas04);
        RolModel rolGas05 = new RolModel(UUID.randomUUID(),"GasDel","Permite cambiar de estado los gastos");
        roles.add(rolGas05);

        //Permisos Asuetos
        RolModel rolAsu01 = new RolModel(UUID.randomUUID(),"AsuT","Permite a ver todos los Asutos");
        roles.add(rolAsu01);
        RolModel rolAsu02 = new RolModel(UUID.randomUUID(),"AsuId","Permite a ver los Asutos por Id");
        roles.add(rolAsu02);
        RolModel rolAsu03 = new RolModel(UUID.randomUUID(),"AsuAdd","Permite agregar sAsutos");
        roles.add(rolAsu03);
        RolModel rolAsu04 = new RolModel(UUID.randomUUID(),"AsuUpd","Permite actualizar los Asutos");
        roles.add(rolAsu04);
        RolModel rolAsu05 = new RolModel(UUID.randomUUID(),"AsuDel","Permite cambiar de estado los Asutos");
        roles.add(rolAsu05);

        //Permisos AsgHoretos
        RolModel rolAsgHor01 = new RolModel(UUID.randomUUID(),"AsgHorT","Permite a ver todos los AsgHortos");
        roles.add(rolAsgHor01);
        RolModel rolAsgHor02 = new RolModel(UUID.randomUUID(),"AsgHorId","Permite a ver los AsgHortos por Id");
        roles.add(rolAsgHor02);
        RolModel rolAsgHor03 = new RolModel(UUID.randomUUID(),"AsgHorAdd","Permite agregar sAsgHortos");
        roles.add(rolAsgHor03);
        RolModel rolAsgHor04 = new RolModel(UUID.randomUUID(),"AsgHorUpd","Permite actualizar los AsgHortos");
        roles.add(rolAsgHor04);
        RolModel rolAsgHor05 = new RolModel(UUID.randomUUID(),"AsgHorDel","Permite cambiar de estado los AsgHortos");
        roles.add(rolAsgHor05);

        //Permisos Horarioetos
        RolModel rolHorario01 = new RolModel(UUID.randomUUID(),"HorarioT","Permite a ver todos los Horariotos");
        roles.add(rolHorario01);
        RolModel rolHorario02 = new RolModel(UUID.randomUUID(),"HorarioId","Permite a ver los Horariotos por Id");
        roles.add(rolHorario02);
        RolModel rolHorario03 = new RolModel(UUID.randomUUID(),"HorarioAdd","Permite agregar sHorariotos");
        roles.add(rolHorario03);
        RolModel rolHorario04 = new RolModel(UUID.randomUUID(),"HorarioUpd","Permite actualizar los Horariotos");
        roles.add(rolHorario04);
        RolModel rolHorario05 = new RolModel(UUID.randomUUID(),"HorarioDel","Permite cambiar de estado los Horariotos");
        roles.add(rolHorario05);


        //Permisos Gastos de Servicio Medico
        RolModel rolGasServ01 = new RolModel(UUID.randomUUID(),"GasServT","Permite a ver todos los Gastos de Servicio Medico");
        roles.add(rolGasServ01);
        RolModel rolGasServ02 = new RolModel(UUID.randomUUID(),"GasServId","Permite a ver los Gastos de Servicio Medico por Id");
        roles.add(rolGasServ02);
        RolModel rolGasServ03 = new RolModel(UUID.randomUUID(),"GasServAdd","Permite agregar sGastos de Servicio Medico");
        roles.add(rolGasServ03);
        RolModel rolGasServ04 = new RolModel(UUID.randomUUID(),"GasServUpd","Permite actualizar los Gastos de Servicio Medico");
        roles.add(rolGasServ04);
        RolModel rolGasServ05 = new RolModel(UUID.randomUUID(),"GasServDel","Permite cambiar de estado los Gastos de Servicio Medico");
        roles.add(rolGasServ05);



        //Permisos Examen
        RolModel rolExam01 = new RolModel(UUID.randomUUID(),"ExamT","Permite a ver toda los examenes");
        roles.add(rolExam01);
        RolModel rolExam02 = new RolModel(UUID.randomUUID(),"ExamId","Permite a ver un examenes por Id");
        roles.add(rolExam02);
        RolModel rolExam03 = new RolModel(UUID.randomUUID(),"ExamAdd","Permite agregar una examenes");
        roles.add(rolExam03);
        RolModel rolExam04 = new RolModel(UUID.randomUUID(),"ExamUpd","Permite actualizar por Id una examenes");
        roles.add(rolExam04);
        RolModel rolExam05 = new RolModel(UUID.randomUUID(),"ExamDel","Permite eliminar por Id una examenes");
        roles.add(rolExam05);


        // PERMISOS VICTOR

        //tipo medicamento
        RolModel rolTipoMed01 = new RolModel(UUID.randomUUID(),"TipoMT","Permite ver toda la lista de tipo de medicamentos");
        roles.add(rolTipoMed01);
        RolModel rolTipoMed02 = new RolModel(UUID.randomUUID(),"TipoMId","Permite ver tipo medicamentos por id");
        roles.add(rolTipoMed02);
        RolModel rolTipoMed03 = new RolModel(UUID.randomUUID(),"TipoMAdd","Permite agregar un tipo de medicamento");
        roles.add(rolTipoMed03);
        RolModel rolTipoMed04 = new RolModel(UUID.randomUUID(),"TipoMUpd","Permite actualizar un tipo de medicamento");
        roles.add(rolTipoMed04);
        RolModel rolTipoMed05 = new RolModel(UUID.randomUUID(),"TipoMDel","Permite eliminar un tipo de medicamento");
        roles.add(rolTipoMed05);

        // medicamentos
        RolModel rolMed01 = new RolModel(UUID.randomUUID(),"MedT","Permite ver todos los medicamentos");
        roles.add(rolMed01);
        RolModel rolMed02 = new RolModel(UUID.randomUUID(),"MedId","Permite ver medicamentos por id");
        roles.add(rolMed02);
        RolModel rolMed03 = new RolModel(UUID.randomUUID(),"MedAdd","Permite agregar medicamentos");
        roles.add(rolMed03);
        RolModel rolMed04 = new RolModel(UUID.randomUUID(),"MedUpd","Permite actualizar medicamentos");
        roles.add(rolMed04);
        RolModel rolMed05 = new RolModel(UUID.randomUUID(),"MedDel","Permite borar medicamentos por");
        roles.add(rolMed05);

        // Dosis
        RolModel rolDosis01 = new RolModel(UUID.randomUUID(),"dosisT","Permite ver todas las dosis");
        roles.add(rolDosis01);
        RolModel rolDosis02 = new RolModel(UUID.randomUUID(),"dosisId","Permite ver  las dosis por id");
        roles.add(rolDosis02);
        RolModel rolDosis03 = new RolModel(UUID.randomUUID(),"dosisAdd","Permite agregar las dosis");
        roles.add(rolDosis03);
        RolModel rolDosis04 = new RolModel(UUID.randomUUID(),"dosisUpd","Permite actualizar las dosis");
        roles.add(rolDosis04);
        RolModel rolDosis05 = new RolModel(UUID.randomUUID(),"dosisDel","Permite eliminar las dosis");
        roles.add(rolDosis05);

        // tratamiento
        RolModel rolTrat01 = new RolModel(UUID.randomUUID(),"tratT","Permite ver todos los tratamientos");
        roles.add(rolTrat01);
        RolModel rolTrat02 = new RolModel(UUID.randomUUID(),"tratId","Permite ver por id un tratamiento");
        roles.add(rolTrat02);
        RolModel rolTrat03 = new RolModel(UUID.randomUUID(),"tratAdd","Permite agregar un tratamiento");
        roles.add(rolTrat03);
        RolModel rolTrat04 = new RolModel(UUID.randomUUID(),"tratUpd","Permite actualizar un tratamiento");
        roles.add(rolTrat04);
        RolModel rolTrat05 = new RolModel(UUID.randomUUID(),"tratDel","Permite eliminar un tratamiento");
        roles.add(rolTrat05);

        // Tipo Cita
        RolModel rolTCita01 = new RolModel(UUID.randomUUID(),"tCitaT","Permite ver todos los tipos de citas");
        roles.add(rolTCita01);
        RolModel rolTCita02 = new RolModel(UUID.randomUUID(),"tCitaId","Permite ver un tipo de cita por id");
        roles.add(rolTCita02);
        RolModel rolTCita03 = new RolModel(UUID.randomUUID(),"tCitaAdd","Permite agregar un tipo de cita");
        roles.add(rolTCita03);
        RolModel rolTCita04 = new RolModel(UUID.randomUUID(),"tCitaUpd","Permite actualizar un tipo de cita");
        roles.add(rolTCita04);
        RolModel rolTCita05 = new RolModel(UUID.randomUUID(),"tCitaDel","Permite eliminar un tipo de cita");
        roles.add(rolTCita05);

        // Cita

        RolModel rolCita01 = new RolModel(UUID.randomUUID(),"citaT","Permite ver todas las citas");
        roles.add(rolCita01);
        RolModel rolCita02 = new RolModel(UUID.randomUUID(),"citaId","Permite ver citas por id");
        roles.add(rolCita02);
        RolModel rolCita03 = new RolModel(UUID.randomUUID(),"citaAdd","Permite agregar una cita");
        roles.add(rolCita03);
        RolModel rolCita04 = new RolModel(UUID.randomUUID(),"citaUpd","Permite actualizar una cita");
        roles.add(rolCita04);
        RolModel rolCita05 = new RolModel(UUID.randomUUID(),"citaDel","Permite eliminar una cita");
        roles.add(rolCita05);

        // tipoExamen
        RolModel rolTipoExamen01 = new RolModel(UUID.randomUUID(),"tExamenT","Permite ver todos los examenes");
        roles.add(rolTipoExamen01);
        RolModel rolTipoExamen02 = new RolModel(UUID.randomUUID(),"tExamenId","Permite ver examen por id");
        roles.add(rolTipoExamen02);
        RolModel rolTipoExamen03 = new RolModel(UUID.randomUUID(),"tExamenAdd","Permite agregar un examen");
        roles.add(rolTipoExamen03);
        RolModel rolTipoExamen04 = new RolModel(UUID.randomUUID(),"tExamenUpd","Permite actualizar un examen");
        roles.add(rolTipoExamen04);
        RolModel rolTipoExamen05 = new RolModel(UUID.randomUUID(),"tExamenDel","Permite eliminar un examen");
        roles.add(rolTipoExamen05);

        // BoletaExamen
        RolModel rolBoletaExamen01 = new RolModel(UUID.randomUUID(),"bolExamenT","Permite ver todas las boletas");
        roles.add(rolBoletaExamen01);
        RolModel rolBoletaExamen02 = new RolModel(UUID.randomUUID(),"bolExamenId","Permite ver boletas por id");
        roles.add(rolBoletaExamen02);
        RolModel rolBoletaExamen03 = new RolModel(UUID.randomUUID(),"bolExamenAdd","Permite agregar una boleta");
        roles.add(rolBoletaExamen03);
        RolModel rolBoletaExamen04 = new RolModel(UUID.randomUUID(),"bolExamenUpd","Permite actualizar una boleta");
        roles.add(rolBoletaExamen04);
        RolModel rolBoletaExamen05 = new RolModel(UUID.randomUUID(),"bolExamenDel","Permite eliminar una boleta");
        roles.add(rolBoletaExamen05);

        //PERMISOS SOFIA

        //permisos cirugia
        RolModel rolCirugia01 = new RolModel(UUID.randomUUID(),"cirugiaT","Permite ver todas las cirugias");
        roles.add(rolCirugia01);
        RolModel rolCirugia02 = new RolModel(UUID.randomUUID(),"cirugiaId","Permite ver cirugias por id");
        roles.add(rolCirugia02);
        RolModel rolCirugia03 = new RolModel(UUID.randomUUID(),"cirugiaAdd","Permite agregar una cirugias");
        roles.add(rolCirugia03);
        RolModel rolCirugia04 = new RolModel(UUID.randomUUID(),"cirugiaUpd","Permite actualizar una cirugia");
        roles.add(rolCirugia04);
        RolModel rolCirugia05 = new RolModel(UUID.randomUUID(),"cirugiaDel","Permite eliminar una cirugia");
        roles.add(rolCirugia05);

        //permisos BoletaCirugia
        RolModel rolBC01 = new RolModel(UUID.randomUUID(),"BoletacirugiaT","Permite ver todas las boletas de cirugias");
        roles.add(rolBC01);
        RolModel rolBC02 = new RolModel(UUID.randomUUID(),"BoletacirugiaId","Permite ver las boletas de cirugias por id");
        roles.add(rolBC02);
        RolModel rolBC03 = new RolModel(UUID.randomUUID(),"BoletacirugiaAdd","Permite agregar una boleta cirugias");
        roles.add(rolBC03);
        RolModel rolBC04 = new RolModel(UUID.randomUUID(),"BoletacirugiaUpd","Permite actualizar una boleta de cirugia");
        roles.add(rolBC04);
        RolModel rolBC05 = new RolModel(UUID.randomUUID(),"BoletacirugiaDel","Permite eliminar una boleta de cirugia");
        roles.add(rolBC05);

        //Permisos Area
        RolModel rolArea01 = new RolModel(UUID.randomUUID(),"areaT","Permite ver todas las areas");
        roles.add(rolArea01);
        RolModel rolArea02 = new RolModel(UUID.randomUUID(),"AreaId","Permite ver areas por id");
        roles.add(rolArea02);
        RolModel rolArea03 = new RolModel(UUID.randomUUID(),"AreaAdd","Permite agregar una Area");
        roles.add(rolArea03);
        RolModel rolArea04 = new RolModel(UUID.randomUUID(),"AreaUpd","Permite actualizar una Area");
        roles.add(rolArea04);
        RolModel rolArea05 = new RolModel(UUID.randomUUID(),"AreaDel","Permite eliminar una Area");
        roles.add(rolArea05);

        //Permisos Ingreso
        RolModel rolIngreso01 = new RolModel(UUID.randomUUID(),"IngresoT","Permite ver todos los ingresos");
        roles.add(rolIngreso01);
        RolModel rolIngreso02 = new RolModel(UUID.randomUUID(),"IngresoId","Permite ver los ingresos por id");
        roles.add(rolIngreso02);
        RolModel rolIngreso03 = new RolModel(UUID.randomUUID(),"IngresoAdd","Permite agregar un ingreso");
        roles.add(rolIngreso03);
        RolModel rolIngreso04 = new RolModel(UUID.randomUUID(),"IngresoUpd","Permite actualizar un ingreso");
        roles.add(rolIngreso04);
        RolModel rolIngreso05 = new RolModel(UUID.randomUUID(),"IngresoDel","Permite eliminar un ingreso");
        roles.add(rolIngreso05);

        //Permisos Cama
        RolModel rolCama01 = new RolModel(UUID.randomUUID(),"CamaT","Permite ver todos las camas");
        roles.add(rolCama01);
        RolModel rolCama02 = new RolModel(UUID.randomUUID(),"CamaId","Permite ver las camas por id");
        roles.add(rolCama02);
        RolModel rolCama03 = new RolModel(UUID.randomUUID(),"CamaAdd","Permite agregar una cama");
        roles.add(rolCama03);
        RolModel rolCama04 = new RolModel(UUID.randomUUID(),"CamaUpd","Permite actualizar cama");
        roles.add(rolCama04);
        RolModel rolCama05 = new RolModel(UUID.randomUUID(),"CamaDel","Permite eliminar cama");
        roles.add(rolCama05);

        // WALTER

        //Permisos Paciente
        RolModel rolPaciente01 = new RolModel(UUID.randomUUID(),"PacienteAll","Permite ver todos los Pacientes.");
        roles.add(rolPaciente01);
        RolModel rolPaciente02 = new RolModel(UUID.randomUUID(),"PacienteId","Permite ver un Paciente por Id.");
        roles.add(rolPaciente02);
        RolModel rolPaciente03 = new RolModel(UUID.randomUUID(),"PacienteAdd","Permite agregar un Paciente.");
        roles.add(rolPaciente03);
        RolModel rolPaciente04 = new RolModel(UUID.randomUUID(),"PacienteUpd","Permite actualizar un Paciente.");
        roles.add(rolPaciente04);
        RolModel rolPaciente05 = new RolModel(UUID.randomUUID(),"PacienteState","Permite actualizar el estado del Paciente.");
        roles.add(rolPaciente05);

        //Permisos Responsable
        RolModel rolResponsable01 = new RolModel(UUID.randomUUID(),"ResponsableAll","Permite ver todos los Responsables.");
        roles.add(rolResponsable01);
        RolModel rolResponsable02 = new RolModel(UUID.randomUUID(),"ResponsableId","Permite ver un Responsable por Id.");
        roles.add(rolResponsable02);
        RolModel rolResponsable03 = new RolModel(UUID.randomUUID(),"ResponsableUpd","Permite actualizar un Responsable.");
        roles.add(rolResponsable03);

        //Permisos Personal
        RolModel rolPersonal01 = new RolModel(UUID.randomUUID(),"PersonalAll","Permite ver todo el Personal.");
        roles.add(rolPersonal01);
        RolModel rolPersonal02 = new RolModel(UUID.randomUUID(),"PersonalId","Permite ver un miembro del Personal por Id.");
        roles.add(rolPersonal02);
        RolModel rolPersonal03 = new RolModel(UUID.randomUUID(),"PersonalAdd","Permite agregar un miembro al Personal.");
        roles.add(rolPersonal03);
        RolModel rolPersonal04 = new RolModel(UUID.randomUUID(),"PersonalUpd","Permite actualizar un miembro del Personal.");
        roles.add(rolPersonal04);
        RolModel rolPersonal05 = new RolModel(UUID.randomUUID(),"PersonalState","Permite actualizar estado del Personal.");
        roles.add(rolPersonal05);

        // Tipo Personal
        RolModel rolTipoPersonal01 = new RolModel(UUID.randomUUID(),"TipoPersonalAll","Permite ver todos los Tipos de Personal.");
        roles.add(rolTipoPersonal01);
        RolModel rolTipoPersonal02 = new RolModel(UUID.randomUUID(),"TipoPersonalId","Permite  ver un Tipo de Personal por Id.");
        roles.add(rolTipoPersonal02);
        RolModel rolTipoPersonal03 = new RolModel(UUID.randomUUID(),"TipoPersonalAdd","Permite agregar Tipo de Personal.");
        roles.add(rolTipoPersonal03);
        RolModel rolTipoPersonal04 = new RolModel(UUID.randomUUID(),"TipoPersonalUpd","Permite actualizar un Tipo de Personal.");
        roles.add(rolTipoPersonal04);
        RolModel rolTipoPersonal05 = new RolModel(UUID.randomUUID(),"TipoPersonalDel","Permite eliminar un Tipo de Personal.");
        roles.add(rolTipoPersonal05);


        UsuarioModel usuarioAdmin = new UsuarioModel();
        usuarioAdmin.setIdUsuario(UUID.randomUUID());
        usuarioAdmin.setNombreUsuario("admin");
        usuarioAdmin.setClaveUsuario(bCryptPasswordEncoder.encode("admin1234"));
        Set<RolModel> rolesT =new HashSet<>(roles);
        usuarioAdmin.setRoles(rolesT);
        System.out.println(usuarioAdmin);
        repository2.save(usuarioAdmin);


        return new ResponseEntity(usuarioAdmin, HttpStatus.OK);

    }


}
