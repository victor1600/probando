package com.bad115.configuration.mapper;

import com.bad115.configuration.web.AuthorizationRequest;
import com.bad115.configuration.web.UserResponse;
import com.bad115.model.UsuarioModel;

public class UserMapper {

    private UserMapper() {
    }

    public static UserResponse toResponse(UsuarioModel usuarioModel) {
        return UserResponse.builder().name(usuarioModel.getNombreUsuario()).id(usuarioModel.getIdUsuario()).build();
    }

    public static UsuarioModel toDomain(AuthorizationRequest authorizationRequest) {
        return UsuarioModel.builder().nombreUsuario(authorizationRequest.getUserName()).claveUsuario(authorizationRequest.getPassword())
                .build();
    }
}
