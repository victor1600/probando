package com.bad115.configuration;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JwtAuthenticationFailureHandler implements AuthenticationFailureHandler {

    private final HttpStatus statusErrorResponse;

    public JwtAuthenticationFailureHandler(HttpStatus statusErrorResponse) {
        this.statusErrorResponse = statusErrorResponse;
    }

    public JwtAuthenticationFailureHandler() {
        this.statusErrorResponse = HttpStatus.UNAUTHORIZED;
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                                        AuthenticationException e) throws IOException, ServletException {
        response.setStatus(statusErrorResponse.value());
        response.setContentType("application/json");
        response.getWriter().append(jsonResponse());
    }

    private String jsonResponse() {
        DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        Date date = new Date();
        return
                "{\n" +
                        "\"fecha\": "+ "\""+ fechaFormat.format(date)+"\",\n" +
                        "\"mensaje\": \"Login Conflicto, malas credenciales\",\n" +
                        "\"url\": \"/login\",\n" +
                        "\"httpCodeName\": \"LOGIN ERROR\",\n" +
                        "\"httpCodeNumber\": 401,\n" +
                        "\"httpCodeType\": \"ERROR\"\n" +
                        "}";
    }
}

