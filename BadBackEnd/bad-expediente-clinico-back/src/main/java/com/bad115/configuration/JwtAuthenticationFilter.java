package com.bad115.configuration;
import static com.bad115.configuration.util.Constants.HEADER_AUTHORIZATION_KEY;
import static com.bad115.configuration.util.Constants.TOKEN_BEARER_PREFIX;

import com.bad115.configuration.util.TokenProvider;
import com.bad115.configuration.web.AuthorizationRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;
    DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");


    public JwtAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
        super.setAuthenticationFailureHandler(new JwtAuthenticationFailureHandler());
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        try {
            AuthorizationRequest userCredentials = new ObjectMapper().readValue(request.getInputStream(), AuthorizationRequest.class);

            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    userCredentials.getUserName(), userCredentials.getPassword()));
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {

        String token = TokenProvider.generateToken(authResult);
        String tokenU = TOKEN_BEARER_PREFIX + " " + token;
        response.addHeader(HEADER_AUTHORIZATION_KEY, tokenU);
        DateFormat fechaFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        Date date = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.SECOND,28800);

        String respuesta =
                "{\n" +
                        "\"fecha\": "+ "\""+ fechaFormat.format(date)+"\",\n" +
                        "\"mensaje\": \"Login con exito\",\n" +
                        "\"url\": \"/login\",\n" +
                        "\"httpCodeName\": \"LOGIN EXITOSO\",\n" +
                        "\"httpCodeNumber\": 200,\n" +
                        "\"tokenTiempo\": "+ "\""+ fechaFormat.format(calendar.getTime())+"\",\n" +
                        "\"tokenUsuario\": "+ "\""+ tokenU+"\",\n" +
                        "\"httpCodeType\": \"SUCCESSFUL\"\n" +
                        "}";
        response.getWriter().append(respuesta);
    }

}
