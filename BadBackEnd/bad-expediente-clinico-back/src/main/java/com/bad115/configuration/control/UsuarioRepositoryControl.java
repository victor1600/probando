package com.bad115.configuration.control;

import com.bad115.model.UsuarioModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
public interface UsuarioRepositoryControl extends JpaRepository<UsuarioModel, UUID> {

    UsuarioModel findByNombreUsuario(String nombre);

    UsuarioModel findByIdUsuario(UUID id);

    List<UsuarioModel> findAll();
}