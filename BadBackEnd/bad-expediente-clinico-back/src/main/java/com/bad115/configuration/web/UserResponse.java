package com.bad115.configuration.web;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
public class UserResponse {


    private UUID id;

    private String name;

}