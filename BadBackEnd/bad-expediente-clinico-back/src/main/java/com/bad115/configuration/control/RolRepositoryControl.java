package com.bad115.configuration.control;

import com.bad115.model.RolModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public interface RolRepositoryControl extends JpaRepository<RolModel, UUID> {

    RolModel getRolByIdRol(UUID id);

    RolModel findByNombreRol(String nombre);
}