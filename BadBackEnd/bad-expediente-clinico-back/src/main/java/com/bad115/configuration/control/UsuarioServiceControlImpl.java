package com.bad115.configuration.control;

import com.bad115.configuration.mapper.UserDetailsMapper;
import com.bad115.model.RolModel;
import com.bad115.model.UsuarioModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Service("userDetailsService")
public class UsuarioServiceControlImpl implements UsuarioServiceControl {

    private RolRepositoryControl rolRepositoryControl;

    private UsuarioRepositoryControl usuarioRepositoryControl;

    @Autowired
    public UsuarioServiceControlImpl(UsuarioRepositoryControl usuarioRepositoryControl, RolRepositoryControl rolRepositoryControl) {
        this.usuarioRepositoryControl = usuarioRepositoryControl;
        this.rolRepositoryControl = rolRepositoryControl;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        final UsuarioModel retrievedUsuarioModel = usuarioRepositoryControl.findByNombreUsuario(userName);
        if (retrievedUsuarioModel == null) {
            throw new UsernameNotFoundException("Invalid username or password");
        }

        return UserDetailsMapper.build(retrievedUsuarioModel);
    }

    @Override
    public UsuarioModel getUser(UUID id) {
        return usuarioRepositoryControl.findByIdUsuario(id);
    }

    @Override
    public UsuarioModel save(UsuarioModel usuarioModel) {
        RolModel userRolModel = rolRepositoryControl.findByNombreRol("USER");
        Set<RolModel> rolModels = new HashSet<>();
        rolModels.add(userRolModel);

        UsuarioModel usuarioModelToSave = UsuarioModel.builder().nombreUsuario(usuarioModel.getNombreUsuario()).claveUsuario(usuarioModel.getClaveUsuario()).roles(rolModels).build();

        return usuarioRepositoryControl.save(usuarioModelToSave);
    }
}
