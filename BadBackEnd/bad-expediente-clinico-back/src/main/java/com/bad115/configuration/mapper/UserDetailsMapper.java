package com.bad115.configuration.mapper;

import com.bad115.model.RolModel;
import com.bad115.model.UsuarioModel;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.HashSet;
import java.util.Set;

public class UserDetailsMapper {

    public static UserDetails build(UsuarioModel usuarioModel) {
        return new org.springframework.security.core.userdetails.User(usuarioModel.getNombreUsuario(), usuarioModel.getClaveUsuario(), getAuthorities(usuarioModel));
    }

    private static Set<? extends GrantedAuthority> getAuthorities(UsuarioModel retrievedUsuarioModel) {
        Set<RolModel> rolModels = retrievedUsuarioModel.getRoles();

        Set<SimpleGrantedAuthority> authorities = new HashSet<>();

        rolModels.forEach(rolModel -> authorities.add(new SimpleGrantedAuthority("ROLE_" + rolModel.getNombreRol())));

        return authorities;
    }
}