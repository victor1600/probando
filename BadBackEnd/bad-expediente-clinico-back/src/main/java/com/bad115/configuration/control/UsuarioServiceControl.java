package com.bad115.configuration.control;

import com.bad115.model.UsuarioModel;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.UUID;

public interface UsuarioServiceControl extends UserDetailsService {

    UsuarioModel getUser(UUID id);

    UsuarioModel save(UsuarioModel usuarioModel);
}