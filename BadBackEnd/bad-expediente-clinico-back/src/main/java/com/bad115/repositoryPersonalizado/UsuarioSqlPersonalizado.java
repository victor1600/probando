package com.bad115.repositoryPersonalizado;

import com.bad115.model.RolModel;
import com.bad115.model.UsuarioModel;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Repository("UsuarioSqlPersonalizado")
public class UsuarioSqlPersonalizado {

    @PersistenceContext
    EntityManager manager;


    @Transactional
    public Boolean insertUserInit(UsuarioModel usuario){
        try {
            manager.createNativeQuery("INSERT INTO public.db_usuario(id_usuario, clave_usuario, nombre_usuario) VALUES (?, ?, ?)")
                    .setParameter(1,usuario.getIdUsuario())
                    .setParameter(2,usuario.getClaveUsuario())
                    .setParameter(3, usuario.getNombreUsuario())
                    .executeUpdate();
            for (RolModel rolU: usuario.getRoles()) {
                manager.createNativeQuery("INSERT INTO public.db_usuario_rol(usuario_id, rol_id) VALUES (?, ?)")
                        .setParameter(1,usuario.getIdUsuario())
                        .setParameter(2,rolU.getIdRol())
                        .executeUpdate();
            }
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
