package com.bad115.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "dbBoletaExamen")
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class BoletaExamenModel  {


    public BoletaExamenModel(UUID idBoleta, UUID fkExamen,UUID fkConsulta ,Boolean estaActivo) {
        this.idBoleta = idBoleta;
        this.fkExamen = fkExamen;
        this.estaActivo = estaActivo;
        this.fkConsulta = fkConsulta;
    }

    public BoletaExamenModel() {
    }

    @Id
    @Column(name = "idBoleta")
    private UUID idBoleta;

    @Column(name = "descripBoletaExamen", nullable = false, length = 15)
    private String descripBoletaExamen;

    //// examen

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_examen", nullable = false)
    private ExamenModal examen;

    @Transient
    private UUID fkExamen;

    @Column(name = "estaActivo", nullable = false)
    private Boolean estaActivo;

    /// fk consulta
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_consulta", nullable = false)
    private ConsultaModel consulta;

    @Transient
    private UUID fkConsulta;

    public ConsultaModel getConsulta() {
        return consulta;
    }

    public void setConsulta(ConsultaModel consulta) {
        this.consulta = consulta;
    }

    public UUID getFkConsulta() {
        return fkConsulta;
    }

    public void setFkConsulta(UUID fkConsulta) {
        this.fkConsulta = fkConsulta;
    }

    public Boolean getEstaActivo() {
        return estaActivo;
    }

    public void setEstaActivo(Boolean estaActivo) {
        this.estaActivo = estaActivo;
    }

    public UUID getIdBoleta() {
        return idBoleta;
    }

    public void setIdBoleta(UUID idBoleta) {
        this.idBoleta = idBoleta;
    }

    public String getDescripBoletaExamen() {
        return descripBoletaExamen;
    }

    public void setDescripBoletaExamen(String descripBoletaExamen) {
        this.descripBoletaExamen = descripBoletaExamen;
    }

    public ExamenModal getExamen() {
        return examen;
    }

    public void setExamen(ExamenModal examen) {
        this.examen = examen;
    }

    public UUID getFkExamen() {
        return fkExamen;
    }

    public void setFkExamen(UUID fkExamen) {
        this.fkExamen = fkExamen;
    }
}
