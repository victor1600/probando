package com.bad115.model;

import javax.persistence.*;

@Entity(name = "db_paciente")
@PrimaryKeyJoinColumn(name = "idPaciente")
public class PacienteModel extends PersonaModel {

    @Column(name = "estadoCivilPaciente", length = 1)
    private String estadoCivilPaciente;

    @Column(name = "estadoPaciente", columnDefinition = "boolean default true")
    private boolean estadoPaciente;

    @ManyToOne
    @JoinColumn(name = "id_responsable",nullable=false)
    private ResponsableModel responsable;

    public PacienteModel() {
    }

    public PacienteModel(String estadoCivilPaciente, boolean estadoPaciente, ResponsableModel responsable) {
        this.estadoCivilPaciente = estadoCivilPaciente;
        this.estadoPaciente = estadoPaciente;
        this.responsable = responsable;
    }

    public PacienteModel(String nombre1Persona, String nombre2Persona, String nombre3Persona, String apellido1Persona, String apellido2Persona, String apellido3Persona, String telefonoPersona, String celularPersona, String fechaNacimientoPersona, String duiPersona, String nitPersona, String sexoPersona, DireccionModel direccion, String estadoCivilPaciente, boolean estadoPaciente, ResponsableModel responsable) {
        super(nombre1Persona, nombre2Persona, nombre3Persona, apellido1Persona, apellido2Persona, apellido3Persona, telefonoPersona, celularPersona, fechaNacimientoPersona, duiPersona, nitPersona, sexoPersona, direccion);
        this.estadoCivilPaciente = estadoCivilPaciente;
        this.estadoPaciente = estadoPaciente;
        this.responsable = responsable;
    }

    public String getEstadoCivilPaciente() {
        return estadoCivilPaciente;
    }

    public void setEstadoCivilPaciente(String estadoCivilPaciente) {
        this.estadoCivilPaciente = estadoCivilPaciente;
    }

    public boolean isEstadoPaciente() {
        return estadoPaciente;
    }

    public void setEstadoPaciente(boolean estadoPaciente) {
        this.estadoPaciente = estadoPaciente;
    }

    public ResponsableModel getResponsable() {
        return responsable;
    }

    public void setResponsable(ResponsableModel responsable) {
        this.responsable = responsable;
    }

    @Override
    public String toString() {
        return "Paciente: {" +
                "estadoCivilPaciente='" + estadoCivilPaciente + '\'' +
                ", estadoPaciente=" + estadoPaciente +
                ", responsable=" + responsable +
                ", direccion=" + direccion +
                '}';
    }
}
