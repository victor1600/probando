package com.bad115.model;

import javax.persistence.*;

@Entity(name = "db_personalMedico")
@PrimaryKeyJoinColumn(name = "idPersonal")
public class PersonalMedicoModel extends PersonaModel{

    @Column(name = "jvp", length = 5)
    private String jvp;

    @Column(name = "estadoPersonal", columnDefinition = "boolean default true")
    private boolean estadoPersonal;

    @ManyToOne
    @JoinColumn(name = "id_tipoPersonal",nullable=false)
    private TipoPersonalMedicoModel tipoPersonal;

    @OneToOne
    @JoinColumn(name = "id_medico", unique = true)
    MedicoModel medico;

    public PersonalMedicoModel() {
    }

    public PersonalMedicoModel(String jvp, boolean estadoPersonal, TipoPersonalMedicoModel tipoPersonal, MedicoModel medico) {
        this.jvp = jvp;
        this.estadoPersonal = estadoPersonal;
        this.tipoPersonal = tipoPersonal;
        this.medico = medico;
    }

    public PersonalMedicoModel(String nombre1Persona, String nombre2Persona, String nombre3Persona, String apellido1Persona, String apellido2Persona, String apellido3Persona, String telefonoPersona, String celularPersona, String fechaNacimientoPersona, String duiPersona, String nitPersona, String sexoPersona, DireccionModel direccion, String jvp, boolean estadoPersonal, TipoPersonalMedicoModel tipoPersonal, MedicoModel medico) {
        super(nombre1Persona, nombre2Persona, nombre3Persona, apellido1Persona, apellido2Persona, apellido3Persona, telefonoPersona, celularPersona, fechaNacimientoPersona, duiPersona, nitPersona, sexoPersona, direccion);
        this.jvp = jvp;
        this.estadoPersonal = estadoPersonal;
        this.tipoPersonal = tipoPersonal;
        this.medico = medico;
    }

    public String getJvp() {
        return jvp;
    }

    public void setJvp(String jvp) {
        this.jvp = jvp;
    }

    public boolean isEstadoPersonal() {
        return estadoPersonal;
    }

    public void setEstadoPersonal(boolean estadoPersonal) {
        this.estadoPersonal = estadoPersonal;
    }

    public TipoPersonalMedicoModel getTipoPersonal() {
        return tipoPersonal;
    }

    public void setTipoPersonal(TipoPersonalMedicoModel tipoPersonal) {
        this.tipoPersonal = tipoPersonal;
    }

    public MedicoModel getMedico() {
        return medico;
    }

    public void setMedico(MedicoModel medico) {
        this.medico = medico;
    }

    @Override
    public String toString() {
        return "Personal Medico: {" +
                "jvp='" + jvp + '\'' +
                ", estadoPersonal=" + estadoPersonal +
                ", tipoPersonal=" + tipoPersonal +
                ", medico=" + medico +
                ", direccion=" + direccion +
                '}';
    }
}
