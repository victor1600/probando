package com.bad115.model;

import javax.persistence.*;
import java.util.UUID;

@Entity(name = "db_ingreso")
public class IngresoModel {

    @Id
    @Column(name = "idIngreso")
    private UUID idIngreso;

    @Column(name = "fechaIngreso", nullable = false, length = 10)
    private String fechaIngreso;

    @Column(name = "fechaAltaIngreso", length = 10)
    private String fechaAltaIngreso;

    @Column(name = "motivoIngreso", nullable = false, length = 100)
    private String motivoIngreso;

    @OneToOne(mappedBy = "ingreso")
    CamaModel cama;

       /*
        @ManyToOne
        @JoinColumn(name = "idExpediente",nullable=false)
        private ExpedienteModel expediente;
    */
    @ManyToOne
    @JoinColumn(name = "idMedico",nullable=false)
    private MedicoModel medico;


    public IngresoModel() {
    }

    public IngresoModel(UUID idIngreso,String fechaIngreso, String fechaAltaIngreso, String motivoIngreso){
        this.idIngreso = idIngreso;
        this.fechaIngreso = fechaIngreso;
        this.fechaAltaIngreso = fechaAltaIngreso;
        this.motivoIngreso = motivoIngreso;
    }

    public UUID getIdIngreso() { return idIngreso;  }

    public void setIdIngreso(UUID idIngreso) { this.idIngreso = idIngreso;  }

    public String getFechaIngreso() { return fechaIngreso; }

    public void setFechaIngreso(String fechaIngreso) { this.fechaIngreso = fechaIngreso; }

    public String getFechaAltaIngreso() { return fechaAltaIngreso; }

    public void setFechaAltaIngreso(String fechaAltaIngreso) { this.fechaAltaIngreso = fechaAltaIngreso; }

    public String getMotivoIngreso() { return motivoIngreso; }

    public void setMotivoIngreso(String motivoIngreso) { this.motivoIngreso = motivoIngreso; }

    @Override
    public String toString() {
        return "IngresoModel{" +
                "idIngreso=" + idIngreso +
                ", fechaIngreso='" + fechaIngreso + '\'' +
                ", fechaAltaIngreso='" + fechaAltaIngreso + '\'' +
                ", motivoIngreso='" + motivoIngreso + '\'' +
                '}';
    }
}
