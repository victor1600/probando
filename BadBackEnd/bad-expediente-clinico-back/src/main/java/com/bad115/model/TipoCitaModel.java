package com.bad115.model;



import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "dbTipoCita")

public class TipoCitaModel {

    @Id
    @Column(name = "idTipoCita")
    private UUID idTipoCita;

    @Column(name = "nombreTipoCita", unique = true, nullable = false, length = 50)
    private String nombreTipoCita;

    @Column(name = "estaActivo", nullable = false)
    private Boolean estaActivo;

    public UUID getIdTipoCita() {
        return idTipoCita;
    }

    public void setIdTipoCita(UUID idTipoCita) {
        this.idTipoCita = idTipoCita;
    }

    public String getNombreTipoCita() {
        return nombreTipoCita;
    }

    public void setNombreTipoCita(String nombreTipoCita) {
        this.nombreTipoCita = nombreTipoCita;
    }

    public Boolean getEstaActivo() {
        return estaActivo;
    }

    public void setEstaActivo(Boolean estaActivo) {
        this.estaActivo = estaActivo;
    }


    public TipoCitaModel(UUID idTipoCita, String nombreTipoCita, Boolean estaActivo) {
        this.idTipoCita = idTipoCita;
        this.nombreTipoCita = nombreTipoCita;
        this.estaActivo = estaActivo;
    }

    public TipoCitaModel() {
    }

}
