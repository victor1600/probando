package com.bad115.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Entity
@Builder
@AllArgsConstructor
@Data
@Table(name = "dbUsuario")
public class UsuarioModel {


    public UsuarioModel() {
    }

    public UsuarioModel(String nombreUsuario){
        this.nombreUsuario = nombreUsuario;
    }

    @Id
    @GeneratedValue
    @Column(name = "idUsuario")
    private UUID idUsuario;

    @Column(name = "nombreUsuario", nullable = false, unique =true)
    private String nombreUsuario;

    @Column(name = "claveUsuario", nullable = false)
    private String claveUsuario;

    @Column(name = "tokenUsuario")
    private String tokenUsuario;

    @Column(name = "tokenTiempo")
    private  String tokenTiempo;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "dbUsuarioRol", joinColumns = {
            @JoinColumn(name = "usuario_id") }, inverseJoinColumns = {
            @JoinColumn(name = "rol_id") })
    private Set<RolModel> roles;

}
