package com.bad115.model;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity(name = "db_municipio")
public class MunicipioModel {

    @Id
    @Column(name = "idMunicipio", nullable = false)
    private UUID idMunicipio;

    @Column(name = "codMunicipio", nullable = false, unique = true, length = 4)
    private String codMunicipio;

    @Column(name = "nombreMunicipio", nullable = false, length = 30)
    private String nombreMunicipio;

    @ManyToOne
    @JoinColumn(name = "id_Departamento")
    private DepartamentoModel departamento;

    @OneToMany(mappedBy = "municipio")
    private List<DireccionModel> direcciones;

    public MunicipioModel() {
    }

    public MunicipioModel(UUID idMunicipio, String codMunicipio, String nombreMunicipio, DepartamentoModel departamento) {
        this.idMunicipio = idMunicipio;
        this.codMunicipio = codMunicipio;
        this.nombreMunicipio = nombreMunicipio;
        this.departamento = departamento;
    }

    public UUID getIdMunicipio() {
        return idMunicipio;
    }

    public void setIdMunicipio(UUID idMunicipio) {
        this.idMunicipio = idMunicipio;
    }

    public String getCodMunicipio() {
        return codMunicipio;
    }

    public void setCodMunicipio(String codMunicipio) {
        this.codMunicipio = codMunicipio;
    }

    public String getNombreMunicipio() {
        return nombreMunicipio;
    }

    public void setNombreMunicipio(String nombreMunicipio) {
        this.nombreMunicipio = nombreMunicipio;
    }

    public DepartamentoModel getDepartamento() {
        return departamento;
    }

    public void setDepartamento(DepartamentoModel departamento) {
        this.departamento = departamento;
    }

    @Override
    public String toString() {
        return "Municipio: {" +
                "idMunicipio=" + idMunicipio +
                ", codMunicipio='" + codMunicipio + '\'' +
                ", nombreMunicipio='" + nombreMunicipio + '\'' +
                ", departamento=" + departamento +
                '}';
    }
}

