package com.bad115.model;

import javax.persistence.*;
import java.util.UUID;

@Entity(name ="db_boletaCirugia")
public class BoletaCirugiaModel {
    @Id
    @Column(name = "idBoletaCirugia")
    private UUID idBoletaCirugia;

    @Column(name = "fechaBoletaCirugia")
    private String fechaBoletaCirugia;
/*
    @OneToOne(mappedBy = "boletaCirugia")
    ConsultaModel consulta;
*/
    @ManyToOne
    @JoinColumn(name = "idCirugia",nullable=false)
    private CirugiaModel cirugia;

    public BoletaCirugiaModel() {
    }

    public BoletaCirugiaModel(UUID idBoletaCirugia, String fechaBoletaCirugia, CirugiaModel cirugia) {
        this.idBoletaCirugia = idBoletaCirugia;
        this.fechaBoletaCirugia = fechaBoletaCirugia;
        this.cirugia = cirugia;
    }

    public CirugiaModel getCirugia() {
        return cirugia;
    }

    public void setCirugia(CirugiaModel cirugia) {
        this.cirugia = cirugia;
    }

    public UUID getIdBoletaCirugia() { return idBoletaCirugia; }

    public void setIdBoletaCirugia(UUID idBoletaCirugia) { this.idBoletaCirugia = idBoletaCirugia; }

    public String getFechaBoletaCirugia() {  return fechaBoletaCirugia;  }

    public void setFechaBoletaCirugia(String fechaBoletaCirugia) { this.fechaBoletaCirugia = fechaBoletaCirugia;  }


    @Override
    public String toString() {
        return "BoletaCirugiaModel{" +
                "idBoletaCirugia=" + idBoletaCirugia +
                ", fechaBoletaCirugia='" + fechaBoletaCirugia + '\'' +
                ", cirugia=" + cirugia +
                '}';
    }
}
