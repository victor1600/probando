package com.bad115.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "dbTipoMedicamento")
public class TipoMedicamentoModel {

    public TipoMedicamentoModel() {
    }

    @Id
    @Column(name = "idTipoMedicamento")
    private UUID idTipoMedicamento;

    @Column(name = "nombreTMedicamento", unique = true, nullable = false, length = 50)
    private String nombreTMedicamento;

    @Column(name = "descripcionTipoMedicamento", nullable = false, length = 50)
    private String descripcionTipoMedicamento;

    @Column(name = "estaActivo", nullable = false)
    private Boolean estaActivo;

    public Boolean getEstaActivo() {
        return estaActivo;
    }

    public void setEstaActivo(Boolean estaActivo) {
        this.estaActivo = estaActivo;
    }

    public TipoMedicamentoModel(UUID idTipoMedicamento, String nombreTMedicamento, String descripcionTipoMedicamento, Boolean estaActivo) {
        this.idTipoMedicamento = idTipoMedicamento;
        this.nombreTMedicamento = nombreTMedicamento;
        this.descripcionTipoMedicamento = descripcionTipoMedicamento;
        this.estaActivo = estaActivo;
    }

    public UUID getIdTipoMedicamento() {
        return idTipoMedicamento;
    }

    public void setIdTipoMedicamento(UUID idTipoMedicamento) {
        this.idTipoMedicamento = idTipoMedicamento;
    }

    public String getNombreTMedicamento() {
        return nombreTMedicamento;
    }

    public void setNombreTMedicamento(String descripTMedicamento) {
        this.nombreTMedicamento = descripTMedicamento;
    }

    public String getDescripcionTipoMedicamento() {
        return descripcionTipoMedicamento;
    }

    public void setDescripcionTipoMedicamento(String viaAdministracion) {
        this.descripcionTipoMedicamento = viaAdministracion;
    }

    @Override
    public String toString() {
        return "TipoMedicamentoModel{" +
                "idTipoMedicamento=" + idTipoMedicamento +
                ", descripTMedicamento='" + nombreTMedicamento + '\'' +
                ", viaAdministracion='" + descripcionTipoMedicamento + '\'' +
                '}';
    }
}
