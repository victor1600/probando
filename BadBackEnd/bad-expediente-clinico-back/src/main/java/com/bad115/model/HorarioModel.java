package com.bad115.model;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "dbHorario")
public class HorarioModel {

    @Id
    @Column(name = "idHorario")
    private UUID idHorario;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "idAsignacionHorario",nullable=false)
    private AsignacionHorarioModel asignacionHorario;

    @Transient
    private UUID fkAsignacionHorario;


    @OneToOne()
    @JoinColumn(name = "idCita", referencedColumnName = "idCita", unique = true)
    private CitaModel cita;

    @Transient
    private UUID fkCita;

    @Column(name = "estadoHorario", nullable = false)
    private Boolean estadoHorario;

    @Column(name = "estaActivo", nullable = false)
    private Boolean estaActivo;

    public HorarioModel() {
    }

    public HorarioModel(UUID idHorario, UUID fkAsignacionHorario, UUID fkCita, Boolean estadoHorario, Boolean estaActivo) {
        this.idHorario = idHorario;
        this.fkAsignacionHorario = fkAsignacionHorario;
        this.fkCita = fkCita;
        this.estadoHorario = estadoHorario;
        this.estaActivo = estaActivo;
    }

    public UUID getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(UUID idHorario) {
        this.idHorario = idHorario;
    }

    public AsignacionHorarioModel getAsignacionHorario() {
        return asignacionHorario;
    }

    public void setAsignacionHorario(AsignacionHorarioModel asignacionHorario) {
        this.asignacionHorario = asignacionHorario;
    }

    public UUID getFkAsignacionHorario() {
        return fkAsignacionHorario;
    }

    public void setFkAsignacionHorario(UUID fkAsignacionHorario) {
        this.fkAsignacionHorario = fkAsignacionHorario;
    }

    public CitaModel getCita() {
        return cita;
    }

    public void setCita(CitaModel cita) {
        this.cita = cita;
    }

    public UUID getFkCita() {
        return fkCita;
    }

    public void setFkCita(UUID fkCita) {
        this.fkCita = fkCita;
    }

    public Boolean getEstadoHorario() {
        return estadoHorario;
    }

    public void setEstadoHorario(Boolean estadoHorario) {
        this.estadoHorario = estadoHorario;
    }

    public Boolean getEstaActivo() {
        return estaActivo;
    }

    public void setEstaActivo(Boolean estaActivo) {
        this.estaActivo = estaActivo;
    }

    @Override
    public String toString() {
        return "HorarioModel{" +
                "idHorario=" + idHorario +
                ", asignacionHorario=" + asignacionHorario +
                ", fkAsignacionHorario=" + fkAsignacionHorario +
                ", cita=" + cita +
                ", fkCita=" + fkCita +
                ", estadoHorario=" + estadoHorario +
                ", estaActivo=" + estaActivo +
                '}';
    }
}
