package com.bad115.model;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity(name = "db_area")
public class AreaModel {

    @Id
    @Column(name = "idArea")
    private UUID idArea;

    @Column(name = "codigoArea", nullable = false, length = 3)
    private String codigoArea;

    @Column(name = "nombreArea", nullable = false, length = 25)
    private String nombreArea;

    @Column(name = "cantidadCama", nullable = false)
    private int cantidadCama;

    @Column(name = "cantDisponible")
    private int cantDisponible;

    @OneToMany(mappedBy = "area")
    private List<CamaModel> camas;


     public AreaModel() {
    }

    public AreaModel(UUID idArea, List<CamaModel> camas) {
        this.idArea = idArea;
        this.camas = camas;

    }

    public AreaModel(UUID idArea,String codigoArea, String nombreArea, int cantidadCama, int cantDisponible,  List<CamaModel> camas) {
        this.idArea = idArea;
        this.codigoArea = codigoArea;
        this.nombreArea = nombreArea;
        this.cantidadCama = cantidadCama;
        this.cantDisponible = cantDisponible;
        this.camas = camas;
    }

    public UUID getIdArea() { return idArea;   }

    public void setIdArea(UUID idArea) { this.idArea = idArea; }

    public String getCodigoArea() { return codigoArea;  }

    public void setCodigoArea(String codigoArea) { this.codigoArea = codigoArea; }

    public String getNombreArea() {  return nombreArea; }

    public void setNombreArea(String nombreArea) { this.nombreArea = nombreArea; }

    public int getCantidadCama() { return cantidadCama; }

    public void setCantidadCama(int cantidadCama) { this.cantidadCama = cantidadCama;  }

    public int getCantDisponible() { return cantDisponible;  }

    public void setCantDisponible(int cantDisponible) { this.cantDisponible = cantDisponible;  }

    public List<CamaModel> getCamas() {  return camas;  }

    public void setCamas(List<CamaModel> camas) {  this.camas = camas;  }

    @Override
    public String toString() {
        return "AreaModel{" +
                "idArea=" + idArea +
                ", codigoArea='" + codigoArea + '\'' +
                ", nombreArea='" + nombreArea + '\'' +
                ", cantidadCama=" + cantidadCama +
                ", cantDisponible=" + cantDisponible +
                ", camas=" + camas +
                '}';
    }
}


