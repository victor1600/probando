package com.bad115.model;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity(name= "db_cirugia")
public class CirugiaModel {

    @Id
    @Column(name = "idCirugia")
    private UUID idCirugia;

    @Column(name = "nombreCirugia", nullable = false, length = 50)
    private String nombreCirugia;

    @Column(name = "clasificacionCirugia", nullable = false, length = 10)
    private String clasificacionCirugia;

    @Column(name = "descripcionCirugia", length = 600)
    private String descripcionCirugia;

    @ManyToOne
    @JoinColumn(name = "idEspecialidad")
    private EspecialidadModel especialidad;

    @OneToMany(mappedBy = "cirugia")
    private List<BoletaCirugiaModel> boletaCirugia;

    public CirugiaModel() {
    }

    public CirugiaModel(UUID idCirugia, String nombreCirugia, String clasificacionCirugia, String descripcionCirugia ){
        this.idCirugia = idCirugia;
        this.nombreCirugia = nombreCirugia;
        this.clasificacionCirugia= clasificacionCirugia;
        this.descripcionCirugia= descripcionCirugia;
    }

    public EspecialidadModel getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(EspecialidadModel especialidad) {
        this.especialidad = especialidad;
    }

    public UUID getIdCirugia() { return idCirugia; }

    public void setIdCirugia(UUID idCirugia) { this.idCirugia = idCirugia; }

    public String getNombreCirugia() { return nombreCirugia;  }

    public void setNombreCirugia(String nombreCirugia) { this.nombreCirugia = nombreCirugia; }

    public String getClasificacionCirugia() { return clasificacionCirugia; }

    public void setClasificacionCirugia(String clasificacionCirugia) { this.clasificacionCirugia = clasificacionCirugia; }

    public String getDescripcionCirugia() { return descripcionCirugia; }

    public void setDescripcionCirugia(String descripcionCirugia) { this.descripcionCirugia = descripcionCirugia; }


    @Override
    public String toString() {
        return "Cirugia{" +
                "idCirugia=" + idCirugia +
                ", nombreCirugia='" + nombreCirugia + '\'' +
                ", clasificacionCirugia='" + clasificacionCirugia + '\'' +
                ", descripcionCirugia='" + descripcionCirugia + '\'' +
                '}';
    }
}
