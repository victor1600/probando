package com.bad115.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name="dbExpediente")
public class ExpedienteModel {

    @Id
    @Column(name = "idExpediente")
    private UUID idExpediente;

    @Column(name = "codigoExpediente", unique = true, nullable = false, length = 10)
    private String codigoExpediente;

    @Column(name = "aperturaExpediente", nullable = false, length = 10)
    private String aperturaExpediente;

    public ExpedienteModel() {
    }

    public ExpedienteModel(UUID idExpediente, String codigoExpediente, String aperturaExpediente) {
        this.idExpediente = idExpediente;
        this.codigoExpediente = codigoExpediente;
        this.aperturaExpediente = aperturaExpediente;
    }

    public UUID getIdExpediente() {
        return idExpediente;
    }

    public void setIdExpediente(UUID idExpediente) {
        this.idExpediente = idExpediente;
    }

    public String getCodigoExpediente() {
        return codigoExpediente;
    }

    public void setCodigoExpediente(String codigoExpediente) {
        this.codigoExpediente = codigoExpediente;
    }

    public String getAperturaExpediente() {
        return aperturaExpediente;
    }

    public void setAperturaExpediente(String aperturaExpediente) {
        this.aperturaExpediente = aperturaExpediente;
    }
}
