package com.bad115.model;

import javax.persistence.*;
import java.util.UUID;
@Entity
@Table(name = "dbExamen")
public class ExamenModal {
    @Id
    @Column(name = "idExamen")
    private UUID idExamen;

    @Column(name = "imagenExamen")
    private String imagenExamen;

    @Column(name = "videoExamen")
    private String videoExamen;

    @Column(name = "documentoExamen")
    private String documentoExamen;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_tipo_examen", nullable = false)
    private TipoExamenModel tipoExamen;

    public TipoExamenModel getTipoExamen() {
        return tipoExamen;
    }

    public void setTipoExamen(TipoExamenModel tipoExamen) {
        this.tipoExamen = tipoExamen;
    }

    public UUID getFkTipoExamen() {
        return fkTipoExamen;
    }

    public void setFkTipoExamen(UUID fkTipoExamen) {
        this.fkTipoExamen = fkTipoExamen;
    }

    @Transient
    private UUID fkTipoExamen;

    public ExamenModal() {
    }

    public ExamenModal(UUID idExamen, String imagenExamen, String videoExamen, String documentoExamen, UUID fkTipoExamen) {
        this.idExamen = idExamen;
        this.imagenExamen = imagenExamen;
        this.videoExamen = videoExamen;
        this.documentoExamen = documentoExamen;
        this.fkTipoExamen = fkTipoExamen;
    }

    public UUID getIdExamen() {
        return idExamen;
    }

    public void setIdExamen(UUID idExamen) {
        this.idExamen = idExamen;
    }

    public String getImagenExamen() {
        return imagenExamen;
    }

    public void setImagenExamen(String imagenExamen) {
        this.imagenExamen = imagenExamen;
    }

    public String getVideoExamen() {
        return videoExamen;
    }

    public void setVideoExamen(String videoExamen) {
        this.videoExamen = videoExamen;
    }

    public String getDocumentoExamen() {
        return documentoExamen;
    }

    public void setDocumentoExamen(String documentoExamen) {
        this.documentoExamen = documentoExamen;
    }


    @Override
    public String toString() {
        return "ExamenModal{" +
                "idExamen=" + idExamen +
                ", imagenExamen='" + imagenExamen + '\'' +
                ", videoExamen='" + videoExamen + '\'' +
                ", documentoExamen='" + documentoExamen + '\'' +
                ", tipoExamen=" + tipoExamen +
                ", fkTipoExamen=" + fkTipoExamen +
                '}';
    }
}
