package com.bad115.model;

import javax.persistence.*;
import java.util.UUID;

@Entity(name = "db_medico")
public class MedicoModel {

    @Id
    @GeneratedValue
    @Column(name = "idMedico")
    private UUID idMedico;

    @Column(name = "pacientesPorHora", nullable = false)
    private int pacientesPorHora;

    @OneToOne(mappedBy = "medico")
    PersonalMedicoModel personalMedico;

    @ManyToOne
    @JoinColumn(name = "id_especialidad")
    private EspecialidadModel especialidad;

    public MedicoModel() {
    }

    public MedicoModel(UUID idMedico, int pacientesPorHora) {
        this.idMedico = idMedico;
        this.pacientesPorHora = pacientesPorHora;
    }

    public UUID getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(UUID idMedico) {
        this.idMedico = idMedico;
    }

    public int getPacientesPorHora() {
        return pacientesPorHora;
    }

    public void setPacientesPorHora(int pacientesPorHora) {
        this.pacientesPorHora = pacientesPorHora;
    }

    public EspecialidadModel getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(EspecialidadModel especialidad) {
        this.especialidad = especialidad;
    }

    @Override
    public String toString() {
        return "Medico: {" +
                "idMedico=" + idMedico +
                ", pacientesPorHora=" + pacientesPorHora +
                ", especialidad=" + especialidad +
                '}';
    }
}
