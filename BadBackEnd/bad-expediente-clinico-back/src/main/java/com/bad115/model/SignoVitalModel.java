package com.bad115.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "dbSignoVital")
public class SignoVitalModel {

    @Id
    @Column(name = "idVital")
    private UUID idVital;

    @Column(name = "presionVital")
    private int presionVital;

    @Column(name = "ritmoVital")
    private int ritmoVital;

    @Column(name = "respiracionVital")
    private int respiracionVital;

    @Column(name = "temperaturaVital")
    private float temperaturaVital;

    @Column(name = "estaturaVital")
    private float estaturaVital;

    @Column(name = "pesoVital")
    private float pesoVital;

    public SignoVitalModel() {
    }

    public SignoVitalModel(UUID idVital, int presionVital, int ritmoVital, int respiracionVital, float temperaturaVital, float estaturaVital, float pesoVital) {
        this.idVital = idVital;
        this.presionVital = presionVital;
        this.ritmoVital = ritmoVital;
        this.respiracionVital = respiracionVital;
        this.temperaturaVital = temperaturaVital;
        this.estaturaVital = estaturaVital;
        this.pesoVital = pesoVital;
    }

    public UUID getIdVital() {
        return idVital;
    }

    public void setIdVital(UUID idVital) {
        this.idVital = idVital;
    }

    public int getPresionVital() {
        return presionVital;
    }

    public void setPresionVital(int presionVital) {
        this.presionVital = presionVital;
    }

    public int getRitmoVital() {
        return ritmoVital;
    }

    public void setRitmoVital(int ritmoVital) {
        this.ritmoVital = ritmoVital;
    }

    public int getRespiracionVital() {
        return respiracionVital;
    }

    public void setRespiracionVital(int respiracionVital) {
        this.respiracionVital = respiracionVital;
    }

    public float getTemperaturaVital() {
        return temperaturaVital;
    }

    public void setTemperaturaVital(float temperaturaVItal) {
        this.temperaturaVital = temperaturaVItal;
    }

    public float getEstaturaVital() {
        return estaturaVital;
    }

    public void setEstaturaVital(float estaturaVital) {
        this.estaturaVital = estaturaVital;
    }

    public float getPesoVital() {
        return pesoVital;
    }

    public void setPesoVital(float pesoVital) {
        this.pesoVital = pesoVital;
    }

    @Override
    public String toString() {
        return "SignoVitalModel{" +
                "idVital=" + idVital +
                ", presionVital=" + presionVital +
                ", ritmoVital=" + ritmoVital +
                ", respiracionVital=" + respiracionVital +
                ", temperaturaVItal=" + temperaturaVital +
                ", estaturaVital=" + estaturaVital +
                ", pesoVital=" + pesoVital +
                '}';
    }
}
