package com.bad115.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name="dbServicio")
public class ServicioModel {

    @Id
    @Column(name = "idServicio")
    private UUID idServicio;

    @Column(name = "nombreServicio", nullable = false, length = 60)
    private String nombreServicio;

    @Column(name = "descripcionServicio", nullable = false, length = 150)
    private String descripcionServicio;

    @Column(name = "costoServicio", nullable = false)
    private Float costoServicio;

    @Column(name = "estaActivo", nullable = false)
    private Boolean estaActivo;


    public ServicioModel() {
    }

    public ServicioModel(UUID idServicio, String nombreServicio, String descripcionServicio, Float costoServicio, Boolean estaActivo) {
        this.idServicio = idServicio;
        this.nombreServicio = nombreServicio;
        this.descripcionServicio = descripcionServicio;
        this.costoServicio = costoServicio;
        this.estaActivo = estaActivo;
    }

    public UUID getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(UUID idServicio) {
        this.idServicio = idServicio;
    }

    public String getNombreServicio() {
        return nombreServicio;
    }

    public void setNombreServicio(String nombreServicio) {
        this.nombreServicio = nombreServicio;
    }

    public String getDescripcionServicio() {
        return descripcionServicio;
    }

    public void setDescripcionServicio(String descripcionServicio) {
        this.descripcionServicio = descripcionServicio;
    }

    public Float getCostoServicio() {
        return costoServicio;
    }

    public void setCostoServicio(Float costoServicio) {
        this.costoServicio = costoServicio;
    }

    public Boolean getEstaActivo() {
        return estaActivo;
    }

    public void setEstaActivo(Boolean estaActivo) {
        this.estaActivo = estaActivo;
    }

    @Override
    public String toString() {
        return "ServicioModel{" +
                "idServicio=" + idServicio +
                ", nombreServicio='" + nombreServicio + '\'' +
                ", descripcionServicio='" + descripcionServicio + '\'' +
                ", costoServicio=" + costoServicio +
                ", estaActivo=" + estaActivo +
                '}';
    }
}
