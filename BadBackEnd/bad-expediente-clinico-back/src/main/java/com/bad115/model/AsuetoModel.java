package com.bad115.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name="dbAsueto")
public class AsuetoModel {

    @Id
    @Column(name = "idAsueto")
    private UUID idAsueto;

    @Column(name = "fechaAsueto", nullable = false, length = 10)
    private String fechaAsueto;

    @Column(name = "nombreAsueto", nullable = false)
    private String nombreAsueto;

    @Column(name = "estaActivo", nullable = false)
    private Boolean estaActivo;

    public AsuetoModel() {
    }

    public AsuetoModel(UUID idAsueto, String fechaAsueto, String nombreAsueto, Boolean estaActivo) {
        this.idAsueto = idAsueto;
        this.fechaAsueto = fechaAsueto;
        this.nombreAsueto = nombreAsueto;
        this.estaActivo = estaActivo;
    }

    public UUID getIdAsueto() {
        return idAsueto;
    }

    public void setIdAsueto(UUID idAsueto) {
        this.idAsueto = idAsueto;
    }

    public String getFechaAsueto() {
        return fechaAsueto;
    }

    public void setFechaAsueto(String fechaAsueto) {
        this.fechaAsueto = fechaAsueto;
    }

    public String getNombreAsueto() {
        return nombreAsueto;
    }

    public void setNombreAsueto(String nombreAsueto) {
        this.nombreAsueto = nombreAsueto;
    }

    public Boolean getEstaActivo() {
        return estaActivo;
    }

    public void setEstaActivo(Boolean estaActivo) {
        this.estaActivo = estaActivo;
    }
}
