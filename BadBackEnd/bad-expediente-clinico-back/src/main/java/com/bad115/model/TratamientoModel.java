package com.bad115.model;



import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "dbTratamiento")
public class TratamientoModel {

    public TratamientoModel(UUID idTratamiento, String descripcionTratamiento, Boolean estaActivo, UUID fkConsultaId) {
        this.idTratamiento = idTratamiento;
        this.descripcionTratamiento = descripcionTratamiento;
        this.estaActivo = estaActivo;
        this.fkConsultaId = fkConsultaId;
    }

    @Id
    @Column(name = "idTratamiento")
    private UUID idTratamiento;

    @Column(name = "descripcionTratamiento", nullable = false, length = 500)
    private String descripcionTratamiento;

    @Column(name = "estaActivo", nullable = false)
    private Boolean estaActivo;

    public TratamientoModel() {
    }

    @OneToOne()
    @JoinColumn(name = "idConsulta", referencedColumnName = "idConsulta", unique = true)
    private ConsultaModel consulta;

    @Transient
    private UUID fkConsultaId;

    public ConsultaModel getConsulta() {
        return consulta;
    }

    public void setConsulta(ConsultaModel consulta) {
        this.consulta = consulta;
    }

    public UUID getFkConsultaId() {
        return fkConsultaId;
    }

    public void setFkConsultaId(UUID fkConsultaId) {
        this.fkConsultaId = fkConsultaId;
    }

    public UUID getIdTratamiento() {
        return idTratamiento;
    }

    public void setIdTratamiento(UUID idTratamiento) {
        this.idTratamiento = idTratamiento;
    }

    public String getDescripcionTratamiento() {
        return descripcionTratamiento;
    }

    public void setDescripcionTratamiento(String descripcionTratamiento) {
        this.descripcionTratamiento = descripcionTratamiento;
    }

    public Boolean getEstaActivo() {
        return estaActivo;
    }

    public void setEstaActivo(Boolean estaActivo) {
        this.estaActivo = estaActivo;
    }

}
