package com.bad115.model;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "dbTipoExamen")
public class TipoExamenModel {
    public TipoExamenModel() {
    }

    @Id
    @Column(name = "idTipoExamen")
    private UUID idTipoExamen;

    public TipoExamenModel(UUID idTipoExamen) {
        this.idTipoExamen = idTipoExamen;
    }

    public TipoExamenModel(UUID idTipoExamen, String nombreTipoExamen, Boolean estaActivo) {
        this.idTipoExamen = idTipoExamen;
        this.nombreTipoExamen = nombreTipoExamen;
        this.estaActivo = estaActivo;
    }

    @Column(name = "estaActivo", nullable = false)
    private Boolean estaActivo;

    @Column(name = "nombreTipoExamen", unique = true, nullable = false, length = 50)
    private String nombreTipoExamen;

    public UUID getIdTipoExamen() {
        return idTipoExamen;
    }

    public void setIdTipoExamen(UUID idTipoExamen) {
        this.idTipoExamen = idTipoExamen;
    }

    public String getNombreTipoExamen() {
        return nombreTipoExamen;
    }

    public void setNombreTipoExamen(String nombreTipoExamen) {
        this.nombreTipoExamen = nombreTipoExamen;
    }

    @Override
    public String toString() {
        return "TipoExamenModel{" +
                "idTipoExamen=" + idTipoExamen +
                ", nombreTipoExamen='" + nombreTipoExamen + '\'' +
                '}';
    }
}
