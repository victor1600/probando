package com.bad115.model;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity(name = "db_tipoAntecedente")
public class TipoAntecedenteModel {

    @Id
    @GeneratedValue
    @Column(name = "idTipoAntecedente")
    UUID idTipoAntecedente;

    @Column(name = "nombreTipoAntecedente", nullable = false, unique = true, length = 50)
    private String nombreTipoAntecedente;

    @OneToMany(mappedBy = "tipoAntecedente")
    private List<AntecedenteModel> antecedente;

    public TipoAntecedenteModel() {
    }

    public TipoAntecedenteModel(String nombreTipoAntecedente) {
        this.nombreTipoAntecedente = nombreTipoAntecedente;
    }

    public UUID getIdTipoAntecedente() {
        return idTipoAntecedente;
    }

    public void setIdTipoAntecedente(UUID idTipoAntecedente) {
        this.idTipoAntecedente = idTipoAntecedente;
    }

    public String getNombreTipoAntecedente() {
        return nombreTipoAntecedente;
    }

    public void setNombreTipoAntecedente(String nombreTipoAntecedente) {
        this.nombreTipoAntecedente = nombreTipoAntecedente;
    }
}
