package com.bad115.model;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;


@Entity
@Table(name = "dbEspecialidad")
public class EspecialidadModel {

    @Id
    @Column(name="idEspecialidad")
    private UUID idEspecialidad;

    @Column(name = "nombreEspecialidad",unique = true,nullable = false, length = 50)
    private String nombreEspecialidad;

    @OneToMany(mappedBy = "especialidad")
    private List<CirugiaModel> cirugias;

    @OneToMany(mappedBy = "especialidad")
    private List<MedicoModel> medico;

    public EspecialidadModel() {
    }

    public EspecialidadModel(UUID idEspecialidad, String nombreEspecialidad) {
        this.idEspecialidad = idEspecialidad;
        this.nombreEspecialidad = nombreEspecialidad;
    }

    public UUID getIdEspecialidad() {
        return idEspecialidad;
    }

    public void setIdEspecialidad(UUID idEspecialidad) {
        this.idEspecialidad = idEspecialidad;
    }

    public String getNombreEspecialidad() {
        return nombreEspecialidad;
    }

    public void setNombreEspecialidad(String nombreEspecialidad) {
        this.nombreEspecialidad = nombreEspecialidad;
    }

    @Override
    public String toString() {
        return "EspecialidadModel{" +
                "idEspecialidad=" + idEspecialidad +
                ", nombreEspecialidad='" + nombreEspecialidad + '\'' +
                '}';
    }
}
