package com.bad115.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name="dbCie10")
public class Cie10Model {
    @Id
    @Column(name = "idCIE10")
    private UUID idCIE10;

    @Column(name = "codigoCIE10", unique = true, nullable = false, length = 10)
    private String codigoCIE10;


    @Column(name = "descripcionCIE10", nullable = false, length = 600)
    private String descripcionCIE10;

     @Column(name = "sexoCIE10", nullable = false, length = 2)
    private int sexoCIE10;

     @Column(name = "limiteInfCIE10", nullable = false, length = 6)
    private int limiteInfCIE10;

     @Column(name = "limiteSupCIE10",  nullable = false, length = 6)
    private int limiteSupCIE10;


    public Cie10Model() {
    }

    public Cie10Model(UUID idCIE10, String codigoCIE10, String descripcionCIE10, int sexoCIE10, int limiteInfCIE10, int limiteSupCIE10) {
        this.idCIE10 = idCIE10;
        this.codigoCIE10 = codigoCIE10;
        this.descripcionCIE10 = descripcionCIE10;
        this.sexoCIE10 = sexoCIE10;
        this.limiteInfCIE10 = limiteInfCIE10;
        this.limiteSupCIE10 = limiteSupCIE10;
    }

    public UUID getIdCIE10() {
        return idCIE10;
    }

    public void setIdCIE10(UUID idCIE10) {
        this.idCIE10 = idCIE10;
    }

    public String getCodigoCIE10() {
        return codigoCIE10;
    }

    public void setCodigoCIE10(String codigoCIE10) {
        this.codigoCIE10 = codigoCIE10;
    }

    public String getDescripcionCIE10() {
        return descripcionCIE10;
    }

    public void setDescripcionCIE10(String descripcionCIE10) {
        this.descripcionCIE10 = descripcionCIE10;
    }

    public int getSexoCIE10() {
        return sexoCIE10;
    }

    public void setSexoCIE10(int sexoCIE10) {
        this.sexoCIE10 = sexoCIE10;
    }

    public int getLimiteInfCIE10() {
        return limiteInfCIE10;
    }

    public void setLimiteInfCIE10(int limiteInfCIE10) {
        this.limiteInfCIE10 = limiteInfCIE10;
    }

    public int getLimiteSupCIE10() {
        return limiteSupCIE10;
    }

    public void setLimiteSupCIE10(int limiteSupCIE10) {
        this.limiteSupCIE10 = limiteSupCIE10;
    }

    @Override
    public String toString() {
        return "Cie10Model{" +
                "idCIE10=" + idCIE10 +
                ", codigoCIE10='" + codigoCIE10 + '\'' +
                ", descripcionCIE10='" + descripcionCIE10 + '\'' +
                ", sexoCIE10=" + sexoCIE10 +
                ", limiteInfCIE10=" + limiteInfCIE10 +
                ", limiteSupCIE10=" + limiteSupCIE10 +
                '}';
    }
}
