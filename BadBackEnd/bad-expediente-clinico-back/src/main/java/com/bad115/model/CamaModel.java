package com.bad115.model;

import javax.persistence.*;
import java.util.UUID;

@Entity(name = "db_cama")
public class CamaModel {
    @Id
    @Column(name = "idCama")
    private  UUID idCama;

    @Column(name = "codigoCama", length = 6)
    private String codigoCama;

    @Column(name = "estadoCama")
    private Boolean estadoCama;

    @ManyToOne
    @JoinColumn(name = "idArea",nullable=false)
    private AreaModel area;

    @OneToOne
    @PrimaryKeyJoinColumn(name = "idIngreso")
    IngresoModel ingreso;


   public CamaModel() {
    }

    public CamaModel(String codigoCama, Boolean estadoCama){
        this.codigoCama= codigoCama;
        this.estadoCama= estadoCama;
    }

    public String getCodigoCama() { return codigoCama;  }

    public void setCodigoCama(String codigoCama) { this.codigoCama = codigoCama;  }

    public Boolean getEstadoCama() { return estadoCama;  }

    public void setEstadoCama(Boolean estadoCama) { this.estadoCama = estadoCama;  }

    @Override
    public String toString() {
        return "CamaModel{" +
                "codigoCama='" + codigoCama + '\'' +
                "estadoCama='" + estadoCama + '\'' +
                '}';
    }
}
