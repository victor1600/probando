package com.bad115.model;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name="dbServicioGasto")
public class ServicioGastoModel {

    @Id
    @Column(name = "idServicioGasto")
    private UUID idServicioGasto;

    @ManyToOne
    @JoinColumn(name = "idServicio",nullable=false)
    private ServicioModel servicio;
    @ManyToOne
    @JoinColumn(name = "idGasto",nullable=false)
    private GastoMedicoModel gasto;

    public ServicioGastoModel() {
    }

    public ServicioGastoModel(UUID idServicioGasto, ServicioModel servicio, GastoMedicoModel gasto) {
        this.idServicioGasto = idServicioGasto;
        this.servicio = servicio;
        this.gasto = gasto;
    }

    public UUID getIdServicioGasto() {
        return idServicioGasto;
    }

    public void setIdServicioGasto(UUID idServicioGasto) {
        this.idServicioGasto = idServicioGasto;
    }

    public ServicioModel getServicio() {
        return servicio;
    }

    public void setServicio(ServicioModel servicio) {
        this.servicio = servicio;
    }

    public GastoMedicoModel getGasto() {
        return gasto;
    }

    public void setGasto(GastoMedicoModel gasto) {
        this.gasto = gasto;
    }
}
