package com.bad115.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity(name = "db_tipoPersonalMedico")
public class TipoPersonalMedicoModel {

    @Id
    @GeneratedValue
    @Column(name = "idTipoPersonal")
    UUID idTipoPersonal;

    @Column(name = "nombreTipoPersonal", nullable = false, unique = true, length = 30)
    private String nombreTipoPersonal;

    @OneToMany(mappedBy = "tipoPersonal")
    private List<PersonalMedicoModel> personalMedico;

    public TipoPersonalMedicoModel() {
    }

    public TipoPersonalMedicoModel(String nombreTipoPersonal) {
        this.nombreTipoPersonal = nombreTipoPersonal;
    }

    public UUID getIdTipoPersonal() {
        return idTipoPersonal;
    }

    public void setIdTipoPersonal(UUID idTipoPersonal) {
        this.idTipoPersonal = idTipoPersonal;
    }

    public String getNombreTipoPersonal() {
        return nombreTipoPersonal;
    }

    public void setNombreTipoPersonal(String nombreTipoPersonal) {
        this.nombreTipoPersonal = nombreTipoPersonal;
    }

    @Override
    public String toString() {
        return "Tipo Personal Medico: {" +
                "idTipoPersonal=" + idTipoPersonal +
                ", nombreTipoPersonal='" + nombreTipoPersonal + '\'' +
                '}';
    }
}
