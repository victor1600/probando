package com.bad115.model;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "dbConsulta")
public class ConsultaModel {

    @Id
    @Column(name = "idConsulta")
    private UUID idConsulta;

    //Llaves foraneas

    //Referencia Medica
    @OneToOne(mappedBy = "fkConsultaId")
    ReferenciaMedicaModel referenciaMedica;

//    @OneToOne(mappedBy = "consulta")
//    TratamientoModel tratamiento;

    ///
    @OneToOne()
    @JoinColumn(name = "idCita", referencedColumnName = "idCita", unique = true)
    private CitaModel cita;

    @Transient
    private UUID fkCita;

    public CitaModel getCita() {
        return cita;
    }

    public void setCita(CitaModel cita) {
        this.cita = cita;
    }

    public UUID getFkCita() {
        return fkCita;
    }

    public void setFkCita(UUID fkCita) {
        this.fkCita = fkCita;
    }
    ////

    @Column(name = "fechaCosnulta", nullable = false, length = 10)
    private String fechaConsulta;

    @Column(name = "presenteEnfermedadConsulta", nullable = false, length = 50)
    private String presenteEnfermedadConsulta;

    @Column(name = "exploracionFisicaConsulta", nullable = false, length = 250)
    private String exploracionFisicaConsulta;

    @Column(name = "sintomatologiaConsulta", nullable = false, length = 250)
    private String sintomatologiaConsulta;

    public ConsultaModel() {
    }

    public ConsultaModel(UUID idConsulta, String fechaConsulta, String presenteEnfermedadConsulta, String exploracionFisicaConsulta, String sintomatologiaConsulta, UUID fkCita) {
        this.idConsulta = idConsulta;
        this.fechaConsulta = fechaConsulta;
        this.presenteEnfermedadConsulta = presenteEnfermedadConsulta;
        this.exploracionFisicaConsulta = exploracionFisicaConsulta;
        this.sintomatologiaConsulta = sintomatologiaConsulta;
        this.fkCita = fkCita;
    }

    public UUID getIdConsulta() {
        return idConsulta;
    }

    public void setIdConsulta(UUID idConsulta) {
        this.idConsulta = idConsulta;
    }

    public String getFechaConsulta() {
        return fechaConsulta;
    }

    public void setFechaConsulta(String fechaConsulta) {
        this.fechaConsulta = fechaConsulta;
    }

    public String getPresenteEnfermedadConsulta() {
        return presenteEnfermedadConsulta;
    }

    public void setPresenteEnfermedadConsulta(String presenteEnfermedadConsulta) {
        this.presenteEnfermedadConsulta = presenteEnfermedadConsulta;
    }

    public String getExploracionFisicaConsulta() {
        return exploracionFisicaConsulta;
    }

    public void setExploracionFisicaConsulta(String exploracionFisicaConsulta) {
        this.exploracionFisicaConsulta = exploracionFisicaConsulta;
    }

    public String getSintomatologiaConsulta() {
        return sintomatologiaConsulta;
    }

    public void setSintomatologiaConsulta(String sintomatologiaConsulta) {
        this.sintomatologiaConsulta = sintomatologiaConsulta;
    }

    @Override
    public String toString() {
        return "ConsultaModel{" +
                "idConsulta=" + idConsulta +
                ", fechaConsulta='" + fechaConsulta + '\'' +
                ", presenteEnfermedadConsulta='" + presenteEnfermedadConsulta + '\'' +
                ", exploracionFisicaConsulta='" + exploracionFisicaConsulta + '\'' +
                ", sintomatologiaConsulta='" + sintomatologiaConsulta + '\'' +
                '}';
    }
}
