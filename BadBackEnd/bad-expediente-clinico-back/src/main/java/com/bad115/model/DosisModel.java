package com.bad115.model;



import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "dbDosis")
public class DosisModel {

    @Id
    @Column(name = "idDosis")
    private UUID idDosis;

    @Column(name = "cantidadDosis", nullable = false, length = 6)
    private int cantidadDosis;

    @Column(name = "unidadDosis", nullable = false, length = 15)
    private String unidadDosis;

    @Column(name = "frecuenciaDosis", nullable = false, length = 15)
    private String frecuenciaDosis;

    @Transient
    private UUID fkMedicamento;

    //double check this
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_medicamento", nullable = false)
    private MedicamentoModel medicamento;

    public MedicamentoModel getMedicamento() {
        return medicamento;
    }

    public void setMedicamento(MedicamentoModel medicamento) {
        this.medicamento = medicamento;
    }

    @Transient
    private UUID fkTratamiento;

    public UUID getFkMedicamento() {
        return fkMedicamento;
    }

    public void setFkMedicamento(UUID fkMedicamento) {
        this.fkMedicamento = fkMedicamento;
    }

    // double check
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_tratamiento", nullable = true)
    private TratamientoModel tratamiento;

    @Column(name = "estaActivo", nullable = false)
    private Boolean estaActivo;

    public DosisModel(UUID idDosis, int cantidadDosis, String unidadDosis,
                      String frencuenciaDosis, Boolean estaActivo, UUID fkTratamiento, UUID fkMedicamento) {
        this.idDosis = idDosis;
        this.cantidadDosis = cantidadDosis;
        this.unidadDosis = unidadDosis;
        this.frecuenciaDosis = frencuenciaDosis;
        this.estaActivo = estaActivo;
        this.fkTratamiento = fkTratamiento;
        this.fkMedicamento = fkMedicamento;
    }

    public DosisModel() {
    }



    public UUID getIdDosis() {
        return idDosis;
    }

    public void setIdDosis(UUID idDosis) {
        this.idDosis = idDosis;
    }

    public int getCantidadDosis() {
        return cantidadDosis;
    }

    public void setCantidadDosis(int cantidadDosis) {
        this.cantidadDosis = cantidadDosis;
    }

    public String getUnidadDosis() {
        return unidadDosis;
    }

    public void setUnidadDosis(String unidadDosis) {
        this.unidadDosis = unidadDosis;
    }

    public String getFrencuenciaDosis() {
        return frecuenciaDosis;
    }

    public void setFrencuenciaDosis(String frencuenciaDosis) {
        this.frecuenciaDosis = frencuenciaDosis;
    }

    public UUID getFkTratamiento() {
        return fkTratamiento;
    }

    public void setFkTratamiento(UUID fkTratamiento) {
        this.fkTratamiento = fkTratamiento;
    }

    public TratamientoModel getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(TratamientoModel tratamiento) {
        this.tratamiento = tratamiento;
    }

    public Boolean getEstaActivo() {
        return estaActivo;
    }

    public void setEstaActivo(Boolean estaActivo) {
        this.estaActivo = estaActivo;
    }
}
