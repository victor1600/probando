package com.bad115.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "dbMedicamento")
public class MedicamentoModel {
    public MedicamentoModel() {
    }

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "idMedicamento")
    private UUID idMedicamento;

    @Column(name = "codigoMedicamento", unique = true, nullable = false, length = 50)
    private String codigoMedicamento;

    @Column(name = "nombreMedicamento", unique = true, nullable = false, length = 50)
    private String nombreMedicamento;

    @Column(name = "cantidadMedicamento", nullable = false, length = 6)
    private int cantidadMedicamento;

    @Column(name = "indicacionMedicamento", length = 200)
    private String indicacionMedicamento;

    @Column(name = "viaAdministracionMedicamento", length = 50)
    private String viaAdministracionMedicamento;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_tipoMedicamento", nullable = false)
    private TipoMedicamentoModel tipoMedicamento;

    @Column(name = "estaActivo", nullable = false)
    private Boolean estaActivo;


    public Boolean getEstaActivo() {
        return estaActivo;
    }

    public void setEstaActivo(Boolean estaActivo) {
        this.estaActivo = estaActivo;
    }

    @Transient
    private UUID fktipoMedicamento;

    public UUID getFktipoMedicamento() {
        return fktipoMedicamento;
    }

    public void setFktipoMedicamento(UUID fktipoMedicamento) {
        this.fktipoMedicamento = fktipoMedicamento;
    }

    public MedicamentoModel(UUID idMedicamento, String codigoMedicamento, String nombreMedicamento, int cantidadMedicamento, String indicacionMedicamento, String viaAdministracionMedicamento ,UUID fktipoMedicamento, Boolean estaActivo) {
        this.idMedicamento = idMedicamento;
        this.codigoMedicamento = codigoMedicamento;
        this.nombreMedicamento = nombreMedicamento;
        this.cantidadMedicamento = cantidadMedicamento;
        this.indicacionMedicamento = indicacionMedicamento;
        this.fktipoMedicamento = fktipoMedicamento;
        this.viaAdministracionMedicamento = viaAdministracionMedicamento;
        this.estaActivo = estaActivo;
    }


    public UUID getIdMedicamento() {
        return idMedicamento;
    }

    public void setIdMedicamento(UUID idMedicamento) {
        this.idMedicamento = idMedicamento;
    }

    public String getCodigoMedicamento() {
        return codigoMedicamento;
    }

    public void setCodigoMedicamento(String codigoMedicamento) {
        this.codigoMedicamento = codigoMedicamento;
    }

    public String getNombreMedicamento() {
        return nombreMedicamento;
    }

    public void setNombreMedicamento(String nombreMedicamento) {
        this.nombreMedicamento = nombreMedicamento;
    }

    public int getCantidadMedicamento() {
        return cantidadMedicamento;
    }

    public void setCantidadMedicamento(int cantidadMedicamento) {
        this.cantidadMedicamento = cantidadMedicamento;
    }

    public String getIndicacionMedicamento() {
        return indicacionMedicamento;
    }

    public void setIndicacionMedicamento(String indicacionMedicamento) {
        this.indicacionMedicamento = indicacionMedicamento;
    }

    public TipoMedicamentoModel getTipoMedicamento() {
        return tipoMedicamento;
    }

    public void setTipoMedicamento(TipoMedicamentoModel tipoMedicamento) {
        this.tipoMedicamento = tipoMedicamento;
    }

    public String getViaAdministracionMedicamento() {
        return viaAdministracionMedicamento;
    }

    public void setViaAdministracionMedicamento(String viaAdministracionMedicamento) {
        this.viaAdministracionMedicamento = viaAdministracionMedicamento;
    }
}
