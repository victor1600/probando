package com.bad115.model;

import com.bad115.model.ConsultaModel;
import com.bad115.model.ReferenciaMedicaModel;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "dbGastoMedico")
public class GastoMedicoModel {

    @Id
    @Column(name = "idGastoMedico")
    private UUID idGastoMedico;

    @Column(name = "fechaGasto", nullable = false)
    private String fechaGasto;

    /// fk consulta
    @OneToOne()
    @JoinColumn(name = "idConsulta", referencedColumnName = "idConsulta", unique = true)
    private ConsultaModel consulta;

   @Column(name = "estaActivo", nullable = false)
    private Boolean estaActivo;

    public GastoMedicoModel() {
    }

    public GastoMedicoModel(UUID idGastoMedico, String fechaGasto, ConsultaModel consulta, Boolean estaActivo) {
        this.idGastoMedico = idGastoMedico;
        this.fechaGasto = fechaGasto;
        this.consulta = consulta;
        this.estaActivo = estaActivo;
    }

    public UUID getIdGastoMedico() {
        return idGastoMedico;
    }

    public void setIdGastoMedico(UUID idGastoMedico) {
        this.idGastoMedico = idGastoMedico;
    }

    public String getFechaGasto() {
        return fechaGasto;
    }

    public void setFechaGasto(String fechaGasto) {
        this.fechaGasto = fechaGasto;
    }

    public ConsultaModel getConsulta() {
        return consulta;
    }

    public void setConsulta(ConsultaModel consulta) {
        this.consulta = consulta;
    }

    public Boolean getEstaActivo() {
        return estaActivo;
    }

    public void setEstaActivo(Boolean estaActivo) {
        this.estaActivo = estaActivo;
    }

    @Override
    public String toString() {
        return "GastoMedicoModel{" +
                "idGastoMedico=" + idGastoMedico +
                ", fechaGasto=" + fechaGasto +
                ", consulta=" + consulta +
                ", estaActivo=" + estaActivo +
                '}';
    }
}
