package com.bad115.model;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "dbReferenciaMedica")
public class ReferenciaMedicaModel {

    @Id
    @GeneratedValue
    @Column(name = "idReferencia")
    private UUID idReferencia;

    @Column(name = "detalleReferencia", nullable = false, length = 100)
    private String detalleReferencia;


    @OneToOne()
    @JoinColumn(name = "fkConsultaReferencia", referencedColumnName = "idConsulta", unique = true)
    private ConsultaModel fkConsultaId;

    //Crear atributo pivote imaginario
    @Transient
    private UUID fkConsulta;

    public ReferenciaMedicaModel() {
    }

    public ReferenciaMedicaModel(UUID idReferencia, String detalleReferencia, UUID fkConsulta) {
        this.idReferencia = idReferencia;
        this.detalleReferencia = detalleReferencia;
        this.fkConsulta = fkConsulta;
    }


    public UUID getIdReferencia() {
        return idReferencia;
    }

    public void setIdReferencia(UUID idReferencia) {
        this.idReferencia = idReferencia;
    }

    public String getDetalleReferencia() {
        return detalleReferencia;
    }

    public void setDetalleReferencia(String detalleReferencia) {
        this.detalleReferencia = detalleReferencia;
    }

    public ConsultaModel getIdConsultaReferencia() {
        return fkConsultaId;
    }

    public void setFkConsultaId(ConsultaModel fkConsultaId) {
        this.fkConsultaId = fkConsultaId;
    }

    public UUID getFkConsulta() {
        return fkConsulta;
    }

    public void setFkConsulta(UUID fkConsulta) {
        this.fkConsulta = fkConsulta;
    }

    @Override
    public String toString() {
        return "ReferenciaMedicaModel{" +
                "idReferencia=" + idReferencia +
                ", detalleReferencia='" + detalleReferencia + '\'' +
                ", fkConsultaId=" + fkConsultaId +
                ", fkConsulta=" + fkConsulta +
                '}';
    }
}
