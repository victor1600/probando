package com.bad115.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import java.util.List;

@Entity(name = "db_responsable")
@PrimaryKeyJoinColumn(name = "idResponsable")
public class ResponsableModel extends PersonaModel {

    @OneToMany(mappedBy = "responsable",cascade = CascadeType.PERSIST)
    private List<PacienteModel> pacientes;

    public ResponsableModel() {
    }

    public ResponsableModel(String nombre1Persona, String nombre2Persona, String nombre3Persona, String apellido1Persona, String apellido2Persona, String apellido3Persona, String telefonoPersona, String celularPersona, String fechaNacimientoPersona, String duiPersona, String nitPersona, String sexoPersona, DireccionModel direccion) {
        super(nombre1Persona, nombre2Persona, nombre3Persona, apellido1Persona, apellido2Persona, apellido3Persona, telefonoPersona, celularPersona, fechaNacimientoPersona, duiPersona, nitPersona, sexoPersona, direccion);
    }

    @Override
    public String toString() {
        return "Responsable: {" +
                "direccion=" + direccion +
                '}';
    }
}
