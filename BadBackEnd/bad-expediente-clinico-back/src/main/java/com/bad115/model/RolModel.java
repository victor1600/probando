package com.bad115.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@Entity
@Table(name = "dbRol")
public class RolModel {


    public RolModel() {
    }



    @Id
    @GeneratedValue
    @Column(name = "idRol")
    private UUID idRol;

    @Column(name = "nombreRol", unique = true)
    private String nombreRol;

    @Column(name = "descripcionRol")
    private String descripcionRol;

}
