package com.bad115.model;

public class DetalleResponseHttp {
    private String fecha;
    private String mensaje;
    private String url;
    private String httpCodeName;
    private int httpCodeNumber;
    private String httpCodeType;

    public DetalleResponseHttp() {
    }

    public DetalleResponseHttp(String fecha, String mensaje, String url, String httpCodeName, int httpCodeNumber, String httpCodeType) {
        this.fecha = fecha;
        this.mensaje = mensaje;
        this.url = url;
        this.httpCodeName = httpCodeName;
        this.httpCodeNumber = httpCodeNumber;
        this.httpCodeType = httpCodeType;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHttpCodeName() {
        return httpCodeName;
    }

    public void setHttpCodeName(String httpCodeName) {
        this.httpCodeName = httpCodeName;
    }

    public int getHttpCodeNumber() {
        return httpCodeNumber;
    }

    public void setHttpCodeNumber(int httpCodeNumber) {
        this.httpCodeNumber = httpCodeNumber;
    }

    public String getHttpCodeType() {
        return httpCodeType;
    }

    public void setHttpCodeType(String httpCodeType) {
        this.httpCodeType = httpCodeType;
    }
}
