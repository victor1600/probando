package com.bad115.model;

import javax.persistence.*;
import java.util.UUID;

@Entity(name = "db_persona")
@Inheritance(strategy = InheritanceType.JOINED)
public class PersonaModel {

    @Id
    @GeneratedValue
    @Column(name = "idPersona")
    private UUID idPersona;

    @Column(name = "nombre1Persona", nullable = false, length = 20)
    private String nombre1Persona;

    @Column(name = "nombre2Persona", length = 20)
    private String nombre2Persona;

    @Column(name = "nombre3Persona", length = 20)
    private String nombre3Persona;

    @Column(name = "apellido1Persona", nullable = false, length = 20)
    private String apellido1Persona;

    @Column(name = "apellido2Persona", length = 20)
    private String apellido2Persona;

    @Column(name = "apellido3Persona", length = 20)
    private String apellido3Persona;

    @Column(name = "telefonoPersona", length = 9)
    private String telefonoPersona;

    @Column(name = "celularPersona", length = 9)
    private String celularPersona;

    @Column(name = "fechaNacimientoPersona", length = 10)
    private String fechaNacimientoPersona;

    @Column(name = "duiPersona", length = 10)
    private String duiPersona;

    @Column(name = "nitPersona", length = 17)
    private String nitPersona;

    @Column(name = "sexoPersona", length = 1)
    private String sexoPersona;

    @OneToOne
    @JoinColumn(name = "id_direccion")
    DireccionModel direccion;

    public PersonaModel() {
    }

    public PersonaModel(String nombre1Persona, String nombre2Persona, String nombre3Persona, String apellido1Persona, String apellido2Persona, String apellido3Persona, String telefonoPersona, String celularPersona, String fechaNacimientoPersona, String duiPersona, String nitPersona, String sexoPersona, DireccionModel direccion) {
        this.nombre1Persona = nombre1Persona;
        this.nombre2Persona = nombre2Persona;
        this.nombre3Persona = nombre3Persona;
        this.apellido1Persona = apellido1Persona;
        this.apellido2Persona = apellido2Persona;
        this.apellido3Persona = apellido3Persona;
        this.telefonoPersona = telefonoPersona;
        this.celularPersona = celularPersona;
        this.fechaNacimientoPersona = fechaNacimientoPersona;
        this.duiPersona = duiPersona;
        this.nitPersona = nitPersona;
        this.sexoPersona = sexoPersona;
        this.direccion = direccion;
    }

    public UUID getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(UUID idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombre1Persona() {
        return nombre1Persona;
    }

    public void setNombre1Persona(String nombre1Persona) {
        this.nombre1Persona = nombre1Persona;
    }

    public String getNombre2Persona() {
        return nombre2Persona;
    }

    public void setNombre2Persona(String nombre2Persona) {
        this.nombre2Persona = nombre2Persona;
    }

    public String getNombre3Persona() {
        return nombre3Persona;
    }

    public void setNombre3Persona(String nombre3Persona) {
        this.nombre3Persona = nombre3Persona;
    }

    public String getApellido1Persona() {
        return apellido1Persona;
    }

    public void setApellido1Persona(String apellido1Persona) {
        this.apellido1Persona = apellido1Persona;
    }

    public String getApellido2Persona() {
        return apellido2Persona;
    }

    public void setApellido2Persona(String apellido2Persona) {
        this.apellido2Persona = apellido2Persona;
    }

    public String getApellido3Persona() {
        return apellido3Persona;
    }

    public void setApellido3Persona(String apellido3Persona) {
        this.apellido3Persona = apellido3Persona;
    }

    public String getTelefonoPersona() {
        return telefonoPersona;
    }

    public void setTelefonoPersona(String telefonoPersona) {
        this.telefonoPersona = telefonoPersona;
    }

    public String getCelularPersona() {
        return celularPersona;
    }

    public void setCelularPersona(String celularPersona) {
        this.celularPersona = celularPersona;
    }

    public String getFechaNacimientoPersona() {
        return fechaNacimientoPersona;
    }

    public void setFechaNacimientoPersona(String fechaNacimientoPersona) {
        this.fechaNacimientoPersona = fechaNacimientoPersona;
    }

    public String getDuiPersona() {
        return duiPersona;
    }

    public void setDuiPersona(String duiPersona) {
        this.duiPersona = duiPersona;
    }

    public String getNitPersona() {
        return nitPersona;
    }

    public void setNitPersona(String nitPersona) {
        this.nitPersona = nitPersona;
    }

    public String getSexoPersona() {
        return sexoPersona;
    }

    public void setSexoPersona(String sexoPersona) {
        this.sexoPersona = sexoPersona;
    }

    public DireccionModel getDireccion() {
        return direccion;
    }

    public void setDireccion(DireccionModel direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return "Persona :{" +
                "idPersona=" + idPersona +
                ", nombre1Persona='" + nombre1Persona + '\'' +
                ", nombre2Persona='" + nombre2Persona + '\'' +
                ", nombre3Persona='" + nombre3Persona + '\'' +
                ", apellido1Persona='" + apellido1Persona + '\'' +
                ", apellido2Persona='" + apellido2Persona + '\'' +
                ", apellido3Persona='" + apellido3Persona + '\'' +
                ", telefonoPersona='" + telefonoPersona + '\'' +
                ", celularPersona='" + celularPersona + '\'' +
                ", fechaNacimientoPersona='" + fechaNacimientoPersona + '\'' +
                ", duiPersona='" + duiPersona + '\'' +
                ", nitPersona='" + nitPersona + '\'' +
                ", sexoPersona='" + sexoPersona + '\'' +
                ", direccion=" + direccion +
                '}';
    }
}
