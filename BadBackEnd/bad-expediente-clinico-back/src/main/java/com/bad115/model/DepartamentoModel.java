package com.bad115.model;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity(name = "db_departamento")
public class DepartamentoModel {

    @Id
    @Column(name = "idDepartamento", nullable = false)
    private UUID idDepartamento;

    @Column(name = "codDepartamento", nullable = false, unique = true, length = 4)
    private String codDepartamento;

    @Column(name = "nombreDepartamento", nullable = false, length = 30)
    private String nombreDepartamento;

    @OneToMany(mappedBy = "departamento")
    private List<MunicipioModel> Municipios;

    public DepartamentoModel() {
    }

    public DepartamentoModel(UUID idDepartamento, String codDepartamento, String nombreDepartamento) {
        this.idDepartamento = idDepartamento;
        this.codDepartamento = codDepartamento;
        this.nombreDepartamento = nombreDepartamento;
    }

    public UUID getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(UUID idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public String getCodDepartamento() {
        return codDepartamento;
    }

    public void setCodDepartamento(String codDepartamento) {
        this.codDepartamento = codDepartamento;
    }

    public String getNombreDepartamento() {
        return nombreDepartamento;
    }

    public void setNombreDepartamento(String nombreDepartamento) {
        this.nombreDepartamento = nombreDepartamento;
    }

    @Override
    public String toString() {
        return "Departamento: {" +
                "idDepartamento=" + idDepartamento +
                ", codDepartamento='" + codDepartamento + '\'' +
                ", nombreDepartamento='" + nombreDepartamento + '\'' +
                '}';
    }
}
