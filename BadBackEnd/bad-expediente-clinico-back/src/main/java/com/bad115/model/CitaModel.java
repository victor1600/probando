package com.bad115.model;



import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "dbCita")
public class CitaModel  {


    @Id
    @Column(name = "idCita")
    private UUID idCita;

    public CitaModel() {
    }

    public CitaModel(UUID idCita, Date fechaCita, LocalTime horaCita, Boolean estaActivo, UUID fkTipoCita, UUID fkMedicoId, UUID fkPacienteId){
        this.idCita = idCita;
        this.fechaCita = fechaCita;
        this.horaCita = horaCita;
        this.estaActivo = estaActivo;
        this.fkTipoCita = fkTipoCita;
        this.fkMedicoId = fkMedicoId;
        this.fkPacienteId = fkPacienteId;
    }


    @Column(name = "fechaCita", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date fechaCita;

    @Column(name = "hora_cita", columnDefinition = "TIME")
    private LocalTime horaCita;
//    @JsonFormat(pattern="HH:mm:ss")
//    @Column( name= "horaCita", nullable = false)
//    @Temporal(TemporalType.TIME)
//    private java.util.Date horaCita;
//    @Column(name = "horaCita", nullable = false, length = 50)
//    private String horaCita;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_medico", nullable = false)
    private MedicoModel medico;

    @Transient
    private UUID fkMedicoId;


    /////// Paciente
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_paciente", nullable = false)
    private PacienteModel paciente;

    @Transient
    private UUID fkPacienteId;

    public PacienteModel getPaciente() {
        return paciente;
    }

    public void setPaciente(PacienteModel paciente) {
        this.paciente = paciente;
    }

    public UUID getFkPacienteId() {
        return fkPacienteId;
    }

    public void setFkPacienteId(UUID fkPacienteId) {
        this.fkPacienteId = fkPacienteId;
    }

    ////

    @Column(name = "estaActivo", nullable = false)
    private Boolean estaActivo;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_tipo_cita", nullable = false)
    private TipoCitaModel tipoCita;

    @Transient
    private UUID fkTipoCita;

    public MedicoModel getMedico() {
        return medico;
    }

    public void setMedico(MedicoModel medico) {
        this.medico = medico;
    }

    public UUID getFkMedicoId() {
        return fkMedicoId;
    }

    public void setFkMedicoId(UUID fkMedicoId) {
        this.fkMedicoId = fkMedicoId;
    }

    // consulta oneToOne
//    @OneToOne()
//    @JoinColumn(name = "idConsulta", referencedColumnName = "idConsulta", unique = true)
//    private ConsultaModel consulta;
//
//    @Transient
//    private UUID fkConsultaId;
//
//    public ConsultaModel getConsulta() {
//        return consulta;
//    }
//
//    public void setConsulta(ConsultaModel consulta) {
//        this.consulta = consulta;
//    }
//
//    public UUID getFkConsultaId() {
//        return fkConsultaId;
//    }
//
//    public void setFkConsultaId(UUID fkConsultaId) {
//        this.fkConsultaId = fkConsultaId;
//    }
////


    public UUID getIdCita() {
        return idCita;
    }

    public void setIdCita(UUID idCita) {
        this.idCita = idCita;
    }

    public Date getFechaCita() {
        return fechaCita;
    }

    public void setFechaCita(Date fechaCita) {
        this.fechaCita = fechaCita;
    }

    public LocalTime getHoraCita() {
        return horaCita;
    }

    public void setHoraCita(LocalTime horaCita) {
        this.horaCita = horaCita;
    }

    public Boolean getEstaActivo() {
        return estaActivo;
    }

    public void setEstaActivo(Boolean estaActivo) {
        this.estaActivo = estaActivo;
    }

    public TipoCitaModel getTipoCita() {
        return tipoCita;
    }

    public void setTipoCita(TipoCitaModel tipoCita) {
        this.tipoCita = tipoCita;
    }

    public UUID getFkTipoCita() {
        return fkTipoCita;
    }

    public void setFkTipoCita(UUID fkTipoCita) {
        this.fkTipoCita = fkTipoCita;
    }

    @Override
    public String toString() {
        return "CitaModel{" +
                "idCita=" + idCita +
                ", fechaCita=" + fechaCita +
                ", horaCita='" + horaCita + '\'' +
                ", estaActivo=" + estaActivo +
                ", tipoCita=" + tipoCita +
                ", fkTipoCita=" + fkTipoCita +
                '}';
    }
}

// IDCONSULTA