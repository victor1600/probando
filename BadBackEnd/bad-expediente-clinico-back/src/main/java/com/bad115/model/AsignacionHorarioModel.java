package com.bad115.model;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "dbAsignacionHorario")
public class AsignacionHorarioModel {

    @Id
    @Column(name = "idAsignacionHorario")
    private UUID idAsignacionHorario;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "idMedico",nullable=false)
    private MedicoModel medico;
    @Transient
    private UUID fkMedico;
    @Column(name = "fechaInicio", nullable = false)
    private String fechaInicio;
    @Column(name = "fechaFin", nullable = false)
    private String fechaFin;
    @Column(name = "horaInicio", nullable = false)
    private String horaInicio;
    @Column(name = "horaFin", nullable = false)
    private String horaFin;

    @Column(name = "duracionConsulta", nullable = false)
    private int duracionConsulta;

    @Column(name = "turno", nullable = false)
    private int turno;

    @Column(name = "estaActivo", nullable = false)
    private Boolean estaActivo;

    public AsignacionHorarioModel() {
    }

    public AsignacionHorarioModel(UUID idAsignacionHorario, UUID fkMedico, String fechaInicio, String fechaFin, String horaInicio, String horaFin, int duracionConsulta, int turno, Boolean estaActivo) {
        this.idAsignacionHorario = idAsignacionHorario;
        this.fkMedico = fkMedico;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.horaInicio = horaInicio;
        this.horaFin = horaFin;
        this.duracionConsulta = duracionConsulta;
        this.turno = turno;
        this.estaActivo = estaActivo;
    }

    public UUID getIdAsignacionHorario() {
        return idAsignacionHorario;
    }

    public void setIdAsignacionHorario(UUID idAsignacionHorario) {
        this.idAsignacionHorario = idAsignacionHorario;
    }

    public MedicoModel getMedico() {
        return medico;
    }

    public void setMedico(MedicoModel medico) {
        this.medico = medico;
    }

    public UUID getFkMedico() {
        return fkMedico;
    }

    public void setFkMedico(UUID fkMedico) {
        this.fkMedico = fkMedico;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public int getDuracionConsulta() {
        return duracionConsulta;
    }

    public void setDuracionConsulta(int duracionConsulta) {
        this.duracionConsulta = duracionConsulta;
    }

    public int getTurno() {
        return turno;
    }

    public void setTurno(int turno) {
        this.turno = turno;
    }

    public Boolean getEstaActivo() {
        return estaActivo;
    }

    public void setEstaActivo(Boolean estaActivo) {
        this.estaActivo = estaActivo;
    }

    @Override
    public String toString() {
        return "AsignacionHorarioModel{" +
                "idAsignacionHorario=" + idAsignacionHorario +
                ", medico=" + medico +
                ", fkMedico=" + fkMedico +
                ", fechaInicio='" + fechaInicio + '\'' +
                ", fechaFin='" + fechaFin + '\'' +
                ", horaInicio='" + horaInicio + '\'' +
                ", horaFin='" + horaFin + '\'' +
                ", duracionConsulta=" + duracionConsulta +
                ", turno=" + turno +
                ", estaActivo=" + estaActivo +
                '}';
    }
}
