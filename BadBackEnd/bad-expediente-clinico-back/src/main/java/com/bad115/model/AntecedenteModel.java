package com.bad115.model;

import javax.persistence.*;
import java.util.UUID;

@Entity(name = "db_antecedente")
public class AntecedenteModel {

    @Id
    @GeneratedValue
    @Column(name = "idAntecedente")
    UUID idAntecedente;

    @Column(name = "nombreAntecedente", nullable = false, unique = true, length = 50)
    private String nombreAntecedente;

    @Column(name = "descripcionAntecedente", length = 200)
    private String descripcionAntecedente;

    @ManyToOne
    @JoinColumn(name = "id_tipoAntecedente",nullable=false)
    private TipoAntecedenteModel tipoAntecedente;

    public AntecedenteModel() {
    }

    public AntecedenteModel(String nombreAntecedente, String descripcionAntecedente) {
        this.nombreAntecedente = nombreAntecedente;
        this.descripcionAntecedente = descripcionAntecedente;
    }

    public UUID getIdAntecedente() {
        return idAntecedente;
    }

    public void setIdAntecedente(UUID idAntecedente) {
        this.idAntecedente = idAntecedente;
    }

    public String getNombreAntecedente() {
        return nombreAntecedente;
    }

    public void setNombreAntecedente(String nombreAntecedente) {
        this.nombreAntecedente = nombreAntecedente;
    }

    public String getDescripcionAntecedente() {
        return descripcionAntecedente;
    }

    public void setDescripcionAntecedente(String descripcionAntecedente) {
        this.descripcionAntecedente = descripcionAntecedente;
    }

    public TipoAntecedenteModel getTipoAntecedente() {
        return tipoAntecedente;
    }

    public void setTipoAntecedente(TipoAntecedenteModel tipoAntecedente) {
        this.tipoAntecedente = tipoAntecedente;
    }
}
