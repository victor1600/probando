package com.bad115.model;

import javax.persistence.*;
import java.util.UUID;

@Entity(name = "db_direccion")


public class DireccionModel {

    @Id
    @Column(name = "idDireccion")
    private UUID idDireccion;

    @Column(name = "urbanizacion", length = 100)
    private String  urbanizacion;

    @Column(name = "calleAvenida", length = 100)
    private String  calleAvenida;

    @Column(name = "numeroPasaje", length = 16)
    private String  numeroPasaje;

    @Column(name = "numeroCasa", length = 10)
    private String  numeroCasa;

    //@Column(name = "complementoDireccion", length = 50)
    // String  complementoDireccion;

    @ManyToOne
    @JoinColumn(name = "id_Municipio")
    private MunicipioModel municipio;

    @OneToOne(mappedBy = "direccion", cascade = CascadeType.PERSIST)
    PersonaModel persona;

    public DireccionModel() {
    }

    public DireccionModel(UUID idDireccion, String urbanizacion, String calleAvenida, String numeroPasaje, String numeroCasa, String complementoDireccion, MunicipioModel municipio) {
        this.idDireccion = idDireccion;
        this.urbanizacion = urbanizacion;
        this.calleAvenida = calleAvenida;
        this.numeroPasaje = numeroPasaje;
        this.numeroCasa = numeroCasa;
        //this.complementoDireccion = complementoDireccion;
        this.municipio = municipio;
    }

    public UUID getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(UUID idDireccion) {
        this.idDireccion = idDireccion;
    }

    public String getUrbanizacion() {
        return urbanizacion;
    }

    public void setUrbanizacion(String urbanizacion) {
        this.urbanizacion = urbanizacion;
    }

    public String getCalleAvenida() {
        return calleAvenida;
    }

    public void setCalleAvenida(String calleAvenida) {
        this.calleAvenida = calleAvenida;
    }

    public String getNumeroPasaje() {
        return numeroPasaje;
    }

    public void setNumeroPasaje(String numeroPasaje) {
        this.numeroPasaje = numeroPasaje;
    }

    public String getNumeroCasa() {
        return numeroCasa;
    }

    public void setNumeroCasa(String numeroCasa) {
        this.numeroCasa = numeroCasa;
    }

    /*public String getComplementoDireccion() {
        return complementoDireccion;
    }

    public void setComplementoDireccion(String complementoDireccion) {
        this.complementoDireccion = complementoDireccion;
    }*/

    public MunicipioModel getMunicipio() {
        return municipio;
    }

    public void setMunicipio(MunicipioModel municipio) {
        this.municipio = municipio;
    }

    @Override
    public String toString() {
        return "Direccion: {" +
                "idDireccion=" + idDireccion +
                ", urbanizacion='" + urbanizacion + '\'' +
                ", calleAvenida='" + calleAvenida + '\'' +
                ", numeroPasaje='" + numeroPasaje + '\'' +
                ", numeroCasa='" + numeroCasa + '\'' +
                //", complementoDireccion='" + complementoDireccion + '\'' +
                ", municipio=" + municipio +
                '}';
    }
}
