package com.bad115.repository;

import com.bad115.model.CitaModel;
import com.bad115.model.TipoExamenModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("TipoExamenRepository")
public interface TipoExamenRepository extends JpaRepository<TipoExamenModel, UUID> {
    TipoExamenModel findByIdTipoExamen(UUID id);

    TipoExamenModel findByNombreTipoExamen(String nombre);

    @Query(value = "select * from lista_tipo_examenes()", nativeQuery = true)
    List<TipoExamenModel> lista_activos();

    @Procedure(procedureName = "toggle_tipo_examenes")
    Boolean toggle(UUID p_id, Boolean p_state);
}
