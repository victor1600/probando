package com.bad115.repository;

import com.bad115.model.ResponsableModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("ResponsableRepository")
public interface ResponsableRepository extends JpaRepository<ResponsableModel, UUID> {
    ResponsableModel findByIdPersona(UUID id);
    ResponsableModel findByDuiPersona(String dui);

}
