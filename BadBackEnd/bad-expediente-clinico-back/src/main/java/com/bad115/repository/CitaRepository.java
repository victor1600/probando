package com.bad115.repository;

import com.bad115.model.CitaModel;
import com.bad115.model.ServicioModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("CitaRepository")
public interface CitaRepository extends JpaRepository<CitaModel, UUID> {
    CitaModel findByIdCita(UUID id);

    @Query(value = "select * from lista_citas()", nativeQuery = true)
    List<CitaModel> lista_activos();

    @Procedure(procedureName = "toggle_cita")
    Boolean toggle(UUID p_id, Boolean p_state);

    @Procedure(procedureName = "validacion_cita")
    String checker(UUID p_id, UUID p_fk);
}
