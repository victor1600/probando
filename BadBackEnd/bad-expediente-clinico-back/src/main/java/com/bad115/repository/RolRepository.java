package com.bad115.repository;

import com.bad115.model.RolModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("RolRepository")
public interface RolRepository extends JpaRepository<RolModel, UUID> {
    RolModel findByIdRol(UUID id);

    @Query(value="SELECT * FROM db_rol WHERE id_rol != '77fd0c61-0cd7-495f-bce3-f510eec61f53'", nativeQuery=true)
    List<RolModel> findRolesInit();

}
