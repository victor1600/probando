package com.bad115.repository;

import com.bad115.model.BoletaCirugiaModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("BoletaCirugiaRepository")
public interface BoletaCirugiaRepository extends JpaRepository<BoletaCirugiaModel, UUID> {

    BoletaCirugiaModel findByIdBoletaCirugia(UUID id);
    BoletaCirugiaModel findByFechaBoletaCirugia(String fechaCirugia);
}
