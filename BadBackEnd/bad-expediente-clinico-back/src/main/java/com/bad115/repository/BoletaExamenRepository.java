package com.bad115.repository;

import com.bad115.model.BoletaExamenModel;
import com.bad115.model.CitaModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("BoletaExamenRepository")
public interface BoletaExamenRepository extends JpaRepository<BoletaExamenModel, UUID> {
    BoletaExamenModel findByIdBoleta(UUID id);

    @Query(value = "select * from lista_boleta_examen();", nativeQuery = true)
    List<BoletaExamenModel> lista_activos();

    @Procedure(procedureName = "toggle_boleta_examen")
    Boolean toggle(UUID p_id, Boolean p_state);

    @Procedure(procedureName = "validacion_boleta")
    String checker(UUID p_id, UUID p_id2);
}
