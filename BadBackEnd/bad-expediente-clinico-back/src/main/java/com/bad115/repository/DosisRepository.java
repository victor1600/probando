package com.bad115.repository;

import com.bad115.model.DosisModel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("DosisRepository")
public interface DosisRepository extends JpaRepository<DosisModel, UUID> {
    DosisModel findByIdDosis(UUID id);

    @Procedure(procedureName = "validacion_dosis")
    String checker(UUID p_id_med, UUID p_id_tratamiento);

    @Procedure(procedureName = "toggle_medicamentos")
    Boolean toggle(UUID p_id, Boolean p_state);

    // lista_dosis_activos
    @Query(value = "SELECT * from lista_dosis_activos();", nativeQuery = true)
    List<DosisModel> lista_activos();
}
