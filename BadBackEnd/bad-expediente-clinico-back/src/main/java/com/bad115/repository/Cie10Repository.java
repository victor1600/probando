package com.bad115.repository;

import com.bad115.model.Cie10Model;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;


@Repository("Cie10Repository")
public interface Cie10Repository extends JpaRepository<Cie10Model, UUID> {

    Cie10Model findByIdCIE10(UUID id);
    Cie10Model findByCodigoCIE10(String codigo);
    Cie10Model findByDescripcionCIE10(String descripcion);
}

