package com.bad115.repository;

import com.bad115.model.DireccionModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("DireccionRepository")
public interface DireccionRepository extends JpaRepository<DireccionModel, UUID> {

    DireccionModel findByIdDireccion(UUID id);
}
