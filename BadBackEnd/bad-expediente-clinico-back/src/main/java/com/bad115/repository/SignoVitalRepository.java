package com.bad115.repository;

import com.bad115.model.EspecialidadModel;
import com.bad115.model.SignoVitalModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("SignoVitalRepository")
public interface SignoVitalRepository extends JpaRepository<SignoVitalModel, UUID> {
    SignoVitalModel findByIdVital(UUID id);
}
