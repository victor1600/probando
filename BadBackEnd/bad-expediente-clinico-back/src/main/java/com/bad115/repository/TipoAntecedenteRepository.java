package com.bad115.repository;

import com.bad115.model.TipoAntecedenteModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("TipoAntecedenteRepository")
public interface TipoAntecedenteRepository extends JpaRepository<TipoAntecedenteModel, UUID> {

    TipoAntecedenteModel findByIdTipoAntecedente(UUID id);
}
