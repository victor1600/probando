package com.bad115.repository;

import com.bad115.model.ServicioGastoModel;
import com.bad115.model.ServicioModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("ServicioGastoRepository")
public interface ServicioGastoRepository extends JpaRepository<ServicioGastoModel, UUID> {
    ServicioGastoModel findByIdServicioGasto(UUID id);

}
