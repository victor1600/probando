package com.bad115.repository;

import com.bad115.model.Cie10Model;
import com.bad115.model.ServicioModel;
import com.bad115.model.TipoExamenModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("ServicioRepository")
public interface ServicioRepository extends JpaRepository<ServicioModel, UUID> {
    ServicioModel findByIdServicio(UUID id);
    ServicioModel findByNombreServicio(String nombre);

    @Query(value = "select * from lista_servicio()", nativeQuery = true)
    List<ServicioModel> lista_activos();

    @Procedure(procedureName = "toggle_servicio")
    Boolean toggle(UUID p_id, Boolean p_state);
}
