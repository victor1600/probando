package com.bad115.repository;

import com.bad115.model.IngresoModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("IngresoRepository")
public interface IngresoRepository extends JpaRepository<IngresoModel, UUID> {

    IngresoModel findByIdIngreso(UUID id);
    IngresoModel findByFechaIngreso(String fechaIngreso);
}
