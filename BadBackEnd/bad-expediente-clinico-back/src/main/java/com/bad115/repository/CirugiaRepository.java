package com.bad115.repository;

import com.bad115.model.CirugiaModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("CirugiaRepository")
public interface CirugiaRepository extends JpaRepository<CirugiaModel, UUID> {

    CirugiaModel findByIdCirugia(UUID id);
    CirugiaModel findByNombreCirugia(String nombre);

}
