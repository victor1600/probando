package com.bad115.repository;

import com.bad115.model.ReferenciaMedicaModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("ReferenciaMedicaRepository")
public interface ReferenciaMedicaRepository extends JpaRepository<ReferenciaMedicaModel, UUID> {

    ReferenciaMedicaModel findByIdReferencia(UUID id);

    @Query(value="SELECT * FROM db_referencia_medica x WHERE x.fk_consulta_referencia= ?1", nativeQuery=true)
    ReferenciaMedicaModel findReferenciaMedicaModelByFkConsultaId(UUID id);

}
