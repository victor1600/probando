package com.bad115.repository;

import com.bad115.model.PacienteModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("PacienteRepository")
public interface PacienteRepository extends JpaRepository<PacienteModel, UUID> {

    PacienteModel findByIdPersona(UUID id);
    PacienteModel findByNombre1Persona(String nombre);

    @Query("SELECT p FROM db_paciente p WHERE p.idPersona = ?1")
    PacienteModel detallePacienteId(UUID id);

    @Query("SELECT p FROM db_paciente p WHERE p.idPersona = ?1")
    List<PacienteModel> arrayDetallePacienteId(UUID id);

}
