package com.bad115.repository;

import com.bad115.model.CamaModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("CamaRepository")
public interface CamaRepository extends JpaRepository<CamaModel, UUID> {
    CamaModel findByIdCama(UUID id);
    //CamaModel findByIdCodigoCama(String codigo);
}