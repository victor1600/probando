package com.bad115.repository;

import com.bad115.model.MunicipioModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("MunicipioRepository")
public interface MunicipioRepository extends JpaRepository<MunicipioModel, UUID> {
}
