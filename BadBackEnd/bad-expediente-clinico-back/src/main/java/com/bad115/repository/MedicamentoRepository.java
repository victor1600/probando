package com.bad115.repository;

import com.bad115.model.MedicamentoModel;
import com.bad115.model.ReferenciaMedicaModel;
import com.bad115.model.TipoMedicamentoModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("MedicamentoRepository")
public interface MedicamentoRepository extends JpaRepository<MedicamentoModel, UUID> {
    MedicamentoModel findByIdMedicamento(UUID id);

    // ????
    List<MedicamentoModel> findByTipoMedicamento_IdTipoMedicamento(UUID id);

    @Query(value = "SELECT * FROM db_medicamento x WHERE x.id_tipo_medicamento= ?1", nativeQuery = true)
    MedicamentoModel findMedicamentoModelByFktipoMedicamentoId(UUID id);

    MedicamentoModel findByCodigoMedicamento(String nombre);

    MedicamentoModel findByNombreMedicamento(String nombre);


    @Procedure(procedureName = "validacion_medicamento")
    String checker(String p_nombre_med, String p_codigo_med, UUID p_id_tipo_med);

    @Procedure(procedureName = "toggle_medicamentos")
    Boolean toggle(UUID p_id, Boolean p_state);

    @Query(value = "SELECT * from lista_medicamentos_activos();", nativeQuery = true)
    List<MedicamentoModel> lista_activos();

}

