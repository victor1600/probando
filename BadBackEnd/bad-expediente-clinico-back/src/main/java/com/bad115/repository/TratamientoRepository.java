package com.bad115.repository;


import com.bad115.model.TipoCitaModel;
import com.bad115.model.TratamientoModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("TratamientoRepository")
public interface TratamientoRepository extends JpaRepository<TratamientoModel, UUID> {
    TratamientoModel findByIdTratamiento(UUID id);

    @Query(value = "select * from lista_tratamientos_activos();", nativeQuery = true)
    List<TratamientoModel> lista_activos();

    @Procedure(procedureName = "toggle_tratamiento")
    Boolean toggle(UUID p_id, Boolean p_state);

    @Procedure(procedureName = "validacion_tratamiento")
    String checker(UUID p_id, UUID p_fk);
}
