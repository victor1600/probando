package com.bad115.repository;

import com.bad115.model.PersonalMedicoModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("PersonalMedicoRepository")
public interface PersonalMedicoRepository extends JpaRepository<PersonalMedicoModel, UUID> {

    PersonalMedicoModel findByIdPersona(UUID id);

    @Query("SELECT pm FROM db_personalMedico pm WHERE pm.tipoPersonal.nombreTipoPersonal='Medico'")
    List<PersonalMedicoModel> obtenerMedicos();

    @Query("SELECT pm FROM db_personalMedico pm WHERE pm.idPersona =?1 AND pm.tipoPersonal.nombreTipoPersonal='Medico'")
    PersonalMedicoModel obtenerMedicoId(UUID id);
}
