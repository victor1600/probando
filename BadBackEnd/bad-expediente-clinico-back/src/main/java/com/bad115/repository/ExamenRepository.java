package com.bad115.repository;

import com.bad115.model.ExamenModal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("ExamenRepository")
public interface ExamenRepository extends JpaRepository<ExamenModal, UUID> {
ExamenModal findByIdExamen(UUID id);

}
