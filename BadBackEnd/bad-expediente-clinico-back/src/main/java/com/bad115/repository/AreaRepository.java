package com.bad115.repository;

import com.bad115.model.AreaModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("AreaRepository")
public interface AreaRepository extends JpaRepository<AreaModel, UUID> {

    AreaModel findByIdArea(UUID id);
    AreaModel findByNombreArea(String nombre);
}

