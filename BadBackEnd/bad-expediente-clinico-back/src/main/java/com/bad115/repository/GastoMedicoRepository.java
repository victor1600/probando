package com.bad115.repository;

import com.bad115.model.GastoMedicoModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository("GastoMedicoRepository")
public interface GastoMedicoRepository extends JpaRepository<GastoMedicoModel, UUID> {
    GastoMedicoModel findByIdGastoMedico(UUID id);
    GastoMedicoModel findByFechaGasto(Date fechaGasto);

    @Query(value = "select * from lista_gasto_medico()", nativeQuery = true)
    List<GastoMedicoModel> lista_activos();

    @Procedure(procedureName = "toggle_gasto_medico")
    Boolean toggle(UUID p_id, Boolean p_state);

}
