package com.bad115.repository;

import com.bad115.model.MedicamentoModel;
import com.bad115.model.TipoCitaModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("TipoCitaRepository")
public interface TipoCitaRepository extends JpaRepository<TipoCitaModel, UUID> {
    TipoCitaModel findByIdTipoCita(UUID id);

    @Procedure(procedureName = "validacion_tipo_cita")
    String checker(String p_nombre, UUID p_id);

    //toggle_tipo_cita
    @Procedure(procedureName = "toggle_tipo_cita")
    Boolean toggle(UUID p_id, Boolean p_state);


    @Query(value = "select * from lista_tipo_citas_activos();", nativeQuery = true)
    List<TipoCitaModel> lista_activos();
}
