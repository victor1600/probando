package com.bad115.repository;

import com.bad115.model.MedicamentoModel;
import com.bad115.model.MedicoModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository("MedicoRepository")
public interface MedicoRepository extends JpaRepository<MedicoModel, UUID> {
    MedicoModel findByIdMedico(UUID id);

    /* Procedimiento agregado por victor para obtener medicos para las citas:
     */
    @Query(value = "SELECT * from doctores_disponibles(:p_fecha_cita, :p_hora_elegida, :p_id_especialidad);", nativeQuery = true)
    List<MedicoModel> doctoresCita(@Param("p_fecha_cita") String p_fecha_cita, @Param("p_hora_elegida") LocalTime p_hora_elegida,
                                   @Param("p_id_especialidad") UUID p_id_especialidad);
}
