package com.bad115.repository;

import com.bad115.model.AsignacionHorarioModel;
import com.bad115.model.HorarioModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("HorarioRepository")
public interface HorarioRepository extends JpaRepository<HorarioModel, UUID> {
    HorarioModel findByIdHorario(UUID id);

    @Query(value = "select * from lista_horario()", nativeQuery = true)
    List<HorarioModel> lista_activos();

    @Procedure(procedureName = "toggle_horario")
    Boolean toggle(UUID p_id, Boolean p_state);
}
