package com.bad115.repository;

import com.bad115.model.DosisModel;
import com.bad115.model.TipoMedicamentoModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("TipoMedicamentoRepository")
public interface TipoMedicamentoRepository extends JpaRepository<TipoMedicamentoModel, UUID> {
    TipoMedicamentoModel findByIdTipoMedicamento(UUID id);

    TipoMedicamentoModel findByNombreTMedicamento(String nombre);

    @Procedure(procedureName = "toggle_tipoM")
    Boolean toggle(UUID p_id, Boolean p_state);


    @Query(value = "Select * from lista_tipo_medicamentos_activos();", nativeQuery = true)
    List<TipoMedicamentoModel> lista_activos();
}
