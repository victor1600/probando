package com.bad115.repository;

import com.bad115.model.AntecedenteModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("AntecedenteRepository")
public interface AntecedenteRepository extends JpaRepository<AntecedenteModel, UUID> {

    AntecedenteModel findByIdAntecedente(UUID id);
}

