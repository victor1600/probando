package com.bad115.repository;

import com.bad115.model.AsuetoModel;
import com.bad115.model.ServicioModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("AsuetoRepository")
public interface AsuetoRepository extends JpaRepository<AsuetoModel, UUID> {
    AsuetoModel findByIdAsueto(UUID id);
    AsuetoModel findByNombreAsueto(String nombre);

    @Query(value = "select * from lista_asueto()", nativeQuery = true)
    List<AsuetoModel> lista_activos();

    @Procedure(procedureName = "toggle_asueto")
    Boolean toggle(UUID p_id, Boolean p_state);
}
