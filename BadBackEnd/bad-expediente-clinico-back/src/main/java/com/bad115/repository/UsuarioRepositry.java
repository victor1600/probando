package com.bad115.repository;

import com.bad115.model.UsuarioModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.UUID;

@Repository("UsuarioRepository")
public interface UsuarioRepositry extends JpaRepository <UsuarioModel, UUID> {

    UsuarioModel findByNombreUsuario(String nombre);

    @Procedure(procedureName = "proc_check_permiso")
    Integer checkPermiso(String nombreUsuario, String nombrePermiso);


}
