package com.bad115.repository;

import com.bad115.model.AsignacionHorarioModel;
import com.bad115.model.GastoMedicoModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository("AsignacionHorarioRepository")
public interface AsignacionHorarioRepository extends JpaRepository<AsignacionHorarioModel, UUID> {
    AsignacionHorarioModel findByIdAsignacionHorario(UUID id);
    AsignacionHorarioModel findByTurno(int turno);

    @Query(value = "select * from lista_asignacion_horario()", nativeQuery = true)
    List<AsignacionHorarioModel> lista_activos();

    @Procedure(procedureName = "toggle_asignacion_horario")
    Boolean toggle(UUID p_id, Boolean p_state);
}
