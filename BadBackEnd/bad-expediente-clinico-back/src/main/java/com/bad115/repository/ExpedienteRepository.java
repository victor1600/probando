package com.bad115.repository;

import com.bad115.model.ExpedienteModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("ExpedienteRepository")
public interface ExpedienteRepository extends JpaRepository<ExpedienteModel, UUID> {
    ExpedienteModel findByIdExpediente(UUID id);
    ExpedienteModel findByCodigoExpediente(String codigoExpediente);

}
