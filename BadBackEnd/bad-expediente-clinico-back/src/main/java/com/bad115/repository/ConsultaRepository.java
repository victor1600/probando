package com.bad115.repository;

import com.bad115.model.ConsultaModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("ConsultaRepository")
public interface ConsultaRepository extends JpaRepository<ConsultaModel, UUID> {
    ConsultaModel findByIdConsulta(UUID id);
}
