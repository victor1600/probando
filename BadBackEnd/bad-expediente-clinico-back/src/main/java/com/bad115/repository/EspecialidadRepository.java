package com.bad115.repository;

import com.bad115.model.EspecialidadModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("EspecialidadRepository")
public interface EspecialidadRepository extends JpaRepository<EspecialidadModel, UUID> {

    EspecialidadModel findByIdEspecialidad(UUID id);
    EspecialidadModel findByNombreEspecialidad(String nombre);

}
