package com.bad115.repository;

import com.bad115.model.TipoPersonalMedicoModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("TipoPersonalMedicoRepository")
public interface TipoPersonalMedicoRepository extends JpaRepository<TipoPersonalMedicoModel, UUID> {

    TipoPersonalMedicoModel findByIdTipoPersonal(UUID id);
    TipoPersonalMedicoModel findByNombreTipoPersonal(String nombre);

}
