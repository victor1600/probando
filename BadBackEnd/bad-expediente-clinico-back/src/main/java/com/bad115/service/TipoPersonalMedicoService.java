package com.bad115.service;

import com.bad115.model.PacienteModel;
import com.bad115.model.PersonalMedicoModel;
import com.bad115.model.TipoPersonalMedicoModel;
import com.bad115.repository.PersonalMedicoRepository;
import com.bad115.repository.TipoPersonalMedicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("TipoPersonalMedicoService")
public class TipoPersonalMedicoService {
    @Autowired
    @Qualifier("TipoPersonalMedicoRepository")
    TipoPersonalMedicoRepository repository;

    public List<TipoPersonalMedicoModel> obtenerTodosTipoPersonalMedico(){
        return repository.findAll();
    }

    public TipoPersonalMedicoModel obtenerTipoPersonalMedicoId(UUID id){
        TipoPersonalMedicoModel tipoPersonal = repository.findByIdTipoPersonal(id);
        return tipoPersonal;
    }

    public TipoPersonalMedicoModel obtenerTipoPersonalMedicoNombre(String nombre){
        return repository.findByNombreTipoPersonal(nombre);
    }


    public boolean agregarTipoPersonalMedico(TipoPersonalMedicoModel tipoPersonal){
        try {
            repository.save(tipoPersonal);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean actualizarTipoPersonalMedico(TipoPersonalMedicoModel tipoPersonal){
        try {
            repository.save(tipoPersonal);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean eliminarTipoPersonalMedico(UUID id){
        try{
            TipoPersonalMedicoModel tipoPersonal = repository.findByIdTipoPersonal(id);
            repository.delete(tipoPersonal);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
