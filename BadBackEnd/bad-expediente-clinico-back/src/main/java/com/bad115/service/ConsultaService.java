package com.bad115.service;

import com.bad115.model.ConsultaModel;
import com.bad115.repository.ConsultaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("ConsultaService")
public class ConsultaService {

    @Autowired
    @Qualifier("ConsultaRepository")
    ConsultaRepository repository;

    public ConsultaModel obtenerIdConsulta(UUID id){
        return repository.findByIdConsulta(id);
    }
}
