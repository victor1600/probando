package com.bad115.service;

import com.bad115.model.ExpedienteModel;
import com.bad115.repository.ExpedienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("ExpedienteService")
public class ExpedienteService {

    @Autowired
    @Qualifier("ExpedienteRepository")
    ExpedienteRepository repository;

    public List<ExpedienteModel> obtenerTodasExpediente(){
        return repository.findAll();
    }
    public ExpedienteModel obtenerExpedienteId(UUID id){
        return repository.findByIdExpediente(id);
    }
    public ExpedienteModel obtenerExpedienteCodigo(String codigoExpediente){
        return repository.findByCodigoExpediente(codigoExpediente);
    }

    public boolean agregarExpediente(ExpedienteModel expediente){
        try {
            repository.save(expediente);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean actualizarExpediente(ExpedienteModel expediente){
        try {
            repository.save(expediente);
            return true;
        }catch (Exception e){
            return false;
        }
    }




}
