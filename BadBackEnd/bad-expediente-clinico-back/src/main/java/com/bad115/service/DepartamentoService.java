package com.bad115.service;

import com.bad115.model.DepartamentoModel;
import com.bad115.repository.DepartamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("DepartamentoService")
public class DepartamentoService {

    @Autowired
    @Qualifier("DepartamentoRepository")
    DepartamentoRepository repository;

    public List<DepartamentoModel> obtenerDepartamentos(){
        return repository.findAll();
    }
}
