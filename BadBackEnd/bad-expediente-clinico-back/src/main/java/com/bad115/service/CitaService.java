package com.bad115.service;

import com.bad115.model.CitaModel;
import com.bad115.repository.CitaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("CitaService")
public class CitaService {
    @Autowired
    @Qualifier("CitaRepository")
    CitaRepository repository;

    public List<CitaModel> obtenerTodos() {
        return repository.findAll();
    }

    public CitaModel obtenerPorId(UUID id) {
        return repository.findByIdCita(id);
    }

    public boolean agregar(CitaModel cita) {
        try {
            repository.save(cita);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean eliminar(UUID id) {
        try {
            CitaModel cita = repository.findByIdCita(id);
            repository.delete(cita);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public List<CitaModel> lista_activos(){
        return repository.lista_activos();
    }

    public Boolean toggle(UUID p_id, Boolean p_state){
        return repository.toggle(p_id, p_state);
    }

    public String checker(UUID p_id, UUID p_fk) {
        return repository.checker(p_id, p_fk);
    }
}
