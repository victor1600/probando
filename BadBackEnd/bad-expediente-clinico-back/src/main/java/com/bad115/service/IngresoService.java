package com.bad115.service;

import com.bad115.model.AreaModel;
import com.bad115.model.IngresoModel;
import com.bad115.repository.IngresoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("IngresoService")
public class IngresoService {

    @Autowired
    @Qualifier("IngresoRepository")
    IngresoRepository repository;

    public List<IngresoModel> obtenerTodosLosIngresos(){
        return repository.findAll();
    }

    public IngresoModel obtenerIngresoId(UUID id){
        IngresoModel ingreso = repository.findByIdIngreso(id);
        return ingreso;
    }

    public IngresoModel obtenerIngresoFecha(String fechaIngreso){ return repository.findByFechaIngreso(fechaIngreso); }

    public boolean agregarIngreso(IngresoModel ingreso){
        try {
            repository.save(ingreso);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean actualizarIngreso(IngresoModel ingreso){
        try {
            repository.save(ingreso);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean eliminarIngreso(UUID id){
        try{
            IngresoModel ingreso = repository.findByIdIngreso(id);
            repository.delete(ingreso);
            return true;
        }catch (Exception e){
            return false;
        }
    }


}