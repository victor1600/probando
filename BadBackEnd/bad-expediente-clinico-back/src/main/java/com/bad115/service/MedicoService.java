package com.bad115.service;

import com.bad115.model.MedicoModel;
import com.bad115.model.PacienteModel;
import com.bad115.model.TipoExamenModel;
import com.bad115.repository.MedicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service("MedicoService")
public class MedicoService {

    @Autowired
    @Qualifier("MedicoRepository")
    MedicoRepository repository;

    public List<MedicoModel> obtenerMedicos(){
        return repository.findAll();
    }

    public MedicoModel obtenerMedicoId(UUID id) {
        MedicoModel medico = repository.findByIdMedico(id);
        return medico;
    }

    public MedicoModel findByIdMedico(UUID id){
        return repository.findByIdMedico(id);
    }
    public boolean guardarMedico(MedicoModel medico){
        try {
            repository.save(medico);
            return true;
        }catch (Exception e){
            return false;
        }
    }
    // Service agregado por victor
    public List<MedicoModel> doctoresCita(String p_fecha_cita, LocalTime p_hora_elegida, UUID p_id_especialidad){
        return repository.doctoresCita(p_fecha_cita,p_hora_elegida,p_id_especialidad );
    }

    // Service agergado por victor
}
