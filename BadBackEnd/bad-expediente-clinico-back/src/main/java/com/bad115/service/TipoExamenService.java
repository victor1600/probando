package com.bad115.service;

import com.bad115.model.TipoExamenModel;
import com.bad115.repository.TipoExamenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("TipoExamenService")
public class TipoExamenService {
    @Autowired
    @Qualifier("TipoExamenRepository")
    TipoExamenRepository repository;

    public List<TipoExamenModel> obtenerTodosTipoExamenes() {
        return repository.findAll();
    }

    public TipoExamenModel obtenerTipoExamenId(UUID id) {
        TipoExamenModel TipoExamen = repository.findByIdTipoExamen(id);
        return TipoExamen;
    }

    public TipoExamenModel obtenerTipoExamenNombre(String nombre) {
        return repository.findByNombreTipoExamen(nombre);
    }

    public boolean agregarTipoExamen(TipoExamenModel TipoExamen) {
        try {
            repository.save(TipoExamen);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean actualizarTipoExamen(TipoExamenModel TipoExamen) {
        try {
            repository.save(TipoExamen);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean eliminarTipoExamen(UUID id) {
        try {
            TipoExamenModel TipoExamen = repository.findByIdTipoExamen(id);
            repository.delete(TipoExamen);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public Boolean toggle(UUID p_id, Boolean p_state){
        return repository.toggle(p_id,p_state);
    }

    public List<TipoExamenModel> lista_activos(){
        return repository.lista_activos();
    }

}
