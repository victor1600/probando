package com.bad115.service;

import com.bad115.model.SignoVitalModel;
import com.bad115.repository.SignoVitalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("SignoVitalService")
public class SignoVitalService {
    @Autowired
    @Qualifier("SignoVitalRepository")
    SignoVitalRepository repository;

    public List<SignoVitalModel> obtenerTodasVitales(){
        return repository.findAll() ;
    }

    public SignoVitalModel obtenerVitalId(UUID id){
        return repository.findByIdVital(id);
    }

    public boolean agregarVital(SignoVitalModel signoVital){
        try {
            repository.save(signoVital);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean actualizarVital(SignoVitalModel signoVital){
        try {
            repository.save(signoVital);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean eliminarVital(UUID id){
        try {
            SignoVitalModel signoVital = repository.findByIdVital(id);
            repository.delete(signoVital);
            return true;
        }catch (Exception e){
            return false;
        }
    }


}
