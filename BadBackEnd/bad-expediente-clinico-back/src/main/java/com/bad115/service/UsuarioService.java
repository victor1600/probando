package com.bad115.service;

import com.bad115.model.UsuarioModel;
import com.bad115.repository.UsuarioRepositry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("UsuarioService")
public class UsuarioService {

    @Autowired
    @Qualifier("UsuarioRepository")
    UsuarioRepositry repository;

    public List<UsuarioModel> obtenerTodosUsuario(){return repository.findAll();}
    public UsuarioModel obtenerUsuarioNombreUsuario(String nombre){return repository.findByNombreUsuario(nombre);}


    //Agregar-Actualizar
    public boolean guardarUsuario(UsuarioModel usuario){
        try {
            repository.save(usuario);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    //Permiso

    public Integer checkPermiso(String usuario, String permiso){
        return repository.checkPermiso(usuario, permiso);
    }



}
