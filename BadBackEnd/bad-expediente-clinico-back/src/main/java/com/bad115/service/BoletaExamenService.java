package com.bad115.service;

import com.bad115.model.BoletaExamenModel;
import com.bad115.model.DosisModel;
import com.bad115.repository.BoletaExamenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("BoletaExamenService")
public class BoletaExamenService {
    @Autowired
    @Qualifier("BoletaExamenRepository")
    BoletaExamenRepository repository;

    public List<BoletaExamenModel> obtenerTodas() {
        return repository.findAll();
    }

    public boolean guardar(BoletaExamenModel boletaExamen) {
        try {
            repository.save(boletaExamen);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public BoletaExamenModel obtenerById(UUID id) {
        BoletaExamenModel boletaExamen = repository.findByIdBoleta(id); // findByIdDosis(id);
        return boletaExamen;
    }

    public boolean eliminar(UUID id) {
        try {
            BoletaExamenModel boletaExamen = repository.findByIdBoleta(id); //findByIdDosis(id);// findByIdMedicamento(id);
            repository.delete(boletaExamen);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<BoletaExamenModel> lista_activos() {
        return repository.lista_activos();
    }

    public Boolean toggle(UUID p_id, Boolean p_state) {
        return repository.toggle(p_id, p_state);
    }

    public String checker(UUID p_id, UUID p_id2) {
        return repository.checker(p_id, p_id2);
    }


}
