package com.bad115.service;

import com.bad115.model.MunicipioModel;
import com.bad115.repository.MunicipioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("MunicipioService")
public class MunicipioService {
    @Autowired
    @Qualifier("MunicipioRepository")
    MunicipioRepository repository;

    public List<MunicipioModel> obtenerMunicipios(){
        return repository.findAll();
    }
}
