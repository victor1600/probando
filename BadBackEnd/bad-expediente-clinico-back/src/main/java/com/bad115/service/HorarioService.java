package com.bad115.service;

import com.bad115.model.AsignacionHorarioModel;
import com.bad115.model.HorarioModel;
import com.bad115.repository.AsignacionHorarioRepository;
import com.bad115.repository.HorarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("HorarioService")
public class HorarioService {

    @Autowired
    @Qualifier("HorarioRepository")
    HorarioRepository repository;

    public HorarioModel obtenerIdHorario(UUID id){
        return repository.findByIdHorario(id);
    }

    public boolean agregarHorario(HorarioModel horario){
        try {
            repository.save(horario);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean actualizarHorario(HorarioModel horario){
        try {
            repository.save(horario);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public Boolean toggle(UUID p_id, Boolean p_state){
        return repository.toggle(p_id,p_state);
    }

    public List<HorarioModel> lista_activos(){
        return repository.lista_activos();
    }

}
