package com.bad115.service;

import com.bad115.model.CamaModel;
import com.bad115.repository.CamaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("CamaService")
public class CamaService {

    @Autowired
    @Qualifier("CamaRepository")
    CamaRepository repository;

    public List<CamaModel> obtenerCamas(){
        return repository.findAll();
    }

    public CamaModel obtenerCamaId(UUID id){ return repository.findByIdCama(id);
    }


    public boolean agregarCama(CamaModel cama){
        try {
            repository.save(cama);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean actualizarCama(CamaModel cama){
        try {
            repository.save(cama);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean eliminarCama(UUID id){
        try{
            CamaModel cama = repository.findByIdCama(id);
            repository.delete(cama);
            return true;
        }catch (Exception e){
            return false;
        }
    }

}