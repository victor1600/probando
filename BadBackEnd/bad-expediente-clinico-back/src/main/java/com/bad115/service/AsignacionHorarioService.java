package com.bad115.service;

import com.bad115.model.AsignacionHorarioModel;
import com.bad115.model.ExamenModal;
import com.bad115.model.GastoMedicoModel;
import com.bad115.repository.AsignacionHorarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("AsignacionHorarioService")
public class AsignacionHorarioService {

    @Autowired
    @Qualifier("AsignacionHorarioRepository")
    AsignacionHorarioRepository repository;

    public List<AsignacionHorarioModel> obtenerTodasAsHr(){
        return repository.findAll() ;
    }

    public AsignacionHorarioModel obtenerIdAsignacionHorario(UUID id){
        return repository.findByIdAsignacionHorario(id);
    }
    public AsignacionHorarioModel obtenerTurno(Integer turno){
        return repository.findByTurno(turno);
    }

    public boolean agregarAsigHor(AsignacionHorarioModel asignacion){
        try {
            repository.save(asignacion);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean actualizarAsigHor(AsignacionHorarioModel asignacion){
        try {
            repository.save(asignacion);
            return true;
        }catch (Exception e){
            return false;
        }
    }


    public Boolean toggle(UUID p_id, Boolean p_state){
        return repository.toggle(p_id,p_state);
    }

    public List<AsignacionHorarioModel> lista_activos(){
        return repository.lista_activos();
    }


}
