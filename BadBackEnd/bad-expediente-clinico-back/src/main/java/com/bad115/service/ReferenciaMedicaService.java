package com.bad115.service;

import com.bad115.model.ReferenciaMedicaModel;
import com.bad115.repository.ReferenciaMedicaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("ReferenciaMedicaService")
public class ReferenciaMedicaService {

    @Autowired
    @Qualifier("ReferenciaMedicaRepository")
    ReferenciaMedicaRepository repository;

    public List<ReferenciaMedicaModel> obtenerTodosReferencias(){
        return repository.findAll();
    }

    public ReferenciaMedicaModel obtenerIdReferencia(UUID id){
        return repository.findByIdReferencia(id);
    }

    public ReferenciaMedicaModel obtenerFkReferencia(UUID fk){
        return repository.findReferenciaMedicaModelByFkConsultaId(fk);
    }

    //Agregar-Actualizar
    public boolean guardarReferenciaMedica(ReferenciaMedicaModel referencia){
        try {
            repository.save(referencia);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean eliminarReferenciaMedica(UUID id){
        try {
            repository.delete(repository.findByIdReferencia(id));
            return true;
        }catch (Exception e){
            return false;
        }
    }

}
