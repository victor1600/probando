package com.bad115.service;

import com.bad115.model.DosisModel;

import com.bad115.model.MedicamentoModel;
import com.bad115.repository.DosisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("DosisService")
public class DosisService {
    @Autowired
    @Qualifier("DosisRepository")
    DosisRepository repository;

    public List<DosisModel> obtenerTodasDosis() {
        return repository.findAll();
    }

    public boolean guardar(DosisModel dosis) {
        try {
            repository.save(dosis);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public DosisModel obtenerById(UUID id) {
        DosisModel dosis = repository.findByIdDosis(id);
        return dosis;
    }

    public boolean eliminar(UUID id) {
        try {
            DosisModel dosis = repository.findByIdDosis(id);// findByIdMedicamento(id);
            repository.delete(dosis);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<DosisModel> lista_activos() {
        return repository.lista_activos();
    }

    public String checker(UUID p_id_med, UUID p_id_tratamiento) {
        return repository.checker(p_id_med, p_id_tratamiento);
    }

    public boolean toggle(UUID p_id, Boolean p_state) {
        return repository.toggle(p_id, p_state);
    }
}
