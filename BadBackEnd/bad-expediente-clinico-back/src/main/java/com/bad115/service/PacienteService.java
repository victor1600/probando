package com.bad115.service;

import com.bad115.model.PacienteModel;
import com.bad115.repository.PacienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("PacienteService")
public class PacienteService {
    @Autowired
    @Qualifier("PacienteRepository")
    PacienteRepository repository;

    public List<PacienteModel> obtenerTodosPacientes(){
        return repository.findAll();
    }

    public PacienteModel findByIdPaciente(UUID id){
        return repository.findByIdPersona(id);
    }

    public PacienteModel obtenerPacienteNombre(String nombre){
        return repository.findByNombre1Persona(nombre);
    }

    public boolean guardarPaciente(PacienteModel paciente){
        try {
            repository.save(paciente);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean estadoPaciente(UUID id){
        try{
            PacienteModel paciente = repository.findByIdPersona(id);
            if(paciente.isEstadoPaciente()){
                paciente.setEstadoPaciente(false);
            }else {
                paciente.setEstadoPaciente(true);
            }
            repository.save(paciente);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}

