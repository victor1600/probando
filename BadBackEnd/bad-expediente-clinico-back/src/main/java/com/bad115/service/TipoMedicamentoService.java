package com.bad115.service;

import com.bad115.model.TipoMedicamentoModel;
import com.bad115.repository.TipoMedicamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("TipoMedicamentoService")
public class TipoMedicamentoService {
    @Autowired
    @Qualifier("TipoMedicamentoRepository")
    TipoMedicamentoRepository repository;

    public List<TipoMedicamentoModel> obtenerTodosTipoMedicamentos() {
        return repository.findAll();
    }

    public TipoMedicamentoModel obtenerTipoMedicamentoId(UUID id) {
        TipoMedicamentoModel tipoMedicamento = repository.findByIdTipoMedicamento(id);
        return tipoMedicamento;
    }

    public TipoMedicamentoModel obtenerTipoMedicamentoDescripcion(String nombre) {
        return repository.findByNombreTMedicamento(nombre);
    }

    public boolean agregarTipoMedicamento(TipoMedicamentoModel tipoMedicamento) {
        try {
            repository.save(tipoMedicamento);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean actualizarTipoMedicamento(TipoMedicamentoModel tipoMedicamento) {
        try {
            repository.save(tipoMedicamento);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean eliminarTipoMedicamento(UUID id) {
        try {
            TipoMedicamentoModel tipoMedicamento = repository.findByIdTipoMedicamento(id);
            repository.delete(tipoMedicamento);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public Boolean toggle(UUID p_id, Boolean p_state) {
        return repository.toggle(p_id, p_state);
    }

    public List<TipoMedicamentoModel> lista_activos() {
        return repository.lista_activos();
    }


}
