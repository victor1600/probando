package com.bad115.service;

import com.bad115.model.TipoAntecedenteModel;
import com.bad115.repository.TipoAntecedenteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("TipoAntecedenteService")
public class TipoAntecedenteService {

    @Autowired
    @Qualifier("TipoAntecedenteRepository")
    TipoAntecedenteRepository repository;

    public List<TipoAntecedenteModel> obtenerTiposAntecedente(){
        return repository.findAll();
    }

    public TipoAntecedenteModel obtenerTipoAntecedenteId(UUID id){
        TipoAntecedenteModel tipoAntecedente = repository.findByIdTipoAntecedente(id);
        return tipoAntecedente;
    }

    public boolean agregarTipoAntecedente(TipoAntecedenteModel tipoAntecedente){
        try {
            repository.save(tipoAntecedente);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean eliminarTipoAntecedente(UUID id){
        try{
            TipoAntecedenteModel tipoAntecedente = repository.findByIdTipoAntecedente(id);
            repository.delete(tipoAntecedente);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
