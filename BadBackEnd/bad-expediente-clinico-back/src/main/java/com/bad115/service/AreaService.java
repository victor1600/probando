package com.bad115.service;

import com.bad115.model.AreaModel;
import com.bad115.repository.AreaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("AreaService")
public class AreaService {
    @Autowired
    @Qualifier("AreaRepository")
    AreaRepository repository;

    public List<AreaModel> obtenerTodosLasAreas(){
        return repository.findAll();
    }

    public AreaModel obtenerAreaId(UUID id){
        AreaModel area = repository.findByIdArea(id);
        return area;
    }

    public AreaModel obtenerAreaNombre(String nombre){
        return repository.findByNombreArea(nombre);
    }


    public boolean agregarArea(AreaModel area){
        try {
            repository.save(area);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean actualizarArea(AreaModel area){
        try {
            repository.save(area);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean eliminarArea(UUID id){
        try{
            AreaModel area = repository.findByIdArea(id);
            repository.delete(area);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
