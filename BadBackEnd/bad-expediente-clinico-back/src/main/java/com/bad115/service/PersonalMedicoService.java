package com.bad115.service;

import com.bad115.model.PersonalMedicoModel;
import com.bad115.repository.PersonalMedicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("PersonalMedicoService")
public class PersonalMedicoService {
    @Autowired
    @Qualifier("PersonalMedicoRepository")
    PersonalMedicoRepository repository;

    public List<PersonalMedicoModel> obtenerTodosPersonal(){
        return repository.findAll();
    }

    public PersonalMedicoModel obtenerPersonalMedicoId(UUID id){
        return repository.findByIdPersona(id);
    }

    public  List<PersonalMedicoModel> obtenerMedicos(){
        return repository.obtenerMedicos();
    }

    public PersonalMedicoModel obtenerMedicoId(UUID id){
        return repository.obtenerMedicoId(id);
    }


    public boolean agregarPersonalMedico(PersonalMedicoModel personalMedico){
        try {
            repository.save(personalMedico);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean actualizarPersonalMedico(PersonalMedicoModel personalMedico){
        try {
            repository.save(personalMedico);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean estadoPersonalMedico(UUID id){
        try{
            PersonalMedicoModel personalMedico = repository.findByIdPersona(id);
            if(personalMedico.isEstadoPersonal()){
                personalMedico.setEstadoPersonal(false);
            }else {
                personalMedico.setEstadoPersonal(true);
            }
            repository.save(personalMedico);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
