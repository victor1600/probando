package com.bad115.service;

import com.bad115.model.CitaModel;
import com.bad115.model.TipoCitaModel;
import com.bad115.repository.TipoCitaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("TipoCitaService")
public class TipoCitaService {

    @Autowired
    @Qualifier("TipoCitaRepository")
    TipoCitaRepository repository;

    public List<TipoCitaModel> obtenerTodos() {
        return repository.findAll();
    }

    public TipoCitaModel obtenerPorId(UUID id) {
        return repository.findByIdTipoCita(id);
    }

    public boolean agregar(TipoCitaModel tipoCita) {
        try {
            repository.save(tipoCita);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean eliminar(UUID id) {
        try {
            TipoCitaModel tipoCitaModel = repository.findByIdTipoCita(id);
            repository.delete(tipoCitaModel);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public String checker(String p_nombre, UUID p_id) {
        return repository.checker(p_nombre, p_id);
    }

    public Boolean toggle(UUID p_id, Boolean p_state){
        return repository.toggle(p_id, p_state);
    }

    public List<TipoCitaModel> lista_activos(){
        return repository.lista_activos();
    }

}
