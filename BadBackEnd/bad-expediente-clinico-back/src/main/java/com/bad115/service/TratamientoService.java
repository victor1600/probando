package com.bad115.service;


import com.bad115.model.TipoMedicamentoModel;
import com.bad115.model.TratamientoModel;
import com.bad115.repository.TratamientoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("TratamientoService")
public class TratamientoService {

    @Autowired
    @Qualifier("TratamientoRepository")
    TratamientoRepository repository;

    public List<TratamientoModel> obtenerTodosTratamientos() {
        return repository.findAll();
    }

    public TratamientoModel obtenerTratamientoPorId(UUID id) {
        return repository.findByIdTratamiento(id);
    }

    public boolean agregarTratamiento(TratamientoModel tratamiento) {
        try {
            repository.save(tratamiento);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean eliminarTratamiento(UUID id) {
        try {
            TratamientoModel tratamiento = repository.findByIdTratamiento(id); //IdTipoMedicamento(id);
            repository.delete(tratamiento);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<TratamientoModel> lista_activos() {
        return repository.lista_activos();
    }

    public Boolean toggle(UUID p_id, Boolean p_state) {
        return repository.toggle(p_id, p_state);
    }

    public String checker(UUID p_id, UUID p_fk) {
        return repository.checker(p_id, p_fk);
    }
}
