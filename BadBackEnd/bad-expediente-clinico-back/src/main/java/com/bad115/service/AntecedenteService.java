package com.bad115.service;

import com.bad115.model.AntecedenteModel;
import com.bad115.repository.AntecedenteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("AntecedenteService")
public class AntecedenteService {

    @Autowired
    @Qualifier("AntecedenteRepository")
    AntecedenteRepository repository;

    public List<AntecedenteModel> obtenerTodosAntecedentes(){
        return repository.findAll();
    }

    public AntecedenteModel obtenerAntecedenteId(UUID id){
        AntecedenteModel antecedente = repository.findByIdAntecedente(id);
        return antecedente;
    }

    public boolean agregarAntecedente(AntecedenteModel antecedente){
        try {
            repository.save(antecedente);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean eliminarAntecedente(UUID id){
        try{
            AntecedenteModel antecedente = repository.findByIdAntecedente(id);
            repository.delete(antecedente);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
