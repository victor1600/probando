package com.bad115.service;

import com.bad115.model.Cie10Model;
import com.bad115.model.ServicioModel;
import com.bad115.model.TipoExamenModel;
import com.bad115.repository.Cie10Repository;
import com.bad115.repository.ServicioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("ServicioService")
public class ServicioService {
    @Autowired
    @Qualifier("ServicioRepository")
    ServicioRepository repository;

    public List<ServicioModel> obtenerTodasServicio(){
        return repository.findAll() ;
    }
    public ServicioModel obtenerServicioId(UUID id){
        return repository.findByIdServicio(id);
    }
    public ServicioModel obtenerServicioNombre(String nombre){
        return repository.findByNombreServicio(nombre);
    }

    public boolean agregarServicio(ServicioModel servicio){
        try {
            repository.save(servicio);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean actualizarServicio(ServicioModel servicio){
        try {
            repository.save(servicio);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public Boolean toggle(UUID p_id, Boolean p_state){
        return repository.toggle(p_id,p_state);
    }

    public List<ServicioModel> lista_activos(){
        return repository.lista_activos();
    }




}
