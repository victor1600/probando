package com.bad115.service;

import com.bad115.model.EspecialidadModel;
import com.bad115.repository.EspecialidadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("EspecialidadService")
public class EspecialidadService {
    @Autowired
    @Qualifier("EspecialidadRepository")
    EspecialidadRepository repository;

    public List<EspecialidadModel> obtenerTodasEspecilidad(){
        return repository.findAll() ;
    }

    public EspecialidadModel obtenerEspecialidadId(UUID id){
        return repository.findByIdEspecialidad(id);
    }

    public EspecialidadModel obtenerNombreEspecialidad(String nombre) {
        return repository.findByNombreEspecialidad(nombre);
    }

    public boolean agregarEspecialidad(EspecialidadModel especialidad){
        try {
            repository.save(especialidad);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean actualizarEspecialidad(EspecialidadModel especialidad){
        try {
            repository.save(especialidad);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean eliminarEspecialidad(UUID id){
        try {
            EspecialidadModel especialidad = repository.findByIdEspecialidad(id);
            repository.delete(especialidad);
            return true;
        }catch (Exception e){
            return false;
        }
    }

}
