package com.bad115.service;

import com.bad115.model.GastoMedicoModel;
import com.bad115.repository.GastoMedicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service("GastoMedicoService")
public class GastoMedicoService {

    @Autowired
    @Qualifier("GastoMedicoRepository")
    GastoMedicoRepository repository;

    public GastoMedicoModel obtenerIdGastoMedico(UUID id){
        return repository.findByIdGastoMedico(id);
    }
    public GastoMedicoModel obtenerFechaGastoMedico(Date fechaGasto){
        return repository.findByFechaGasto(fechaGasto);
    }

    public boolean agregarGastoMedico(GastoMedicoModel gasto){
        try {
            repository.save(gasto);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean actualizarGastoMedico(GastoMedicoModel gasto){
        try {
            repository.save(gasto);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public Boolean toggle(UUID p_id, Boolean p_state){
        return repository.toggle(p_id,p_state);
    }

    public List<GastoMedicoModel> lista_activos(){
        return repository.lista_activos();
    }


}
