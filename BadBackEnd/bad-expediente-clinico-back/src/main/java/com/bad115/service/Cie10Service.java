package com.bad115.service;

import com.bad115.model.Cie10Model;
import com.bad115.repository.Cie10Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("Cie10Service")
public class Cie10Service {
    @Autowired
    @Qualifier("Cie10Repository")
    Cie10Repository repository;

    public List<Cie10Model> obtenerTodasCie10(){
        return repository.findAll() ;
    }
    public Cie10Model obtenerCie10Id(UUID id){
        return repository.findByIdCIE10(id);
    }
    public Cie10Model obtenerCie10Codigo(String codigo){
        return repository.findByCodigoCIE10(codigo);
    }

    public boolean agregarCie10(Cie10Model cie10){
        try {
            repository.save(cie10);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean actualizarCie10(Cie10Model cie10){
        try {
            repository.save(cie10);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean eliminarCie10(UUID id){
        try {
            Cie10Model cie10 = repository.findByIdCIE10(id);
            repository.delete(cie10);
            return true;
        }catch (Exception e){
            return false;
        }
    }

}
