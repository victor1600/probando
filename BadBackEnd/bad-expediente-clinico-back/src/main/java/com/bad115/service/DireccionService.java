package com.bad115.service;

import com.bad115.model.DireccionModel;
import com.bad115.repository.DireccionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service("DireccionService")
public class DireccionService {

    @Autowired
    @Qualifier("DireccionRepository")
    DireccionRepository repository;

    public boolean guardarDireccion(DireccionModel direccion){
        try {
            repository.save(direccion);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean eliminarDireccion(UUID id){
        try{
            DireccionModel direccion = repository.findByIdDireccion(id);
            repository.delete(direccion);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
