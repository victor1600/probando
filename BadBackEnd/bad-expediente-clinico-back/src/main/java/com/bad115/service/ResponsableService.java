package com.bad115.service;

import com.bad115.model.PacienteModel;
import com.bad115.model.ResponsableModel;
import com.bad115.repository.ResponsableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("ResponsableService")
public class ResponsableService {

    @Autowired
    @Qualifier("ResponsableRepository")
    ResponsableRepository repository;

    public List<ResponsableModel> obtenerTodosResponsable(){
        return repository.findAll();
    }

    public ResponsableModel findByIdResponsable(UUID id){
        return repository.findByIdPersona(id);
    }

    public boolean guardarResponsable(ResponsableModel responsable){
        try {
            repository.save(responsable);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean eliminarResponsable(String dui){
        try{
            ResponsableModel responsable = repository.findByDuiPersona(dui);
            repository.delete(responsable);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
