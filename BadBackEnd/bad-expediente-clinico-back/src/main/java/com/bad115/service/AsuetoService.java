package com.bad115.service;

import com.bad115.model.AsuetoModel;
import com.bad115.repository.AsuetoRepository;
import com.bad115.repository.ServicioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("AsuetoService")
public class AsuetoService {
    @Autowired
    @Qualifier("AsuetoRepository")
    AsuetoRepository repository;

    public List<AsuetoModel> obtenerTodasAsueto(){
        return repository.findAll() ;
    }
    public AsuetoModel obtenerAsuetoId(UUID id){
        return repository.findByIdAsueto(id);
    }
    public AsuetoModel obtenerAsuetoNombre(String nombre){
        return repository.findByNombreAsueto(nombre);
    }

    public boolean agregarAsueto(AsuetoModel asueto){
        try {
            repository.save(asueto);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean actualizarAsueto(AsuetoModel asueto){
        try {
            repository.save(asueto);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public Boolean toggle(UUID p_id, Boolean p_state){
        return repository.toggle(p_id,p_state);
    }

    public List<AsuetoModel> lista_activos(){
        return repository.lista_activos();
    }



}
