package com.bad115.service;

import com.bad115.model.CirugiaModel;
import com.bad115.repository.CirugiaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("CirugiaService")
public class CirugiaService {

    @Autowired
    @Qualifier("CirugiaRepository")
    CirugiaRepository repository;

    public List<CirugiaModel> obtenerTodasCirugias(){
        return repository.findAll();
    }

    public CirugiaModel obtenerCirugiaId(UUID id){
        CirugiaModel cirugia = repository.findByIdCirugia(id);
        return cirugia;
    }

    public CirugiaModel obtenerCirugiaNombre(String nombre){
        return repository.findByNombreCirugia(nombre);
    }


    public boolean agregarCirugia(CirugiaModel cirugia){
        try {
            repository.save(cirugia);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean actualizarCirugia(CirugiaModel cirugia){
        try {
            repository.save(cirugia);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean eliminarCirugia(UUID id){
        try{
            CirugiaModel cirugia = repository.findByIdCirugia(id);
            repository.delete(cirugia);
            return true;
        }catch (Exception e){
            return false;
        }
    }

}
