package com.bad115.service;

import com.bad115.model.BoletaCirugiaModel;
import com.bad115.repository.BoletaCirugiaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("BoletaCirugiaService")
public class BoletaCirugiaService {

    @Autowired
    @Qualifier("BoletaCirugiaRepository")
    BoletaCirugiaRepository repository;

    public List<BoletaCirugiaModel> obtenerBoletasCirugias(){
        return repository.findAll();
    }

    public BoletaCirugiaModel obtenerBoletaCirugiaId(UUID id){
        BoletaCirugiaModel boletaCirugia = repository.findByIdBoletaCirugia(id);
        return boletaCirugia;
    }

    public BoletaCirugiaModel obtenerBoletaCirugiaFecha(String fechaCirugia){
        return repository.findByFechaBoletaCirugia(fechaCirugia);
    }


    public boolean agregarBoletaCirugia(BoletaCirugiaModel boletaCirugia){
        try {
            repository.save(boletaCirugia);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean actualizarBoletaCirugia(BoletaCirugiaModel boletaCirugia){
        try {
            repository.save(boletaCirugia);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean eliminarBoletaCirugia(UUID id){
        try{
            BoletaCirugiaModel boletaCirugia = repository.findByIdBoletaCirugia(id);
            repository.delete(boletaCirugia);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
