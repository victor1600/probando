package com.bad115.service;

import com.bad115.model.ExamenModal;
import com.bad115.repository.ExamenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("ExamenService")
public class ExamenService {
    @Autowired
    @Qualifier("ExamenRepository")
    ExamenRepository repository;

    public List<ExamenModal> obtenerTodasExamen(){
        return repository.findAll() ;
    }

    public ExamenModal obtenerExamenId(UUID id){
        return repository.findByIdExamen(id);
    }

    public boolean agregarExamen(ExamenModal examen){
        try {
            repository.save(examen);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean actualizarExamen(ExamenModal examen){
        try {
            repository.save(examen);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean eliminarExamen(UUID id){
        try {
            ExamenModal examen = repository.findByIdExamen(id);
            repository.delete(examen);
            return true;
        }catch (Exception e){
            return false;
        }
    }


}
