package com.bad115.service;

import com.bad115.model.ServicioGastoModel;
import com.bad115.model.ServicioModel;
import com.bad115.repository.ServicioGastoRepository;
import com.bad115.repository.ServicioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("ServicioGastoService")
public class ServicioGastoService {
    @Autowired
    @Qualifier("ServicioGastoRepository")
    ServicioGastoRepository repository;

    public List<ServicioGastoModel> obtenerTodasServicioGasto(){
        return repository.findAll() ;
    }
    public ServicioGastoModel obtenerServicioGastoId(UUID id){
        return repository.findByIdServicioGasto(id);
    }

    public boolean agregarServicioGasto(ServicioGastoModel servicioGasto){
        try {
            repository.save(servicioGasto);
            return true;
        }catch (Exception e){
            return false;
        }
    }
    public boolean actualizarServicioGasto(ServicioGastoModel servicioGasto){
        try {
            repository.save(servicioGasto);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
