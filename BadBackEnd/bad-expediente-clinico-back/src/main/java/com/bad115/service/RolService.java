package com.bad115.service;

import com.bad115.model.RolModel;
import com.bad115.repository.RolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service("RolService")
public class RolService {
    @Autowired
    @Qualifier("RolRepository")
    RolRepository repository;

    public RolModel obtenerIdRol(UUID id){ return repository.findByIdRol(id);}
}
