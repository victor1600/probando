package com.bad115.service;

import com.bad115.model.MedicamentoModel;
import com.bad115.model.ReferenciaMedicaModel;
import com.bad115.model.TipoMedicamentoModel;
import com.bad115.repository.MedicamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service("MedicamentoService")
public class MedicamentoService {

    @Autowired
    @Qualifier("MedicamentoRepository")
    MedicamentoRepository repository;

    public List<MedicamentoModel> obtenerTodosMedicamentos() {
        return repository.findAll();
    }

    public MedicamentoModel obtenerById(UUID id) {
        MedicamentoModel medicamento = repository.findByIdMedicamento(id);
        return medicamento;
    }

    public MedicamentoModel obtenerByCodigo(String nombre) {
        MedicamentoModel medicamento = repository.findByCodigoMedicamento(nombre);
        return medicamento;
    }

    public MedicamentoModel obtenerByNombre(String nombre) {
        MedicamentoModel medicamento = repository.findByNombreMedicamento(nombre);
        return medicamento;
    }

    public boolean guardar(MedicamentoModel medicamento) {
        try {
            repository.save(medicamento);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public MedicamentoModel obtenerFk(UUID fk) {
        return repository.findMedicamentoModelByFktipoMedicamentoId(fk);
    }


    public boolean eliminar(UUID id) {
        try {
            MedicamentoModel medicamento = repository.findByIdMedicamento(id);
            repository.delete(medicamento);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

//    public String checker(String p_nombre_med, String p_codigo_med, UUID p_id_tipo_med) {
//        return repository.checker(p_nombre_med, p_codigo_med, p_id_tipo_med);
//
//    }
    public String checker(String p_nombre_med, String p_codigo_med, UUID p_id_tipo_med){
        return repository.checker(p_nombre_med, p_codigo_med, p_id_tipo_med);
    }

    public List<MedicamentoModel> lista_activos(){
        return repository.lista_activos();
    }

    public boolean toggle(UUID p_id, Boolean p_state) {
        return repository.toggle(p_id, p_state);
    }



}
