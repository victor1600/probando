Sistema clínico para la materia de Bases de Datos de la Universidad de El Salvador para el ciclo-I del año 2020
BackEnd

### Universidad de El Salvador ###

* BAD115-Ciclo-01-2020
* Bases de datos
* Ing. Elmer Arturo Carballo Ruíz MSc.

### Grupo 02 -Teorico02- Integrantes###

* Cristian
* Victor
* Dina
* Ovidio
* Walter

### Tecnologia ###
* BackEnd Spring Boot-DataJPA-Web
* Base de datos Postgres
* Data JPA
* Web
