import React, {Fragment, useState} from 'react'
import {useForm} from 'react-hook-form'
import {useParams, withRouter} from "react-router-dom";

const ActualizarPaciente = (props) => {

    const estilosInicio = {
        fondoHeader: {
            backgroundColor: '#80cbc4',
            textAlign: 'center'
        },
        fondoBody: {
            backgroundColor: '#F5F5F6'
        },
        letraCarrousel: {
            color: 'black'
        }
    };

    const estadoCivil = [
        { id: 1, value: 'S', name: 'Soltero'},
        { id: 2, value: 'C', name: 'Casado'},
        { id: 3, value: 'D', name: 'Divorciado'},
        { id: 4, value: 'V', name: 'Viudo'},
      ];
    
    // Validar Acceso
    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)

    // Obtener Paciente
    const [jsonGE, setJsonGE] = React.useState(false);
    const [jsonGS, setJsonGS] = React.useState(false);
    const [msjGet, setMsjGet] = React.useState(['']);
    const [paciente, setPaciente] = React.useState([]);
    const {idPersona} = useParams(); 
    
    React.useEffect(() => {

        if(loginE === true){
            const obtenerPaciente = async () => {
                const data = await fetch(`http://localhost:8080/pacientes/${idPersona}`,{
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });
                const dataPaciente = await data.json();

                switch(data.status) {
                    case 403:
                        props.history.push('/error403');
                        break;
                    case 302:
                        setJsonGS(true);
                        setJsonGE(false);
                        setPaciente(dataPaciente);
                        break;
                    case 404:
                        setJsonGS(false);
                        setJsonGE(true);
                        setMsjGet([
                        String(dataPaciente.mensaje)
                        ]);
                        break;
                    default:
                        props.history.push('/error');
                    }

            }
            obtenerPaciente();
            
        }else{
            props.history.push('/login');
        }

    }, [idPersona, props, loginE,token])

    //Actualizar Paciente
    const [jsonUE, setJsonUE] = React.useState(false);
    const [jsonUS, setJsonUS] = React.useState(false);
    const [msjUpdate, setMsjUpdate] = React.useState(['']);
    const [tituloUpdate, setTituloUpdate] = React.useState(['']);
    
    const { register, handleSubmit, errors } = useForm();
    const onSubmit = async (data, e) => {
        e.preventDefault();
        const btnModal = document.getElementById("modal");
        console.log(data)
        const jsonP = await fetch(`http://localhost:8080/pacientes/actualizar/${idPersona}`,{
            method: 'PUT',
            body: JSON.stringify({
                idPersona: paciente.idPersona,
                nombre1Persona: data.nombre1Persona,
                nombre2Persona: data.nombre2Persona,
                nombre3Persona: data.nombre3Persona,
                apellido1Persona: data.apellido1Persona,
                apellido2Persona: data.apellido2Persona,
                apellido3Persona: data.apellido3Persona,
                fechaNacimientoPersona: data.fechaNacimientoPersona,
                sexoPersona: data.sexoPersona,
                duiPersona: data.duiPersona,
                nitPersona: data.nitPersona,
                direccion: {
                    idDireccion: paciente?.direccion?.idDireccion,
                    urbanizacion: data.urbanizacion,
                    calleAvenida: data.calleAvenida,
                    numeroPasaje: data.numeroPasaje,
                    numeroCasa: data.numeroCasa,
                    municipio: {
                        idMunicipio: data.idMunicipio,
                        departamento: {
                            idDepartamento: data.idDepartamento,
                        }
                    }
                },
                estadoCivilPaciente: data.estadoCivilPaciente,
                responsable: {
                    idPersona: paciente?.responsable?.idPersona,
                    nombre1Persona: data.nombre1Responsable,
                    nombre2Persona: data.nombre2Responsable,
                    nombre3Persona: data.nombre3Responsable,
                    apellido1Persona: data.apellido1Responsable,
                    apellido2Persona: data.apellido2Responsable,
                    apellido3Persona: data.apellido3Responsable,
                    telefonoPersona: data.telefonoResponsable,
                    celularPersona: data.celularResponsable,
                    duiPersona: data.duiResponsable,
                    nitPersona: data.nitResponsable
                },
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                "Authorization": token
            }
        });
        console.log(jsonP);
        const info = await jsonP.json();

        if (jsonP.status === 202) {
            setJsonUE(false);
            setJsonUS(true);
            setMsjUpdate([
                String(info.mensaje)
            ]);
            setTituloUpdate([
                String(info.httpCodeName)
            ]);
            btnModal.click();
            e.target.reset();

        } else {
            setJsonUE(true);
            setJsonUS(false);
            setMsjUpdate([
                String(info.mensaje)
            ]);
            setTituloUpdate([
                String(info.httpCodeName)
            ]);
            btnModal.click();
        }
    }

    // Obtener Departamentos
    const [deptId, setDeptId] = useState();  //Estado select 

    const [jsonED, setJsonED] = React.useState(false);
    const [jsonSD, setJsonSD] = React.useState(false);
    const [msjDept, setMsjDept] = React.useState(['']);
    const [departamentos, setDepartamentos] = React.useState([]);

    React.useEffect(() => {
        if (loginE === true) {

            const obtenerDepartamentos = async () => {
                const data = await fetch('http://localhost:8080/departamentos/todos', {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });
                console.log(data.status);
                console.log(data.Departamentos);
                const dataDepartamentos = await data.json();
                if (data.status === 302) {
                    setJsonSD(true);
                    setJsonED(false);
                    setDepartamentos(dataDepartamentos);
                } else {
                    setJsonSD(false);
                    setJsonED(true);
                    setMsjDept([
                        String(dataDepartamentos.mensaje)
                    ]);
                }
            }
            obtenerDepartamentos();
            
        } else {
            props.history.push('/login');
        }
    }, [props, loginE, token])

    // Obtener Municicpios
    const [jsonEM, setJsonEM] = React.useState(false);
    const [jsonSM, setJsonSM] = React.useState(false);
    const [msjMuni, setMsjMuni] = React.useState(['']);
    const [municipios, setMunicipios] = React.useState([]);

    React.useEffect(() => {
        if (loginE === true) {

            const obtenerMunicipios = async () => {
                const data = await fetch('http://localhost:8080/municipios/todos', {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });
                console.log(data.status);
                console.log(data.Municipios);
                const dataMunicipios = await data.json();
                if (data.status === 302) {
                    setJsonSM(true);
                    setJsonEM(false);
                    setMunicipios(dataMunicipios);
                } else {
                    setJsonSM(false);
                    setJsonEM(true);
                    setMsjMuni([
                        String(dataMunicipios.mensaje)
                    ]);
                }
            }
            obtenerMunicipios();
            
        } else {
            props.history.push('/login');
        }
    }, [props, loginE, token])

    return (
        <Fragment>
            <div className="m-5">
                <div className="card">
                    <div className="card-header" style={estilosInicio.fondoHeader}>
                        <h2>Actualizar Paciente</h2>
                    </div>
                    {jsonGS &&
                    
                    <div className="card-body" style={estilosInicio.fondoBody}>
                        <form className="form-horizontal" onSubmit={handleSubmit(onSubmit)}>
                    
                            {/* Información Personal */}
                            
                            <div className="card-body">
                            <h5>Información Personal:</h5><br/>
                                
                                <div className="row col-sm-12">
                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Primer Nombre:</label>
                                            <input type="text" className="form-control" name="nombre1Persona"
                                                required maxLength="20" pattern="([A-Za-z ]+)" defaultValue={paciente.nombre1Persona}
                                                ref={register({
                                                    required: { value: true, message: "El primer nombre es obligatorio" },
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Segundo Nombre:</label>
                                            <input type="text" className="form-control" name="nombre2Persona"
                                                maxLength="20" pattern="([A-Za-z ]+)" defaultValue={paciente.nombre2Persona}
                                                ref={register({
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Tercer Nombre:</label>
                                            <input type="text" className="form-control" name="nombre3Persona"
                                                maxLength="20" pattern="([A-Za-z ]+)" defaultValue={paciente.nombre3Persona}
                                                ref={register({
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span>
                                        </div>
                                    </div>
                                </div>{/* Fin nombres */}

                                <div className="row col-sm-12">
                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Primer Apellido:</label>
                                            <input type="text" className="form-control" name="apellido1Persona"
                                                required maxLength="20" pattern="([A-Za-z ]+)" defaultValue={paciente.apellido1Persona}
                                                ref={register({
                                                    required: { value: true, message: "El primer apellido es obligatorio" },
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Segundo Apellido:</label>
                                            <input type="text" className="form-control" name="apellido2Persona"
                                                maxLength="20" pattern="([A-Za-z ]+)" defaultValue={paciente.apellido2Persona}
                                                ref={register({
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Tercer Apellido:</label>
                                            <input type="text" className="form-control" name="apellido3Persona"
                                                maxLength="20" pattern="([A-Za-z ]+)" defaultValue={paciente.apellido3Persona}
                                                ref={register({
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span>
                                        </div>
                                    </div>
                                </div>{/* Fin apellidos */}

                                <div className="row col-sm-12">
                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Fecha de Nacimiento:</label>
                                            <input type="date" className="form-control" name="fechaNacimientoPersona"
                                                required maxLength="10" defaultValue={paciente.fechaNacimientoPersona}
                                                    ref={register({
                                                    required: { value: true, message: "Fecha de nacimiento es obligatorio" },
                                                    maxLength: { value: 10, message: "Máximo 10 caracteres" },
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Sexo:</label>
                                            <select id="sexoPersona" name="sexoPersona" className="form-control" required
                                                ref={register({
                                                required: { value: true, message: "El campo sexo es obligatorio" },
                                                pattern: { value: /[M F]/, message: "Solamente letras" }
                                            })}>
                                                {paciente.sexoPersona === 'M' ? <option value={paciente.sexoPersona}> Masculino</option> : <option value={paciente.sexoPersona}> Femenino</option>}
                                                {paciente.sexoPersona === 'M' ? <option value='F'> Femenino</option> : <option value='M'> Masculino</option>}
                                            </select>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Estado Civil:</label>
                                            <select id="estadoCivilPaciente" name="estadoCivilPaciente" className="form-control" required
                                            ref={register({
                                                required: { value: true, message: "El campo estado civil es obligatorio" }})}>
                                                <option value={paciente.estadoCivilPaciente}>{paciente.estadoCivilPaciente==='S' ? 'Soltero' : paciente.estadoCivilPaciente==='C' ? 'Casado': paciente.estadoCivilPaciente==='D' ? 'Divorciado' : 'Viudo'}</option>
                                                {estadoCivil.filter(item => item.value !== paciente.estadoCivilPaciente).map((item) => (
                                                    <option key={item.id} value={item.value}>{item.name}</option>
                                                ))}
                                            </select>
                                        </div>
                                    </div>
                                </div>{/* fecha - sexo - estado */}
                                <div className="row col-sm-12">
                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">DUI:</label>
                                            <input type="text" className="form-control" name="duiPersona" 
                                                maxLength="10" defaultValue={paciente.duiPersona}
                                                ref={register({
                                                    maxLength: { value: 10, message: "Máximo 10 caracteres" },
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">NIT:</label>
                                            <input type="text" className="form-control" name="nitPersona"
                                                maxLength="17" defaultValue={paciente.nitPersona}
                                                ref={register({
                                                    maxLength: { value: 17, message: "Máximo 17 caracteres" },
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            {/* Información de Contacto */}
                            
                            <div className="card-body">
                                <h5>Información de Contacto:</h5><br/>
                                <div className="row col-sm-12">
                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Teléfono Fijo:</label>
                                            <input type="text" className="form-control" name="telefonoPersona" maxLength="9"
                                                    defaultValue={paciente.telefonoPersona}
                                                    ref={register({
                                                    maxLength: { value: 9, message: "Máximo 9 caracteres" },
                                                    pattern: { value: /[0-9]/, message: "Solamente números y guion medio." }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Celular:</label>
                                            <input type="text" className="form-control" name="celularPersona" maxLength="9"
                                                    defaultValue={paciente.celularPersona}
                                                    ref={register({
                                                    maxLength: { value: 9, message: "Máximo 9 caracteres" },
                                                    pattern: { value: /[0-9]/, message: "Solamente números y guion medio." }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span>
                                        </div>
                                    </div>

                                </div>
                                {/* Fin telefonos */}

                                <div className="row col-sm-12">
                                    {jsonSD &&
                                        <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Departamento:</label>
                                            <select value={deptId} onChange={e => setDeptId(e.target.value)} 
                                                id="idDepartamento" name="idDepartamento" className="browser-default custom-select" required
                                                ref={register({
                                                    required: { value: true, message: "El campo departamento es obligatorio" }
                                                })}>
                                                <option value={paciente?.direccion?.municipio?.departamento?.idDepartamento}>{paciente?.direccion?.municipio?.departamento?.nombreDepartamento}</option>
                                                {departamentos.filter(item => item.idDepartamento !== paciente?.direccion?.municipio?.departamento?.idDepartamento).map((item) => (
                                                    <option key={item.idDepartamento} value={item.idDepartamento}>{item.nombreDepartamento}</option>
                                                ))}
                                            </select>
                                        </div>
                                    </div>
                                    }
                                    {jsonED &&
                                        <div className="m-5">
                                            <h5><li>{msjDept}</li></h5>
                                        </div>
                                    }
                                    {jsonSM &&
                                        <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Municipio:</label>
                                            <select id="idMunicipio" name="idMunicipio" className="browser-default custom-select" required
                                                ref={register({
                                                    required: { value: true, message: "El campo municipio es obligatorio" },
                                                })}>
                                                {(deptId === undefined || deptId === paciente?.direccion?.municipio?.departamento?.idDepartamento) && 
                                                <option value={paciente?.direccion?.municipio?.idMunicipio}>{paciente?.direccion?.municipio?.nombreMunicipio}</option>} 
                                                
                                                {municipios.filter(item => (deptId === undefined ? (item?.departamento?.idDepartamento === paciente?.direccion?.municipio?.departamento?.idDepartamento)
                                                :(item?.departamento?.idDepartamento === deptId)) && (item.idMunicipio !== paciente?.direccion?.municipio?.idMunicipio)).map((item) => (
                                                    <option key={item.idMunicipio} value={item.idMunicipio}>{item.nombreMunicipio}</option>
                                                ))}
                                            </select>
                                        </div>
                                    </div>
                                    }
                                    {jsonEM &&
                                        <div className="m-5">
                                            <h5><li>{msjMuni}</li></h5>
                                        </div>
                                    }
                                </div>

                                <div className="row col-sm-12">
                                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                                        <div className="form-group">
                                        <label className="control-label col-sm-12">Urbanizacion/ Colonia/ Barrio:</label>
                                            <input type="text" className="form-control" name="urbanizacion" required 
                                                    maxLength="100" defaultValue={paciente?.direccion?.urbanizacion}
                                                    ref={register({
                                                    required: { value: true, message: "El campo Urbanizacion/Colonia/Barrio es obligatorio" },
                                                    maxLength: { value: 100, message: "Máximo 100 caracteres" },
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span> 
                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                                        <div className="form-group">
                                        <label className="control-label col-sm-12">Calle/Avenida:</label>
                                            <input type="text" className="form-control" name="calleAvenida"
                                                    maxLength="100" defaultValue={paciente?.direccion?.calleAvenida}
                                                    ref={register({
                                                    maxLength: { value: 100, message: "Máximo 100 caracteres" },
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span> 
                                        </div>
                                    </div>
                                    <div className=" row col-xs-12 col-md-9 col-lg-4">
                                        <div className="col-xs-12 col-sm-4 col-md-4 col-lg-6">
                                            <div className="form-group">
                                            <label className="control-label col-sm-12">Pasaje:</label>
                                                <input type="text" className="form-control" name="numeroPasaje"
                                                        maxLength="16" defaultValue={paciente?.direccion?.numeroPasaje}
                                                        ref={register({
                                                        maxLength: { value: 16, message: "Máximo 16 caracteres" },
                                                    })}
                                                />
                                                <span className="text-danger text-small d-block mb-2">
                                                    {errors?.paciente?.message}
                                                </span> 
                                            </div>
                                        </div>
                                        <div className="col-xs-12 col-sm-4 col-md-4 col-lg-6">
                                            <div className="form-group">
                                            <label className="control-label col-sm-12">#Casa:</label>
                                                <input type="text" className="form-control" name="numeroCasa"
                                                        maxLength="10" defaultValue={paciente?.direccion?.numeroCasa}
                                                        ref={register({
                                                        maxLength: { value: 10, message: "Máximo 10 caracteres" },
                                                    })}
                                                />
                                                <span className="text-danger text-small d-block mb-2">
                                                    {errors?.paciente?.message}
                                                </span> 
                                            </div>
                                        </div>
                                    </div>
                                </div><br/>
                                {/* Fin Direccion */}
                            </div> 
                            {/* Fin Seccion 2*/}

                            {/* Información Responsable */}
                            <div className="card-body">
                                <h5>Persona Responsable:</h5><br/>
                                
                                <div className="row col-sm-12">
                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Primer Nombre:</label>
                                            <input type="text" className="form-control" name="nombre1Responsable"
                                                required maxLength="20" pattern="([A-Za-z ]+)" defaultValue={paciente?.responsable?.nombre1Persona}
                                                ref={register({
                                                    required: { value: true, message: "El primer nombre es obligatorio" },
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Segundo Nombre:</label>
                                            <input type="text" className="form-control" name="nombre2Responsable"
                                                maxLength="20" pattern="([A-Za-z ]+)" defaultValue={paciente?.responsable?.nombre2Persona}
                                                ref={register({
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Tercer Nombre:</label>
                                            <input type="text" className="form-control" name="nombre3Responsable"
                                                maxLength="20" pattern="([A-Za-z ]+)" defaultValue={paciente?.responsable?.nombre3Persona}
                                                ref={register({
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span>
                                        </div>
                                    </div>
                                </div>{/* Fin nombres */}

                                <div className="row col-sm-12">
                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Primer Apellido:</label>
                                            <input type="text" className="form-control" name="apellido1Responsable"
                                                required maxLength="20" pattern="([A-Za-z ]+)" defaultValue={paciente?.responsable?.apellido1Persona}
                                                ref={register({
                                                    required: { value: true, message: "El primer apellido es obligatorio" },
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Segundo Apellido:</label>
                                            <input type="text" className="form-control" name="apellido2Responsable"
                                                maxLength="20" pattern="([A-Za-z ]+)" defaultValue={paciente?.responsable?.apellido2Persona}
                                                ref={register({
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Tercer Apellido:</label>
                                            <input type="text" className="form-control" name="apellido3Responsable"
                                                maxLength="20" pattern="([A-Za-z ]+)" defaultValue={paciente?.responsable?.apellido3Persona}
                                                ref={register({
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span>
                                        </div>
                                    </div>
                                </div>{/* Fin apellidos */}

                                <div className="row col-sm-12">
                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Teléfono Fijo:</label>
                                            <input type="text" className="form-control" name="telefonoResponsable" maxLength="9"
                                                    defaultValue={paciente?.responsable?.telefonoPersona}
                                                    ref={register({
                                                    maxLength: { value: 9, message: "Máximo 9 caracteres" },
                                                    pattern: { value: /[0-9]/, message: "Solamente números y guion medio." }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Celular:</label>
                                            <input type="text" className="form-control" name="celularResponsable" maxLength="9"
                                                    defaultValue={paciente?.responsable?.celularPersona}
                                                    ref={register({
                                                    maxLength: { value: 9, message: "Máximo 9 caracteres" },
                                                    pattern: { value: /[0-9]/, message: "Solamente números y guion medio." }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span>
                                        </div>
                                    </div>

                                </div>
                                {/* Fin telefonos */}

                                <div className="row col-sm-12">
                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">DUI:</label>
                                            <input type="text" className="form-control" name="duiResponsable" required
                                                maxLength="10" defaultValue={paciente?.responsable?.duiPersona}
                                                ref={register({
                                                    maxLength: { value: 10, message: "Máximo 10 caracteres" },
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">NIT:</label>
                                            <input type="text" className="form-control" name="nitResponsable" required
                                                maxLength="17" defaultValue={paciente?.responsable?.nitPersona}
                                                ref={register({
                                                    maxLength: { value: 17, message: "Máximo 17 caracteres" },
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.paciente?.message}
                                            </span>
                                        </div>
                                    </div>
                                </div> <br/><br/>

                                <div className="row col-sm-12">
                                    <div className="col-sm-2">
                                        <div className="form-group ">
                                            <button type="submit" className="btn btn-success">Guardar</button>
                                        </div>
                                    </div>
                                    <div className="col-sm-2">
                                        <div className="form-group ">
                                        <a href="/pacientes" className="btn btn-info">Regresar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* Fin Respopnsable*/}
                        </form>
                     
                    </div>
                    }
                    {jsonGE &&
                        <div className="m-5">
                            <h3><li>{msjGet}</li></h3>
                        </div>
                    }

                    {/* Modal */}
                    <button id="modal" type="button" className="d-none" data-toggle="modal" data-target="#exampleModalCenter">
                        X
                    </button>
                    <div className="modal fade" id="exampleModalCenter" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalCenterTitle">{tituloUpdate}</h5>
                                </div>
                                <div className="modal-body">
                                    {msjUpdate}
                                </div>
                                <div className="modal-footer">
                                    {jsonUE &&
                                        <button type="button" className="btn btn-warning" data-dismiss="modal">Cancelar</button>
                                    }
                                    {jsonUS &&
                                        <div>
                                            <a type="button" className="btn btn-primary" href="/pacientes">Aceptar</a>
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </Fragment>
    );
}
export default withRouter(ActualizarPaciente);