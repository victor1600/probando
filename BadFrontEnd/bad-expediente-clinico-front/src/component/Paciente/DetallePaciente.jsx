import React, {Fragment, useState} from 'react'
import {useParams, withRouter} from "react-router-dom";

const DetallePaciente = (props) => {
    const estilosInicio = {
        fondoHeader: {
            backgroundColor: '#80cbc4',
            textAlign: 'center'
        },
        fondoBody: {
            backgroundColor: '#F5F5F6'
        },
        letraCarrousel: {
            color: 'black'
        }
    };
    const activo = {
        si:{
            color:'#239B56'
        },
        no:{
            color:'red'
        }
    };

    // Validar Acceso
    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)

    // Obtener Paciente
    const [jsonGE, setJsonGE] = React.useState(false);
    const [jsonGS, setJsonGS] = React.useState(false);
    const [msjGet, setMsjGet] = React.useState(['']);
    const [paciente, setPaciente] = React.useState([]); 
    const {idPersona} = useParams();

    React.useEffect(() => {
        if(loginE === true){
            const obtenerPaciente = async () => {
                const data = await fetch(`http://localhost:8080/pacientes/${idPersona}`, {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });  
                                    
                const dataPaciente = await data.json();
                switch(data.status) {
                    case 403:
                        props.history.push('/error403');
                      break;
                    case 302:
                        setJsonGS(true);
                        setJsonGE(false);
                        setPaciente(dataPaciente);
                      break;
                    case 404:
                        setJsonGS(false);
                        setJsonGE(true);
                        setMsjGet([
                            String(dataPaciente.mensaje)
                        ]);
                      break;
                    default:
                        props.history.push('/error');
                  }
            }
            obtenerPaciente()

        }else{
            props.history.push('/login');
        } 
    },[idPersona, props, token, loginE])

    return (
        <Fragment>
            <div className="m-5">
                <div className="card">
                    <div className="card-header" style={estilosInicio.fondoHeader}>
                        <h2>Datos del Paciente</h2>
                    </div>
                    <div className="card-body" style={estilosInicio.fondoBody}>
                        {jsonGS &&
                        <div>
                            <ul>
                                <li> <b>Dirección:</b> {paciente?.direccion?.urbanizacion} {paciente?.direccion?.calleAvenida} {paciente?.direccion?.numeroPasaje} {paciente?.direccion?.numeroCasa} <b> /</b> {paciente?.direccion?.municipio?.nombreMunicipio} 
                                        <b> / </b> {paciente?.direccion?.municipio?.departamento?.nombreDepartamento}</li>
                                <li> <b>Nombres:</b> {paciente.nombre1Persona} {paciente.nombre2Persona} </li>
                                <li> <b>Apellidos:</b> {paciente.apellido1Persona} {paciente.apellido2Persona}</li>
                                <li> <b>Fecha de nacimiento:</b> {paciente.fechaNacimientoPersona}</li>
                                <li> <b>Teléfono fijo: </b> {paciente.telefonoPersona===null ? 'No registrado': paciente.telefonoPersona}<b> Celular: </b> {paciente.celularPersona} </li>
                                <li> <b>Sexo: </b>{paciente.sexoPersona ==='M' ? 'Maculino' : 'Femenino'} </li>
                                <li> <b>Estado civil:</b> {paciente.estadoCivilPaciente==='S' ? 'Soltero' : paciente.estadoCivilPaciente==='C' ? 'Casado': paciente.estadoCivilPaciente==='D' ? 'Divorciado' : 'Viudo'} </li>
                                <li> <b>Estado del paciente:</b> {paciente.estadoPaciente===true ? <b style={activo.si}>Activo</b> : <b style={activo.no}>Inactivo</b>} </li>
                            </ul>
                            
                        </div>
                        }
                        {jsonGE &&
                            <div className="m-5">
                                <h1>{msjGet}</h1>
                            </div>
                        }
                        <br/>
                        <div className="row col-sm-12">
                            <div className="col-sm-2">
                                <div className="form-group ">
                                <a href="/pacientes" className="btn btn-primary">Regresar</a>
                                </div>
                            </div>
                            <div className="col-sm-2">
                                <div className="form-group ">
                                    <a href={`/pacientes/actualizar/${idPersona}`} className="btn btn-warning">Editar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default withRouter(DetallePaciente);