import React, { Fragment, useState } from 'react'
import { useForm } from 'react-hook-form'
import {withRouter} from "react-router-dom";

const AgregarIngreso = (props) => {
   const [loginE] = useState(localStorage.getItem('login') ? true:false)
   const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)
   const [usuario] = useState(localStorage.getItem('usuario') ? localStorage.getItem('usuario'):null)

   React.useEffect(()=>{

       if(loginE === true){

           const obtenerPermiso = async () => {
               const data = await fetch(`http://localhost:8080/usuario/${usuario}/IngresoAdd`, {
                   method: 'GET',
                   headers: {
                       "Content-type": "application/json; charset=UTF-8",
                       "Authorization": token
                   }
               });
               const info = await data.json();
               console.log(info);


               switch(data.status) {
                   case 200:
                       if(info.mensaje === "1"){}else{
                           props.history.push('/error403');
                       }
                     break;
                   default:
                       props.history.push('/error');
                 }


           }

           obtenerPermiso()

       }else{
           props.history.push('/login');
       }


   },[props,loginE,token,usuario])

   const [jsonOE, setJsonOE] = React.useState(false);
   const [jsonOS, setJsonOS] = React.useState(false);
   const [msj, setMsj] = React.useState(['']);
   const [titulo, setTitulo] = React.useState(['']);

   const {register, handleSubmit, errors} = useForm();
   const onSubmit = async (data, e) => {
       e.preventDefault();
       const btnModal = document.getElementById("modal");
       console.log(data);
       const jsonP = await fetch('http://localhost:8080/ingreso/agregar', {
           method: 'POST',
           body: JSON.stringify({
               fechaIngreso: data.fechaIngreso,
               motivoIngreso: data.motivoIngreso,
               fechaAltaIngreso: data.fechaAltaIngreso,
           }),
           headers: {
               "Content-type": "application/json; charset=UTF-8",
               "Authorization": token
           }
       });
       console.log(jsonP);
       const info = await jsonP.json();
       
       if (jsonP.status === 201) {
           setJsonOE(false);
           setJsonOS(true);
           setMsj([
               String(info.mensaje)
           ]);
           setTitulo([
               String(info.httpCodeName)
           ]);
           btnModal.click();
           e.target.reset();

       } else {
           setJsonOE(true);
           setJsonOS(false);
           setMsj([
               String(info.mensaje)
           ]);
           setTitulo([
               String(info.httpCodeName)
           ]);
           btnModal.click();
       }


   }
  


   // Obtener Area

   const [jsonEE, setJsonEE] = React.useState(false);
   const [jsonSE, setJsonSE] = React.useState(false);
   const [msjEspe, setMsjEspe] = React.useState(['']);
   const [area, setArea] = React.useState([]);

   React.useEffect(() => {
       if (loginE === true) {

           const obtenerArea = async () => {
               const data = await fetch('http://localhost:8080/area/todos', {
                   method: 'GET',
                   headers: {
                       "Content-type": "application/json; charset=UTF-8",
                       "Authorization": token
                   }
               });
               console.log(data.status);
               console.log(data.Areas);
               const dataAreas = await data.json();
               if (data.status === 302) {
                   setJsonSE(true);
                   setJsonEE(false);
                   setArea(dataAreas);
               } else {
                   setJsonSE(false);
                   setJsonEE(true);
                   setMsjEspe([
                       String(dataAreas.mensaje)
                   ]);
               }
           }
           obtenerArea();

       } else {
           props.history.push('/login');
       }
   }, [props, loginE, token])



   const handleInputChange = (event) => {
      var entrada = event.target.value;
      var entrada2 = event.target.value;
      if (entrada.length === 10) {
         var hoy = new Date()
         var fechaIngreso = new Date(entrada)
      } else {
         if (entrada2.length === 10) {
            var hoy = new Date()
            var fechaAltaIngreso = new Date(entrada2)
         }

      }
   }


   return (
       <Fragment>
           <div className="m-5">
               <div className="card">
                   <div className="card-header">
                       <h1>HOSPITALIZACIÓN</h1>
                   </div>
                   <div className="card-body">
                       <form className="form-horizontal" onSubmit={handleSubmit(onSubmit)}>

                       <div className="col-xs-12 col-sm-6 col-md-4">
                                <div className="form-group">
                                    <label className="control-label col-sm-12">Fecha de ingreso:</label>
                                    <input type="date" onChange={handleInputChange} className="form-control" name="fechaIngreso"
                                        required maxLength="10"
                                        ref={register({
                                            required: { value: true, message: "Fecha es obligatoria" },
                                            maxLength: { value: 10, message: "Máximo 10 caracteres" },
                                        })}
                                    />
                                    <span className="text-danger text-small d-block mb-2">
                                        {errors?.fechaIngreso?.message}
                                    </span>
                                </div>
                            </div>
                          


                            <div className="form-group">
                                <label className="control-label col-sm-2">Motivo:</label>
                                <div className="col-sm-10">
                                    <textarea type="text" className="form-control" placeholder="Ingrese el motivo" rows="4" 
                                           name="motivoIngreso"
                                           required maxLength="600" 
                                           ref={register({
                                               maxLength: {value: 600, message: "Maximo 600 caracteres"},
                                           })}
                                    />
                                    <span className="text-danger text-small d-block mb-2">
                                        {errors?.motivoIngreso?.message}
                                    </span>
                                </div>
                            </div>

                           


                           <div className="col-xs-12 col-sm-6 col-md-4">
                                <div className="form-group">
                                    <label className="control-label col-sm-12">Fecha de alta:</label>
                                    <input type="date" onChange={handleInputChange} className="form-control" name="fechaAltaIngreso"
                                        maxLength="10"
                                        ref={register({
                                          maxLength: { value: 10, message: "Máximo 10 caracteres" },
                                        })}
                                    />
                                    <span className="text-danger text-small d-block mb-2">
                                        {errors?.fechaAltaIngreso?.message}
                                    </span>
                                </div>
                            </div>
                           
                           <div className="form-group">
                               <div className="col-sm-offset-2 col-sm-10">
                                   <button type="submit" className="btn btn-success">Guardar</button>
                               </div>
                           </div>
                       </form>
                   </div>

                   <div className="m-5">
                       <a href="/ingreso" className="btn btn-info">Regresar</a>
                   </div>


                   <button id="modal" type="button" className="d-none" data-toggle="modal"
                           data-target="#exampleModalCenter">
                       Launch demo modal
                   </button>


                   <div className="modal fade" id="exampleModalCenter" tabIndex="-1" role="dialog"
                        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                       <div className="modal-dialog modal-dialog-centered" role="document">
                           <div className="modal-content">
                               <div className="modal-header">
                                   <h5 className="modal-title" id="exampleModalCenterTitle">{titulo}</h5>
                               </div>
                               <div className="modal-body">
                                   {msj}
                               </div>
                               <div className="modal-footer">
                                   {jsonOE &&
                                   <button type="button" className="btn btn-warning"
                                           data-dismiss="modal">Cancelar</button>
                                   }
                                   {jsonOS &&
                                   <div>
                                       <button type="button" className="btn btn-secondary mr-3"
                                               data-dismiss="modal">Agregar otra
                                       </button>
                                       <a type="button" className="btn btn-primary"
                                          href="/ingreso">Guardar</a>
                                   </div>
                                   }
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>

       </Fragment>
   );

   }
    export default withRouter(AgregarIngreso);