import React from "react";
import Listar from "../Reusables/Listar";

const ListarTM = () => {
  // The title to be displayed
  const title = "tipo de medicamento";
  const id = "idTipoMedicamento";

  // Endpoint and url for Links
  const endpoint = "http://localhost:8080/tipomedicamentos";
  const url = "tipoMedicamentos";

  // id used for linking correclty to actualizar and details
  const linkID = "idTipoMedicamento";

  // Name used to display in table of Listar
  const firstField = "nombreTMedicamento";

  // table headers for non parent, including first field.
  const tableHeaders = ["Nombre", "descripcion"];

  // Attributes to display that are not the id or the LINK attribute, or the parent
  const toDisplayAttributes = ["descripcionTipoMedicamento"];

  // table header for parents
  const tableParentHeaders = [];

  return (
    <Listar
      title={title}
      id={id}
      endpoint={endpoint}
      url={url}
      linkID={linkID}
      firstField={firstField}
      tableHeaders={tableHeaders}
      toDisplayAttributes={toDisplayAttributes}
      tableParentHeaders={tableParentHeaders}
    />
  );
};

export default ListarTM;
