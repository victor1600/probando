import React from "react";
import Actualizar from "../Reusables/Actualizar";

const ActualizarTM = () => {
  const title = "tipo de medicamento";
  const endpoint = "http://localhost:8080/tipomedicamentos";
  const url = "tipoMedicamentos";
  const postNames = ["nombreTMedicamento", "descripcionTipoMedicamento"];

  // Here you will define the rules for every field individually
  const field1 = {
    labelTitle: "nombre",
    placeholder: "Ingrese nombre",
    name: "nombre",
    maxLength: "50",
    pattern: "([A-Za-z ]+)",
    ref: {
      required: {
        value: true,
        message: "El nombre es obligatorio",
      },
      maxLength: {
        value: 50,
        message: "Maximo 10 caracteres",
      },
      pattern: {
        value: /[A-Za-z ]/, // TODO: test
        message: "solamente letras",
      },
    },
  };

  const field2 = {
    labelTitle: "descripcion",
    placeholder: "ingrese descripcion",
    name: "descripcion",
    maxLength: "50",
    pattern: "([A-Za-z ]+)",
    ref: {
      required: {
        value: true,
        message: "La descripcion es obligatoria",
      },
      maxLength: {
        value: 50,
        message: "Maximo 10 caracteres",
      },
      pattern: {
        value: /[A-Za-z ]/, // TODO: test
        message: "solamente letras",
      },
    },
  };

  // Array that will hold all the fields that you want to appear in the form. Dont include parent here.
  const fields = [field1, field2];

  return (
    <Actualizar
      title={title}
      endpoint={endpoint}
      url={url}
      postNames={postNames}
      fields={fields}
    />
  );
};

export default ActualizarTM;
