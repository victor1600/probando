import React, { Fragment } from "react";
import Detalle from "../Reusables/Detalle";

const DetalleTM = () => {
  const title = "tipo de medicamento";
  const endpoint = "http://localhost:8080/tipomedicamentos";
  const url = "tipoMedicamentos";
  // const id = 'idTipoMedicamento';
  const attributes = ["nombreTMedicamento", "descripcionTipoMedicamento"];
  const labels = ["Nombre", "Descripcion"];

  // const parents = [];
  //
  //
  // // list of main attribute of each parent we wanto to show
  // const parentsMainAttribute = []

  return (
    <Fragment>
      <Detalle
        title={title}
        endpoint={endpoint}
        url={url}
        attributes={attributes}
        labels={labels}
      />
    </Fragment>
  );
};

export default React.memo(DetalleTM);
