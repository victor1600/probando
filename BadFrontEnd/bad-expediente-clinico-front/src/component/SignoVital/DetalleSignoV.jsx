import React, {Fragment, useState} from 'react'
import {withRouter} from "react-router-dom";


//ruteo
import {useParams} from "react-router-dom";

const DetalleSignoV = (props) => {
    const estilosInicio = {
        fondoHeader: {
            backgroundColor: '#80cbc4',
            textAlign: 'center'
        },
        fondoBody: {
            backgroundColor: '#F5F5F6'
        },
        letraCarrousel: {
            color: 'black'
        }
    };

    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)

    const {idVital} = useParams();

    const [jsonOE, setJsonOE] = React.useState(false);
    const [jsonOS, setJsonOS] = React.useState(false);
    const [msj, setMsj] = React.useState(['']);

    const [signoV, setSignoV] = React.useState([]);

    React.useEffect(() => {

        if(loginE === true){
        const obtenerSignoV = async () => {
            const data = await fetch(`http://localhost:8080/signosVitales/${idVital}`, {
                method: 'GET',
                headers: {
                    "Content-type": "application/json; charset=UTF-8",
                    "Authorization": token
                }
            });                
            const dataSignoV = await data.json();
            switch(data.status) {
                case 403:
                    props.history.push('/error403');
                  break;
                case 302:
                    setJsonOS(true);
                    setJsonOE(false);
                    setSignoV(dataSignoV);
                  break;
                case 404:
                    setJsonOS(false);
                    setJsonOE(true);
                    setMsj([
                    String(dataSignoV.mensaje)
                    ]);
                  break;
                default:
                    props.history.push('/error');
              }
    
        }

        obtenerSignoV();

    }else{
        props.history.push('/login');
    } 
}, [idVital,props,token,loginE])



    return (
        <Fragment>
            <div className="m-5">
                <div className="card">
                    <div className="card-header" style={estilosInicio.fondoHeader}>
                        <h2>Los datos de Signo Vital con id {idVital} son:</h2>
                    </div>
                    <div className="card-body" style={estilosInicio.fondoBody}>
                        {jsonOS &&
                        <div>
                            <br/><br/>
                            <ul>
                                <li>Presion={signoV.presionVital}</li>
                                <li>Ritmo={signoV.ritmoVital}</li>
                                <li>Respiracion={signoV.respiracionVital}</li>
                                <li>Temperatura={signoV.temperaturaVital}</li>
                                <li>Estatura={signoV.estaturaVital}</li>
                                <li>Peso={signoV.pesoVital}</li>
                            </ul>
                        </div>
                        }
                        {
                            jsonOE &&
                            <div className="m-5">
                                <h1>{msj}</h1>
                            </div>
                        }
                        <a href="/signosVitales" className="btn btn-info">Regresar</a>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default withRouter(DetalleSignoV);