import React, {Fragment, useState} from 'react'
import {useForm} from 'react-hook-form'
import {useParams, withRouter} from "react-router-dom";

const ActualizarTPM = (props) => {

    const estilosInicio = {
        fondoHeader: {
            backgroundColor: '#80cbc4',
            textAlign: 'center'
        },
        fondoBody: {
            backgroundColor: '#F5F5F6'
        },
        letraCarrousel: {
            color: 'black'
        }
    };

    // Validar Acceso
    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)

    // Obtener Tipo Personal 
    const [jsonOE, setJsonGE] = React.useState(false);
    const [jsonOS, setJsonGS] = React.useState(false);
    const [msjGet, setMsjGet] = React.useState(['']);
    const [tipoPersonal, setTipoPersonal] = React.useState([]);
    const {idTipoPersonal} = useParams();

    React.useEffect(() => {
        if(loginE === true){
            const obtenerTipoPersonal = async () => {
                const data = await fetch(`http://localhost:8080/tipoPersonalMedico/${idTipoPersonal}`,{
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });
                const dataTipoPersonal = await data.json();
                switch(data.status) {
                    case 403:
                        props.history.push('/error403');
                        break;
                    case 302:
                        setJsonGS(true);
                        setJsonGE(false);
                        setTipoPersonal(dataTipoPersonal);
                        break;
                    case 404:
                        setJsonGS(false);
                        setJsonGE(true);
                        setMsjGet([
                        String(dataTipoPersonal.mensaje)
                        ]);
                        break;
                    default:
                        props.history.push('/error');
                    }
            }
            obtenerTipoPersonal();
            
        }else{
            props.history.push('/login');
        }
    }, [idTipoPersonal, props, loginE,token])

    // Actualizar Tipo Personal
    const [jsonPE, setJsonPE] = React.useState(false);
    const [jsonPS, setJsonPS] = React.useState(false);
    const [msj, setMsj] = React.useState(['']);
    const [titulo, setTitulo] = React.useState(['']);

    const {register, handleSubmit, errors} = useForm();
    const onSubmit = async (data, e) => {
        e.preventDefault();
        const btnModal = document.getElementById("modal");
        console.log(data)
        const jsonP = await fetch(`http://localhost:8080/tipoPersonalMedico/actualizar/${idTipoPersonal}`, {
            method: 'PUT',
            body: JSON.stringify({
                nombreTipoPersonal: data.tipoPersonal,
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                "Authorization": token
            }
        });
        console.log(jsonP);
        const info = await jsonP.json();

        if (jsonP.status === 202) {
            setJsonPE(false);
            setJsonPS(true);
            setMsj([
                String(info.mensaje)
            ]);
            setTitulo([
                String(info.httpCodeName)
            ]);
            btnModal.click();
            e.target.reset();

        } else {
            setJsonPE(true);
            setJsonPS(false);
            setMsj([
                String(info.mensaje)
            ]);
            setTitulo([
                String(info.httpCodeName)
            ]);
            btnModal.click();
        }
    }

    return (
        <Fragment>
            {jsonOS &&
            <div className="m-5">
                <div className="card">
                    <div className="card-header" style={estilosInicio.fondoHeader}>
                        <h2>Actualizar Tipo de Personal</h2>
                    </div>
                    <div className="card-body" style={estilosInicio.fondoBody}>
                        <form className="form-horizontal" onSubmit={handleSubmit(onSubmit)}>
                            
                            <div className="col-sm-4"><br/>
                                <div className="form-group">
                                    <label className="control-label col-sm-12">Nombre Tipo Personal:</label>
                                    <input type="text" className="form-control" placeholder="Ingrese Tipo Personal"
                                        name="tipoPersonal"
                                        required maxLength="50" pattern="([A-Za-zv ]+)"
                                        defaultValue={tipoPersonal.nombreTipoPersonal}
                                        ref={register({
                                            required: {value: true, message: "El Tipo Personal es obligatorio"},
                                            maxLength: {value: 30, message: "Maximo 30 caracteres"},
                                            pattern: {value: /[A-Za-z ]/, message: "Solamente letras"}
                                        })}
                                    />
                                    <span className="text-danger text-small d-block mb-2">
                                        {errors?.tipoPersonal?.message}
                                    </span>
                                </div>
                            </div><br/>

                            <div className="row col-sm-12">
                                <div className="col-sm-2">
                                    <div className="form-group ">
                                        <button type="submit" className="btn btn-success">Guardar</button>
                                    </div>
                                </div>
                                <div className="col-sm-2">
                                    <div className="form-group ">
                                    <a href="/tipoPersonalMedico" className="btn btn-info">Regresar</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            }
            {jsonOE &&
                <div className="m-5">
                    <h5><li>{msjGet}</li></h5>
                </div>
            }

            <button id="modal" type="button" className="d-none" data-toggle="modal" data-target="#exampleModalCenter">
                Launch demo modal
            </button>

            <div className="modal fade" id="exampleModalCenter" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalCenterTitle">{titulo}</h5>
                        </div>
                        <div className="modal-body">
                            {msj}
                        </div>
                        <div className="modal-footer">
                            {jsonPE &&
                            <button type="button" className="btn btn-warning" data-dismiss="modal">Cancelar</button>
                            }
                            {jsonPS &&
                            <div>
                                <a type="button" className="btn btn-primary" href="/tipoPersonalMedico">Aceptar</a>
                            </div>
                            }
                        </div>
                    </div>
                </div>
            </div>

        </Fragment>
    );
}

export default withRouter(ActualizarTPM);