import React, {Fragment, useState} from 'react'
import {Link, withRouter} from "react-router-dom"; 

const ListarTPM = (props) => {

    const estilosInicio = {
        fondoHeader: {
            backgroundColor: '#80cbc4',
            textAlign: 'center'
        },
        fondoBody: {
            backgroundColor: '#F5F5F6'
        },
        letraCarrousel: {
            color: 'black'
        }
    };
   
    // Validar Acceso
    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)
    
    // Obtener Tipo Personal
    const [jsonOE, setJsonOE] = React.useState(false);
    const [jsonOS, setJsonOS] = React.useState(false);
    const [msj, setMsj] = React.useState(['']);
    const [titulo, setTitulo] = React.useState(['']); 
    const [tipoPersonal, setTipoPersonal] = React.useState([]);

    React.useEffect(() => {
        if (loginE === true) {
            const obtenerTipoPersonal = async () => {
                const data = await fetch('http://localhost:8080/tipoPersonalMedico/todos', {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });

                const dataTipoPersonal = await data.json();
                switch(data.status) {
                    case 403:
                        props.history.push('/error403');
                      break;
                    case 302:
                        setJsonOS(true);
                        setJsonOE(false);
                        setTipoPersonal(dataTipoPersonal);
                      break;
                    case 404:
                        setJsonOS(false);
                        setJsonOE(true);
                        setMsj([
                        String(dataTipoPersonal.mensaje)
                    ]);
                        break;
                    default:
                        props.history.push('/error');
                }
            }
            obtenerTipoPersonal();
            
        } else {
            props.history.push('/login');
        }
    }, [props, loginE,token])

    // Eliminar Tipo Personal
    const [jsonEE, setJsonEE] = React.useState(false);
    const [jsonES, setJsonES] = React.useState(false);

    const eliminarTipoPersonalMedico = async (e) => {
        const btnCerrar = document.getElementById(`btn-${e.target.value}`)
        const btnInfo = document.getElementById('btn-infoDelete')
        const dataE = await fetch(`http://localhost:8080/tipoPersonalMedico/eliminar/${e.target.value}`, {
            method: 'DELETE',
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                "Authorization": token
            }
        })
        const infoEP = await dataE.json()

        console.log(infoEP)

        if (dataE.status === 200) {
            setJsonES(true)
            setJsonEE(false)
            setTitulo([
                String(infoEP.httpCodeName)
            ]);
            setMsj([
                String(infoEP.mensaje)
            ]);
        } else {
            setJsonES(false)
            setJsonEE(true)
            setTitulo([
                String(infoEP.httpCodeName)
            ]);
            setMsj([
                String(infoEP.mensaje)
            ]);
        }
        btnCerrar.click()
        btnInfo.click()
    }

    return (
        <Fragment>
            <div className="m-5">
                <div className="card">
                    <div className="card-header" style={estilosInicio.fondoHeader}>
                        <h2>Listado de tipo de Personal</h2>
                    </div>
                    <div className="card-body" style={estilosInicio.fondoBody}>
                        {jsonOS &&
                        <div className="table-responsive">
                            <table className="table table-hover">
                                <thead>
                                <tr>
                                    <th>Nombre Tipo Personal</th>
                                </tr>
                                </thead>
                                <tbody>
                                {tipoPersonal.map((item) => (
                                    <tr key={item.idTipoPersonal}>
                                        <td>
                                            <Link to={`/tipoPersonalMedico/${item.idTipoPersonal}`}>
                                                {item.nombreTipoPersonal}
                                            </Link>
                                        </td>
                                        <td><a href={`/tipoPersonalMedico/actualizar/${item.idTipoPersonal}`}
                                               className="btn btn-warning">Editar</a>
                                        </td>
                                        <td>
                                            <button type="button" className="btn btn-danger" data-toggle="modal"
                                                data-target={`#modal-${item.idTipoPersonal}`}> Eliminar
                                            </button>
                                        </td>
                                    </tr>
                                ))}
                                </tbody>
                            </table>
                        </div>
                        }
                        {jsonOE &&
                            <div className="m-5">
                                <h3><li>{msj}</li></h3>
                            </div>
                        }
                        <a href="/tipoPersonalMedico/agregar" className="btn btn-primary">Agregar</a>
                    </div>

                    {tipoPersonal.map((item) => (
                        <div key={item.idTipoPersonal} className="modal fade" id={`modal-${item.idTipoPersonal}`}
                             tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="exampleModalLongTitle">Advertencia
                                            Eliminación</h5>
                                    </div>
                                    <div className="modal-body">
                                        ¿Usted desea eliminar al tipo de
                                        personal <b>{item.nombreTipoPersonal}</b>?
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" id={`btn-${item.idTipoPersonal}`}
                                                className="btn btn-secondary" data-dismiss="modal">Cancelar
                                        </button>
                                        <button type="button" className="btn btn-success" value={item.idTipoPersonal}
                                                onClick={eliminarTipoPersonalMedico}>Si, deseo eliminar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}

                    <button id="btn-infoDelete" type="button" className="d-none" data-dismiss="modal"
                            data-toggle="modal" data-target="#infoDelete"> info del modal
                    </button>

                    <div className="modal fade" id="infoDelete" tabIndex="-1" role="dialog"
                         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalLongTitle">{titulo}</h5>
                                </div>
                                <div className="modal-body">
                                    {msj}
                                </div>
                                <div className="modal-footer">
                                    {jsonES &&
                                        <a type="button" href="/tipoPersonalMedico" className="btn btn-primary">Enterado</a>
                                    }
                                    {jsonEE &&
                                        <button type="button" className="btn btn-warning" data-dismiss="modal">Enterado</button>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default withRouter(ListarTPM);