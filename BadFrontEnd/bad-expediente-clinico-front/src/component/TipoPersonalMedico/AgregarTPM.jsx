import React, {Fragment, useState} from 'react'
import {useForm} from 'react-hook-form'
import {withRouter} from "react-router-dom";

const AgregarTPM = (props) => {

    const estilosInicio = {
        fondoHeader: {
            backgroundColor: '#80cbc4',
            textAlign: 'center'
        },
        fondoBody: {
            backgroundColor: '#F5F5F6'
        },
        letraCarrousel: {
            color: 'black'
        }
    };

    // Permiso de Acceso
    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)
    const [usuario] = useState(localStorage.getItem('usuario') ? localStorage.getItem('usuario'):null)

    React.useEffect(()=>{

        if(loginE === true){

            const obtenerPermiso = async () => {
                const data = await fetch(`http://localhost:8080/usuario/${usuario}/TipoPersonalAdd`, {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });
                const info = await data.json();
                console.log(info);

                switch(data.status) {
                    case 200:
                        if(info.mensaje === "1"){}else{
                            props.history.push('/error403');
                        }
                        break;
                    default:
                        props.history.push('/error');
                    }
            }
            obtenerPermiso()

        }else{
            props.history.push('/login');
        }

    },[props,loginE,token,usuario])

    const [jsonOE, setJsonOE] = React.useState(false);
    const [jsonOS, setJsonOS] = React.useState(false);
    const [msj, setMsj] = React.useState(['']);
    const [titulo, setTitulo] = React.useState(['']);

    const {register, handleSubmit, errors} = useForm();
    const onSubmit = async (data, e) => {
        e.preventDefault();
        const btnModal = document.getElementById("modal");
        //console.log(data)
        const jsonP = await fetch('http://localhost:8080/tipoPersonalMedico/agregar', {
            method: 'POST',
            body: JSON.stringify({
                nombreTipoPersonal: data.nombreTipoPersonal,
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                "Authorization": token
            }
        });
        console.log(jsonP);
        const info = await jsonP.json();

        if (jsonP.status === 201) {
            setJsonOE(false);
            setJsonOS(true);
            setMsj([
                String(info.mensaje)
            ]);
            setTitulo([
                String(info.httpCodeName)
            ]);
            btnModal.click();
            e.target.reset();

        } else {
            setJsonOE(true);
            setJsonOS(false);
            setMsj([
                String(info.mensaje)
            ]);
            setTitulo([
                String(info.httpCodeName)
            ]);
            btnModal.click();
        }
    }

    return (
        <Fragment>
            <div className="m-5">
                <div className="card">
                    <div className="card-header" style={estilosInicio.fondoHeader}> 
                        <h2>Agregar Tipo de Personal</h2>
                    </div>
                    <div className="card-body" style={estilosInicio.fondoBody}>
                        <form className="form-horizontal" onSubmit={handleSubmit(onSubmit)}>
                        
                            <div className="col-sm-4"><br/>
                                <div className="form-group">
                                    <label className="control-label col-sm-12">Nombre Tipo Personal:</label>
                                    <input type="text" className="form-control" name="nombreTipoPersonal"
                                        required maxLength="30" pattern="([A-Za-z ]+)"
                                        ref={register({
                                            required: {value: true, message: "El tipo Personal es obligatorio"},
                                            maxLength: {value: 30, message: "Maximo 30 caracteres"},
                                            pattern: {value: /[A-Za-z ]/, message: "Solamente letras"}
                                        })}
                                    />
                                    <span className="text-danger text-small d-block mb-2">
                                        {errors?.paciente?.message}
                                    </span>
                                </div>
                            </div><br/>

                            <div className="row col-sm-12">
                                <div className="col-sm-2">
                                    <div className="form-group ">
                                        <button type="submit" className="btn btn-success">Guardar</button>
                                    </div>
                                </div>
                                <div className="col-sm-2">
                                    <div className="form-group ">
                                    <a href="/tipoPersonalMedico" className="btn btn-info">Regresar</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <button id="modal" type="button" className="d-none" data-toggle="modal" data-target="#exampleModalCenter">
                        Modal
                    </button>

                    <div className="modal fade" id="exampleModalCenter" tabIndex="-1" role="dialog"
                         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalCenterTitle">{titulo}</h5>
                                </div>
                                <div className="modal-body">
                                    {msj}
                                </div>
                                <div className="modal-footer">
                                    {jsonOE &&
                                        <button type="button" className="btn btn-warning" data-dismiss="modal">
                                            Cancelar
                                        </button>
                                    }
                                    {jsonOS &&
                                        <div>
                                            <button type="button" className="btn btn-secondary mr-3" data-dismiss="modal">
                                                Agregar Nuevo
                                            </button>
                                            <a type="button" className="btn btn-primary" href="/tipoPersonalMedico">
                                                Guardar</a>
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default withRouter(AgregarTPM);