import React, {Fragment, useState} from 'react'
import {useParams, withRouter} from "react-router-dom";

const DetallePersonal = (props) => {
    const estilosInicio = {
        fondoHeader: {
            backgroundColor: '#80cbc4',
            textAlign: 'center'
        },
        fondoBody: {
            backgroundColor: '#F5F5F6'
        },
        letraCarrousel: {
            color: 'black'
        }
    };
    const activo = {
        si:{
            color:'#239B56'
        },
        no:{
            color:'red'
        }
    };

    // Validar Acceso
    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)

    // Obtener Personal
    const [jsonGE, setJsonGE] = React.useState(false);
    const [jsonGS, setJsonGS] = React.useState(false);
    const [msjGet, setMsjGet] = React.useState(['']);
    const [personal, setPersonal] = React.useState([]);
    const {idPersona} = useParams();

    React.useEffect(() => {
        if(loginE === true){
            const obtenerPersonal = async () => {
                const data = await fetch(`http://localhost:8080/personalMedico/${idPersona}`, {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });  
                                    
                const dataPersonal = await data.json();
                switch(data.status) {
                    case 403:
                        props.history.push('/error403');
                      break;
                    case 302:
                        setJsonGS(true);
                        setJsonGE(false);
                        setPersonal(dataPersonal);
                      break;
                    case 404:
                        setJsonGS(false);
                        setJsonGE(true);
                        setMsjGet([
                            String(dataPersonal.mensaje)
                        ]);
                      break;
                    default:
                        props.history.push('/error');
                  }
            }
            obtenerPersonal()

        }else{
            props.history.push('/login');
        } 
    },[idPersona, props, token, loginE])

    return (
        <Fragment>
            <div className="m-5">
                <div className="card">
                    <div className="card-header" style={estilosInicio.fondoHeader}>
                        <h2>Datos del Personal</h2>
                    </div>
                    <div className="card-body" style={estilosInicio.fondoBody}>
                        {jsonGS &&
                        <div>
                            <ul>
                                <li> <b>Dirección:</b> {personal?.direccion?.urbanizacion}, {personal?.direccion?.calleAvenida} {personal?.direccion?.numeroPasaje} {personal?.direccion?.numeroCasa} <b> /</b> {personal?.direccion?.municipio?.nombreMunicipio} 
                                        <b> / </b> {personal?.direccion?.municipio?.departamento?.nombreDepartamento}</li>
                                <li> <b>Nombres:</b> {personal.nombre1Persona} {personal.nombre2Persona} </li>
                                <li> <b>Apellidos:</b> {personal.apellido1Persona} {personal.apellido2Persona}</li>
                                <li> <b>Fecha de nacimiento:</b> {personal.fechaNacimientoPersona}</li>
                                <li> <b>Teléfono fijo: </b> {personal.telefonoPersona===null ? 'No registrado': personal.telefonoPersona}<b> Celular: </b> {personal.celularPersona} </li>
                                <li> <b>Sexo: </b>{personal.sexoPersona ==='M' ? 'Maculino' : 'Femenino'} </li>
                                <li> <b>Tipo Personal:</b> {personal?.tipoPersonal?.nombreTipoPersonal} </li>
                                <li> <b>JVP:</b> {personal.jvp} </li>
                                <li> <b>Estado del personal:</b> {personal.estadoPersonal===true ? <b style={activo.si}>Activo</b> : <b style={activo.no}>Inactivo</b>} </li>
                            </ul>
                        </div>
                        }
                        {jsonGE &&
                            <div className="m-5">
                                <h1>{msjGet}</h1>
                            </div>
                        }
                        <br/>
                        <div className="row col-sm-12">
                            <div className="col-sm-2">
                                <div className="form-group ">
                                <a href="/personalMedico" className="btn btn-primary">Regresar</a>
                                </div>
                            </div>
                            <div className="col-sm-2">
                                <div className="form-group ">
                                    <a href={`/personalMedico/actualizar/${idPersona}`} className="btn btn-warning">Editar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default withRouter(DetallePersonal);