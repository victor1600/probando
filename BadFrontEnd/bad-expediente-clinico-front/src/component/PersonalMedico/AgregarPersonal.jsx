import React, {Fragment, useState} from 'react'
import {useForm} from 'react-hook-form'
import {withRouter} from "react-router-dom";

const AgregarPersonal = (props) => {

    const estilosInicio = {
        fondoHeader: {
            backgroundColor: '#80cbc4',
            textAlign: 'center'
        },
        fondoBody: {
            backgroundColor: '#F5F5F6'
        },
        letraCarrousel: {
            color: 'black'
        }
    };
    
    // Permiso de Acceso
    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)
    const [usuario] = useState(localStorage.getItem('usuario') ? localStorage.getItem('usuario'):null)

    React.useEffect(()=>{ 

        if(loginE === true){
            const obtenerPermiso = async () => {
                const data = await fetch(`http://localhost:8080/usuario/${usuario}/PersonalAdd`, {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });
                const info = await data.json();
                //console.log(info);
                switch(data.status) {
                    case 200:
                        if(info.mensaje === "1"){}else{
                            props.history.push('/error403');
                        }
                      break;
                    default:
                        props.history.push('/error');
                  }
            }
            obtenerPermiso()

        }else{
            props.history.push('/login');
        }

    },[props,loginE,token,usuario])

    // Agregar Personal
    const [jsonOE, setJsonOE] = React.useState(false);
    const [jsonOS, setJsonOS] = React.useState(false);
    const [msj, setMsj] = React.useState(['']);
    const [titulo, setTitulo] = React.useState(['']);
    
    const { register, handleSubmit, errors } = useForm();
    const onSubmit = async (data, e) => {
        e.preventDefault();
        const btnModal = document.getElementById("modal");
        console.log(data)
        const jsonP = await fetch('http://localhost:8080/personalMedico/agregar', {
            method: 'POST',
            body: JSON.stringify({
                nombre1Persona: data.nombre1Persona,
                nombre2Persona: data.nombre2Persona,
                nombre3Persona: data.nombre3Persona,
                apellido1Persona: data.apellido1Persona,
                apellido2Persona: data.apellido2Persona,
                apellido3Persona: data.apellido3Persona,
                fechaNacimientoPersona: data.fechaNacimientoPersona,
                sexoPersona: data.sexoPersona,
                duiPersona: data.duiPersona,
                nitPersona: data.nitPersona,
                direccion: {
                    urbanizacion: data.urbanizacion,
                    calleAvenida: data.calleAvenida,
                    numeroPasaje: data.numeroPasaje,
                    numeroCasa: data.numeroCasa,
                    municipio: {
                        idMunicipio: data.idMunicipio,
                        departamento: {
                            idDepartamento: data.idDepartamento,
                        }
                    }
                },
                estadoPersonal: data.estadoPersonal=true,
                jvp: data.jvp,
                tipoPersonal: {
                    idTipoPersonal : data.idTipoPersonal,
                }
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                "Authorization": token
            }
        });
        console.log(jsonP);
        const info = await jsonP.json();

        if (jsonP.status === 201) {
            setJsonOE(false);
            setJsonOS(true);
            setMsj([
                String(info.mensaje)
            ]);
            setTitulo([
                String(info.httpCodeName)
            ]);
            btnModal.click();
            e.target.reset();

        } else {
            setJsonOE(true);
            setJsonOS(false);
            setMsj([
                String(info.mensaje)
            ]);
            setTitulo([
                String(info.httpCodeName)
            ]);
            btnModal.click();
        }
    }

    // Obtener Tipo Personal
    const [jsonETP, setJsonETP] = React.useState(false);
    const [jsonSTP, setJsonSTP] = React.useState(false);
    const [msjTP, setMsjTP] = React.useState(['']);
    const [tipoPersonal, setTipoPersonal] = React.useState([]);

    React.useEffect(() => {
        if (loginE === true) {

            const obtenerTipoPersonal = async () => {
                const data = await fetch('http://localhost:8080/tipoPersonalMedico/todos', {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });

                const dataTipoPersonal = await data.json();
                if (data.status === 302) {
                    setJsonSTP(true);
                    setJsonETP(false);
                    setTipoPersonal(dataTipoPersonal);
                } else {
                    setJsonSTP(false);
                    setJsonETP(true);
                    setMsjTP([
                        String(dataTipoPersonal.mensaje)
                    ]);
                }
            }
            obtenerTipoPersonal();
            
        } else {
            props.history.push('/login');
        }
    }, [props, loginE, token])

    // Obtener Departementos
    const [deptId, setDeptId] = useState(0);
    const [jsonED, setJsonED] = React.useState(false);
    const [jsonSD, setJsonSD] = React.useState(false);
    const [msjDept, setMsjDept] = React.useState(['']);
    const [departamentos, setDepartamentos] = React.useState([]);

    React.useEffect(() => {
        if (loginE === true) {

            const obtenerDepartamentos = async () => {
                const data = await fetch('http://localhost:8080/departamentos/todos', {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });
                console.log(data.status);
                console.log(data.Departamentos);
                const dataDepartamentos = await data.json();
                if (data.status === 302) {
                    setJsonSD(true);
                    setJsonED(false);
                    setDepartamentos(dataDepartamentos);
                } else {
                    setJsonSD(false);
                    setJsonED(true);
                    setMsjDept([
                        String(dataDepartamentos.mensaje)
                    ]);
                }
            }
            obtenerDepartamentos();
            
        } else {
            props.history.push('/login');
        }
    }, [props, loginE, token])

    // Obtener Muncicipios
    const [jsonEM, setJsonEM] = React.useState(false);
    const [jsonSM, setJsonSM] = React.useState(false);
    const [msjMuni, setMsjMuni] = React.useState(['']);
    const [municipios, setMunicipios] = React.useState([]);

    React.useEffect(() => {
        if (loginE === true) {

            const obtenerMunicipios = async () => {
                const data = await fetch('http://localhost:8080/municipios/todos', {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });
                console.log(data.status);
                console.log(data.Municipios);
                const dataMunicipios = await data.json();
                if (data.status === 302) {
                    setJsonSM(true);
                    setJsonEM(false);
                    setMunicipios(dataMunicipios);
                } else {
                    setJsonSM(false);
                    setJsonEM(true);
                    setMsjMuni([
                        String(dataMunicipios.mensaje)
                    ]);
                }
            }
            obtenerMunicipios();
            
        } else {
            props.history.push('/login');
        }
    }, [props, loginE, token])

    return (
        <Fragment>
            <div className="m-5">
                <div className="card">
                    <div className="card-header" style={estilosInicio.fondoHeader}>
                        <h2>Agregar Personal</h2>
                    </div>
                    <div className="card-body" style={estilosInicio.fondoBody}>
                        <form className="form-horizontal" onSubmit={handleSubmit(onSubmit)}>
                            
                            {/* Seccion 1 */}
                            <div className="card-body">
                                <h5>Información Personal:</h5><br/>
                                <div className="row col-sm-12">
                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">1° Nombre:</label>
                                            <input type="text" className="form-control" name="nombre1Persona"
                                                required maxLength="20" pattern="([A-Za-z ]+)"
                                                ref={register({
                                                    required: { value: true, message: "El primer nombre es obligatorio" },
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.personal?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">2° Nombre:</label>
                                            <input type="text" className="form-control" name="nombre2Persona"
                                                maxLength="20" pattern="([A-Za-z ]+)"
                                                ref={register({
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.personal?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">3° Nombre:</label>
                                            <input type="text" className="form-control" name="nombre3Persona"
                                                maxLength="20" pattern="([A-Za-z ]+)"
                                                ref={register({
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.personal?.message}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                {/* Fin nombres */}

                                <div className="row col-sm-12">
                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">1° Apellido:</label>
                                            <input type="text" className="form-control" name="apellido1Persona"
                                                required maxLength="20" pattern="([A-Za-z ]+)"
                                                ref={register({
                                                    required: { value: true, message: "El primer apellido es obligatorio" },
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.personal?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">2° Apellido:</label>
                                            <input type="text" className="form-control" name="apellido2Persona"
                                                maxLength="20" pattern="([A-Za-z ]+)"
                                                ref={register({
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.personal?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">3° Apellido:</label>
                                            <input type="text" className="form-control" name="apellido3Persona"
                                                maxLength="20" pattern="([A-Za-z ]+)"
                                                ref={register({
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.personal?.message}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                {/* Fin apellidos */}

                                <div className="row col-sm-12">
                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Fecha de Nacimiento:</label>
                                            <input type="date" className="form-control" name="fechaNacimientoPersona"
                                                required maxLength="10"
                                                    ref={register({
                                                    required: { value: true, message: "Fecha de nacimiento es obligatorio" },
                                                    maxLength: { value: 10, message: "Máximo 10 caracteres" },
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.personal?.message}
                                            </span>
                                        </div>
                                    </div>

                                     <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Sexo:</label>
                                            <select id="sexoPersona" name="sexoPersona" className="form-control" required
                                                ref={register({
                                                required: { value: true, message: "El campo sexo es obligatorio" },
                                                pattern: { value: /[M F]/, message: "Solamente letras" }
                                            })}>
                                                <option value="">Seleccione una opción...</option>
                                                <option value="M">Masculino</option>
                                                <option value="F">Femenino</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>{/* fecha - sexo */}

                                <div className="row col-sm-12">
                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">DUI:</label>
                                            <input type="text" className="form-control" name="duiPersona" maxLength="10" required
                                                ref={register({
                                                    maxLength: { value: 10, message: "Máximo 10 caracteres" },
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.personal?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">NIT:</label>
                                            <input type="text" className="form-control" name="nitPersona" maxLength="17" required
                                                ref={register({
                                                    maxLength: { value: 17, message: "Máximo 17 caracteres" },
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.personal?.message}
                                            </span>
                                        </div>
                                    </div>
                                </div> {/* Fin DUI - NIT */}
                            </div> 
                            
                            {/*Seccion 2 */}
                            <div className="card-body">
                                <h5>Información de Contacto:</h5><br/>
                                <div className="row col-sm-12">
                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Teléfono Fijo:</label>
                                            <input type="text" className="form-control" name="telefonoPersona" maxLength="9"
                                                    ref={register({
                                                    maxLength: { value: 9, message: "Máximo 9 caracteres" },
                                                    pattern: { value: /[0-9]/, message: "Solamente números y guion medio." }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.personal?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Celular:</label>
                                            <input type="text" className="form-control" name="celularPersona" maxLength="9"
                                                    ref={register({
                                                    maxLength: { value: 9, message: "Máximo 9 caracteres" },
                                                    pattern: { value: /[0-9]/, message: "Solamente números y guion medio." }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.personal?.message}
                                            </span>
                                        </div>
                                    </div>

                                </div>
                                {/* Fin telefono */}

                                <div className="row col-sm-12">
                                    {jsonSD &&
                                        <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Departamento:</label>
                                            <select value={deptId} onChange={e => setDeptId(e.target.value)} 
                                                id="idDepartamento" name="idDepartamento" className="browser-default custom-select" required
                                                ref={register({
                                                    required: { value: true, message: "El campo departamento es obligatorio" }
                                                })}>
                                                <option value="">Seleccionar Departamento</option>
                                                {departamentos.map((item) => (
                                                    <option key={item.idDepartamento} value={item.idDepartamento}>{item.nombreDepartamento}</option>
                                                ))}
                                            </select>
                                        </div>
                                    </div>
                                    }
                                    {jsonED &&
                                        <div className="m-5">
                                            <h5><li>{msjDept}</li></h5>
                                        </div>
                                    }
                                    {jsonSM &&
                                        <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Municipio:</label>
                                            <select  id="idMunicipio" name="idMunicipio" className="browser-default custom-select" required
                                                ref={register({
                                                    required: { value: true, message: "El campo municipio es obligatorio" },
                                                })}>
                                                <option value="">Seleccionar Municipio</option>
                                                {municipios.filter(item => item.departamento.idDepartamento === deptId).map((item) => (
                                                    <option key={item.idMunicipio} value={item.idMunicipio}>{item.nombreMunicipio}</option>
                                                ))}
                                            </select>
                                        </div>
                                    </div>
                                    }
                                    {jsonEM &&
                                        <div className="m-5">
                                            <h5><li>{msjMuni}</li></h5>
                                        </div>
                                    }
                                </div>

                                <div className="row col-sm-12">
                                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                                        <div className="form-group">
                                        <label className="control-label col-sm-12">Urbanizacion/ Colonia/ Barrio:</label>
                                            <input type="text" className="form-control" name="urbanizacion"
                                                required maxLength="100"
                                                    ref={register({
                                                    required: { value: true, message: "El campo Urbanizacion/Colonia/Barrio es obligatorio" },
                                                    maxLength: { value: 100, message: "Máximo 100 caracteres" },
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.personal?.message}
                                            </span> 
                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                                        <div className="form-group">
                                        <label className="control-label col-sm-12">Calle/Avenida:</label>
                                            <input type="text" className="form-control" name="calleAvenida" maxLength="100"
                                                    ref={register({
                                                    maxLength: { value: 100, message: "Máximo 100 caracteres" },
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.personal?.message}
                                            </span> 
                                        </div>
                                    </div>
                                    <div className=" row col-xs-12 col-md-9 col-lg-4">
                                        <div className="col-xs-12 col-sm-4 col-md-4 col-lg-6">
                                            <div className="form-group">
                                            <label className="control-label col-sm-12">Pasaje:</label>
                                                <input type="text" className="form-control" name="numeroPasaje" maxLength="16"
                                                        ref={register({
                                                        maxLength: { value: 16, message: "Máximo 16 caracteres" },
                                                    })}
                                                />
                                                <span className="text-danger text-small d-block mb-2">
                                                    {errors?.personal?.message}
                                                </span> 
                                            </div>
                                        </div>
                                        <div className="col-xs-12 col-sm-4 col-md-4 col-lg-6">
                                            <div className="form-group">
                                            <label className="control-label col-sm-12">#Casa:</label>
                                                <input type="text" className="form-control" name="numeroCasa" maxLength="10"
                                                        ref={register({
                                                        maxLength: { value: 10, message: "Máximo 10 caracteres" },
                                                    })}
                                                />
                                                <span className="text-danger text-small d-block mb-2">
                                                    {errors?.personal?.message}
                                                </span> 
                                            </div>
                                        </div>
                                    </div>
                                </div><br/>

                                <div className="row col-sm-12">
                                    {jsonSTP &&
                                        <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Tipo de Personal:</label>
                                            <select  id="idTipoPersonal" name="idTipoPersonal" className="browser-default custom-select" required
                                                ref={register({
                                                    required: { value: true, message: "El campo tipo personal es obligatorio." },
                                                })}>
                                                <option value="">Seleccionar Tipo Personal</option>
                                                {tipoPersonal.map((item) => (
                                                    <option key={item.idTipoPersonal} value={item.idTipoPersonal}>{item.nombreTipoPersonal}</option>
                                                ))}
                                            </select>
                                        </div>
                                    </div>
                                    }
                                    {jsonETP &&
                                        <div className="m-5">
                                            <h5><li>{msjTP}</li></h5>
                                        </div>
                                    }

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">JVP:</label>
                                            <input type="text" className="form-control" name="jvp" maxLength="5" required
                                                    ref={register({
                                                    maxLength: { value: 5, message: "Máximo 5 caracteres" },
                                                    pattern: { value: /[0-9]/, message: "Solamente números." }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.personal?.message}
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div className="row col-sm-12">
                                    <div className="col-sm-2">
                                        <div className="form-group ">
                                            <button type="submit" className="btn btn-success">Guardar</button>
                                        </div>
                                    </div>
                                    <div className="col-sm-2">
                                        <div className="form-group ">
                                        <a href="/personalMedico" className="btn btn-info">Regresar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </form>
                    </div>

                    {/*Modal*/}
                    <button id="modal" type="button" className="d-none" data-toggle="modal" data-target="#exampleModalCenter">
                        Launch demo modal
                    </button>
                    <div className="modal fade" id="exampleModalCenter" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalCenterTitle">{titulo}</h5>
                                </div>
                                <div className="modal-body">
                                    {msj}
                                </div>
                                <div className="modal-footer">
                                    {jsonOE &&
                                        <button type="button" className="btn btn-warning" data-dismiss="modal">Cancelar</button>
                                    }
                                    {jsonOS &&
                                        <div>
                                            <button type="button" className="btn btn-secondary mr-3" data-dismiss="modal">Agregar otra </button>
                                            <a type="button" className="btn btn-primary" href="/personalMedico">Guardar</a>
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </Fragment>
    );
}
export default withRouter(AgregarPersonal);