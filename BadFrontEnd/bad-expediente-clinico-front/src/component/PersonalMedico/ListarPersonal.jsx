import React, {Fragment, useState} from 'react'
import {Link, withRouter} from "react-router-dom";

const ListarPersonal = (props) => {

    const estilosInicio = {
        fondoHeader: {
            backgroundColor: '#80cbc4',
            textAlign: 'center'
        },
        fondoBody: {
            backgroundColor: '#F5F5F6'
        },
        letraCarrousel: {
            color: 'black'
        }
    };
    const activo = {
        si:{
            color:'#239B56'
        },
        no:{
            color:'red'
        }
    };

    // Validar Acceso
    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)
    
    // Obtener Personal
    const [jsonOE, setJsonOE] = React.useState(false);
    const [jsonOS, setJsonOS] = React.useState(false);
    const [msj, setMsj] = React.useState(['']);
    const [titulo, setTitulo] = React.useState(['']);
    const [personal, setPersonal] = React.useState([]);
    
    React.useEffect(() => {
        if (loginE === true) {

            const obtenerPersonal = async () => {
                const data = await fetch('http://localhost:8080/personalMedico/todos', {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });

                const dataPersonal = await data.json();
                switch(data.status) {
                    case 403:
                        props.history.push('/error403');
                      break;
                    case 302:
                        setJsonOS(true);
                        setJsonOE(false);
                        setPersonal(dataPersonal);
                      break;
                    case 404:
                        setJsonOS(false);
                        setJsonOE(true);
                        setMsj([
                        String(dataPersonal.mensaje)
                    ]);
                        break;
                    default:
                        props.history.push('/error');
                }
            }
            obtenerPersonal();
            
        } else {
            props.history.push('/login');
        }
    }, [props, loginE,token])

    // Estado Personal
    const [jsonEE, setJsonEE] = React.useState(false);
    const [jsonES, setJsonES] = React.useState(false);
    
    const estadoPersonal = async (e) => {
        const btnCerrar = document.getElementById(`btn-${e.target.value}`)
        const btnInfo = document.getElementById('btn-infoDelete')
        const dataE = await fetch(`http://localhost:8080/personalMedico/estado/${e.target.value}`, {
            method: 'PUT',
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                "Authorization": token
            }
        })
        const infoEP = await dataE.json()
        console.log(infoEP)

        if (dataE.status === 200) {
            setJsonES(true)
            setJsonEE(false)
            setTitulo([
                String(infoEP.httpCodeName)
            ]);
            setMsj([
                String(infoEP.mensaje)
            ]);
        } else {
            setJsonES(false)
            setJsonEE(true)
            setTitulo([
                String(infoEP.httpCodeName)
            ]);
            setMsj([
                String(infoEP.mensaje)
            ]);
        }

        btnCerrar.click()
        btnInfo.click()
    }

    return (
        <Fragment>
            <div className="m-5">
                <div className="card">
                    <div className="card-header" style={estilosInicio.fondoHeader}>
                        <h2>Listado Personal Medico</h2>
                    </div>
                    <div className="card-body" style={estilosInicio.fondoBody}>
                        {jsonOS &&
                        <div className="table-responsive">
                            <table className="table table-hover">
                                <thead>
                                <tr>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Tipo Personal</th>
                                    <th>Celular</th>
                                    <th>Estado</th>
                                </tr>
                                </thead>
                                <tbody>
                                {personal.map((item) => (
                                    <tr key={item.idPersona}>
                                        <td>
                                            <Link to={`/personalMedico/${item.idPersona}`}>
                                                {item.nombre1Persona} {item.nombre2Persona}
                                            </Link>
                                        </td>
                                        <td>
                                            <Link to={`/personalMedico/${item.idPersona}`}>
                                                {item.apellido1Persona} {item.apellido2Persona}
                                            </Link>
                                        </td>
                                        <td>
                                            <Link to={`/personalMedico/${item.idPersona}`}>
                                                {item.tipoPersonal.nombreTipoPersonal}
                                            </Link>
                                        </td>
                                        <td>
                                            <Link to={`/personalMedico/${item.idPersona}`}>
                                                {item.celularPersona}
                                            </Link>
                                        </td>
                                        <td>
                                            <Link to={`/personalMedico/${item.idPersona}`}>
                                                {item.estadoPersonal===true ? <p style={activo.si}>Activo</p> : <p style={activo.no}>Inactivo</p>} 
                                            </Link>
                                        </td>
                                        <td><a href={`/personalMedico/actualizar/${item.idPersona}`}
                                               className="btn btn-warning">Editar</a>
                                        </td>
                                        <td>
                                            <button type="button" className="btn btn-danger" data-toggle="modal"
                                                    data-target={`#modal-${item.idPersona}`}> Estado
                                            </button>
                                        </td>
                                    </tr>
                                ))}
                                </tbody>
                            </table>
                        </div>
                        }
                        {jsonOE &&
                            <div className="m-5">
                                <h3><li>{msj}</li></h3>
                            </div>
                        }
                        <a href="/personalMedico/agregar" className="btn btn-primary">Agregar</a>
                    </div>

                    {personal.map((item) => (
                        <div key={item.idPersona} className="modal fade" id={`modal-${item.idPersona}`}
                            tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="exampleModalLongTitle">Advertecia cambio de estado</h5>
                                    </div>
                                    <div className="modal-body">
                                        ¿Desea cambiar estado del personal {item.nombre1Persona}?
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" id={`btn-${item.idPersona}`}
                                            className="btn btn-secondary" data-dismiss="modal">Cancelar
                                        </button>
                                        <button type="button" className="btn btn-success" value={item.idPersona}
                                            onClick={estadoPersonal}>Si, deseo cambiar estado.
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}

                    <button id="btn-infoDelete" type="button" className="d-none" data-dismiss="modal" data-toggle="modal" data-target="#infoDelete">
                        info del modal
                    </button>


                    <div className="modal fade" id="infoDelete" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalLongTitle">{titulo}</h5>
                                </div>
                                <div className="modal-body">
                                    {msj}
                                </div>
                                <div className="modal-footer">
                                    {jsonES &&
                                        <a type="button" href="/personalMedico" className="btn btn-primary">Enterado</a>
                                    }
                                    {jsonEE &&
                                        <button type="button" className="btn btn-warning" data-dismiss="modal">Enterado</button>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </Fragment>
    );
}

export default withRouter(ListarPersonal);