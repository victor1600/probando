import React from "react";
let FetchUseEffect = (
  loginE,
  token,
  props,
  setJsonOS,
  setJsonOE,
  setPacientes,
  setMsj,
  endpoint
) => {
  React.useEffect(() => {
    if (loginE === true) {
      const obtenerValores = async () => {
        const data = await fetch(endpoint, {
          method: "GET",
          headers: {
            "Content-type": "application/json; charset=UTF-8",
            Authorization: token,
          },
        });
        //console.log(data.status);
        //console.log(data.pacientes);
        const dataPacientes = await data.json();
        switch (data.status) {
          case 403:
            props.history.push("/error403");
            break;
          case 302:
            // setJsonOS(true);
            // setJsonOE(false);
            setPacientes(dataPacientes);
            break;
          case 200:
            // setJsonOS(true);
            // setJsonOE(false);
            setPacientes(dataPacientes);
            break;
          case 404:
            setJsonOS(false);
            setJsonOE(true);
            setMsj([String(dataPacientes.mensaje)]);
            break;
          default:
            props.history.push("/error");
        }
      };
      obtenerValores();
    } else {
      props.history.push("/login");
    }
  }, [
    props,
    loginE,
    token,
    endpoint,
    setJsonOE,
    setJsonOS,
    setMsj,
    setPacientes,
  ]);
};

export default FetchUseEffect;
