import React, { Fragment, useState } from "react";

let PostFunction = async (token, postURL, nuevaCita) => {
  const jsonP = await fetch(postURL, {
    method: "POST",
    body: JSON.stringify(nuevaCita),
    headers: {
      "Content-type": "application/json; charset=UTF-8",
      Authorization: token,
    },
  });
  const info = await jsonP.json();
  //console.log(info);
  return info;
};
export default PostFunction;
