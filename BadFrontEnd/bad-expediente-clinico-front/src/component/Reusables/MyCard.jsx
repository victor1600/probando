import React, { Fragment } from "react";

const MyCard = ({ myTitle, children, linkBack }) => {
  return (
    <Fragment>
      <div className="m-5">
        <div className="card">
          <div className="card-header" style={estilosInicio.fondoHeader}>
            <h2>{myTitle}</h2>
          </div>
          <div className="card-body" style={estilosInicio.fondoBody}>
            {children}
            <a
              href={linkBack}
              style={{
                marginTop: 35,
                marginLeft: 30,
              }}
              className="btn btn-info"
            >
              Regresar
            </a>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

const estilosInicio = {
  fondoHeader: {
    backgroundColor: "#80cbc4",
    textAlign: "center",
  },
  fondoBody: {
    backgroundColor: "#F5F5F6",
  },
  letraCarrousel: {
    color: "black",
  },
};
export default MyCard;
