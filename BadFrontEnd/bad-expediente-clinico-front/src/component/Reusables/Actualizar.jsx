import React, { Fragment, useState } from "react";
import { useParams } from "react-router-dom";
import { useForm } from "react-hook-form";

const Actualizar = (props) => {
  const [loginE] = useState(localStorage.getItem("login") ? true : false);
  const [token] = useState(
    localStorage.getItem("token") ? localStorage.getItem("token") : null
  );
  const { id } = useParams();
  const { register, handleSubmit, errors } = useForm();

  const [jsonOE, setJsonOE] = React.useState(false);
  const [jsonOS, setJsonOS] = React.useState(false);

  //Errores Actualizar
  const [jsonPE, setJsonPE] = React.useState(false);
  const [jsonPS, setJsonPS] = React.useState(false);

  const [msj, setMsj] = React.useState([""]);
  const [titulo, setTitulo] = React.useState([""]);

  const [element, setelement] = React.useState([""]);

  const getValuesFromSelect = () => {
    const selectedIds = {};
    let dropdown = null;
    let dropdownValue = null;
    const len = props.fks.length;
    for (let i = 0; i < len; i++) {
      dropdown = document.getElementById(props.fks[i].idSelect);
      dropdownValue = dropdown.options[dropdown.selectedIndex].value;
      selectedIds[props.fks[i].fakeAttributeName] = dropdownValue;
    }
    return selectedIds;
  };

  // const handleChange = (event) => {
  //   setelement(event.target.value);
  // };

  React.useEffect(() => {
    const getElement = async () => {
      const data = await fetch(`${props.endpoint}/${id}`, {
        method: "GET",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Authorization: token,
        },
      });
      const dataJSON = await data.json();

      switch (data.status) {
        case 403:
          props.history.push("/error403");
          break;
        case 302:
          setJsonOS(true);
          setJsonOE(false);
          setelement(dataJSON);
          break;
        case 404:
          setJsonOS(false);
          setJsonOE(true);
          setMsj([String(dataJSON.mensaje)]);
          break;
        case 200:
          setJsonOS(true);
          setJsonOE(false);
          setelement(dataJSON);
          break;

        default:
          props.history.push("/error");
      }
      // if (data.status === 200) {
      // setJsonOS(true);
      // setJsonOE(false);
      // setelement(dataJSON);
      // } else {
      //   setJsonOS(false);
      //   setJsonOE(true);
      //   setMsj([
      //     // TODO: check if dataJSON or data
      //     String(data.mensaje),
      //   ]);
      // }
    };
    getElement();
  }, [id, props, token, loginE]);

  const onSubmit = async (data, e) => {
    e.preventDefault();

    const btnModal = document.getElementById("modal");

    // Putting the elements of the form into a single object.
    const newElement = {};
    for (let i = 0; i < props.postNames.length; i++) {
      newElement[props.postNames[i]] = data[props.fields[i].name];
    }
    let dropdownIds;
    if (props.fks !== undefined) {
      dropdownIds = getValuesFromSelect();
    } else {
      dropdownIds = {};
    }

    const newElementFinished = {
      ...newElement,
      ...dropdownIds,
      estaActivo: true,
    };
    console.log(newElementFinished);

    const jsonP = await fetch(`${props.endpoint}/actualizar/${id}`, {
      method: "PUT",
      body: JSON.stringify(newElementFinished),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
        Authorization: token,
      },
    });
    // console.log(jsonP);

    const info = await jsonP.json();

    if (jsonP.status === 200) {
      setJsonPE(false);
      setJsonPS(true);
      setMsj([String(info.mensaje)]);
      setTitulo([String(info.httpCodeName)]);
      btnModal.click();
      e.target.reset();
    } else {
      setJsonPE(true);
      setJsonPS(false);
      setMsj([String(info.mensaje)]);
      setTitulo([String(info.httpCodeName)]);
      btnModal.click();
    }
  };

  return (
    <Fragment>
      {jsonOS && (
        <div className="m-5">
          <div className="card">
            <div className="card-header">
              <h1>Actualizar {props.title}</h1>
            </div>
            <div className="card-body">
              <form
                className="form-horizontal"
                onSubmit={handleSubmit(onSubmit)}
              >
                {/*Testing area*/}
                {props.fields.map(function (field, i) {
                  return (
                    <div className="form-group" key={i}>
                      <label className="control-label col-sm-2">
                        {field.labelTitle}:
                      </label>
                      <div className="col-sm-10">
                        <input
                          type="text"
                          className="form-control"
                          defaultValue={element[props.postNames[i]]}
                          // onChange={handleChange}
                          placeholder={`${field.placeholder}`}
                          name={`${field.name}`}
                          maxLength={`${field.maxLength}`}
                          pattern={`${field.pattern}`}
                          ref={register({
                            required: {
                              value: field.ref.required.value,
                              message: `${field.ref.required.message}`,
                            },
                            maxLength: {
                              value: field.ref.maxLength.value,
                              message: `${field.ref.maxLength.message}`,
                            },
                            pattern: {
                              value: field.ref.pattern.value,
                              message: `${field.ref.pattern.message}`,
                            },
                          })}
                        />
                        <span className="text-danger text-small d-block mb-2">
                          {errors[field.name]?.message}
                        </span>
                      </div>
                    </div>
                  );
                })}
                {/*Print several selects bases on foreign keys present in fks array */}
                {props.fks &&
                  element &&
                  props.fks.map((fk, i) => {
                    return (
                      <div
                        key={id}
                        className="form-row align-items-center"
                        style={{ marginLeft: 8 }}
                      >
                        <div className="col-auto my-1">
                          <label
                            className="mr-sm-2"
                            htmlFor="inlineFormCustomSelect"
                          >
                            {fk.fkTitle}
                          </label>
                          <select
                            className="custom-select mr-sm-2"
                            id={fk.idSelect}
                          >
                            {element[fk.nameInChild] !== undefined ? (
                              <option
                                value={
                                  element[fk.nameInChild][fk.fkAttributes.idFk]
                                }
                              >
                                {
                                  element[fk.nameInChild][
                                    fk.fkAttributes.nameFk
                                  ]
                                }
                              </option>
                            ) : null}
                            {element[fk.nameInChild] !== undefined
                              ? fk.content.map((data, x) => {
                                  const presentIterationKey =
                                    data[fk.fkAttributes.idFk];
                                  const elementKey =
                                    element[fk.nameInChild][
                                      fk.fkAttributes.idFk
                                    ];
                                  if (presentIterationKey === elementKey) {
                                    console.log("im the selected one!");
                                    // return <option key={x}
                                    //                value={`${data[fk.fkAttributes.idFk]}`}>{data[fk.fkAttributes.nameFk]}</option>
                                    return null;
                                  } else {
                                    console.log("no!");
                                    return (
                                      <option
                                        key={x}
                                        value={`${data[fk.fkAttributes.idFk]}`}
                                      >
                                        {data[fk.fkAttributes.nameFk]}
                                      </option>
                                    );
                                  }
                                })
                              : null}
                          </select>
                        </div>
                      </div>
                    );
                  })}
                {/*Testing area*/}
                <br />
                <br />
                <br />

                <div className="form-group">
                  <div className="col-sm-offset-2 col-sm-10">
                    <button type="submit" className="btn btn-success">
                      Guardar
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      )}
      {jsonOE && (
        <div className="m-5">
          <h1>{msj}</h1>
        </div>
      )}
      <div className="m-5">
        <a href={`/${props.url}`} className="btn btn-info">
          Regresar
        </a>
      </div>

      <button
        id="modal"
        type="button"
        className="d-none"
        data-toggle="modal"
        data-target="#exampleModalCenter"
      >
        Launch demo modal
      </button>

      <div
        className="modal fade"
        id="exampleModalCenter"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalCenterTitle">
                {titulo}
              </h5>
            </div>
            <div className="modal-body">{msj}</div>
            <div className="modal-footer">
              {jsonPE && (
                <button
                  type="button"
                  className="btn btn-warning"
                  data-dismiss="modal"
                >
                  Cancelar
                </button>
              )}
              {jsonPS && (
                <div>
                  <a
                    type="button"
                    className="btn btn-primary"
                    href={`/${props.url}`}
                  >
                    Aceptar
                  </a>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};
// const estilosInicio = {
//   fondoHeader: {
//     backgroundColor: "#80cbc4",
//     textAlign: "center",
//   },
//   fondoBody: {
//     backgroundColor: "#F5F5F6",
//   },
//   letraCarrousel: {
//     color: "black",
//   },
// };

export default Actualizar;
