import React from "react";

const MyModal = ({ titulo, msj, jsonOE, jsonOS, urlReturn }) => {
  return (
    <div>
      <button
        id="modal"
        type="button"
        className="d-none"
        data-toggle="modal"
        data-target="#exampleModalCenter"
      >
        Launch demo modal
      </button>

      <div
        className="modal fade"
        id="exampleModalCenter"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalCenterTitle">
                {titulo}
              </h5>
            </div>
            <div className="modal-body">{msj}</div>
            <div className="modal-footer">
              {jsonOE && (
                <button
                  type="button"
                  className="btn btn-warning"
                  data-dismiss="modal"
                >
                  Cancelar
                </button>
              )}
              {jsonOS && (
                <div>
                  <button
                    type="button"
                    className="btn btn-secondary mr-3"
                    data-dismiss="modal"
                  >
                    Agregar otra
                  </button>
                  <a
                    type="button"
                    className="btn btn-primary"
                    href={`/${urlReturn}`}
                  >
                    Regresar
                  </a>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MyModal;
