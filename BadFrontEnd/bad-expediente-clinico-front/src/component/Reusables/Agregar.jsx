import React, { Fragment, useState } from "react";
import { useForm } from "react-hook-form";
import { withRouter } from "react-router-dom";

const Agregar = (props) => {
  const [loginE] = useState(localStorage.getItem("login") ? true : false);
  const [token] = useState(
    localStorage.getItem("token") ? localStorage.getItem("token") : null
  );
  const [usuario] = useState(
    localStorage.getItem("usuario") ? localStorage.getItem("usuario") : null
  );

  const [jsonOE, setJsonOE] = React.useState(false);
  const [jsonOS, setJsonOS] = React.useState(false);
  const [msj, setMsj] = React.useState([""]);
  const [titulo, setTitulo] = React.useState([""]);

  React.useEffect(() => {
    if (loginE === true) {
      const obtenerPermiso = async () => {
        const data = await fetch(
          `http://localhost:8080/usuario/${usuario}/CieAdd`,
          {
            method: "GET",
            headers: {
              "Content-type": "application/json; charset=UTF-8",
              Authorization: token,
            },
          }
        );
        const info = await data.json();
        console.log(info);

        switch (data.status) {
          case 200:
            if (info.mensaje === "1") {
            } else {
              props.history.push("/error403");
            }
            break;
          default:
            props.history.push("/error");
        }
      };

      obtenerPermiso();
    } else {
      props.history.push("/login");
    }
  }, [props, loginE, token, usuario]);

  const { register, handleSubmit, errors } = useForm();

  const getValuesFromSelect = () => {
    const selectedIds = {};
    let dropdown = null;
    let dropdownValue = null;
    const len = props.fks.length;
    for (let i = 0; i < len; i++) {
      dropdown = document.getElementById(props.fks[i].idSelect);
      dropdownValue = dropdown.options[dropdown.selectedIndex].value;
      selectedIds[props.fks[i].fakeAttributeName] = dropdownValue;
    }
    return selectedIds;
  };

  const onSubmit = async (data, e) => {
    e.preventDefault();

    const btnModal = document.getElementById("modal");

    // Putting the elements of the form into a single object.
    const newElement = {};
    for (let i = 0; i < props.postNames.length; i++) {
      newElement[props.postNames[i]] = data[props.fields[i].name];
    }
    let dropdownIds;
    if (props.fks !== undefined) {
      dropdownIds = getValuesFromSelect();
    } else {
      dropdownIds = {};
    }

    const newElementFinished = {
      ...newElement,
      ...dropdownIds,
      // TODO: TEST THIS
      estaActivo: true,
    };
    console.log(newElementFinished);

    const jsonP = await fetch(`${props.endpoint}/agregar`, {
      method: "POST",
      body: JSON.stringify(newElementFinished),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
        Authorization: token,
      },
    });
    // console.log(jsonP);

    const info = await jsonP.json();

    if (jsonP.status === 200) {
      setJsonOE(false);
      setJsonOS(true);
      setMsj([String(info.mensaje)]);
      setTitulo([String(info.httpCodeName)]);
      btnModal.click();
      e.target.reset();
    } else {
      setJsonOE(true);
      setJsonOS(false);
      setMsj([String(info.mensaje)]);
      setTitulo([String("error")]);
      btnModal.click();
    }
  };

  // TODO: manage required or not required.
  return (
    <Fragment>
      <div className="m-5">
        <div className="card">
          <div className="card-header">
            <h1>Formulario para agregar {`${props.title}`}</h1>
          </div>
          <div className="card-body">
            <form className="form-horizontal" onSubmit={handleSubmit(onSubmit)}>
              {/*Testing area*/}
              {props.fields.map(function (field, i) {
                return (
                  <div className="form-group" key={i}>
                    <label className="control-label col-sm-2">
                      {field.labelTitle}:
                    </label>
                    <div className="col-sm-10">
                      <input
                        type={`${field.type}`}
                        className="form-control"
                        placeholder={`${field.placeholder}`}
                        name={`${field.name}`}
                        maxLength={`${field.maxLength}`}
                        pattern={`${field.pattern}`}
                        ref={register({
                          required: {
                            value: field.ref.required.value,
                            message: `${field.ref.required.message}`,
                          },
                          maxLength: {
                            value: field.ref.maxLength.value,
                            message: `${field.ref.maxLength.message}`,
                          },
                          pattern: {
                            value: field.ref.pattern.value,
                            message: `${field.ref.pattern.message}`,
                          },
                        })}
                      />
                      <span className="text-danger text-small d-block mb-2">
                        {errors[field.name]?.message}
                      </span>
                    </div>
                  </div>
                );
              })}
              {/*Print several selects bases on foreign keys present in fks array */}
              {props.fks &&
                props.fks.map((fk, i) => {
                  return (
                    <div
                      key={i}
                      className="form-row align-items-center"
                      style={{ marginLeft: 8 }}
                    >
                      <div className="col-auto my-1">
                        <label
                          className="mr-sm-2"
                          htmlFor="inlineFormCustomSelect"
                        >
                          {fk.fkTitle}
                        </label>
                        <select
                          className="custom-select mr-sm-2"
                          id={fk.idSelect}
                        >
                          <option defaultValue>Escoja {fk.fkTitle}</option>
                          {fk.content.map((data, x) => {
                            // console.log(data[fk.fkAttributes.idFk])
                            return (
                              <option
                                key={x}
                                value={`${data[fk.fkAttributes.idFk]}`}
                              >
                                {data[fk.fkAttributes.nameFk]}
                              </option>
                            );
                          })}
                        </select>
                      </div>
                    </div>
                  );
                })}

              {/*Testing area*/}
              <br />
              <br />
              <br />

              <div className="form-group">
                <div className="col-sm-offset-2 col-sm-10">
                  <button type="submit" className="btn btn-success">
                    Guardar
                  </button>
                </div>
              </div>
            </form>
          </div>

          <div className="m-5">
            <a href={`/${props.url}`} className="btn btn-info">
              Regresar
            </a>
          </div>

          <button
            id="modal"
            type="button"
            className="d-none"
            data-toggle="modal"
            data-target="#exampleModalCenter"
          >
            Launch demo modal
          </button>

          <div
            className="modal fade"
            id="exampleModalCenter"
            tabIndex="-1"
            role="dialog"
            aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true"
          >
            <div className="modal-dialog modal-dialog-centered" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title" id="exampleModalCenterTitle">
                    {titulo}
                  </h5>
                </div>
                <div className="modal-body">{msj}</div>
                <div className="modal-footer">
                  {jsonOE && (
                    <button
                      type="button"
                      className="btn btn-warning"
                      data-dismiss="modal"
                    >
                      Cancelar
                    </button>
                  )}
                  {jsonOS && (
                    <div>
                      <button
                        type="button"
                        className="btn btn-secondary mr-3"
                        data-dismiss="modal"
                      >
                        Agregar otra
                      </button>
                      <a
                        type="button"
                        className="btn btn-primary"
                        href={`/${props.url}`}
                      >
                        Regresar
                      </a>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};
// const estilosInicio = {
//   fondoHeader: {
//     backgroundColor: "#80cbc4",
//     textAlign: "center",
//   },
//   fondoBody: {
//     backgroundColor: "#F5F5F6",
//   },
//   letraCarrousel: {
//     color: "black",
//   },
// };

export default withRouter(Agregar);
