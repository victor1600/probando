import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
}));

const TimeHelper = ({ titleName, initialState, setMyTime, labelName }) => {
  const classes = useStyles();
  return (
    <div
      style={{
        marginTop: 10,
        marginBottom: 30,
      }}
    >
      <h6 style={{ marginLeft: 8, marginBottom: 15 }}>{titleName}:</h6>
      {/* <form className={classes.container} noValidate> */}
      <TextField
        id="time"
        label={labelName}
        type="time"
        onChange={(e) => setMyTime(e.target.value)}
        defaultValue={initialState}
        className={classes.textField}
        InputLabelProps={{
          shrink: true,
        }}
        inputProps={{
          step: 300, // 5 min
        }}
      />
      {/* </form> */}
    </div>
  );
};

export default TimeHelper;
