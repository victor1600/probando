let PostFunction = async (
  data,
  token,
  setJsonOE,
  setJsonOS,
  setMsj,
  setTitulo,
  // btnModal,
  e,
  postURL,
  nuevaCita,
  props
) => {
  //   console.log(nuevaCita);
  const jsonP = await fetch(postURL, {
    method: "POST",
    body: JSON.stringify(nuevaCita),
    headers: {
      "Content-type": "application/json; charset=UTF-8",
      Authorization: token,
    },
  });

  const info = await jsonP.json();
  console.log(info);
  if (jsonP.status === 200) {
    setJsonOE(false);
    setJsonOS(true);
    setMsj([String(info.mensaje)]);
    setTitulo([String(info.httpCodeName)]);
    // btnModal.click();
    // props.history.push("/citas");
    e.target.reset();
  } else {
    setJsonOE(true);
    setJsonOS(false);
    setMsj([String(info.mensaje)]);
    setTitulo([String(info.httpCodeName)]);
    // btnModal.click();
  }
};
export default PostFunction;
