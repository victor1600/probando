import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
}));

export default function DateHelper({
  titleName,
  initialState,
  setMyDate,
  labelName,
}) {
  const classes = useStyles();

  return (
    <div
      style={{
        marginTop: 20,
        marginBottom: 30,
      }}
    >
      <h6 style={{ marginLeft: 8, marginBottom: 15 }}>{titleName}:</h6>
      {/* <form className={classes.container} noValidate> */}
      <TextField
        onChange={(e) => setMyDate(e.target.value)}
        id="date"
        label={labelName}
        type="date"
        defaultValue={initialState}
        className={classes.textField}
        InputLabelProps={{
          shrink: true,
        }}
      />
      {/* </form> */}
    </div>
  );
}
