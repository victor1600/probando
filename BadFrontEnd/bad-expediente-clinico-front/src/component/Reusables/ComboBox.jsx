/* eslint-disable no-use-before-define */
import React from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
// NOTA: ESPERA QUE EL OBJETO QUE SE LE ENVIE SE LLAME ID Y TITLE
export default function ComboBox({
  setCombo,
  comboOptions,
  defaultValue,
  comboConfig,
  labelName,
  isDisabled,
}) {
  // const [selection, setSelection] = React.useState({});
  if (isDisabled === undefined) {
    isDisabled = false;
  }

  return (
    <div
      style={{
        marginBottom: 25,
      }}
    >
      {"title2" in comboConfig ? (
        defaultValue === undefined ? (
          <Autocomplete
            disabled={isDisabled}
            onChange={(event, value) => setCombo(value)}
            style={{ marginBottom: 10 }}
            id="combo-box-demo"
            options={comboOptions}
            getOptionLabel={(option) =>
              option[comboConfig.title] + " " + option[comboConfig.title2]
            }
            // style={{ width: 300 }}
            // getOptionSelected={(option, value) => option.iso === value.iso}
            getOptionSelected={(option, value) =>
              option[comboConfig.title] === value[comboConfig.title]
            }
            renderInput={(params) => (
              <TextField {...params} label={labelName} variant="outlined" />
            )}
          />
        ) : (
          <Autocomplete
            onChange={(event, value) => setCombo(value)}
            id="combo-box-demo"
            style={{ margin: 10 }}
            options={comboOptions}
            getOptionLabel={(option) =>
              option[comboConfig.title] + " " + option[comboConfig.title2]
            }
            defaultValue={defaultValue}
            // style={{ width: 300 }}
            // getOptionSelected={(option, value) => option.iso === value.iso}
            getOptionSelected={(option, value) =>
              option[comboConfig.title] === value[comboConfig.title]
            }
            renderInput={(params) => (
              <TextField {...params} label={labelName} variant="outlined" />
            )}
          />
        )
      ) : defaultValue === undefined ? (
        <Autocomplete
          disabled={isDisabled}
          onChange={(event, value) => setCombo(value)}
          style={{ marginBottom: 10 }}
          id="combo-box-demo"
          options={comboOptions}
          getOptionLabel={(option) => option[comboConfig.title]}
          // style={{ width: 300 }}
          // getOptionSelected={(option, value) => option.iso === value.iso}
          getOptionSelected={(option, value) =>
            option[comboConfig.title] === value[comboConfig.title]
          }
          renderInput={(params) => (
            <TextField {...params} label={labelName} variant="outlined" />
          )}
        />
      ) : (
        <Autocomplete
          onChange={(event, value) => setCombo(value)}
          id="combo-box-demo"
          style={{ margin: 10 }}
          options={comboOptions}
          getOptionLabel={(option) => option[comboConfig.title]}
          defaultValue={defaultValue}
          // style={{ width: 300 }}
          // getOptionSelected={(option, value) => option.iso === value.iso}
          getOptionSelected={(option, value) =>
            option[comboConfig.title] === value[comboConfig.title]
          }
          renderInput={(params) => (
            <TextField {...params} label={labelName} variant="outlined" />
          )}
        />
      )}
    </div>
  );
}

// Top 100 films as rated by IMDb users. http://www.imdb.com/chart/top
