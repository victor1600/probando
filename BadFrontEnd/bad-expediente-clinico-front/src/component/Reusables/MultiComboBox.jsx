import React from "react";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";

const MultiComboBox = ({
  setMultiCombo,
  top100Films,
  defaultValue,
  multiComboConfig,
  labelName,
}) => {
  //   const classes = useStyles();
  return (
    <div>
      {defaultValue === undefined ? (
        <Autocomplete
          multiple
          id="tags-outlined"
          options={top100Films}
          getOptionLabel={(option) => option[multiComboConfig.title]}
          filterSelectedOptions
          getOptionSelected={(option, value) =>
            option[multiComboConfig.title] === value[multiComboConfig.title]
          }
          onChange={(event, value) => setMultiCombo(value)}
          renderInput={(params) => (
            <TextField
              {...params}
              variant="outlined"
              label={labelName}
              placeholder="Favorites"
            />
          )}
        />
      ) : (
        <Autocomplete
          multiple
          id="tags-outlined"
          options={top100Films}
          getOptionLabel={(option) => option[multiComboConfig.title]}
          defaultValue={defaultValue}
          filterSelectedOptions
          getOptionSelected={(option, value) =>
            option[multiComboConfig.title] === value[multiComboConfig.title]
          }
          onChange={(event, value) => setMultiCombo(value)}
          renderInput={(params) => (
            <TextField
              {...params}
              variant="outlined"
              label={labelName}
              placeholder="Favorites"
            />
          )}
        />
      )}
    </div>
  );
};

export default MultiComboBox;
