import React, { Fragment, useState } from "react";
import { withRouter } from "react-router-dom";
//ruteo
import { useParams } from "react-router-dom";

const Detalle = (props) => {
  const estilosInicio = {
    fondoHeader: {
      backgroundColor: "#80cbc4",
      textAlign: "center",
    },
    fondoBody: {
      backgroundColor: "#F5F5F6",
    },
    letraCarrousel: {
      color: "black",
    },
  };

  const [loginE] = useState(localStorage.getItem("login") ? true : false);
  const [token] = useState(
    localStorage.getItem("token") ? localStorage.getItem("token") : null
  );

  const { id } = useParams();

  const [jsonOE, setJsonOE] = React.useState(false);
  const [jsonOS, setJsonOS] = React.useState(false);
  const [msj, setMsj] = React.useState([""]);

  const [element, setelement] = React.useState([]);

  React.useEffect(() => {
    if (loginE === true) {
      const obtenerCie10 = async () => {
        const data = await fetch(`${props.endpoint}/${id}`, {
          method: "GET",
          headers: {
            "Content-type": "application/json; charset=UTF-8",
            Authorization: token,
          },
        });
        const dataJSON = await data.json();
        switch (data.status) {
          case 403:
            props.history.push("/error403");
            break;
          case 302:
            setJsonOS(true);
            setJsonOE(false);
            // setCie10(dataJSON);
            break;
          case 200:
            setJsonOS(true);
            setJsonOE(false);
            setelement(dataJSON);
            break;
          case 404:
            setJsonOS(false);
            setJsonOE(true);
            setMsj([String(dataJSON.mensaje)]);
            break;
          default:
            props.history.push("/error");
        }
      };

      obtenerCie10();
    } else {
      props.history.push("/login");
    }
  }, [id, props, token, loginE]);

  //   React.useEffect(() => {
  //     const getElement = async () => {
  //       const data = await fetch(`${props.endpoint}/${id}`);
  //       const dataJSON = await data.json();
  //       if (data.status === 200) {
  //   setJsonOS(true);
  //   setJsonOE(false);
  //   setelement(dataJSON);
  //       } else {
  //         setJsonOS(false);
  //         setJsonOE(true);
  //         setMsj([
  //           // TODO: check if dataJSON or data
  //           String(data.mensaje),
  //         ]);
  //       }
  //     };
  //     getElement();
  //   }, [id, props.endpoint]);

  return (
    <Fragment>
      <div className="m-5">
        <div className="card">
          <div className="card-header" style={estilosInicio.fondoHeader}>
            <h2>
              Los datos de {props.title} {element[props.attributes[0]]} son:
            </h2>
          </div>
          <div className="card-body" style={estilosInicio.fondoBody}>
            {jsonOS && (
              <div>
                <br />
                <br />
                <ul>
                  {props.attributes.map((item, index) => {
                    return (
                      <li key={index}>
                        {props.labels[index]}:{" "}
                        {element[props.attributes[index]]}
                      </li>
                    );
                  })}

                  {props.parents
                    ? props.parents.map((parent, i) => {
                        return (
                          <li key={i}>
                            {props.parentLabels[i]} :{" "}
                            {element[parent]?.[
                              props.parentsMainAttribute[i]
                            ] !== undefined
                              ? element[parent][props.parentsMainAttribute[i]]
                              : null}
                          </li>
                        ); //TODO CHECK
                      })
                    : null}
                </ul>
              </div>
            )}
            {jsonOE && (
              <div className="m-5">
                <h1>{msj}</h1>
              </div>
            )}
            <a href={`/${props.url}`} className="btn btn-info">
              Regresar
            </a>
          </div>
        </div>
      </div>
    </Fragment>
  );
};
// const estilosInicio = {
//   fondoHeader: {
//     backgroundColor: "#80cbc4",
//     textAlign: "center",
//   },
//   fondoBody: {
//     backgroundColor: "#F5F5F6",
//   },
//   letraCarrousel: {
//     color: "black",
//   },
// };

export default withRouter(Detalle);
