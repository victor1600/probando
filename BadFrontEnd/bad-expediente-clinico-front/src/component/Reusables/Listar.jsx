import React, { useState } from "react";

import { Link, withRouter } from "react-router-dom";

const Listar = (props) => {
  const [loginE] = useState(localStorage.getItem("login") ? true : false);
  const [token] = useState(
    localStorage.getItem("token") ? localStorage.getItem("token") : null
  );
  /*
   hooks to display messagges and control rendering
     */
  const [jsonOE, setJsonOE] = React.useState(false);
  const [jsonOS, setJsonOS] = React.useState(false);
  const [msj, setMsj] = React.useState([""]);
  const [titulo, setTitulo] = React.useState([""]);
  const [jsonEE, setJsonEE] = React.useState(false);
  const [jsonES, setJsonES] = React.useState(false);

  // List of elements fetched from the API
  const [elementsList, setElementsList] = useState([]);

  // const [cie10, setCie10] = React.useState([]);
  React.useEffect(() => {
    if (loginE === true) {
      const obtenerCie10 = async () => {
        const data = await fetch(`${props.endpoint}/todas`, {
          method: "GET",
          headers: {
            "Content-type": "application/json; charset=UTF-8",
            Authorization: token,
          },
        });
        console.log(data);
        const dataJSON = await data.json();

        // console.log(data.status);
        // console.log(dataJSON);

        switch (data.status) {
          case 403:
            props.history.push("/error403");
            break;
          case 302:
            // setCie10(dataJSON);
            setJsonOS(true);
            setJsonOE(false);
            break;
          case 404:
            setJsonOS(false);
            setJsonOE(true);
            setMsj([String(dataJSON.mensaje)]);
            break;
          case 200:
            setJsonOS(true);
            setJsonOE(false);
            setElementsList(dataJSON);
            break;
          default:
            props.history.push("/error");
        }
      };

      obtenerCie10();
    } else {
      props.history.push("/login");
    }
  }, [props, loginE, token, props.endpoint]);

  // React.useEffect(() => {
  //   const fetchContent = async () => {
  //     const data = await fetch(`${props.endpoint}`);
  //     const dataJSON = await data.json();
  //     console.log(dataJSON);
  //     if (data.status === 200) {
  //       setJsonOS(true);
  //       setJsonOE(false);
  //       setElementsList(dataJSON);
  //     } else {
  //       setJsonOS(false);
  //       setJsonOE(true);
  //       setMsj([
  //         String(dataJSON.mensaje), //TODO: look for this
  //       ]);
  //     }
  //   };
  //   fetchContent();
  // }, [props.endpoint]);

  const deleteRow = async (e) => {
    console.log("hey", e.target.value);
    const btnCerrar = document.getElementById(`btn-${e.target.value}`);
    const btnInfo = document.getElementById("btn-infoDelete");
    const dataE = await fetch(`${props.endpoint}/eliminar/${e.target.value}`, {
      method: "DELETE",
      headers: {
        "Content-type": "application/json; charset=UTF-8",
        Authorization: token,
      },
    });
    const infoEP = await dataE.json();

    console.log(infoEP);
    // TODO: GET SURE U SET CORRECTLY DETALLE IN BACKEND

    if (dataE.status === 200) {
      setJsonES(true);
      setJsonEE(false);
      setTitulo([String(infoEP.httpCodeName)]);
      setMsj([String(infoEP.mensaje)]);
    } else {
      setJsonES(false);
      setJsonEE(true);
      setTitulo([String(infoEP.httpCodeName)]);
      setMsj([String(infoEP.mensaje)]);
    }

    btnCerrar.click();
    btnInfo.click();
  };

  return (
    <div className="m-5">
      <div className="card">
        <div className="card-header" style={estilosInicio.fondoHeader}>
          <h1>Listado de {props.title}</h1>
        </div>
        <div className="card-body">
          {jsonOS && (
            <div className="table-responsive">
              <table className="table table-hover">
                <thead>
                  <tr>
                    {props.tableHeaders.map((header, i) => (
                      <th key={i}>{header}</th>
                    ))}
                    {props.tableParentHeaders.map((header, i) => (
                      <th key={i}>{header}</th>
                    ))}
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {elementsList.map((item, i) => (
                    <tr key={i}>
                      <td>
                        <Link to={`/${props.url}/${item[props.linkID]}`}>
                          {item[props.firstField]}
                        </Link>
                      </td>
                      {props.toDisplayAttributes
                        ? props.toDisplayAttributes.map(
                            (toDisplayAttribute, index) => (
                              <td key={index}>{item[toDisplayAttribute]}</td>
                            )
                          )
                        : null}
                      {props.parents
                        ? props.parents.map((parent, i) => (
                            <td key={i}>
                              {item[parent][props.parentsMainAttribute[i]]}
                            </td>
                          ))
                        : null}
                      <td>
                        <a
                          href={`/${props.url}/actualizar/${
                            item[props.linkID]
                          }`}
                          className="btn btn-warning"
                        >
                          Editar
                        </a>
                      </td>
                      <td>
                        <button
                          type="button"
                          className="btn btn-danger"
                          data-toggle="modal"
                          data-target={`#modal-${item[props.id]}`}
                        >
                          Eliminar
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          )}
          {jsonOE && (
            <div className="m-5">
              <h1>{msj}</h1>
            </div>
          )}
          <a href={`/${props.url}/agregar`} className="btn btn-primary">
            Agregar
          </a>
        </div>
        {elementsList.map((item, i) => (
          <div
            key={i}
            className="modal fade"
            id={`modal-${item[props.id]}`}
            tabIndex="-1"
            role="dialog"
            aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true"
          >
            <div className="modal-dialog modal-dialog-centered" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title" id="exampleModalLongTitle">
                    Advertencia Eliminacion
                  </h5>
                </div>
                <div className="modal-body">
                  ¿Usted desea eliminar {props.title}: {item[props.firstField]}?
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    id={`btn-${item[props.id]}`}
                    className="btn btn-secondary"
                    data-dismiss="modal"
                  >
                    Cancelar
                  </button>
                  <button
                    type="button"
                    className="btn btn-success"
                    value={item[props.id]}
                    onClick={deleteRow}
                  >
                    Si, deseo eliminar
                  </button>
                </div>
              </div>
            </div>
          </div>
        ))}
        <button
          id="btn-infoDelete"
          type="button"
          className="d-none"
          data-dismiss="modal"
          data-toggle="modal"
          data-target="#infoDelete"
        >
          info del modal
        </button>
        <div
          className="modal fade"
          id="infoDelete"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalCenterTitle"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLongTitle">
                  {titulo}
                </h5>
              </div>
              <div className="modal-body">{msj}</div>
              <div className="modal-footer">
                {jsonES && (
                  <a
                    type="button"
                    href={`/${props.url}`}
                    className="btn btn-primary"
                  >
                    Enterado
                  </a>
                )}
                {jsonEE && (
                  <button
                    type="button"
                    className="btn btn-warning"
                    data-dismiss="modal"
                  >
                    Enterado
                  </button>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const estilosInicio = {
  fondoHeader: {
    backgroundColor: "#80cbc4",
    textAlign: "center",
  },
  fondoBody: {
    backgroundColor: "#F5F5F6",
  },
  letraCarrousel: {
    color: "black",
  },
};

export default withRouter(Listar);
