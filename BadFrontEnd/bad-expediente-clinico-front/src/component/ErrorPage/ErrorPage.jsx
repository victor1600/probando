import React, { Fragment} from 'react'
import {withRouter} from "react-router-dom";

import './ErrorPage.css';

const ErrorPage = () => {
    const regresar = (e) => {
        window.history.back();
    }

    return (
        <Fragment>
            <div className="error-content">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 ">
                            <div className="error-text">
                                <h1 className="error">Error</h1>
                                <div className="errorImg">
   
                                </div>
                                <h4 className="errorPH4">¡Oops! ¡Ah ocurrido un error!</h4>
                                <p className="errorPP">Lo siento, no se encuentra la pagina o ha habido un error interno.</p>
                                <button className="btn btn-primary btn-round" onClick={regresar}  >Regresar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default withRouter(ErrorPage)
