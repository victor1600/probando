import React, {Fragment, useState} from 'react'
import {useForm} from 'react-hook-form'
import {useParams, withRouter} from "react-router-dom";

const ActualizarCie10 = (props) => {
    const {idCIE10} = useParams();
    const [cie10, setCie10] = React.useState([]);
    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)

    //Errorres de obtener tipoExamens
    const [jsonOE, setJsonOE] = React.useState(false);
    const [jsonOS, setJsonOS] = React.useState(false);

    //Errores Actualizar
    const [jsonPE, setJsonPE] = React.useState(false);
    const [jsonPS, setJsonPS] = React.useState(false);

    const [msj, setMsj] = React.useState(['']);

    React.useEffect(() => {

        if(loginE === true){
            const obtenerCie10 = async () => {
                const data = await fetch(`http://localhost:8080/cie10/${idCIE10}`, {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });
                const dataCie10 = await data.json();

                switch(data.status) {
                    case 403:
                        props.history.push('/error403');
                      break;
                    case 302:
                        setJsonOS(true);
                        setJsonOE(false);
                        setCie10(dataCie10);
                      break;
                    case 404:
                        setJsonOS(false);
                        setJsonOE(true);
                        setMsj([
                        String(dataCie10.mensaje)
                        ]);
                        break;
                    default:
                        props.history.push('/error');
                  }
    
            }
    
            obtenerCie10();
        }else{
            props.history.push('/login');
        }

    }, [idCIE10, props, loginE,token])


    const [titulo, setTitulo] = React.useState(['']);

    const {register, handleSubmit, errors} = useForm();
    const onSubmit = async (data, e) => {
        e.preventDefault();
        const btnModal = document.getElementById("modal");
        //console.log(data)
        const jsonP = await fetch(`http://localhost:8080/cie10/actualizar/${idCIE10}`, {
            method: 'PUT',
            body: JSON.stringify({
                codigoCIE10: data.codigo,
                descripcionCIE10: data.descripcion,
                sexoCIE10: data.sexo,
                limiteInfCIE10: data.inferior,
                limiteSupCIE10: data.superior
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                "Authorization": token
            }
        });
        console.log(jsonP);
        const info = await jsonP.json();

        if (jsonP.status === 202) {
            setJsonPE(false);
            setJsonPS(true);
            setMsj([
                String(info.mensaje)
            ]);
            setTitulo([
                String(info.httpCodeName)
            ]);
            btnModal.click();
            e.target.reset();

        } else {
            setJsonPE(true);
            setJsonPS(false);
            setMsj([
                String(info.mensaje)
            ]);
            setTitulo([
                String(info.httpCodeName)
            ]);
            btnModal.click();
        }


    }

    return (
        <Fragment>
            {jsonOS &&
            <div className="m-5">
                <div className="card">
                    <div className="card-header">
                        <h1>Actualizar Cie10</h1>
                    </div>
                    <div className="card-body">
                        <form className="form-horizontal" onSubmit={handleSubmit(onSubmit)}>
                            <div className="form-group">
                                <label className="control-label col-sm-2">Codigo:</label>
                                <div className="col-sm-10">
                                    <input type="text" className="form-control" placeholder="Ingrese cie10"
                                           name="codigo"
                                           required maxLength="50" 
                                           defaultValue={cie10.codigoCIE10}
                                           ref={register({
                                               required: {value: true, message: "El codigo es obligatorio"},
                                               maxLength: {value: 50, message: "Maximo 50 caracteres"},
                                           })}
                                    />
                                    <span className="text-danger text-small d-block mb-2">
                                        {errors?.codigoCIE10?.message}
                                    </span>
                                </div>
                            </div>

                            <div className="form-group">
                                <label className="control-label col-sm-2">Descripcion:</label>
                                <div className="col-sm-10">
                                    <input type="text" className="form-control" placeholder="Ingrese Descripcion cie10"
                                           name="descripcion"
                                           required maxLength="50" 
                                           defaultValue={cie10.descripcionCIE10}
                                           ref={register({
                                               maxLength: {value: 600, message: "Maximo 600 caracteres"},
                                           })}
                                    />
                                    <span className="text-danger text-small d-block mb-2">
                                        {errors?.descripcionCIE10?.message}
                                    </span>
                                </div>
                            </div>

                            <div className="form-group">
                                <label className="control-label col-sm-2">Sexo:</label>
                                <div className="col-sm-10">
                                    <input type="text" className="form-control" placeholder="Ingrese Sexo"
                                           name="sexo"
                                           required maxLength="50" pattern="([1,2,0]+)"
                                           defaultValue={cie10.sexoCIE10}
                                           ref={register({
                                               maxLength: {value: 2, message: "Maximo 600 caracteres"},
                                           })}
                                    />
                                    <span className="text-danger text-small d-block mb-2">
                                        {errors?.sexoCIE10?.message}
                                    </span>
                                </div>
                            </div>

                            <div className="form-group">
                                <label className="control-label col-sm-2">Limite Inferior:</label>
                                <div className="col-sm-10">
                                    <input type="text" className="form-control" placeholder="Ingrese Edad Inferior"
                                           name="inferior"
                                           required maxLength="4" pattern="([0-9]+)"
                                           defaultValue={cie10.limiteInfCIE10}
                                           ref={register({
                                               maxLength: {value: 4, message: "Maximo 4 numeros"},
                                           })}
                                    />
                                    <span className="text-danger text-small d-block mb-2">
                                        {errors?.limiteInfCIE10?.message}
                                    </span>
                                </div>
                            </div>

                            <div className="form-group">
                                <label className="control-label col-sm-2">Limite Superior:</label>
                                <div className="col-sm-10">
                                    <input type="text" className="form-control" placeholder="Ingrese Limite Superior"
                                           name="superior"
                                           required maxLength="4" pattern="([0-9]+)"
                                           defaultValue={cie10.limiteSupCIE10}
                                           ref={register({
                                               maxLength: {value: 4, message: "Maximo 4 Numeros"},
                                           })}
                                    />
                                    <span className="text-danger text-small d-block mb-2">
                                        {errors?.limiteSupCIE10?.message}
                                    </span>
                                </div>
                            </div>


                            



                            <div className="form-group">
                                <div className="col-sm-offset-2 col-sm-10">
                                    <button type="submit" className="btn btn-success">Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>


            </div>
            }
            {jsonOE &&
            <div className="m-5">
                <h1>{msj}</h1>
            </div>
            }

            <div className="m-5">
                <a href="/cie10" className="btn btn-info">Regresar</a>
            </div>


            <button id="modal" type="button" className="d-none" data-toggle="modal" data-target="#exampleModalCenter">
                Launch demo modal
            </button>


            <div className="modal fade" id="exampleModalCenter" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalCenterTitle">{titulo}</h5>
                        </div>
                        <div className="modal-body">
                            {msj}
                        </div>
                        <div className="modal-footer">
                            {jsonPE &&
                            <button type="button" className="btn btn-warning" data-dismiss="modal">Cancelar</button>
                            }
                            {jsonPS &&
                            <div>
                                <a type="button" className="btn btn-primary" href="/cie10">Guardar</a>
                            </div>
                            }
                        </div>
                    </div>
                </div>
            </div>

        </Fragment>
    );
}

export default withRouter(ActualizarCie10);