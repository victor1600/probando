import React, {Fragment, useState} from 'react'
import {withRouter} from "react-router-dom";


//ruteo
import {useParams} from "react-router-dom";

const DetalleCie10 = (props) => {
    const estilosInicio = {
        fondoHeader: {
            backgroundColor: '#80cbc4',
            textAlign: 'center'
        },
        fondoBody: {
            backgroundColor: '#F5F5F6'
        },
        letraCarrousel: {
            color: 'black'
        }
    };

    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)


    const {idCIE10} = useParams();

    const [jsonOE, setJsonOE] = React.useState(false);
    const [jsonOS, setJsonOS] = React.useState(false);
    const [msj, setMsj] = React.useState(['']);

    const [cie10, setCie10] = React.useState([]);

    React.useEffect(() => {

        if(loginE === true){

            const obtenerCie10 = async () => {
                const data = await fetch(`http://localhost:8080/cie10/${idCIE10}`, {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });                
                const dataCie10 = await data.json();
                switch(data.status) {
                    case 403:
                        props.history.push('/error403');
                      break;
                    case 302:
                        setJsonOS(true);
                        setJsonOE(false);
                        setCie10(dataCie10);
                      break;
                    case 404:
                        setJsonOS(false);
                        setJsonOE(true);
                        setMsj([
                        String(dataCie10.mensaje)
                        ]);
                      break;
                    default:
                        props.history.push('/error');
                  }
        
            }
            
            obtenerCie10()


        }else{
            props.history.push('/login');
        } 
    }, [idCIE10,props,token,loginE])

    
    return (
        <Fragment>
            <div className="m-5">
                <div className="card">
                    <div className="card-header" style={estilosInicio.fondoHeader}>
                        <h2>Los datos de CIE10 con id {idCIE10} son:</h2>
                    </div>
                    <div className="card-body" style={estilosInicio.fondoBody}>
                        {jsonOS &&
                        <div>
                            <br/><br/>
                            <ul>
                                <li>{cie10.idCIE10}</li>
                                <li>{cie10.codigoCIE10}</li>
                                <li>{cie10.descripcionCIE10}</li>
                                <li>{cie10.sexoCIE10}</li>
                                <li>{cie10.limiteInfCIE10}</li>
                                <li>{cie10.limiteSupCIE10}</li>
                            </ul>
                        </div>
                        }
                        {
                            jsonOE &&
                            <div className="m-5">
                                <h1>{msj}</h1>
                            </div>
                        }
                        <a href="/cie10" className="btn btn-info">Regresar</a>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default withRouter(DetalleCie10);