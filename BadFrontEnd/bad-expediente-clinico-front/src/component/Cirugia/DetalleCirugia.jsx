import React, {Fragment, useState} from 'react'
import {withRouter} from "react-router-dom";


//ruteo
import {useParams} from "react-router-dom";

const DetalleCirugia = (props) => {
    const estilosInicio = {
        fondoHeader: {
            backgroundColor: '#80cbc4',
            textAlign: 'center'
        },
        fondoBody: {
            backgroundColor: '#F5F5F6'
        },
        letraCarrousel: {
            color: 'black'
        }
    };

    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)


    const {idCirugia} = useParams();

    const [jsonOE, setJsonOE] = React.useState(false);
    const [jsonOS, setJsonOS] = React.useState(false);
    const [msj, setMsj] = React.useState(['']);

    const [cirugia, setCirugia] = React.useState([]);

    React.useEffect(() => {

        if(loginE === true){

            const obtenerCirugia = async () => {
                const data = await fetch(`http://localhost:8080/cirugia/${idCirugia}`, {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });                
                const dataCirugia = await data.json();
                switch(data.status) {
                    case 403:
                        props.history.push('/error403');
                      break;
                    case 302:
                        setJsonOS(true);
                        setJsonOE(false);
                        setCirugia(dataCirugia);
                      break;
                    case 404:
                        setJsonOS(false);
                        setJsonOE(true);
                        setMsj([
                        String(dataCirugia.mensaje)
                        ]);
                      break;
                    default:
                        props.history.push('/error');
                  }
        
            }
            
            obtenerCirugia()


        }else{
            props.history.push('/login');
        } 
    }, [idCirugia,props,token,loginE])

    
    return (
        <Fragment>
            <div className="m-5">
                <div className="card">
                    <div className="card-header" style={estilosInicio.fondoHeader}>
                        <h2>Los datos de la cirugia :</h2>
                    </div>
                    <div className="card-body" style={estilosInicio.fondoBody}>
                        {jsonOS &&
                        <div>
                            <br/><br/>
                            <ul>
                                <li><b>Nombres:</b>     {cirugia.nombreCirugia}</li>
                                <li><b>Clasificación:</b>     {cirugia.clasificacionCirugia}</li>
                                <li><b>Descripción:</b>     {cirugia.descripcionCirugia}</li>
                                
                            </ul>
                        </div>
                        }
                        {
                            jsonOE &&
                            <div className="m-5">
                                <h1>{msj}</h1>
                            </div>
                        }
                        <a href="/cirugia" className="btn btn-info">Regresar</a>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default withRouter(DetalleCirugia);