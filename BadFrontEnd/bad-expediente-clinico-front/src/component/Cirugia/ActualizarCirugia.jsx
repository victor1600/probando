import React, {Fragment, useState} from 'react'
import {useForm} from 'react-hook-form'
import {useParams, withRouter} from "react-router-dom";

const ActualizarCirugia = (props) => {
    const {idCirugia} = useParams();
    const [cirugia, setCirugia] = React.useState([]);
    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)

    //Errorres de obtener cirugias
    const [jsonOE, setJsonOE] = React.useState(false);
    const [jsonOS, setJsonOS] = React.useState(false);

    //Errores Actualizar
    const [jsonPE, setJsonPE] = React.useState(false);
    const [jsonPS, setJsonPS] = React.useState(false);

    const [msj, setMsj] = React.useState(['']);

    React.useEffect(() => {

        if(loginE === true){
            const obtenerCirugia = async () => {
                const data = await fetch(`http://localhost:8080/cirugia/${idCirugia}`, {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });
                const dataCirugia = await data.json();

                switch(data.status) {
                    case 403:
                        props.history.push('/error403');
                      break;
                    case 302:
                        setJsonOS(true);
                        setJsonOE(false);
                        setCirugia(dataCirugia);
                      break;
                    case 404:
                        setJsonOS(false);
                        setJsonOE(true);
                        setMsj([
                        String(dataCirugia.mensaje)
                        ]);
                        break;
                    default:
                        props.history.push('/error');
                  }
    
            }
    
            obtenerCirugia();
        }else{
            props.history.push('/login');
        }

    }, [idCirugia, props, loginE,token])


    const [titulo, setTitulo] = React.useState(['']);

    const {register, handleSubmit, errors} = useForm();
    const onSubmit = async (data, e) => {
        e.preventDefault();
        const btnModal = document.getElementById("modal");
        //console.log(data)
        const jsonP = await fetch(`http://localhost:8080/cirugia/actualizar/${idCirugia}`, {
            method: 'PUT',
            body: JSON.stringify({
                nombreCirugia: data.nombre,
                clasificacionCirugia: data.clasificacion,
                descripcionCirugia: data.descripcion,
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                "Authorization": token
            }
        });
        console.log(jsonP);
        const info = await jsonP.json();

        if (jsonP.status === 202) {
            setJsonPE(false);
            setJsonPS(true);
            setMsj([
                String(info.mensaje)
            ]);
            setTitulo([
                String(info.httpCodeName)
            ]);
            btnModal.click();
            e.target.reset();

        } else {
            setJsonPE(true);
            setJsonPS(false);
            setMsj([
                String(info.mensaje)
            ]);
            setTitulo([
                String(info.httpCodeName)
            ]);
            btnModal.click();
        }


    }

    return (
        <Fragment>
            {jsonOS &&
            <div className="m-5">
                <div className="card">
                    <div className="card-header">
                        <h1>Actualizar Cirugia</h1>
                    </div>
                    <div className="card-body">
                        <form className="form-horizontal" onSubmit={handleSubmit(onSubmit)}>

                            <div className="form-group">
                                <label className="control-label col-sm-2">Nombre:</label>
                                <div className="col-sm-10">
                                    <input type="text" className="form-control" placeholder="Ingrese nombre"
                                           name="nombre"
                                           required maxLength="100" 
                                           defaultValue={cirugia.nombreCirugia}
                                           ref={register({
                                               required: {value: true, message: "El nombre es obligatorio"},
                                               maxLength: {value: 100, message: "Maximo 100 caracteres"},
                                           })}
                                    />
                                    <span className="text-danger text-small d-block mb-2">
                                        {errors?.nombreCirugia?.message}
                                    </span>
                                </div>
                            </div>

                            <div className="form-group">
                                <label className="control-label col-sm-2">Clasificacion:</label>
                                <div className="col-sm-10">
                                    <input type="text" className="form-control" placeholder="Ingrese la clasificacion"
                                           name="clasificacion"
                                           required maxLength="10" 
                                           defaultValue={cirugia.clasificacionCirugia}
                                           ref={register({
                                               maxLength: {value: 10, message: "Maximo 10 caracteres"},
                                           })}
                                    />
                                    <span className="text-danger text-small d-block mb-2">
                                        {errors?.clasificacionCirugia?.message}
                                    </span>
                                </div>
                            </div>   


                            <div className="form-group">
                                <label className="control-label col-sm-2">Descripcion:</label>
                                <div className="col-sm-10">
                                    <input type="text" className="form-control" placeholder="Ingrese la descripcion"
                                           name="descripcion"
                                           required maxLength="600" 
                                           defaultValue={cirugia.descripcionCirugia}
                                           ref={register({
                                               maxLength: {value: 600, message: "Maximo 600 caracteres"},
                                           })}
                                    />
                                    <span className="text-danger text-small d-block mb-2">
                                        {errors?.descripcionCirugia?.message}
                                    </span>
                                </div>
                            </div>                  

                            <div className="form-group">
                                <div className="col-sm-offset-2 col-sm-10">
                                    <button type="submit" className="btn btn-success">Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>


            </div>
            }
            {jsonOE &&
            <div className="m-5">
                <h1>{msj}</h1>
            </div>
            }

            <div className="m-5">
                <a href="/cirugia" className="btn btn-info">Regresar</a>
            </div>


            <button id="modal" type="button" className="d-none" data-toggle="modal" data-target="#exampleModalCenter">
                Launch demo modal
            </button>


            <div className="modal fade" id="exampleModalCenter" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalCenterTitle">{titulo}</h5>
                        </div>
                        <div className="modal-body">
                            {msj}
                        </div>
                        <div className="modal-footer">
                            {jsonPE &&
                            <button type="button" className="btn btn-warning" data-dismiss="modal">Cancelar</button>
                            }
                            {jsonPS &&
                            <div>
                                <a type="button" className="btn btn-primary" href="/cirugia">Guardar</a>
                            </div>
                            }
                        </div>
                    </div>
                </div>
            </div>

        </Fragment>
    );
}

export default withRouter(ActualizarCirugia);