import React from "react";
import Listar from "../Reusables/Listar";
// import Listar from "../reusables/Listar";

const ListarM = () => {
  const title = "medicamento";

  const id = "idMedicamento";

  // Endpoint and url for Links
  const endpoint = "http://localhost:8080/medicamentos";
  const url = "medicamentos";

  // id used for linking correclty to actualizar and details
  const linkID = "idMedicamento";

  // Name used to display in table of Listar
  const firstField = "codigoMedicamento";

  // table headers for non parent, including first field.
  const tableHeaders = ["Codigo", "Nombre", "Cantidad"];

  // Attributes to display that are not the id or the LINK attribute, or the parent
  const toDisplayAttributes = ["nombreMedicamento", "cantidadMedicamento"];

  // table header for parents
  const tableParentHeaders = ["Tipo de medicamento"];

  // parent attributes associated
  const parents = ["tipoMedicamento"];

  // list of main attribute of each parent we wanto to show
  const parentsMainAttribute = ["nombreTMedicamento"];

  return (
    <Listar
      title={title}
      id={id}
      endpoint={endpoint}
      url={url}
      linkID={linkID}
      firstField={firstField}
      tableHeaders={tableHeaders}
      toDisplayAttributes={toDisplayAttributes}
      tableParentHeaders={tableParentHeaders}
      parents={parents}
      parentsMainAttribute={parentsMainAttribute}
    />
  );
};

export default ListarM;
