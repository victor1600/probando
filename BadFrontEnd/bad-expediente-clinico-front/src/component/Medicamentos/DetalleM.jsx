import React from "react";
import Detalle from "../Reusables/Detalle";

const DetalleM = () => {
  const title = "medicamento";
  const endpoint = "http://localhost:8080/medicamentos";
  const url = "medicamentos";

  const attributes = [
    "codigoMedicamento",
    "nombreMedicamento",
    "cantidadMedicamento",
    "indicacionMedicamento",
    "viaAdministracionMedicamento",
  ];

  const labels = [
    "Codigo",
    "Nombre del medicamento",
    "Cantidad de medicamento",
    "indicacion",
    "via de administracion",
  ];

  // Parents attributes
  // parent attributes associated
  const parents = ["tipoMedicamento"];

  // list of main attribute of each parent we wanto to show
  const parentsMainAttribute = ["nombreTMedicamento"];

  // Labels
  const parentLabels = ["Tipo del medicamento"];

  // combination

  return (
    <Detalle
      title={title}
      endpoint={endpoint}
      url={url}
      attributes={attributes}
      labels={labels}
      parents={parents}
      parentsMainAttribute={parentsMainAttribute}
      parentLabels={parentLabels}
    />
  );
};

export default DetalleM;
