import React, { useState } from "react";
import Agregar from "../Reusables/Agregar";

const AgregarM = () => {
  const title = "Medicamento";
  const endpoint = "http://localhost:8080/medicamentos";
  const url = "medicamentos";
  const [token] = useState(
    localStorage.getItem("token") ? localStorage.getItem("token") : null
  );

  // Define names of fields when doing post:
  // TODO add FK
  const postNames = [
    "codigoMedicamento",
    "nombreMedicamento",
    "cantidadMedicamento",
    "indicacionMedicamento",
    "viaAdministracionMedicamento",
  ];
  // Here you will define the rules for every field individually
  const field1 = {
    labelTitle: "Codigo del Medicamento",
    placeholder: "Ingrese codigo del medicamento",
    name: "codigo",
    type: "text",
    maxLength: "50",
    pattern: "([A-Za-z0-9 ]+)", // TODO change this to allow numbers
    ref: {
      required: {
        value: true,
        message: "El codigo es obligatorio",
      },
      maxLength: {
        value: 50,
        message: "Maximo 50 caracteres",
      },
      pattern: {
        value: /[A-Za-z0-9 ]/, // TODO: test
        message: "solamente letras o numeros",
      },
    },
  };
  const field2 = {
    labelTitle: "Nombre del Medicamento",
    placeholder: "Ingrese nombre del medicamento",
    name: "nombre",
    type: "text",
    maxLength: "50",
    pattern: "([A-Za-z0-9 ]+)", // TODO change this to allow numbers
    ref: {
      required: {
        value: true,
        message: "El nombre es obligatorio",
      },
      maxLength: {
        value: 50,
        message: "Maximo 50 caracteres",
      },
      pattern: {
        value: /[A-Za-z0-9 ]/, // TODO: test
        message: "solamente letras o numeros",
      },
    },
  };

  const field3 = {
    labelTitle: "Cantidad del Medicamento",
    placeholder: "Ingrese cantidad del medicamento",
    name: "cantidad",
    maxLength: "6",
    type: "number",
    pattern: "([0-9]+)", // TODO just numbers
    ref: {
      required: {
        value: true,
        message: "La cantidad es obligatorio",
      },
      maxLength: {
        value: 6,
        message: "Maximo 6 digitos",
      },
      pattern: {
        value: /[0-9]/, // TODO: test
        message: "solamente numeros",
      },
    },
  };

  const field4 = {
    labelTitle: "Indicacion del Medicamento",
    placeholder: "Indicacion del medicamento",
    name: "indicacion",
    maxLength: "200",
    type: "text",
    pattern: "([A-Za-z0-9 ]+)", // TODO
    ref: {
      required: {
        value: true,
        message: "La indicacion es obligatoria",
      },
      maxLength: {
        value: 50,
        message: "Maximo 50 caracteres",
      },
      pattern: {
        value: "/[A-Za-z0-9 ]/", // TODO: test
        message: "solamente letras o numeros",
      },
    },
  };

  const field5 = {
    labelTitle: "Via de administracion del medicamento",
    placeholder: "Via de administracion del medicamento",
    name: "administracion",
    maxLength: "40",
    type: "text",
    pattern: "([A-Za-z ]+)", // TODO
    ref: {
      required: {
        value: true,
        message: "La via de administracion es obligatoria",
      },
      maxLength: {
        value: 50,
        message: "Maximo 50 caracteres",
      },
      pattern: {
        value: "/[A-Za-z ]/", // TODO: test
        message: "solamente letras",
      },
    },
  };

  const fields = [field1, field2, field3, field4, field5];

  // TODO: You have to create a useState, UseEffect and an object for each dropdown.
  // TODO: Then you have to append it to the fks array

  const [tipoMedicamentos, setTipoMedicamentos] = useState([]);
  React.useEffect(() => {
    const fetchContent = async () => {
      const data = await fetch("http://localhost:8080/tipomedicamentos/todas", {
        method: "GET",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Authorization: token,
        },
      });
      const dataJSON = await data.json();
      if (data.status === 200) {
        setTipoMedicamentos(dataJSON);
      } else {
        console.log("cant retrieve data");
      }
    };
    fetchContent();
  }, [token]);

  const fk1 = {
    fkTitle: "Tipo de Medicamento",
    idSelect: "tipoMedicamentosD",
    fakeAttributeName: "fktipoMedicamento",
    fkAttributes: {
      idFk: "idTipoMedicamento",
      nameFk: "nombreTMedicamento",
    },
    content: tipoMedicamentos,
  };
  const fks = [fk1];

  // TODO: implement api fetch here.

  return (
    <Agregar
      title={title}
      endpoint={endpoint}
      url={url}
      postNames={postNames}
      fields={fields}
      fks={fks}
    />
  );
};
export default AgregarM;
