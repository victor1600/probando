import React from "react";
import Detalle from "../Reusables/Detalle";

const DetallesTipoCita = () => {
  const title = "tipo de citas";
  const endpoint = "http://localhost:8080/tipocitas";
  const url = "tipoCitas";

  // const id = 'idTipoMedicamento';
  const attributes = ["nombreTipoCita"];
  const labels = ["Nombre"];
  return (
    <Detalle
      title={title}
      endpoint={endpoint}
      url={url}
      attributes={attributes}
      labels={labels}
    />
  );
};

export default DetallesTipoCita;
