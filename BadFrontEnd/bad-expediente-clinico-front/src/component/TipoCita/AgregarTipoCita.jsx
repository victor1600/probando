import React from "react";
import Agregar from "../Reusables/Agregar";

const AgregarTipoCita = () => {
  const title = "tipo de citas";
  const endpoint = "http://localhost:8080/tipocitas";
  const url = "tipoCitas";

  // Define names of fields when doing post:
  const postNames = ["nombreTipoCita"];
  // Here you will define the rules for every field individually
  const field1 = {
    labelTitle: "nombre",
    placeholder: "Ingrese nombre",
    name: "nombre",
    maxLength: "50",
    pattern: "([A-Za-z ]+)",
    ref: {
      required: {
        value: true,
        message: "El nombre es obligatorio",
      },
      maxLength: {
        value: 50,
        message: "Maximo 40 caracteres",
      },
      pattern: {
        value: /[A-Za-z ]/, // TODO: test
        message: "solamente letras",
      },
    },
  };

  // Array that will hold all the fields that you want to appear in the form. Dont include parent here.
  const fields = [field1];
  return (
    <Agregar
      title={title}
      endpoint={endpoint}
      url={url}
      postNames={postNames}
      fields={fields}
    />
  );
};

export default AgregarTipoCita;
