import React from "react";
import Actualizar from "../Reusables/Actualizar";
const ActualizarTipoCita = () => {
  const title = "tipo de citas";
  const endpoint = "http://localhost:8080/tipocitas";
  const url = "tipoCitas";
  const postNames = ["nombreTipoCita"];
  const field1 = {
    labelTitle: "nombre",
    placeholder: "Ingrese nombre",
    name: "nombre",
    maxLength: "50",
    pattern: "([A-Za-z ]+)",
    ref: {
      required: {
        value: true,
        message: "El nombre es obligatorio",
      },
      maxLength: {
        value: 50,
        message: "Maximo 40 caracteres",
      },
      pattern: {
        value: /[A-Za-z ]/, // TODO: test
        message: "solamente letras",
      },
    },
  };
  const fields = [field1];
  return (
    <Actualizar
      title={title}
      endpoint={endpoint}
      url={url}
      postNames={postNames}
      fields={fields}
    />
  );
};

export default ActualizarTipoCita;
