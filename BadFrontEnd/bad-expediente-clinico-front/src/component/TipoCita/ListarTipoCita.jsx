import React from "react";
import Listar from "../Reusables/Listar";

const ListarTipoCita = () => {
  // The title to be displayed
  const title = "tipo de citas";
  const id = "idTipoCita";

  // Endpoint and url for Links
  const endpoint = "http://localhost:8080/tipocitas";
  const url = "tipoCitas";

  // id used for linking correclty to actualizar and details
  const linkID = "idTipoCita";

  // Name used to display in table of Listar
  const firstField = "nombreTipoCita";

  // table headers for non parent, including first field.
  const tableHeaders = ["Nombre"];

  // Attributes to display that are not the id or the LINK attribute, or the parent
  const toDisplayAttributes = ["descripcionTipoMedicamento"];

  // table header for parents
  const tableParentHeaders = [];

  return (
    <Listar
      title={title}
      id={id}
      endpoint={endpoint}
      url={url}
      linkID={linkID}
      firstField={firstField}
      tableHeaders={tableHeaders}
      toDisplayAttributes={toDisplayAttributes}
      tableParentHeaders={tableParentHeaders}
    />
  );
};

export default ListarTipoCita;
