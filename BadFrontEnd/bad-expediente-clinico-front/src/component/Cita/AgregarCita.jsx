import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { withRouter } from "react-router-dom";

import MyCard from "../Reusables/MyCard";
import DateHelper from "../Reusables/DateHelper";
import TimeHelper from "../Reusables/TimeHelper";
import FetchUseEffect from "../Reusables/FetchUseEffect";
import PostFunction from "../Reusables/PostFunction";
import ComboBox from "../Reusables/ComboBox";
import MyModal from "../Reusables/MyModal";
import InputModal from "../Reusables/InputModal";
// import ObtenerDePost from "../Reusables/ObtenerDePost";

const AgregarCita = (props) => {
  const [loginE] = useState(localStorage.getItem("login") ? true : false);
  const [token] = useState(
    localStorage.getItem("token") ? localStorage.getItem("token") : null
  );
  const [usuario] = useState(
    localStorage.getItem("usuario") ? localStorage.getItem("usuario") : null
  );

  const { handleSubmit } = useForm();

  const [jsonOE, setJsonOE] = React.useState(false);
  const [jsonOS, setJsonOS] = React.useState(false);
  const [msj, setMsj] = React.useState([""]);
  const [titulo, setTitulo] = React.useState([""]);

  // Dia y tiempo
  const [myDate, setMyDate] = useState(null);
  const [myTime, setMyTime] = useState(null);

  const onSubmit = (data, e) => {
    e.preventDefault();
    const btnModal = document.getElementById("modal");
    let postURL = "http://localhost:8080/citas/agregar";
    console.log(medicoSeleccionado);
    let nuevaCita = {};
    if (!medicoSeleccionado) {
      nuevaCita = {
        fechaCita: myDate,
        horaCita: myTime,
        fkMedicoId: medicos[0].medico.idMedico,
        estaActivo: true,
        fkTipoCita: tipoCitaSeleccionada.idTipoCita,
        fkPacienteId: pacienteSeleccionado.idPersona,
      };
    } else {
      nuevaCita = {
        fechaCita: myDate,
        horaCita: myTime,
        fkMedicoId: medicoSeleccionado.medico.idMedico,
        estaActivo: true,
        fkTipoCita: tipoCitaSeleccionada.idTipoCita,
        fkPacienteId: pacienteSeleccionado.idPersona,
      };
    }

    PostFunction(
      data,
      token,
      setJsonOE,
      setJsonOS,
      setMsj,
      setTitulo,
      // btnModal,
      e,
      postURL,
      nuevaCita,
      props
    );
    btnModal.click();
    window.location.replace("http://localhost:3000/citas/");
  };

  // Paciente

  const [pacientes, setPacientes] = useState([]);
  const endpoint = "http://localhost:8080/pacientes/todos";
  FetchUseEffect(
    loginE,
    token,
    props,
    setJsonOS,
    setJsonOE,
    setPacientes,
    setMsj,
    endpoint
  );
  const [pacienteSeleccionado, setPacienteSeleccionado] = useState(null);
  const comboConfigPacientes = {
    // NO CAMBIAR LAS KEYS
    // LO QUE SE CAMBIA ES EL TEXTO, QUE CORRESPONDERA CON LOS NOMBRES PARTICULARES DE LOS OBJETOS A ENVIARSE.
    id: "idPersona",
    title: "nombre1Persona",
    title2: "apellido1Persona",
  };

  // Especialidades
  const [especialidades, setEspecialidades] = useState([]);
  const [especialidadSeleccionada, setEspecialidadSeleccionada] = useState(
    null
  );
  const endpointEspecialidades = "http://localhost:8080/especialidad/todas";
  FetchUseEffect(
    loginE,
    token,
    props,
    setJsonOS,
    setJsonOE,
    setEspecialidades,
    setMsj,
    endpointEspecialidades
  );

  const comboConfigEspecialidades = {
    id: "idEspecialidad",
    title: "nombreEspecialidad",
  };

  // TipoCitas
  const [tipoCitas, setTipoCitas] = useState([]);
  const [tipoCitaSeleccionada, setTipoCitaSeleccionada] = useState(null);
  const endpointTipoCitas = "http://localhost:8080/tipocitas/todas";
  FetchUseEffect(
    loginE,
    token,
    props,
    setJsonOS,
    setJsonOE,
    setTipoCitas,
    setMsj,
    endpointTipoCitas
  );

  const comboConfigTipoCitas = {
    id: "idTipoCita",
    title: "nombreTipoCita",
  };

  // Medico
  const [medicos, setMedicos] = useState([]);
  // const endpoint2 = "http://localhost:8080/Medicos/todos";
  // FetchUseEffect(
  //   loginE,
  //   token,
  //   props,
  //   setJsonOS,
  //   setJsonOE,
  //   setMedicos,
  //   setMsj,
  //   endpoint2
  // );

  const [medicoSeleccionado, setMedicoSeleccionado] = useState(null);
  const comboConfigMedicos = {
    // NO CAMBIAR LAS KEYS
    // LO QUE SE CAMBIA ES EL TEXTO, QUE CORRESPONDERA CON LOS NOMBRES PARTICULARES DE LOS OBJETOS A ENVIARSE.
    id: "idPersona",
    title: "nombre1Persona",
    title2: "apellido1Persona",
  };

  React.useEffect(() => {
    if (loginE === true) {
      const obtenerPacientes = async () => {
        const data = await fetch("http://localhost:8080/pacientes/todos", {
          method: "GET",
          headers: {
            "Content-type": "application/json; charset=UTF-8",
            Authorization: token,
          },
        });

        const dataPacientes = await data.json();
        switch (data.status) {
          case 403:
            props.history.push("/error403");
            break;
          case 302:
            setJsonOS(true);
            setJsonOE(false);
            setPacientes(dataPacientes);
            break;
          case 404:
            setJsonOS(false);
            setJsonOE(true);
            setMsj([String(dataPacientes.mensaje)]);
            break;
          default:
            props.history.push("/error");
        }
      };
      obtenerPacientes();
    } else {
      props.history.push("/login");
    }
  }, [props, loginE, token]);

  let obtenerDoctores = async (token, postURL, nuevaCita) => {
    const jsonP = await fetch(postURL, {
      method: "POST",
      body: JSON.stringify(nuevaCita),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
        Authorization: token,
      },
    });
    const info = await jsonP.json();
    setMedicos(info);
  };

  const populateDoctors = (data, e) => {
    // e.preventDefault();
    console.log(myDate);
    let postURL = "http://localhost:8080/citas/buscar";

    const btnModal = document.getElementById("modalinput");
    const nuevaCita = {
      fechaCita: myDate,
      horaCita: myTime,
      fkMedicoId: especialidadSeleccionada.idEspecialidad,
      estaActivo: true,
      fkTipoCita: "3953ac6f-cc1b-4939-b022-0f12dba6772f",
      fkPacienteId: pacienteSeleccionado.idPersona,
    };

    obtenerDoctores(token, postURL, nuevaCita);

    btnModal.click();
  };

  return (
    <MyCard linkBack="/citas" myTitle="Creacion de una nueva cita">
      <form className="form-horizontal" onSubmit={handleSubmit(onSubmit)}>
        <div
          style={{
            marginLeft: 30,
          }}
        >
          <DateHelper
            titleName="Dia de la cita"
            labelName="Selecione un dia"
            setMyDate={setMyDate}
          />
          <TimeHelper
            titleName="Hora de la cita"
            labelName="Digite una hora AM/PM"
            setMyTime={setMyTime}
          />
          <div
            style={{
              flexDirection: "row",
            }}
          >
            <ComboBox
              setCombo={setPacienteSeleccionado}
              comboOptions={pacientes}
              comboConfig={comboConfigPacientes} //
              labelName="Seleccione un Paciente"
            />

            <ComboBox
              setCombo={setEspecialidadSeleccionada}
              comboOptions={especialidades}
              comboConfig={comboConfigEspecialidades} //
              labelName="Seleccione una Especialidad"
            />

            <ComboBox
              setCombo={setTipoCitaSeleccionada}
              comboOptions={tipoCitas}
              comboConfig={comboConfigTipoCitas} //
              labelName="Seleccione un tipo de cita"
            />
          </div>

          {myDate &&
          myTime &&
          especialidadSeleccionada &&
          pacienteSeleccionado &&
          tipoCitaSeleccionada ? (
            <div>
              <button
                type="button"
                onClick={populateDoctors}
                className="btn btn-success"
              >
                Seleccionar doctor
              </button>
            </div>
          ) : null}
        </div>
        {/* {medicoSeleccionado ? (
          <h6
            style={{
              marginLeft: 30,
              marginTop: 20,
              marginBottom: 20,
            }}
          >
            Medico:{" "}
            {medicoSeleccionado.nombre1Persona +
              " " +
              medicoSeleccionado.apellido1Persona}
          </h6>
        ) : null} */}
        {/* {myDate &&
        myTime &&
        especialidadSeleccionada &&
        pacienteSeleccionado &&
        medicoSeleccionado &&
        tipoCitaSeleccionada ? (
          <button
            style={{ marginLeft: 30, marginTop: 25 }}
            type="submit"
            className="btn btn-success"
          >
            Guardar
          </button>
   
        ) : null} */}
        <InputModal
          titulo="Seleccion un doctor"
          msj="hola"
          submitButton={
            Array.isArray(medicos) && medicos.length ? (
              <button
                style={{ marginLeft: 30, marginTop: 25 }}
                type="submit"
                className="btn btn-success"
              >
                Guardar
              </button>
            ) : null
          }
        >
          {Array.isArray(medicos) && medicos.length ? (
            <div>
              <ComboBox
                defaultValue={medicos[0]}
                setCombo={setMedicoSeleccionado}
                comboOptions={medicos}
                comboConfig={comboConfigMedicos}
                labelName="Seleccione un Medico"
              />
            </div>
          ) : (
            "No se encontraron medicos"
          )}
        </InputModal>
      </form>

      <MyModal
        titulo={titulo}
        msj={msj}
        jsonOE={jsonOE}
        jsonOS={jsonOS}
        urlReturn="citas/"
      />
    </MyCard>
  );
};

export default withRouter(AgregarCita);
