import React from "react";
import Listar from "../Reusables/Listar";
const ListarCitas = () => {
  const title = "Citas";
  const id = "idCita";

  // Endpoint and url for Links
  const endpoint = "http://localhost:8080/citas";
  const url = "citas";

  // id used for linking correclty to actualizar and details
  const linkID = "idCita";

  // Name used to display in table of Listar
  const firstField = "fechaCita";

  // table headers for non parent, including first field.
  const tableHeaders = ["Fecha de cita", "Hora de cita"];

  // Attributes to display that are not the id or the LINK attribute, or the parent
  const toDisplayAttributes = ["horaCita"];

  // table header for parents
  const tableParentHeaders = ["Tipo de cita"];

  // parent attributes associated
  const parents = ["tipoCita"];

  // list of main attribute of each parent we wanto to show
  const parentsMainAttribute = ["nombreTipoCita"];

  return (
    <Listar
      title={title}
      id={id}
      endpoint={endpoint}
      url={url}
      linkID={linkID}
      firstField={firstField}
      tableHeaders={tableHeaders}
      toDisplayAttributes={toDisplayAttributes}
      tableParentHeaders={tableParentHeaders}
      parents={parents}
      parentsMainAttribute={parentsMainAttribute}
    />
  );
};

export default ListarCitas;
