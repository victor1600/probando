import React, {Fragment, useState} from 'react'
import {withRouter} from "react-router-dom";


//ruteo
import {useParams} from "react-router-dom";

const DetalleEspecialidad = (props) => {
    const estilosInicio = {
        fondoHeader: {
            backgroundColor: '#80cbc4',
            textAlign: 'center'
        },
        fondoBody: {
            backgroundColor: '#F5F5F6'
        },
        letraCarrousel: {
            color: 'black'
        }
    };

    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)

    const {idEspecialidad} = useParams();

    const [jsonOE, setJsonOE] = React.useState(false);
    const [jsonOS, setJsonOS] = React.useState(false);
    const [msj, setMsj] = React.useState(['']);

    const [especialidad, setEspecialidad] = React.useState([]);

    React.useEffect(() => {

        if(loginE === true){
        const obtenerEspecialidad = async () => {
            const data = await fetch(`http://localhost:8080/especialidad/${idEspecialidad}`, {
                method: 'GET',
                headers: {
                    "Content-type": "application/json; charset=UTF-8",
                    "Authorization": token
                }
            });                
            const dataEspecialidad = await data.json();
            switch(data.status) {
                case 403:
                    props.history.push('/error403');
                  break;
                case 302:
                    setJsonOS(true);
                    setJsonOE(false);
                    setEspecialidad(dataEspecialidad);
                  break;
                case 404:
                    setJsonOS(false);
                    setJsonOE(true);
                    setMsj([
                    String(dataEspecialidad.mensaje)
                    ]);
                  break;
                default:
                    props.history.push('/error');
              }
    
        }

        obtenerEspecialidad();

    }else{
        props.history.push('/login');
    } 
}, [idEspecialidad,props,token,loginE])


    return (
        <Fragment>
            <div className="m-5">
                <div className="card">
                    <div className="card-header" style={estilosInicio.fondoHeader}>
                        <h2>Los datos de la Especialidad con id {idEspecialidad} son:</h2>
                    </div>
                    <div className="card-body" style={estilosInicio.fondoBody}>
                        {jsonOS &&
                        <div>
                            <br/><br/>
                            <ul>
                                <li>{especialidad.idEspecialidad}</li>
                                <li>{especialidad.nombreEspecialidad}</li>
                            </ul>
                        </div>
                        }
                        {
                            jsonOE &&
                            <div className="m-5">
                                <h1>{msj}</h1>
                            </div>
                        }
                        <a href="/especialidad" className="btn btn-info">Regresar</a>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default withRouter(DetalleEspecialidad);