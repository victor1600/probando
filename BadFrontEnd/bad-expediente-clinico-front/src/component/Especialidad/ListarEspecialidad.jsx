import React, { Fragment, useState } from 'react'
import {Link, withRouter} from "react-router-dom";


const ListarEspecialidad = (props) => {

    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)


    const estilosInicio = {
        fondoHeader: {
            backgroundColor: '#80cbc4',
            textAlign: 'center'
        },
        fondoBody: {
            backgroundColor: '#F5F5F6'
        },
        letraCarrousel: {
            color: 'black'
        }
    };


    const [jsonOE, setJsonOE] = React.useState(false);
    const [jsonOS, setJsonOS] = React.useState(false);
    const [msj, setMsj] = React.useState(['']);
    const [titulo, setTitulo] = React.useState(['']);
    const [jsonEE, setJsonEE] = React.useState(false);
    const [jsonES, setJsonES] = React.useState(false);

    const [especialidad, setEspecialidad] = React.useState([]);
    React.useEffect(() => {
        if (loginE === true) {
            const obtenerEspecialidad = async () => {
            const data = await fetch('http://localhost:8080/especialidad/todas', {
                method: 'GET',
                headers: {
                    "Content-type": "application/json; charset=UTF-8",
                    "Authorization": token
                }
            }); 
            const dataEspecialidad = await data.json();
                switch(data.status) {
                    case 403:
                        props.history.push('/error403');
                      break;
                    case 302:
                        setEspecialidad(dataEspecialidad);
                        setJsonOS(true);
                        setJsonOE(false);
                      break;
                    case 404:
                        setJsonOS(false);
                        setJsonOE(true);
                        setMsj([
                        String(dataEspecialidad.mensaje)
                    ]);
                        break;
                    default:
                        props.history.push('/error');
                }
            }
        obtenerEspecialidad();
    } else {
        props.history.push('/login');
    }
    }, [props, loginE,token])

    const eliminarEspecialidad = async (e) => {
        const btnCerrar = document.getElementById(`btn-${e.target.value}`)
        const btnInfo = document.getElementById('btn-infoDelete')
        const dataE = await fetch(`http://localhost:8080/especialidad/eliminar/${e.target.value}`, {
            method: 'DELETE',
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                "Authorization": token
            }
        })   
        const infoEP = await dataE.json()


        if (dataE.status === 200) {
            setJsonEE(false)
            setJsonES(true)
            setTitulo([
                String(infoEP.httpCodeName)
            ]);
            setMsj([
                String(infoEP.mensaje)
            ]);
        } else {
            setJsonEE(true)
            setJsonES(false)
            setTitulo([
                String(infoEP.httpCodeName)
            ]);
            setMsj([
                String(infoEP.mensaje)
            ]);
        }

        btnCerrar.click()
        btnInfo.click()
    }

    
    return (
        <Fragment>
            <div className="m-5">
                <div className="card">
                    <div className="card-header" style={estilosInicio.fondoHeader}>
                        <h1>Listado de Especialidades</h1>
                    </div>
                    <div className="card-body" style={estilosInicio.fondoBody}>
                        {jsonOS &&

                        <div className="table-responsive">
                            <table className="table table-hover">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                {especialidad.map((item) => (
                                    <tr key={item.idEspecialidad}>
                                    <td>
                                        <Link to={`/especialidad/${item.idEspecialidad}`}>
                                        {item.nombreEspecialidad}
                                        </Link>
                                    </td>
                                    <td><a href={`/especialidad/actualizar/${item.idEspecialidad}`}
                                           className="btn btn-warning">Editar</a>
                                           </td>
                                    <td><button type="button" className="btn btn-danger" data-toggle="modal"
                                                data-target={`#modal-${item.idEspecialidad}`}>
                                                Eliminar
                                        </button>
                                    </td>
                                </tr>
                                    
                                ))}
                                </tbody>
                            </table>
                        </div>


                        }
                        {
                            jsonOE &&
                            <div className="m-5">
                                <h1>{msj}</h1>
                            </div>
                        }
                        <a href="/especialidad/agregar" className="btn btn-primary">Agregar</a>

                    </div>

                    {especialidad.map((item) => (
                        <div key={item.idEspecialidad} className="modal fade" id={`modal-${item.idEspecialidad}`}
                             tabIndex="-1"
                             role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="exampleModalLongTitle">Advertencia
                                            Eliminacion</h5>
                                    </div>
                                    <div className="modal-body">
                                        ¿Usted desea eliminar la especialidad {item.nombreEspecialidad}?
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" id={`btn-${item.idEspecialidad}`}
                                                className="btn btn-secondary"
                                                data-dismiss="modal">Cancelar
                                        </button>
                                        <button type="button" className="btn btn-success" data-dismiss="modal" value={item.idEspecialidad} onClick={eliminarEspecialidad}>Si, Deseo Eliminar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}

                    <button id="btn-infoDelete" type="button" className="d-none" data-dismiss="modal"
                            data-toggle="modal"
                            data-target="#infoDelete">
                        info del modal
                    </button>


                    <div className="modal fade" id="infoDelete" tabIndex="-1" role="dialog"
                         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalLongTitle">{titulo}</h5>
                                </div>
                                <div className="modal-body">
                                    {msj}
                                </div>
                                <div className="modal-footer">
                                    {jsonES &&
                                    <a type="button" href="/especialidad" className="btn btn-primary">Enterado</a>
                                    }
                                    {jsonEE &&
                                    <button type="button" className="btn btn-warning"
                                            data-dismiss="modal">Enterado</button>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            
        </Fragment>
    );
}

export default withRouter(ListarEspecialidad);
