import React, {Fragment, useState} from 'react'
import {useForm} from 'react-hook-form'
import {useParams, withRouter} from "react-router-dom";

const ActualizarEspecialidad = (props) => {
    const {idEspecialidad} = useParams();
    const [especialidad, setEspecialidad] = React.useState([]);
    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)

    //Errorres de obtener tipoExamens
    const [jsonOE, setJsonOE] = React.useState(false);
    const [jsonOS, setJsonOS] = React.useState(false);

    //Errores Actualizar
    const [jsonPE, setJsonPE] = React.useState(false);
    const [jsonPS, setJsonPS] = React.useState(false);

    const [msj, setMsj] = React.useState(['']);

    React.useEffect(() => {
        if(loginE === true){
        const obtenerEspecialidad = async () => {
            const data = await fetch(`http://localhost:8080/especialidad/${idEspecialidad}`, {
                method: 'GET',
                headers: {
                    "Content-type": "application/json; charset=UTF-8",
                    "Authorization": token
                }
            });
            const dataEspecialidad = await data.json();
            switch(data.status) {
                case 403:
                    props.history.push('/error403');
                  break;
                case 302:
                    setJsonOS(true);
                    setJsonOE(false);
                    setEspecialidad(dataEspecialidad);
                  break;
                case 404:
                    setJsonOS(false);
                    setJsonOE(true);
                    setMsj([
                    String(dataEspecialidad.mensaje)
                    ]);
                    break;
                default:
                    props.history.push('/error');
              }

        }


        obtenerEspecialidad();

    }else{
        props.history.push('/login');
    }

}, [idEspecialidad, props, loginE,token])


    const [titulo, setTitulo] = React.useState(['']);

    const {register, handleSubmit, errors} = useForm();
    const onSubmit = async (data, e) => {
        e.preventDefault();
        const btnModal = document.getElementById("modal");
        //console.log(data)
        const jsonP = await fetch(`http://localhost:8080/especialidad/actualizar/${idEspecialidad}`, {
            method: 'PUT',
            body: JSON.stringify({
                nombreEspecialidad: data.nombre
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                "Authorization": token
            }
        });
        console.log(jsonP);
        const info = await jsonP.json();

        if (jsonP.status === 202) {
            setJsonPE(false);
            setJsonPS(true);
            setMsj([
                String(info.mensaje)
            ]);
            setTitulo([
                String(info.httpCodeName)
            ]);
            btnModal.click();
            e.target.reset();

        } else {
            setJsonPE(true);
            setJsonPS(false);
            setMsj([
                String(info.mensaje)
            ]);
            setTitulo([
                String(info.httpCodeName)
            ]);
            btnModal.click();
        }


    }

    return (
        <Fragment>
            {jsonOS &&
            <div className="m-5">
                <div className="card">
                    <div className="card-header">
                        <h1>Actualizar Especialidad</h1>
                    </div>
                    <div className="card-body">
                        <form className="form-horizontal" onSubmit={handleSubmit(onSubmit)}>
                            <div className="form-group">
                                <label className="control-label col-sm-2">Especialidad:</label>
                                <div className="col-sm-10">
                                    <input type="text" className="form-control" placeholder="Ingrese especialidad"
                                           name="nombre"
                                           required maxLength="50" pattern="([A-Za-zv ]+)"
                                           defaultValue={especialidad.nombreEspecialidad}
                                           ref={register({
                                               required: {value: true, message: "El nombre es obligatorio"},
                                               maxLength: {value: 50, message: "Maximo 50 caracteres"},
                                               pattern: {value: /[A-Za-z ]/, message: "Solamente letras"}
                                           })}
                                    />
                                    <span className="text-danger text-small d-block mb-2">
                                {errors?.nombre?.message}
                            </span>
                                </div>
                            </div>


                            <div className="form-group">
                                <div className="col-sm-offset-2 col-sm-10">
                                    <button type="submit" className="btn btn-success">Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>


            </div>
            }
            {jsonOE &&
            <div className="m-5">
                <h1>{msj}</h1>
            </div>
            }

            <div className="m-5">
                <a href="/especialidad" className="btn btn-info">Regresar</a>
            </div>


            <button id="modal" type="button" className="d-none" data-toggle="modal" data-target="#exampleModalCenter">
                Launch demo modal
            </button>


            <div className="modal fade" id="exampleModalCenter" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalCenterTitle">{titulo}</h5>
                        </div>
                        <div className="modal-body">
                            {msj}
                        </div>
                        <div className="modal-footer">
                            {jsonPE &&
                            <button type="button" className="btn btn-warning" data-dismiss="modal">Cancelar</button>
                            }
                            {jsonPS &&
                            <div>
                                <a type="button" className="btn btn-primary" href="/especialidad">Guardar</a>
                            </div>
                            }
                        </div>
                    </div>
                </div>
            </div>

        </Fragment>
    );
}

export default withRouter(ActualizarEspecialidad);