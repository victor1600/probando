import React, { Fragment, useState } from 'react'
import { useForm } from 'react-hook-form'
import { withRouter } from "react-router-dom";


const AgregarExamen = (props) => {

    const [loginE] = useState(localStorage.getItem('login') ? true : false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token') : null)
    const [usuario] = useState(localStorage.getItem('usuario') ? localStorage.getItem('usuario') : null)



    const [jsonOE, setJsonOE] = React.useState(false);
    const [jsonOS, setJsonOS] = React.useState(false);
    const [msj, setMsj] = React.useState(['']);
    const [titulo, setTitulo] = React.useState(['']);

    const [examen, setTipoExamen] = React.useState([]);
    React.useEffect(() => {

        if (loginE === true) {

            const obtenerPermiso = async () => {
                const data = await fetch(`http://localhost:8080/usuario/${usuario}/ExamAdd`, {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });
                const info = await data.json();
                console.log(info);


                switch (data.status) {
                    case 200:
                        if (info.mensaje === "1") { } else {
                            props.history.push('/error403');
                        }
                        break;
                    default:
                        props.history.push('/error');
                }


            }

            const ObtenerExamen = async () => {
                const data = await fetch('http://localhost:8080/tipoexamenes/todas', {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });

                const dataExamen = await data.json();
                switch (data.status) {
                    case 403:
                        props.history.push('/error403');
                        break;
                    case 302:
                        setTipoExamen(dataExamen);
                        setJsonOS(true);
                        setJsonOE(false);
                        break;
                    case 404:
                        setJsonOS(false);
                        setJsonOE(true);
                        setMsj([
                            String(dataExamen.mensaje)
                        ]);
                        break;
                    default:
                        props.history.push('/error');
                }
            }
            obtenerPermiso();
            ObtenerExamen();
        } else {
            props.history.push('/login');
        }
    }, [props, loginE, token, usuario])

    const { register, handleSubmit, errors } = useForm();
    const onSubmit = async (data, e) => {
        e.preventDefault();
        const btnModal = document.getElementById("modal");
        //console.log(data)
        const jsonP = await fetch('http://localhost:8080/examen/agregar', {
            method: 'POST',
            body: JSON.stringify({

                imagenExamen: data.imagenExamen,
                videoExamen: data.videoExamen,
                documentoExamen: data.documentoExamen,
                fkTipoExamen: data.idTipoExamen
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                "Authorization": token
            }
        });


        console.log(jsonP);
        const info = await jsonP.json();

        if (jsonP.status === 201) {
            setJsonOE(false);
            setJsonOS(true);
            setMsj([
                String(info.mensaje)
            ]);
            setTitulo([
                String(info.httpCodeName)
            ]);
            btnModal.click();
            e.target.reset();

        } else {
            setJsonOE(true);
            setJsonOS(false);
            setMsj([
                String(info.mensaje)
            ]);
            setTitulo([
                String(info.httpCodeName)
            ]);
            btnModal.click();
        }


    }

    return (
        <Fragment>
            <div className="m-5">
                <div className="card">
                    <div className="card-header">
                        <h1>Formulario para agregar Examen</h1>
                    </div>
                    <div className="card-body">
                        <form className="form-horizontal" onSubmit={handleSubmit(onSubmit)}>
                            <div className="form-group">
                                <label className="control-label col-sm-2">Imagen:</label>
                                <div className="col-sm-10">
                                    <input type="text" className="form-control" placeholder="Ingrese imagen"
                                        name="imagenExamen"
                                        maxLength="50"
                                        ref={register({
                                            maxLength: { value: 100, message: "Maximo 10 caracteres" },
                                        })}
                                    />
                                    <span className="text-danger text-small d-block mb-2">
                                        {errors?.imagenExamen?.message}
                                    </span>
                                </div>
                            </div>

                            <div className="form-group">
                                <label className="control-label col-sm-2">videoExamen:</label>
                                <div className="col-sm-10">
                                    <input type="text" className="form-control" placeholder="Ingrese videoExamen"
                                        name="videoExamen"
                                        maxLength="100"
                                        ref={register({
                                            maxLength: { value: 100, message: "Maximo 100 caracteres" },
                                        })}
                                    />
                                    <span className="text-danger text-small d-block mb-2">
                                        {errors?.videoExamen?.message}
                                    </span>
                                </div>
                            </div>


                            <div className="form-group">
                                <label className="control-label col-sm-2">documentoExamen:</label>
                                <div className="col-sm-10">
                                    <input type="text" className="form-control" placeholder="Ingrese documentoExamen"
                                        name="documentoExamen"
                                        maxLength="100"
                                        ref={register({
                                            maxLength: { value: 100, message: "Maximo 100 caracteres" },
                                        })}
                                    />
                                    <span className="text-danger text-small d-block mb-2">
                                        {errors?.documentoExamen?.message}
                                    </span>
                                </div>
                            </div>



                            <div className="form-group">
                                <label className="control-label col-sm-2">Tipo de Examen:</label>
                                <div className="col-sm-10">
                                    <select className="form-control" name="idTipoExamen" value={examen.value}>
                                        {examen.map((e) => {
                                            return <option key={e.idTipoExamen} value={e.idTipoExamen}>{e.nombreTipoExamen}</option>;
                                        })}
                                    </select>
                                    <span className="text-danger text-small d-block mb-2">
                                        {errors?.idTipoExamen?.message}
                                    </span>
                                </div>
                            </div>

                            <div className="form-group">
                                <div className="col-sm-offset-2 col-sm-10">
                                    <button type="submit" className="btn btn-success">Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div className="m-5">
                        <a href="/examen" className="btn btn-info">Regresar</a>
                    </div>


                    <button id="modal" type="button" className="d-none" data-toggle="modal"
                        data-target="#exampleModalCenter">
                        Launch demo modal
                    </button>


                    <div className="modal fade" id="exampleModalCenter" tabIndex="-1" role="dialog"
                        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalCenterTitle">{titulo}</h5>
                                </div>
                                <div className="modal-body">
                                    {msj}
                                </div>
                                <div className="modal-footer">
                                    {jsonOE &&
                                        <button type="button" className="btn btn-warning"
                                            data-dismiss="modal">Cancelar</button>
                                    }
                                    {jsonOS &&
                                        <div>
                                            <button type="button" className="btn btn-secondary mr-3"
                                                data-dismiss="modal">Agregar otra
                                        </button>
                                            <a type="button" className="btn btn-primary"
                                                href="/examen">Guardar</a>
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </Fragment>
    );
}

export default withRouter(AgregarExamen);
