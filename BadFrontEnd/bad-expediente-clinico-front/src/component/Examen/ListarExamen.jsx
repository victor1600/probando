import React, { Fragment, useState } from 'react'
import {Link, withRouter} from "react-router-dom";


const ListarExamen = (props) => {

    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)

    const estilosInicio = {
        fondoHeader: {
            backgroundColor: '#80cbc4',
            textAlign: 'center'
        },
        fondoBody: {
            backgroundColor: '#F5F5F6'
        },
        letraCarrousel: {
            color: 'black'
        }
    };


    const [jsonOE, setJsonOE] = React.useState(false);
    const [jsonOS, setJsonOS] = React.useState(false);
    const [msj, setMsj] = React.useState(['']);
    const [titulo, setTitulo] = React.useState(['']);
    const [jsonEE, setJsonEE] = React.useState(false);
    const [jsonES, setJsonES] = React.useState(false);

    const [examen, setExamen] = React.useState([]);
    React.useEffect(() => {

        if (loginE === true) {
            const ObtenerExamen = async () => {
                const data = await fetch('http://localhost:8080/examen/todas', {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });             
                
                const dataExamen = await data.json();
                switch(data.status) {
                    case 403:
                        props.history.push('/error403');
                      break;
                    case 302:
                        setExamen(dataExamen);
                        setJsonOS(true);
                        setJsonOE(false);
                      break;
                    case 404:
                        setJsonOS(false);
                        setJsonOE(true);
                        setMsj([
                        String(dataExamen.mensaje)
                    ]);
                        break;
                    default:
                        props.history.push('/error');
                }
            }

        ObtenerExamen();
    } else {
        props.history.push('/login');
    }
    }, [props, loginE,token])
   
    const eliminarExamen = async (e) => {
        const btnCerrar = document.getElementById(`btn-${e.target.value}`)
        const btnInfo = document.getElementById('btn-infoDelete')
        const dataE = await fetch(`http://localhost:8080/examen/eliminar/${e.target.value}`, {
            method: 'DELETE',
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                "Authorization": token
            }
        })   
        const infoEP = await dataE.json()


        if (dataE.status === 200) {
            setJsonEE(false)
            setJsonES(true)
            setTitulo([
                String(infoEP.httpCodeName)
            ]);
            setMsj([
                String(infoEP.mensaje)
            ]);
        } else {
            setJsonEE(true)
            setJsonES(false)
            setTitulo([
                String(infoEP.httpCodeName)
            ]);
            setMsj([
                String(infoEP.mensaje)
            ]);
        }

        btnCerrar.click()
        btnInfo.click()
    }

            
    return (
        <Fragment>
            <div className="m-5">
                <div className="card">
                    <div className="card-header" style={estilosInicio.fondoHeader}>
                        <h1>Listado de Examenes</h1>
                    </div>
                    <div className="card-body" style={estilosInicio.fondoBody}>
                        {jsonOS &&

                        <div className="table-responsive">
                            <table className="table table-hover">
                                <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Imagen</th>
                                    <th>Video</th>
                                    <th>Documento</th>
                                    <th>TipoExamen</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                {examen.map((item) => (
                                    <tr key={item.idExamen}>
                                    <td>
                                        <Link to={`/examen/${item.idExamen}`}>
                                        {item.idExamen}
                                        </Link>
                                    </td>
                                    <td>
                                        {item.imagenExamen}
                                    </td>
                                    <td>
                                        {item.videoExamen}
                                    </td>
                                    <td>
                                        {item.documentoExamen}
                                    </td>
                                    <td>
                                        {item.tipoExamen.nombreTipoExamen}
                                    </td>
                                    
                                    <td><a href={`/examen/actualizar/${item.idExamen}`}
                                           className="btn btn-warning">Editar</a>
                                           </td>
                                    <td><button type="button" className="btn btn-danger" data-toggle="modal"
                                                data-target={`#modal-${item.idExamen}`}>
                                                Eliminar
                                        </button>
                                    </td>
                                </tr>
                                    
                                ))}
                                </tbody>
                            </table>
                        </div>


                        }
                        {
                            jsonOE &&
                            <div className="m-5">
                                <h1>{msj}</h1>
                            </div>
                        }
                        <a href="/examen/agregar" className="btn btn-primary">Agregar</a>

                    </div>

                    {examen.map((item) => (
                        <div key={item.idExamen} className="modal fade" id={`modal-${item.idExamen}`}
                             tabIndex="-1"
                             role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="exampleModalLongTitle">Advertencia
                                            Eliminacion</h5>
                                    </div>
                                    <div className="modal-body">
                                    <p>¿Usted desea eliminar Examen con: 
                                        idDeExamen={item.idExamen},                                        
                                        imagenDeExamen={item.imagenExamen},                                    
                                        videoDeExamen={item.videoExamen},                                    
                                        DocumentoDeExamen={item.documentoExamen}                                    
                                        TipoDeExamen={item.tipoExamen.nombreTipoExamen}
                                        ?</p>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" id={`btn-${item.idExamen}`}
                                                className="btn btn-secondary"
                                                data-dismiss="modal">Cancelar
                                        </button>
                                        <button type="button" className="btn btn-success" data-dismiss="modal" value={item.idExamen} onClick={eliminarExamen}>Si, Deseo Eliminar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}

                    <button id="btn-infoDelete" type="button" className="d-none" data-dismiss="modal"
                            data-toggle="modal"
                            data-target="#infoDelete">
                        info del modal
                    </button>


                    <div className="modal fade" id="infoDelete" tabIndex="-1" role="dialog"
                         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalLongTitle">{titulo}</h5>
                                </div>
                                <div className="modal-body">
                                    {msj}
                                </div>
                                <div className="modal-footer">
                                    {jsonES &&
                                    <a type="button" href="/examen" className="btn btn-primary">Enterado</a>
                                    }
                                    {jsonEE &&
                                    <button type="button" className="btn btn-warning"
                                            data-dismiss="modal">Enterado</button>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            
        </Fragment>
    );
}

export default withRouter(ListarExamen);
