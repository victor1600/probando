import React, {Fragment, useState} from 'react'
import {withRouter} from "react-router-dom";


//ruteo
import {useParams} from "react-router-dom";

const DetalleExamen = (props) => {
    const estilosInicio = {
        fondoHeader: {
            backgroundColor: '#80cbc4',
            textAlign: 'center'
        },
        fondoBody: {
            backgroundColor: '#F5F5F6'
        },
        letraCarrousel: {
            color: 'black'
        }
    };

    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)

    const {idExamen} = useParams();

    const [jsonOE, setJsonOE] = React.useState(false);
    const [jsonOS, setJsonOS] = React.useState(false);
    const [msj, setMsj] = React.useState(['']);

    const [examen, setExamen] = React.useState([]);
    const [tipoExamen, setTipoExamen] = React.useState([]);

    React.useEffect(() => {
        if(loginE === true){
        const obtenerExamen = async () => {
            const data = await fetch(`http://localhost:8080/examen/${idExamen}`, {
                method: 'GET',
                headers: {
                    "Content-type": "application/json; charset=UTF-8",
                    "Authorization": token
                }
            });                

            const dataExamen = await data.json();
            //const infoTipo = dataExamen.tipoExamen;
            switch(data.status) {
                case 403:
                    props.history.push('/error403');
                  break;
                case 302:
                    setJsonOS(true);
                    setJsonOE(false);
                    setExamen(dataExamen);
                    setTipoExamen(dataExamen.tipoExamen);
                  break;
                case 404:
                    setJsonOS(false);
                    setJsonOE(true);
                    setMsj([
                    String(dataExamen.mensaje)
                    ]);
                  break;
                default:
                    props.history.push('/error');
              }
    
        }

        obtenerExamen();

    }else{
        props.history.push('/login');
    } 
}, [idExamen,props,token,loginE])


    return (
        <Fragment>
            <div className="m-5">
                <div className="card">
                    <div className="card-header" style={estilosInicio.fondoHeader}>
                        <h2>Los datos de examen con id {idExamen} son:</h2>
                    </div>
                    <div className="card-body" style={estilosInicio.fondoBody}>
                        {jsonOS &&
                        <div>
                            <br/><br/>
                            <ul>
                                <li>idDeExamen={examen.idExamen}</li>
                                <li>imagenDeExamen={examen.imagenExamen}</li>
                                <li>videoDeExamen={examen.videoExamen}</li>
                                <li>DocumentoDeExamen={examen.documentoExamen}</li>
                                <li>TipoDeExamen={tipoExamen.nombreTipoExamen}</li>
                            </ul>
                               
                                </div>
                        }
                        {
                            jsonOE &&
                            <div className="m-5">
                                <h1>{msj}</h1>
                            </div>
                        }
                        <a href="/examen" className="btn btn-info">Regresar</a>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default withRouter(DetalleExamen);