import React, { Fragment, useState} from 'react'
import { useForm } from 'react-hook-form'

import './LoginForm.css';


const LoginForm = () => {


    const [msj, setMsj] = useState(['']);
    const [usuario, setUsuario] = useState(['']);
    const [token, setToken] = useState(['']);
    const [tokenTime, setTokenTime] = useState(['']);
    const [titulo, setTitulo] = useState(['']);

    const { register, handleSubmit, errors } = useForm()
    const onSubmit = async(data,e) => {
        const btnLogin = document.getElementById("login");
        setMsj(['']) ;
        setUsuario(['']) ;
        e.preventDefault();
        const btnModal = document.getElementById("modal");
        const jsonR = await fetch('http://localhost:8080/login', {
            method: 'POST',
            body: JSON.stringify({
                user: data.usuario,
                password: data.clave,
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        });      
        const info = await jsonR.json();
        if(jsonR.status === 200){
            console.log(info);
            e.target.reset();
            setMsj([
                String(info.mensaje)
            ]);
            setTitulo([
                String(info.httpCodeName)
            ]);
            setUsuario([
                String(data.usuario)
            ]);
            localStorage.setItem('usuario', String(data.usuario))            
            setToken([
                String(info.tokenUsuario)
            ]);
            localStorage.setItem('token', String(info.tokenUsuario))
            setTokenTime([
                String(info.tokenTiempo)
            ]);
            localStorage.setItem('login', true)

            btnLogin.click()
        }else{
            console.log(info);
            setMsj([
                String(info.mensaje)
            ]);
            setTitulo([
                String(info.httpCodeName)
            ]);
            btnModal.click()
        }
    }

    const tokenSend = async (e) => {
        const dataT = await fetch(`http://localhost:8080/usuario/actualizar2/${usuario}`, {
            method: 'PUT',
            body: JSON.stringify({
                "tokenUsuario": "\""+token+"\"",
                "tokenTiempo": "\""+tokenTime+"\""
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                "Authorization": token
            }
        })

        const infoEP = await dataT.json()
        console.log(token)
        console.log(tokenTime)
        console.log(usuario)
        console.log(dataT)

        console.log(infoEP)
    }


    return ( 
        <Fragment>

            <div className="loginContenedor">
                <div className="d-flex justify-content-center h-100">
                    <div className="card cardLogin">
                        <div className="card-header cardHLogin">
                            <h3>Iniciar Sesión</h3>
                        </div>
                        <div className="card-body">
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <div className="input-group form-group">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text loginInputS"><i className="fas fa-user"></i></span>
                                    </div>
                                    <input type="text" className="form-control loginInputF" placeholder="usuario"
                                    name="usuario"
                                    ref={register({
                                        required : {value:true, message:'Usuario obligatorio'},
        
                                    })} />
                                    <span className="text-danger text-small d-block mb-2">
                                        {errors?.usuario?.message}
                                    </span>

                                </div>
                                <div className="input-group form-group">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text loginInputS"><i className="fas fa-key"></i></span>
                                    </div>
                                    <input type="password" className="form-control loginInputF" placeholder="contraseña"
                                    name="clave"
                                    title="Solo letras"
                                    ref={register({
                                        required : {value:true, message:'Apellido obligatorio'},
        
                                    })}></input>
                                    <span className="text-danger text-small d-block mb-2">
                                        {errors?.clave?.message}
                                    </span>
                                </div>
                                {/* <div className="row align-items-center remember">
                                    <input type="checkbox" />Remember Me
                                </div> */}
                                <div className="form-group">
                                    <input type="submit" value="Login" className="btn float-right login_btn" />
                                </div>
                            </form>
                        </div>
                        <div className="card-footer">
                            <div className="d-flex justify-content-center links">
                                <a href="/">¿Olvidó la contraseña?</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <a id="login" type="button" className="d-none" href="/" onClick={tokenSend} >Success</a>


            <div>

                <button id="modal" type="button" className="d-none" data-toggle="modal" data-target="#exampleModalCenter">
                    Modal
                </button>

                <div className="modal fade" id="exampleModalCenter" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalCenterTitle">{titulo}</h5>
                            </div>
                            <div className="modal-body">
                                {msj}
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-warning" data-dismiss="modal">De acuerdo!</button>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </Fragment>
     );
}

export default LoginForm;