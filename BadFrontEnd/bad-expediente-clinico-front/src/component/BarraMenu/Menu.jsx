import React, { Fragment, useState } from 'react';

//Logo
import Logo from './Logo.png'

const Menu = () => {

    // Como no hemos echo la tabla menu lo haremos manual luego esto lo hara un for el active
    const inicioActive = window.location.pathname === "/" ? "active" : ""
    const pacientesActive = window.location.pathname === "/pacientes" ? "active" : ""
    const doctoresActive = window.location.pathname === "/doctores" ? "active" : ""
    const tipoMedicamentosActive = window.location.pathname === "/tipoMedicamentos" ? "active" : ""
    const medicamentosActive = window.location.pathname === "/medicamentos" ? "active" : ""
    const medicinaActive = (medicamentosActive === "active" || tipoMedicamentosActive ==="active") ? "active" : ""
    
    const examenesActive = window.location.pathname === "/" ? "active" : ""
    const consultaActive = window.location.pathname === "/" ? "active" : ""
    const personalMedicoActive = window.location.pathname === "/" ? "active" : ""


    // Fin de navActive

    const estilosMenu = {
        navBar: {
            backgroundColor: '#00897b',
        },
        navIcon:{
            fontSize: '20px'
            //color: 'black'
        },
        logoNav: {
            width: '50px'
        },
        dropdown: {
            backgroundColor: '#00897b',
            width: '200px'
        }
    };

    const [usuario] = useState(localStorage.getItem('usuario') ? localStorage.getItem('usuario'):'No ha iniciado sesion')

    const cerrarSesion = () =>{
        localStorage.clear()
        window.location.reload();
    }


    return (
        <Fragment>
            <nav id="fijo" name="fijo" className="navbar navbar-expand-lg navbar-dark static-top" style={estilosMenu.navBar}>
                <div className="container">
                    <a className="navbar-brand" href="/">
                        <img src={Logo} alt="Logo" style={estilosMenu.logoNav} />
                    </a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse " id="navbarResponsive">
                        <ul className="navbar-nav ml-auto">
                            <li className={`nav-item ${inicioActive} d-flex align-items-center`}>
                                <a className="nav-link" href="/"><span style={estilosMenu.navIcon}><i className="fas fa-home mr-1"></i>Inicio</span></a>
                            </li>

                            {/* Doctores. */}
                            <li className={`nav-item dropdown ${doctoresActive} d-flex align-items-center`}>
                                <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={estilosMenu.navIcon}>

                                    <span style={estilosMenu.navIcon}><i className="far fa-address-card"></i> Doctores</span>
                                </a>
                                <div className="dropdown-menu" style={estilosMenu.dropdown} aria-labelledby="navbarDropdown">
                                    <a className="nav-link" href="/doctores"><span style={estilosMenu.navIcon}><i className="far fa-address-card"></i> Doctores</span></a>
                                    <a className="nav-link" href="/especialidad"><span style={estilosMenu.navIcon}><i className="fas fa-user-md"></i> Especialidad</span></a>
                                </div>
                            </li>

                             {/* Pacientes. */}
                             <li className={`nav-item dropdown ${pacientesActive} d-flex align-items-center`}>
                                <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={estilosMenu.navIcon}>

                                    <span style={estilosMenu.navIcon}><i className="fas fa-hospital-user"></i> Pacientes</span> <i ></i>

                                </a>
                                <div className="dropdown-menu" style={estilosMenu.dropdown} aria-labelledby="navbarDropdown">
                                    <a className="nav-link" href="/pacientes"><span style={estilosMenu.navIcon}><i className="fas fa-user-injured mr-1"></i> Pacientes</span></a>
                                    <a className="nav-link" href="/responsables"><span style={estilosMenu.navIcon}><i className="fas fa-user"></i> Responsables</span></a>
                                </div>
                            </li>

                            {/* Personal Medico. */}
                            <li className={`nav-item dropdown ${personalMedicoActive} d-flex align-items-center`}>
                                <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={estilosMenu.navIcon}>

                                    <span style={estilosMenu.navIcon}><i className="fas fa-hospital-user"></i> Personal Medico</span>
                                </a>
                                <div className="dropdown-menu" style={estilosMenu.dropdown} aria-labelledby="navbarDropdown">
                                    <a className="nav-link" href="/PersonalMedico"><span style={estilosMenu.navIcon}><i className="fas fa-user-injured mr-1"></i> Personal</span></a>
                                    <a className="nav-link" href="/tipoPersonalMedico"><span style={estilosMenu.navIcon}><i className="fas fa-user-injured mr-1"></i> Tipo Personal</span></a>
                                </div>
                            </li>

                            {/* Consulta. */}
                            <li className={`nav-item dropdown ${consultaActive} d-flex align-items-center`}>
                                <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={estilosMenu.navIcon}>

                                    <span style={estilosMenu.navIcon}><i className="fas fa-user-nurse"></i> Consulta</span>
                                </a>
                                <div className="dropdown-menu" style={estilosMenu.dropdown} aria-labelledby="navbarDropdown">
                                    <a className="nav-link" href="/signosVitales"><span style={estilosMenu.navIcon}><i className="fas fa-file-medical-alt"></i> Signos Vitales</span></a>
                                    <a className="nav-link" href="/cie10"><span style={estilosMenu.navIcon}><i className="fas fa-book-medical"></i> CIE10</span></a>
                                </div>
                            </li>

                            {/* Medicamentos! */}
                            <li className={`nav-item dropdown ${medicinaActive} d-flex align-items-center`}>
                                <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={estilosMenu.navIcon}>

                                    <span style={estilosMenu.navIcon}><i className="fas fa-prescription-bottle"></i> Medicina</span>
                                </a>
                                <div className="dropdown-menu" style={estilosMenu.dropdown} aria-labelledby="navbarDropdown">
                                    <a className="nav-link" href="/medicamentos"><span style={estilosMenu.navIcon}><i className="fas fa-pills"></i> Medicamentos</span></a>
                                    <a className="nav-link" href="/tipoMedicamentos"><span style={estilosMenu.navIcon}><i className="fas fa-prescription-bottle-alt"></i> Tipos</span></a>
                                </div>
                            </li>

                            {/* Examenes. */}
                            <li className={`nav-item dropdown ${examenesActive} d-flex align-items-center`}>
                                <a className="nav-link dropdown-toggle" href="/" id="navbarDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={estilosMenu.navIcon}>

                                    <span style={estilosMenu.navIcon}><i className="fas fa-microscope"></i> Examenes</span>
                                </a>
                                <div className="dropdown-menu" style={estilosMenu.dropdown} aria-labelledby="navbarDropdown">
                                    <a className="nav-link" href="/tipoExamenes"><span style={estilosMenu.navIcon}><i className="fas fa-vials"></i> Tipo Examenes</span></a>
                                </div>
                            </li>

                        

                            <li className="nav-item avatar dropdown">
                                <a className="nav-link dropdown-toggle" href="/" id="navbarDropdownMenuLink-55" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <img src="https://mdbootstrap.com/img/Photos/Avatars/avatar-2.jpg" className="rounded-circle z-depth-0" alt="User" width="50px" height="50px" />
                                </a>
                                <div className="dropdown-menu dropdown-menu-lg-right dropdown-secondary"
                                    aria-labelledby="navbarDropdownMenuLink-55">
                                    <a className="dropdown-item" href="/">{usuario}</a>
                                    <button className="dropdown-item" onClick={cerrarSesion}>Cerrar Sesion</button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </Fragment>
    );
}

export default Menu;