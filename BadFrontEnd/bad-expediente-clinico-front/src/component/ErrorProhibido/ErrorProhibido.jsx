import React, { Fragment, useState } from 'react'
import {withRouter} from "react-router-dom";

import './ErrorProhibido.css';

const ErrorProhibido = (props) => {
    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const regresar = (e) => {
        window.history.back();
    }

    React.useEffect(() => {       
        

        if(loginE === true){}else{
            props.history.push('/login');
        }


    }, [loginE, props])
    return (
        <Fragment>
            <div className="error-content">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 ">
                            <div className="error-text">
                                <h1 className="error">403 Error</h1>
                                <div className="im-sheep">
                                    <div className="top">
                                        <div className="body"></div>
                                        <div className="head">
                                            <div className="im-eye one"></div>
                                            <div className="im-eye two"></div>
                                            <div className="im-ear one"></div>
                                            <div className="im-ear two"></div>
                                        </div>
                                    </div>
                                    <div className="im-legs">
                                        <div className="im-leg"></div>
                                        <div className="im-leg"></div>
                                        <div className="im-leg"></div>
                                        <div className="im-leg"></div>
                                    </div>
                                </div>
                                <h4 className="errorPH4">¡Oops! ¡Permiso Negado!</h4>
                                <p className="errorPP">Lo siento, no tienes los permisos suficientes para hacer esta accion.</p>
                                <button className="btn btn-primary btn-round" onClick={regresar}  >Regresar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default withRouter(ErrorProhibido)
