import React, {Fragment, useState} from 'react'
import {withRouter} from "react-router-dom";


const Inicio = (props) => {

    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    
    


    React.useEffect(() => {       
        

        if(loginE === true){}else{
            props.history.push('/login');
        }


    }, [loginE, props])
    




    const estilosInicio= {
        fondoHeader: {
            backgroundColor: '#80cbc4',
            textAlign: 'center'
        },
        fondoBody: {
            backgroundColor: '#F5F5F6'
        },
        letraCarrousel: {
            color: 'black'
        }
    };

    return (
        <Fragment>

            <div className="m-5">
                <div className="card">
                    <div className="card-header" style={estilosInicio.fondoHeader}>
                        <h1>Sistema Informatico  Expediente Clinico</h1>          
                    </div>
                    <div className="card-body" style={estilosInicio.fondoBody}>

                        <div id="demo" className="carousel slide" data-ride="carousel">
                            <ul className="carousel-indicators">
                                <li data-target="#demo" data-slide-to="0" className="active"></li>
                                <li data-target="#demo" data-slide-to="1"></li>
                                <li data-target="#demo" data-slide-to="2"></li>
                            </ul>
                            <div className="carousel-inner">
                                <div className="carousel-item active">
                                    <img src={require('./Medico.jpg')} alt="Los Angeles" width="1250" height="500" />
                                    <div className="carousel-caption">
                                        <h3 style={estilosInicio.letraCarrousel}>Los Angeles</h3>
                                        <p style={estilosInicio.letraCarrousel}>We had such a great time in LA!</p>
                                    </div>
                                </div>
                                <div className="carousel-item">
                                    <img src={require('./hospital1.jpg')} alt="Chicago" width="1250" height="500" />
                                    <div className="carousel-caption">
                                        <h3 style={estilosInicio.letraCarrousel}>Chicago</h3>
                                        <p style={estilosInicio.letraCarrousel}>Thank you, Chicago!</p>
                                    </div>
                                </div>
                                <div className="carousel-item">
                                    <img src={require('./laboratorio.jpg')} alt="New York" width="1250" height="500" />
                                    <div className="carousel-caption">
                                        <h3 style={estilosInicio.letraCarrousel}>New York</h3>
                                        <p style={estilosInicio.letraCarrousel}>We love the Big Apple!</p>
                                    </div>
                                </div>
                            </div>
                            <a className="carousel-control-prev" href="#demo" data-slide="prev">
                                <span className="carousel-control-prev-icon"></span>
                            </a>
                            <a className="carousel-control-next" href="#demo" data-slide="next">
                                <span className="carousel-control-next-icon"></span>
                            </a>
                        </div>


                    </div>
                </div>
            </div>


        </Fragment>
      );
}
 
export default withRouter(Inicio);