import React, {Fragment, useState} from 'react'
import {useParams, withRouter} from "react-router-dom";

const DetalleResponsable = (props) => {
    const estilosInicio = {
        fondoHeader: {
            backgroundColor: '#80cbc4',
            textAlign: 'center'
        },
        fondoBody: {
            backgroundColor: '#F5F5F6'
        },
        letraCarrousel: {
            color: 'black'
        }
    };

    // Validar Acceso
    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)

    // Obtener Responsable
    const [jsonGE, setJsonGE] = React.useState(false);
    const [jsonGS, setJsonGS] = React.useState(false);
    const [msjGet, setMsjGet] = React.useState(['']);
    const [responsable, setResponsable] = React.useState([]);
    const {idPersona} = useParams();
    
    React.useEffect(() => {
        if(loginE === true){
            const obtenerResponsable = async () => {
                const data = await fetch(`http://localhost:8080/responsables/${idPersona}`, {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });  
                                    
                const dataResponsable = await data.json();
                switch(data.status) {
                    case 403:
                        props.history.push('/error403');
                      break;
                    case 302:
                        setJsonGS(true);
                        setJsonGE(false);
                        setResponsable(dataResponsable);
                      break;
                    case 404:
                        setJsonGS(false);
                        setJsonGE(true);
                        setMsjGet([
                            String(dataResponsable.mensaje)
                        ]);
                      break;
                    default:
                        props.history.push('/error');
                  }
            }
            obtenerResponsable()

        }else{
            props.history.push('/login');
        } 
    },[idPersona, props, token, loginE])

    return (
        <Fragment>
            <div className="m-5">
                <div className="card">
                    <div className="card-header" style={estilosInicio.fondoHeader}>
                        <h2>Datos del Responsable</h2>
                    </div>
                    <div className="card-body" style={estilosInicio.fondoBody}>
                        {jsonGS &&
                        <div>
                            <ul>
                            <li> <b>Dirección:</b> {responsable?.direccion?.urbanizacion} {responsable?.direccion?.calleAvenida} {responsable?.direccion?.numeroPasaje} {responsable?.direccion?.numeroCasa} <b> /</b> {responsable?.direccion?.municipio?.nombreMunicipio} 
                                        <b> / </b> {responsable?.direccion?.municipio?.departamento?.nombreDepartamento}</li>
                                <li> <b>Nombres:</b> {responsable.nombre1Persona} {responsable.nombre2Persona} </li>
                                <li> <b>Apellidos:</b> {responsable.apellido1Persona} {responsable.apellido2Persona}</li>
                                <li> <b>Fecha de nacimiento:</b> {responsable.fechaNacimientoPersona}</li>
                                <li> <b>Teléfono fijo: </b> {responsable.telefonoPersona===null ? 'No registrado': responsable.telefonoPersona}<b> Celular: </b> {responsable.celularPersona} </li>
                            </ul>
                            
                        </div>
                        }
                        {jsonGE &&
                            <div className="m-5">
                                <h1>{msjGet}</h1>
                            </div>
                        }
                        <br/>
                        <div className="row col-sm-12">
                            <div className="col-sm-2">
                                <div className="form-group ">
                                <a href="/responsables" className="btn btn-primary">Regresar</a>
                                </div>
                            </div>
                            <div className="col-sm-2">
                                <div className="form-group ">
                                    <a href={`/responsables/actualizar/${idPersona}`} className="btn btn-warning">Editar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default  withRouter(DetalleResponsable);