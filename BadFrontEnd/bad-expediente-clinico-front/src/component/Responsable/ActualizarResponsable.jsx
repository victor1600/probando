import React, {Fragment, useState} from 'react'
import {useForm} from 'react-hook-form'
import {useParams, withRouter} from "react-router-dom";

const ActualizarResponsable = (props) => {

    const estilosInicio = {
        fondoHeader: {
            backgroundColor: '#80cbc4',
            textAlign: 'center'
        },
        fondoBody: {
            backgroundColor: '#F5F5F6'
        },
        letraCarrousel: {
            color: 'black'
        }
    };
    
    // Validar Acceso
    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)
    const {idPersona} = useParams(); 

    // Obtener Responsable
    const [jsonGE, setJsonGE] = React.useState(false);
    const [jsonGS, setJsonGS] = React.useState(false);
    const [msjGet, setMsjGet] = React.useState(['']);
    const [responsable, setResponsable] = React.useState([]);

    React.useEffect(() => { 

        if(loginE === true){
            const obtenerResponsable = async () => {
                const data = await fetch(`http://localhost:8080/responsables/${idPersona}`,{
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });
                const dataResponsable = await data.json();

                switch(data.status) {
                    case 403:
                        props.history.push('/error403');
                        break;
                    case 302:
                        setJsonGS(true);
                        setJsonGE(false);
                        setResponsable(dataResponsable);
                        break;
                    case 404:
                        setJsonGS(false);
                        setJsonGE(true);
                        setMsjGet([
                        String(dataResponsable.mensaje)
                        ]);
                        break;
                    default:
                        props.history.push('/error');
                    }

            }
            obtenerResponsable();
            
        }else{
            props.history.push('/login');
        }

    }, [idPersona, props, loginE,token])

    //Actualizar responsable
    const [jsonUE, setJsonUE] = React.useState(false);
    const [jsonUS, setJsonUS] = React.useState(false);
    const [msjUpdate, setMsjUpdate] = React.useState(['']);
    const [tituloUpdate, setTituloUpdate] = React.useState(['']);
    
    const { register, handleSubmit, errors } = useForm();
    const onSubmit = async (data, e) => {
        e.preventDefault();
        const btnModal = document.getElementById("modal");
        console.log(data)
        const jsonP = await fetch(`http://localhost:8080/responsables/actualizar/${idPersona}`,{
            method: 'PUT',
            body: JSON.stringify({
                idPersona: responsable.idPersona,
                nombre1Persona: data.nombre1Responsable,
                nombre2Persona: data.nombre2Responsable,
                nombre3Persona: data.nombre3Responsable,
                apellido1Persona: data.apellido1Responsable,
                apellido2Persona: data.apellido2Responsable,
                apellido3Persona: data.apellido3Responsable,
                duiPersona: data.duiResponsable,
                nitPersona: data.nitResponsable,
                direccion: {
                    idDireccion: responsable?.direccion.idDireccion,
                    urbanizacion: data.urbanizacion,
                    calleAvenida: data.calleAvenida,
                    numeroPasaje: data.numeroPasaje,
                    numeroCasa: data.numeroCasa,
                    complementoDireccion: data.complementoDireccion,
                    municipio: {
                        idMunicipio: data.idMunicipio,
                        departamento: {
                            idDepartamento: data.idDepartamento,
                        }
                    }
                },
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                "Authorization": token
            }
        });
        console.log(jsonP);
        const info = await jsonP.json();

        if (jsonP.status === 201) {
            setJsonUE(false);
            setJsonUS(true);
            setMsjUpdate([
                String(info.mensaje)
            ]);
            setTituloUpdate([
                String(info.httpCodeName)
            ]);
            btnModal.click();
            e.target.reset();

        } else {
            setJsonUE(true);
            setJsonUS(false);
            setMsjUpdate([
                String(info.mensaje)
            ]);
            setTituloUpdate([
                String(info.httpCodeName)
            ]);
            btnModal.click();
        }
    }

    // Obtener Departamentos
    const [deptId, setDeptId] = useState();  //Estado select 

    const [jsonED, setJsonED] = React.useState(false);
    const [jsonSD, setJsonSD] = React.useState(false);
    const [msjDept, setMsjDept] = React.useState(['']);
    const [departamentos, setDepartamentos] = React.useState([]);

    React.useEffect(() => {
        if (loginE === true) {

            const obtenerDepartamentos = async () => {
                const data = await fetch('http://localhost:8080/departamentos/todos', {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });
                console.log(data.status);
                console.log(data.Departamentos);
                const dataDepartamentos = await data.json();
                if (data.status === 302) {
                    setJsonSD(true);
                    setJsonED(false);
                    setDepartamentos(dataDepartamentos);
                } else {
                    setJsonSD(false);
                    setJsonED(true);
                    setMsjDept([
                        String(dataDepartamentos.mensaje)
                    ]);
                }
            }
            obtenerDepartamentos();
            
        } else {
            props.history.push('/login');
        }
    }, [props, loginE, token])

    // Obtener Municicpios
    const [jsonEM, setJsonEM] = React.useState(false);
    const [jsonSM, setJsonSM] = React.useState(false);
    const [msjMuni, setMsjMuni] = React.useState(['']);
    const [municipios, setMunicipios] = React.useState([]);

    React.useEffect(() => {
        if (loginE === true) {

            const obtenerMunicipios = async () => {
                const data = await fetch('http://localhost:8080/municipios/todos', {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });
                console.log(data.status);
                console.log(data.Municipios);
                const dataMunicipios = await data.json();
                if (data.status === 302) {
                    setJsonSM(true);
                    setJsonEM(false);
                    setMunicipios(dataMunicipios);
                } else {
                    setJsonSM(false);
                    setJsonEM(true);
                    setMsjMuni([
                        String(dataMunicipios.mensaje)
                    ]);
                }
            }
            obtenerMunicipios();
            
        } else {
            props.history.push('/login');
        }
    }, [props, loginE, token])

    return (
        <Fragment>
            <div className="m-5">
                <div className="card">
                    <div className="card-header" style={estilosInicio.fondoHeader}>
                        <h2>Actualizar responsable</h2>
                    </div>
                    {jsonGS &&
                    
                    <div className="card-body" style={estilosInicio.fondoBody}>
                        <form className="form-horizontal" onSubmit={handleSubmit(onSubmit)}>
                            
                            {/* Información Personal */}
                            <div className="card-body">
                                <h5>Información Personal:</h5><br/>
                                
                                <div className="row col-sm-12">
                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Primer Nombre:</label>
                                            <input type="text" className="form-control" name="nombre1Responsable"
                                                required maxLength="20" pattern="([A-Za-z ]+)" defaultValue={responsable.nombre1Persona}
                                                ref={register({
                                                    required: { value: true, message: "El primer nombre es obligatorio" },
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.responsable?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Segundo Nombre:</label>
                                            <input type="text" className="form-control" name="nombre2Responsable"
                                                maxLength="20" pattern="([A-Za-z ]+)" defaultValue={responsable.nombre2Persona}
                                                ref={register({
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.responsable?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Tercer Nombre:</label>
                                            <input type="text" className="form-control" name="nombre3Responsable"
                                                maxLength="20" pattern="([A-Za-z ]+)" defaultValue={responsable.nombre3Persona}
                                                ref={register({
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.responsable?.message}
                                            </span>
                                        </div>
                                    </div>
                                </div>{/* Fin nombres */}

                                <div className="row col-sm-12">
                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Primer Apellido:</label>
                                            <input type="text" className="form-control" name="apellido1Responsable"
                                                required maxLength="20" pattern="([A-Za-z ]+)" defaultValue={responsable.apellido1Persona}
                                                ref={register({
                                                    required: { value: true, message: "El primer apellido es obligatorio" },
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.responsable?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Segundo Apellido:</label>
                                            <input type="text" className="form-control" name="apellido2Responsable"
                                                maxLength="20" pattern="([A-Za-z]+)" defaultValue={responsable.apellido2Persona}
                                                ref={register({
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.responsable?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Tercer Apellido:</label>
                                            <input type="text" className="form-control" name="apellido3Responsable"
                                                maxLength="20" pattern="([A-Za-z ]+)" defaultValue={responsable.apellido3Persona}
                                                ref={register({
                                                    maxLength: { value: 20, message: "Máximo 20 caracteres" },
                                                    pattern: { value: /[A-Za-z]/, message: "Solamente letras" }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.responsable?.message}
                                            </span>
                                        </div>
                                    </div>
                                </div> {/* Fin apellidos */}

                                <div className="row col-sm-12">
                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">DUI:</label>
                                            <input type="text" className="form-control" name="duiResponsable"
                                                required maxLength="10" defaultValue={responsable.duiPersona}
                                                ref={register({
                                                    maxLength: { value: 10, message: "Máximo 10 caracteres" },
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.responsable?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">NIT:</label>
                                            <input type="text" className="form-control" name="nitResponsable"
                                                required maxLength="17" defaultValue={responsable.nitPersona}
                                                ref={register({
                                                    maxLength: { value: 17, message: "Máximo 17 caracteres" },
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.responsable?.message}
                                            </span>
                                        </div>
                                    </div>
                                </div> {/* DUI - NIT */}
                            </div>
                            {/* Fin Seccion 1*/}

                            {/* Información Responsable */}
                            <div className="card-body">
                                <h5>Información de Contacto:</h5><br/>

                                <div className="row col-sm-12">
                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Teléfono Fijo:</label>
                                            <input type="text" className="form-control" name="telefonoResponsable" maxLength="9"
                                                    defaultValue={responsable.telefonoPersona}
                                                    ref={register({
                                                    maxLength: { value: 9, message: "Máximo 9 caracteres" },
                                                    pattern: { value: /[0-9]/, message: "Solamente números y guion medio." }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.responsable?.message}
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Celular:</label>
                                            <input type="text" className="form-control" name="celularResponsable" maxLength="9"
                                                    defaultValue={responsable.celularPersona}
                                                    ref={register({
                                                    maxLength: { value: 9, message: "Máximo 9 caracteres" },
                                                    pattern: { value: /[0-9]/, message: "Solamente números y guion medio." }
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.responsable?.message}
                                            </span>
                                        </div>
                                    </div>

                                </div>
                                {/* Fin telefonos */}
                                
                                <div className="row col-sm-12">
                                    {jsonSD &&
                                        <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Departamento:</label>
                                            <select value={deptId} onChange={e => setDeptId(e.target.value)} 
                                                id="idDepartamento" name="idDepartamento" className="browser-default custom-select"
                                                ref={register({
                                                    required: { value: true, message: "El campo departamento es obligatorio" }
                                                })}>
                                                <option value={responsable?.direccion?.municipio?.departamento?.idDepartamento}>{responsable?.direccion?.municipio?.departamento?.nombreDepartamento}</option>
                                                {departamentos.filter(item => item.idDepartamento !== responsable?.direccion?.municipio?.departamento?.idDepartamento).map((item) => (
                                                    <option key={item.idDepartamento} value={item.idDepartamento}>{item.nombreDepartamento}</option>
                                                ))}
                                            </select>
                                        </div>
                                    </div>
                                    }
                                    {jsonED &&
                                        <div className="m-5">
                                            <h5><li>{msjDept}</li></h5>
                                        </div>
                                    }
                                    {jsonSM &&
                                        <div className="col-xs-12 col-sm-6 col-md-4">
                                        <div className="form-group">
                                            <label className="control-label col-sm-12">Municipio:</label>
                                            <select id="idMunicipio" name="idMunicipio" className="browser-default custom-select"
                                                ref={register({
                                                    required: { value: true, message: "El campo municipio es obligatorio" },
                                                })}>
                                                {(deptId === undefined || deptId === responsable?.direccion?.municipio?.departamento?.idDepartamento) && 
                                                <option value={responsable?.direccion?.municipio?.idMunicipio}>{responsable?.direccion?.municipio?.nombreMunicipio}</option>} 
                                                
                                                {municipios.filter(item => (deptId === undefined ? (item?.departamento?.idDepartamento === responsable?.direccion?.municipio?.departamento?.idDepartamento)
                                                :(item?.departamento?.idDepartamento === deptId)) && (item.idMunicipio !== responsable?.direccion?.municipio?.idMunicipio)).map((item) => (
                                                    <option key={item.idMunicipio} value={item.idMunicipio}>{item.nombreMunicipio}</option>
                                                ))}
                                            </select>
                                        </div>
                                    </div>
                                    }
                                    {jsonEM &&
                                        <div className="m-5">
                                            <h5><li>{msjMuni}</li></h5>
                                        </div>
                                    }
                                </div>

                                <div className="row col-sm-12">
                                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                                        <div className="form-group">
                                        <label className="control-label col-sm-12">Urbanizacion/ Colonia/ Barrio:</label>
                                            <input type="text" className="form-control" name="urbanizacion"
                                                 maxLength="100" defaultValue={responsable?.direccion?.urbanizacion}
                                                    ref={register({
                                                    
                                                    maxLength: { value: 100, message: "Máximo 100 caracteres" },
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.responsable?.message}
                                            </span> 
                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                                        <div className="form-group">
                                        <label className="control-label col-sm-12">Calle/Avenida:</label>
                                            <input type="text" className="form-control" name="calleAvenida"
                                                    maxLength="100" defaultValue={responsable?.direccion?.calleAvenida}
                                                    ref={register({
                                                    maxLength: { value: 100, message: "Máximo 100 caracteres" },
                                                })}
                                            />
                                            <span className="text-danger text-small d-block mb-2">
                                                {errors?.responsable?.message}
                                            </span> 
                                        </div>
                                    </div>
                                    <div className=" row col-xs-12 col-md-9 col-lg-4">
                                        <div className="col-xs-12 col-sm-4 col-md-4 col-lg-6">
                                            <div className="form-group">
                                            <label className="control-label col-sm-12">Pasaje:</label>
                                                <input type="text" className="form-control" name="numeroPasaje"
                                                    maxLength="16" defaultValue={responsable?.direccion?.numeroPasaje}
                                                        ref={register({
                                                        maxLength: { value: 16, message: "Máximo 16 caracteres" },
                                                    })}
                                                />
                                                <span className="text-danger text-small d-block mb-2">
                                                    {errors?.responsable?.message}
                                                </span> 
                                            </div>
                                        </div>
                                        <div className="col-xs-12 col-sm-4 col-md-4 col-lg-6">
                                            <div className="form-group">
                                            <label className="control-label col-sm-12">#Casa:</label>
                                                <input type="text" className="form-control" name="numeroCasa"
                                                        maxLength="10" defaultValue={responsable?.direccion?.numeroCasa}
                                                        ref={register({
                                                        required: { value: true, message: "El campo # Casa es obligatorio" },
                                                        maxLength: { value: 10, message: "Máximo 10 caracteres" },
                                                    })}
                                                />
                                                <span className="text-danger text-small d-block mb-2">
                                                    {errors?.responsable?.message}
                                                </span> 
                                            </div>
                                        </div>
                                    </div>
                                </div> {/* Fin Direccion */} <br/><br/>

                                <div className="row col-sm-12">
                                    <div className="col-sm-2">
                                        <div className="form-group ">
                                            <button type="submit" className="btn btn-success">Guardar</button>
                                        </div>
                                    </div>
                                    <div className="col-sm-2">
                                        <div className="form-group ">
                                        <a href="/responsables" className="btn btn-info">Regresar</a>
                                        </div>
                                    </div>
                                </div>
                            </div> {/* FIN */}

                        </form>
                    </div>
                    }
                    {jsonGE &&
                        <div className="m-5">
                            <h3><li>{msjGet}</li></h3>
                        </div>
                    }

                    {/* Modal */}
                    <button id="modal" type="button" className="d-none" data-toggle="modal" data-target="#exampleModalCenter">
                        X
                    </button>
                    <div className="modal fade" id="exampleModalCenter" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalCenterTitle">{tituloUpdate}</h5>
                                </div>
                                <div className="modal-body">
                                    {msjUpdate}
                                </div>
                                <div className="modal-footer">
                                    {jsonUE &&
                                        <button type="button" className="btn btn-warning" data-dismiss="modal">Cancelar</button>
                                    }
                                    {jsonUS &&
                                        <div>
                                            <button type="button" className="btn btn-secondary mr-3" data-dismiss="modal">Agregar otra </button>
                                            <a type="button" className="btn btn-primary" href="/responsables">Guardar</a>
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </Fragment>
    );
}
export default withRouter(ActualizarResponsable);