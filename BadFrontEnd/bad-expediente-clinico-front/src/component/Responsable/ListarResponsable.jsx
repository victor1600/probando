import React, {Fragment, useState} from 'react'
import {Link, withRouter} from "react-router-dom";

const ListarResponsable = (props) => {

    const estilosInicio = {
        fondoHeader: {
            backgroundColor: '#80cbc4',
            textAlign: 'center'
        },
        fondoBody: {
            backgroundColor: '#F5F5F6'
        },
        letraCarrousel: {
            color: 'black'
        }
    };
    
    // Validar Acceso
    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)

    // Obterner Responsables
    const [jsonOE, setJsonOE] = React.useState(false);
    const [jsonOS, setJsonOS] = React.useState(false);
    const [msj, setMsj] = React.useState(['']);
    const [responsables, setResponsables] = React.useState([]);
    
    React.useEffect(() => {
        if (loginE === true) {

            const obtenerResponsables = async () => {
                const data = await fetch('http://localhost:8080/responsables/todos', {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });

                const dataResponsables = await data.json();
                switch(data.status) {
                    case 403:
                        props.history.push('/error403');
                      break;
                    case 302:
                        setJsonOS(true);
                        setJsonOE(false);
                        setResponsables(dataResponsables);
                      break;
                    case 404:
                        setJsonOS(false);
                        setJsonOE(true);
                        setMsj([
                        String(dataResponsables.mensaje)
                    ]);
                        break;
                    default:
                        props.history.push('/error');
                }
            }
            obtenerResponsables();
            
        } else {
            props.history.push('/login');
        }
    }, [props, loginE,token])

    return (
        <Fragment>
            <div className="m-5">
                <div className="card">
                    <div className="card-header" style={estilosInicio.fondoHeader}>
                        <h2>Listado Responsables de Paciente</h2>
                    </div>
                    <div className="card-body" style={estilosInicio.fondoBody}>
                        {jsonOS &&
                        <div className="table-responsive">
                            <table className="table table-hover">
                                <thead>
                                <tr>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Celular</th>
                                    <th>Estado</th>
                                </tr>
                                </thead>
                                <tbody>
                                {responsables.map((item) => (
                                    <tr key={item.idPersona}>
                                        <td>
                                            <Link to={`/responsables/${item.idPersona}`}>
                                                {item.nombre1Persona} {item.nombre2Persona}
                                            </Link>
                                        </td>
                                        <td>
                                            <Link to={`/responsables/${item.idPersona}`}>
                                                {item.apellido1Persona} {item.apellido2Persona}
                                            </Link>
                                        </td>
                                        <td>
                                            <Link to={`/responsables/${item.idPersona}`}>
                                                {item.celularPersona}
                                            </Link>
                                        </td>
                                        <td><a href={`/responsables/actualizar/${item.idPersona}`}
                                            className="btn btn-warning">Editar</a>
                                        </td>
                                    </tr>
                                ))}
                                </tbody>
                            </table>
                        </div>
                        }
                        {jsonOE &&
                            <div className="m-5">
                                <h3><li>{msj}</li></h3>
                            </div>
                        }
                        <a href="/pacientes/" className="btn btn-primary">Pacientes</a>
                    </div>

                </div>
            </div>

        </Fragment>
    );
}

export default withRouter(ListarResponsable);