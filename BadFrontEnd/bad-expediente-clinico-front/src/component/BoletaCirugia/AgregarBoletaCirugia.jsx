import React, { Fragment, useState } from 'react'
import { useForm } from 'react-hook-form'
import { withRouter } from "react-router-dom";

const AgregarBoletaCirugia = (props) => {

    const [loginE] = useState(localStorage.getItem('login') ? true : false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token') : null)
    const [usuario] = useState(localStorage.getItem('usuario') ? localStorage.getItem('usuario') : null)

    React.useEffect(() => {

        if (loginE === true) {

            const obtenerPermiso = async () => {
                const data = await fetch(`http://localhost:8080/usuario/${usuario}/BoletacirugiaAdd`, {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });
                const info = await data.json();
                console.log(info);


                switch (data.status) {
                    case 200:
                        if (info.mensaje === "1") { } else {
                            props.history.push('/error403');
                        }
                        break;
                    default:
                        props.history.push('/error');
                }


            }

            obtenerPermiso()

        } else {
            props.history.push('/login');
        }


    }, [props, loginE, token, usuario])




    const [jsonOE, setJsonOE] = React.useState(false);
    const [jsonOS, setJsonOS] = React.useState(false);
    const [msj, setMsj] = React.useState(['']);
    const [titulo, setTitulo] = React.useState(['']);

    const { register, handleSubmit, errors } = useForm();
    const onSubmit = async (data, e) => {
        e.preventDefault();
        const btnModal = document.getElementById("modal");
        console.log(data);
        const jsonP = await fetch('http://localhost:8080/boletaCirugia/agregar', {
            method: 'POST',
            body: JSON.stringify({

                fechaBoletaCirugia: data.fechaBoletaCirugia,
                cirugia: {
                    idCirugia: data.idCirugia,
                    nombreCirugia: data.nombreCirugia,
                    clasificacionCirugia: data.clasificacionCirugia,
                    descripcionCirugia: data.descripcionCirugia,
                    especialidad: {
                        idEspecialidad: data.idEspecialidad,
                        nombreEspecialidad: data.nombreEspecialidad,
                    }
                }


            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                "Authorization": token
            }
        });
        console.log(jsonP);
        const info = await jsonP.json();

        if (jsonP.status === 201) {
            setJsonOE(false);
            setJsonOS(true);
            setMsj([
                String(info.mensaje)
            ]);
            setTitulo([
                String(info.httpCodeName)
            ]);
            btnModal.click();
            e.target.reset();

        } else {
            setJsonOE(true);
            setJsonOS(false);
            setMsj([
                String(info.mensaje)
            ]);
            setTitulo([
                String(info.httpCodeName)
            ]);
            btnModal.click();
        }


    }


    // Obtener Cirugias
    const [ciruId, setCiruId] = useState(0);
    const [jsonEB, setJsonEB] = React.useState(false);
    const [jsonSB, setJsonSB] = React.useState(false);
    const [msjCiru, setMsjCiru] = React.useState(['']);
    const [cirugia, setCirugia] = React.useState([]);

    React.useEffect(() => {
        if (loginE === true) {

            const obtenerCirugia = async () => {
                const data = await fetch('http://localhost:8080/cirugia/todas', {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });
                console.log(data.status);
                console.log(data.Cirugias);
                const dataCirugias = await data.json();
                if (data.status === 302) {
                    setJsonSB(true);
                    setJsonEB(false);
                    setCirugia(dataCirugias);
                } else {
                    setJsonSB(false);
                    setJsonEB(true);
                    setMsjCiru([
                        String(dataCirugias.mensaje)
                    ]);
                }
            }
            obtenerCirugia();

        } else {
            props.history.push('/login');
        }
    }, [props, loginE, token])



    // Obtener Especialidad

    const [jsonEE, setJsonEE] = React.useState(false);
    const [jsonSE, setJsonSE] = React.useState(false);
    const [msjEspe, setMsjEspe] = React.useState(['']);
    const [especialidad, setEspecialidad] = React.useState([]);

    React.useEffect(() => {
        if (loginE === true) {

            const obtenerEspecialidad = async () => {
                const data = await fetch('http://localhost:8080/especialidad/todas', {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });
                console.log(data.status);
                console.log(data.Especialidades);
                const dataEspecialidades = await data.json();
                if (data.status === 302) {
                    setJsonSE(true);
                    setJsonEE(false);
                    setEspecialidad(dataEspecialidades);
                } else {
                    setJsonSE(false);
                    setJsonEE(true);
                    setMsjEspe([
                        String(dataEspecialidades.mensaje)
                    ]);
                }
            }
            obtenerEspecialidad();

        } else {
            props.history.push('/login');
        }
    }, [props, loginE, token])


    console.log(ciruId);



    const handleInputChange = (event) => {
        var entrada = event.target.value;
        console.log(entrada);
        if (entrada.length === 10) {
            var hoy = new Date()
            var fechaBoletaCirugia = new Date(entrada)

        }
    }



    return (
        <Fragment>
            <div className="m-5">
                <div className="card">
                    <div className="card-header">
                        <h1>Boleta de cirugia</h1>
                    </div>
                    <div className="card-body">
                        <form className="form-horizontal" onSubmit={handleSubmit(onSubmit)}>

                            {jsonSB &&
                                <div className="col-sm-12">
                                    <div className="form-group">
                                        <label className="control-label col-sm-6">Nombre de cirugia:</label>
                                        <div className="col-sm-6">
                                            <select value={ciruId} onChange={e => setCiruId(e.target.value)}
                                                id="idCirugia" name="idCirugia" className="browser-default custom-select" required
                                                ref={register({
                                                    required: { value: true, message: "El campo nombre es obligatorio" }
                                                })}>
                                                <option>Seleccionar nombre</option>
                                                {cirugia.map((item) => (
                                                    <option key={item.idCirugia} value={item.idCirugia}>{item.nombreCirugia}</option>
                                                ))}
                                            </select>
                                        </div>
                                    </div>

                                    {ciruId === undefined ? <p></p>: (
                                        <div className="col-sm-12">

                                            <div className="form-group">
                                                <label className="control-label col-sm-6">Descripción:</label>
                                                <div className="col-sm-9">
                                                    {cirugia.filter(item => item.idCirugia === ciruId).map((item) => (
                                                        <input type="text"  disabled="disabled" className="form-control" id="idCirugia" name="descripcionCirugia"
                                                            key={item.idCirugia} defaultValue={item.descripcionCirugia}
                                                            ref={register({
                                                                required: { value: true, message: "El campo nombre es obligatorio" }
                                                            })}>
                                                        </input>
                                                    ))}
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <label className="control-label col-sm-6">clasificación:</label>
                                                <div className="col-sm-9">
                                                    {cirugia.filter(item => item.idCirugia === ciruId).map((item) => (
                                                        <input type="text" disabled="disabled" className="form-control" id="idCirugia" name="clasificacionCirugia"
                                                            key={item.idCirugia} defaultValue={item.clasificacionCirugia}
                                                            ref={register({
                                                                required: { value: true, message: "El campo nombre es obligatorio" }
                                                            })}>
                                                        </input>
                                                    ))}
                                                </div>
                                            </div>

                                        </div>
                                    )}
                                </div>
                            }
                            {jsonEB &&
                                <div className="m-5">
                                    <h5><li>{msjCiru}</li></h5>
                                </div>
                            }

                            <div className="col-xs-12 col-sm-6 col-md-4">
                                <div className="form-group">
                                    <label className="control-label col-sm-12">Fecha:</label>
                                    <input type="date" onChange={handleInputChange} className="form-control" name="fechaBoletaCirugia"
                                        required maxLength="10"
                                        ref={register({
                                            required: { value: true, message: "Fecha de boleta es obligatorio" },
                                            maxLength: { value: 10, message: "Máximo 10 caracteres" },
                                        })}
                                    />
                                    <span className="text-danger text-small d-block mb-2">
                                        {errors?.fechaBoletaCirugia?.message}
                                    </span>
                                </div>
                            </div>


                            <div className="form-group">
                                <div className="col-sm-offset-2 col-sm-10">
                                    <button type="submit" className="btn btn-success">Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div className="m-5">
                        <a href="/boletaCirugia" className="btn btn-info">Regresar</a>
                    </div>


                    <button id="modal" type="button" className="d-none" data-toggle="modal"
                        data-target="#exampleModalCenter">
                        Launch demo modal
                    </button>


                    <div className="modal fade" id="exampleModalCenter" tabIndex="-1" role="dialog"
                        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalCenterTitle">{titulo}</h5>
                                </div>
                                <div className="modal-body">
                                    {msj}
                                </div>
                                <div className="modal-footer">
                                    {jsonOE &&
                                        <button type="button" className="btn btn-warning"
                                            data-dismiss="modal">Cancelar</button>
                                    }
                                    {jsonOS &&
                                        <div>
                                            <button type="button" className="btn btn-secondary mr-3"
                                                data-dismiss="modal">Agregar otra
                                        </button>
                                            <a type="button" className="btn btn-primary"
                                                href="/boletaCirugia">Guardar</a>
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </Fragment>
    );
}

export default withRouter(AgregarBoletaCirugia);