import React, {Fragment, useState} from 'react'
import {withRouter} from "react-router-dom";


//ruteo
import {useParams} from "react-router-dom";

const DetalleBoletaCirugia = (props) => {
    const estilosInicio = {
        fondoHeader: {
            backgroundColor: '#80cbc4',
            textAlign: 'center'
        },
        fondoBody: {
            backgroundColor: '#F5F5F6'
        },
        letraCarrousel: {
            color: 'black'
        }
    };

    const [loginE] = useState(localStorage.getItem('login') ? true:false)
    const [token] = useState(localStorage.getItem('token') ? localStorage.getItem('token'):null)


    const {idBoletaCirugia} = useParams();
    const [boleta, setBoleta] = React.useState([]);

    const [jsonOE, setJsonOE] = React.useState(false);
    const [jsonOS, setJsonOS] = React.useState(false);
    const [msj, setMsj] = React.useState(['']);

    

    React.useEffect(() => {

        if(loginE === true){

            const obtenerBoleta = async () => {
                const data = await fetch(`http://localhost:8080/boletaCirugia/${idBoletaCirugia}`, {
                    method: 'GET',
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                        "Authorization": token
                    }
                });                
                const dataBoleta = await data.json();
                switch(data.status) {
                    case 403:
                        props.history.push('/error403');
                      break;
                    case 302:
                        setJsonOS(true);
                        setJsonOE(false);
                        setBoleta(dataBoleta);
                      break;
                    case 404:
                        setJsonOS(false);
                        setJsonOE(true);
                        setMsj([
                        String(dataBoleta.mensaje)
                        ]);
                      break;
                    default:
                        props.history.push('/error');
                  }
        
            }
            
            obtenerBoleta()


        }else{
            props.history.push('/login');
        } 
    }, [idBoletaCirugia,props,token,loginE])

    
    return (
        <Fragment>
            <div className="m-5">
                <div className="card">
                    <div className="card-header" style={estilosInicio.fondoHeader}>
                        <h2>Los datos de la boleta de cirugia :</h2>
                    </div>
                    <div className="card-body" style={estilosInicio.fondoBody}>
                        {jsonOS &&
                        <div>
                            <br/><br/>
                            <ul>
                                <li><b>Nombre de Cirugia:</b>     {boleta?.cirugia?.nombreCirugia}</li>
                                <li><b>Clasificación:</b>     {boleta?.cirugia?.clasificacionCirugia}</li>
                                <li><b>Descripción:</b>     {boleta?.cirugia?.descripcionCirugia}</li>
                                <li><b>Fecha programada:</b>     {boleta.fechaBoletaCirugia}</li>

                                
                            </ul>
                        </div>
                        }
                        {
                            jsonOE &&
                            <div className="m-5">
                                <h1>{msj}</h1>
                            </div>
                        }
                        <a href="/boletaCirugia" className="btn btn-info">Regresar</a>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default withRouter(DetalleBoletaCirugia);