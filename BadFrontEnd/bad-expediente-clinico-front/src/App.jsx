import React, { Fragment, useState } from "react";

//Ruteo
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

//Componentes

//Menu
import Menu from "./component/BarraMenu/Menu";

//Menu
import LoginForm from "./component/Login/LoginForm";

//Principal
import Inicio from "./component/Principal/Inicio";

//Paciente
import ListarPaciente from "./component/Paciente/ListarPaciente";
import DetallePaciente from "./component/Paciente/DetallePaciente";
import AgregarPaciente from "./component/Paciente/AgregarPaciente";
import ActualizarPaciente from "./component/Paciente/ActualizarPaciente";

//Responsable
import ListarResponsable from "./component/Responsable/ListarResponsable";
import DetalleResponsable from "./component/Responsable/DetalleResponsable";
import ActualizarResponsable from "./component/Responsable/ActualizarResponsable";

//Personal
import ListarPersonal from "./component/PersonalMedico/ListarPersonal";
import DetallePersonal from "./component/PersonalMedico/DetallePersonal";
import AgregarPersonal from "./component/PersonalMedico/AgregarPersonal";
import ActualizarPersonal from "./component/PersonalMedico/ActualizarPersonal";

//Tipo Personal
import ListarTPM from "./component/TipoPersonalMedico/ListarTPM";
import AgregarTPM from "./component/TipoPersonalMedico/AgregarTPM";
import ActualizarTPM from "./component/TipoPersonalMedico/ActualizarTPM";

//Especialidades
import ListarEspecialidad from "./component/Especialidad/ListarEspecialidad";
import DetalleEspecialidad from "./component/Especialidad/DetalleEspecialidad";
import AgregarEspecialidad from "./component/Especialidad/agregarEspecialidad";
import ActualizarEspecialidad from "./component/Especialidad/ActualizarEspecialidad";

//Cie10
import ListarCie10 from "./component/Cie10/ListarCie10";
import ActualizarCie10 from "./component/Cie10/ActualizarCie10";
import AgregarCie10 from "./component/Cie10/AgregarCie10";
import DetalleCie10 from "./component/Cie10/DetalleCie10";

//Signo Vital
import ListarSignoV from "./component/SignoVital/ListarSignoV";
import DetalleSignoV from "./component/SignoVital/DetalleSignoV";
import AgregarSignoV from "./component/SignoVital/AgregarSignoV";
import ActualizarSignoV from "./component/SignoVital/ActualizarSignoV";

//Examen
import ListarExamen from "./component/Examen/ListarExamen";
import DetalleExamen from "./component/Examen/DetalleExamen";
import AgregarExamen from "./component/Examen/AgregarExamen";
import ErrorProhibido from "./component/ErrorProhibido/ErrorProhibido";
import ErrorPage from "./component/ErrorPage/ErrorPage";

//Cirugia
import ListarCirugia from "./component/Cirugia/ListarCirugia";
import ActualizarCirugia from "./component/Cirugia/ActualizarCirugia";
import AgregarCirugia from "./component/Cirugia/AgregarCirugia";
import DetalleCirugia from "./component/Cirugia/DetalleCirugia";

//BoletaCirugia
import ListarBoletaCirugia from "./component/BoletaCirugia/ListarBoletaCirugia";
import ActualizarBoletaCirugia from "./component/BoletaCirugia/ActualizarBoletaCirugia";
import AgregarBoletaCirugia from "./component/BoletaCirugia/AgregarBoletaCirugia";
import DetalleBoletaCirugia from "./component/BoletaCirugia/DetalleBoletaCirugia";

//Area
import ListarArea from "./component/Area/ListarArea";
import ActualizarArea from "./component/Area/ActualizarArea";
import AgregarArea from "./component/Area/AgregarArea";
import DetalleArea from "./component/Area/DetalleArea";

//ingreso
import ListarIngreso from "./component/Ingreso/ListarIngreso";
import ActualizarIngreso from "./component/Ingreso/ActualizarIngreso";
import AgregarIngreso from "./component/Ingreso/AgregarIngreso";
import DetalleIngreso from "./component/Ingreso/DetalleIngreso";

// Medicamento
import ListarMedicamentos from "./component/Medicamentos/ListarMedicamentos";
import AgregarM from "./component/Medicamentos/AgregarM";
import DetalleM from "./component/Medicamentos/DetalleM";
import ActualizarM from "./component/Medicamentos/ActualizarM";

// Tipo Medicamentos
import ListarTM from "./component/TipoMedicamentos/ListarTM";
import DetalleTM from "./component/TipoMedicamentos/DetalleTM";
import AgregarTM from "./component/TipoMedicamentos/AgregarTM";
import ActualizarTM from "./component/TipoMedicamentos/ActualizarTM";

// Tipo Citas
import ListarTipoCita from "./component/TipoCita/ListarTipoCita";
import DetallesTipoCita from "./component/TipoCita/DetallesTipoCita";
import AgregarTipoCita from "./component/TipoCita/AgregarTipoCita";
import ActualizarTipoCita from "./component/TipoCita/ActualizarTipoCita";
import ListarCitas from "./component/Cita/ListarCitas";
import AgregarCita from "./component/Cita/AgregarCita";

function App() {
  const [loginE] = useState(localStorage.getItem("login") ? true : false);
  const [vistaLogin] = useState(localStorage.getItem("login") ? false : true);

  return (
    <Fragment>
      <Router>
        {/* Menu */}
        {loginE && <Menu />}

        <Switch>
          {/* Inicio */}
          <Route path="/" exact>
            <Inicio />
          </Route>

          {/* Login*/}
          <Route path="/login" exact>
            {vistaLogin && <LoginForm />}
          </Route>

          {/* Error 403 */}
          <Route path="/error403" exact>
            <ErrorProhibido />
          </Route>

          {/* Paciente */}
          <Route path="/pacientes" exact>
            <ListarPaciente />
          </Route>
          <Route path="/pacientes/agregar" exact>
            <AgregarPaciente />
          </Route>
          <Route path="/pacientes/:idPersona" exact>
            <DetallePaciente />
          </Route>
          <Route path="/pacientes/actualizar/:idPersona" exact>
            <ActualizarPaciente />
          </Route>

          {/* Responsable */}
          <Route path="/responsables" exact>
            <ListarResponsable />
          </Route>
          <Route path="/responsables/:idPersona" exact>
            <DetalleResponsable />
          </Route>
          <Route path="/responsables/actualizar/:idPersona" exact>
            <ActualizarResponsable />
          </Route>

          {/* Personal */}
          <Route path="/personalMedico" exact>
            <ListarPersonal />
          </Route>
          <Route path="/personalMedico/agregar" exact>
            <AgregarPersonal />
          </Route>
          <Route path="/personalMedico/:idPersona" exact>
            <DetallePersonal />
          </Route>
          <Route path="/personalMedico/actualizar/:idPersona" exact>
            <ActualizarPersonal />
          </Route>

          {/* Tipo Personal */}
          <Route path="/tipoPersonalMedico" exact>
            <ListarTPM />
          </Route>
          <Route path="/tipoPersonalMedico/agregar" exact>
            <AgregarTPM />
          </Route>
          <Route path="/tipoPersonalMedico/actualizar/:idTipoPersonal" exact>
            <ActualizarTPM />
          </Route>

          {/* Pacientes */}
          <Route path="/pacientes" exact>
            <div className="m-5">
              <h1>Hola este es pacientes</h1>
            </div>
          </Route>

          {/* Doctores */}
          <Route path="/doctores" exact>
            <div className="m-5">
              <h1>Hola este es Doctores</h1>
            </div>
          </Route>

          {/* Especialidad */}
          <Route path="/especialidad" exact>
            <div className="m-5">
              <ListarEspecialidad />
            </div>
          </Route>
          <Route path="/especialidad/agregar" exact>
            <AgregarEspecialidad />
          </Route>
          <Route path="/especialidad/:idEspecialidad" exact>
            <DetalleEspecialidad />
          </Route>
          <Route path="/especialidad/actualizar/:idEspecialidad" exact>
            <ActualizarEspecialidad />
          </Route>

          {/* CIE10 */}
          <Route path="/cie10" exact>
            <div className="m-5">
              <ListarCie10 />
            </div>
          </Route>
          <Route path="/cie10/agregar" exact>
            <AgregarCie10 />
          </Route>
          <Route path="/cie10/:idCIE10" exact>
            <DetalleCie10 />
          </Route>
          <Route path="/cie10/actualizar/:idCIE10" exact>
            <ActualizarCie10 />
          </Route>

          {/* Signos Vitales */}
          <Route path="/signosVitales" exact>
            <div className="m-5">
              <ListarSignoV />
            </div>
          </Route>
          <Route path="/signosVitales/agregar" exact>
            <AgregarSignoV />
          </Route>
          <Route path="/signosVitales/:idVital" exact>
            <DetalleSignoV />
          </Route>
          <Route path="/signosVitales/actualizar/:idVital" exact>
            <ActualizarSignoV />
          </Route>

          {/*Examenes*/}
          <Route path="/examen" exact>
            <ListarExamen />
          </Route>
          <Route path="/examen/agregar" exact>
            <AgregarExamen />
          </Route>
          <Route path="/examen/:idExamen" exact>
            <DetalleExamen />
          </Route>

          {/* Cirugia */}
          <Route path="/cirugia" exact>
            <ListarCirugia />
          </Route>
          <Route path="/cirugia/agregar" exact>
            <AgregarCirugia />
          </Route>
          <Route path="/cirugia/actualizar/:idCirugia" exact>
            <ActualizarCirugia />
          </Route>
          <Route path="/cirugia/:idCirugia" exact>
            <DetalleCirugia />
          </Route>

          {/* Boleta Cirugia */}
          <Route path="/boletaCirugia" exact>
            <ListarBoletaCirugia />
          </Route>
          <Route path="/boletaCirugia/agregar" exact>
            <AgregarBoletaCirugia />
          </Route>
          <Route path="/boletaCirugia/actualizar/:idBoletaCirugia" exact>
            <ActualizarBoletaCirugia />
          </Route>
          <Route path="/boletaCirugia/:idBoletaCirugia" exact>
            <DetalleBoletaCirugia />
          </Route>

          {/* Area */}
          <Route path="/area" exact>
            <ListarArea />
          </Route>
          <Route path="/area/agregar" exact>
            <AgregarArea />
          </Route>
          <Route path="/area/actualizar/:idArea" exact>
            <ActualizarArea />
          </Route>
          <Route path="/area/:idArea" exact>
            <DetalleArea />
          </Route>

          {/* Ingreso */}
          <Route path="/ingreso" exact>
            <ListarIngreso />
          </Route>
          <Route path="/ingreso/agregar" exact>
            <AgregarIngreso />
          </Route>
          <Route path="/ingreso/actualizar/:idIngreso" exact>
            <ActualizarIngreso />
          </Route>
          <Route path="/ingreso/:idIngreso" exact>
            <DetalleIngreso />
          </Route>

          {/* Medicamento */}
          <Route path="/medicamentos" exact>
            <ListarMedicamentos />
          </Route>
          <Route path="/medicamentos/agregar" exact>
            <AgregarM />
          </Route>
          <Route path="/medicamentos/actualizar/:id" exact>
            <ActualizarM />
          </Route>
          <Route path="/medicamentos/:id" exact>
            <DetalleM />
          </Route>

          {/* Tipo Medicamentos */}
          <Route path="/tipoMedicamentos" exact>
            <ListarTM />
          </Route>
          <Route path="/tipoMedicamentos/agregar" exact>
            <AgregarTM />
          </Route>
          <Route path="/tipoMedicamentos/actualizar/:id" exact>
            <ActualizarTM />
          </Route>
          <Route path="/tipoMedicamentos/:id" exact>
            <DetalleTM />
          </Route>

          {/* Tipo Citas */}
          <Route path="/tipoCitas" exact>
            <ListarTipoCita />
          </Route>
          <Route path="/tipoCitas/agregar" exact>
            <AgregarTipoCita />
          </Route>
          <Route path="/tipoCitas/actualizar/:id" exact>
            <ActualizarTipoCita />
          </Route>
          <Route path="/tipoCitas/:id" exact>
            <DetallesTipoCita />
          </Route>

          {/* Tipo Citas */}
          <Route path="/citas" exact>
            <ListarCitas />
          </Route>
          <Route path="/citas/agregar" exact>
            <AgregarCita />
          </Route>

          {/* error debe ser la ultima */}
          <Route component={ErrorPage}></Route>
        </Switch>
      </Router>
    </Fragment>
  );
}

export default App;
